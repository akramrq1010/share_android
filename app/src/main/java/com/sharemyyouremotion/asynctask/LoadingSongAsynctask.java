package com.sharemyyouremotion.asynctask;

import android.app.Activity;
import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;

import com.sharemyyouremotion.helper.Constants;
import com.sharemyyouremotion.helper.DialogClass;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

/**
 * Created by abc on 8/25/2018.
 */

public class LoadingSongAsynctask {/*extends AsyncTask<Void, Void, Void> {

    public static Handler handler = new Handler();
    public static double startTime = 0;
    public static double finalTime;
    public int oneTimeOnly = 0;
    private static Handler myHandler = new Handler();



    String audioLink;

    Activity activity;
     SeekBar seekBar;
     TextView tvRunningTime;
    TextView tvFinalTime;
    ProgressBar progressBar;
    ImageView pause;

    public LoadingSongAsynctask(String audioLink, Activity activity, SeekBar seekBar, TextView tvRunningTime, TextView tvFinalTime, ProgressBar progressBar, ImageView pause) {
        this.audioLink = audioLink;
        this.activity = activity;
        this.seekBar = seekBar;
        this.tvRunningTime = tvRunningTime;
        this.tvFinalTime = tvFinalTime;
        this.progressBar = progressBar;
        this.pause = pause;
    }

    @Override
    protected void onPreExecute() {
        progressBar.setVisibility(View.VISIBLE);
        super.onPreExecute();
    }

    @Override
    protected Void doInBackground(Void... voids) {
        Constants.mediaPlayer = new MediaPlayer();
        Constants.mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        try {
            Constants.mediaPlayer.setDataSource(songLink);
            Constants.mediaPlayer.prepare();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        progressBar.setVisibility(View.GONE);
        pause.setVisibility(View.VISIBLE);
        Constants.mediaPlayer.start();

        Constants.mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {

            }
        });

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                finalTime = Constants.mediaPlayer.getDuration();
                startTime = Constants.mediaPlayer.getCurrentPosition();

                if (oneTimeOnly == 0) {
                    seekBar.setMax((int) finalTime);
                    oneTimeOnly = 1;
                }

              *//*  tvFinalTime.setText(String.format("%d:%d",
                        TimeUnit.MILLISECONDS.toMinutes((long) finalTime),
                        TimeUnit.MILLISECONDS.toSeconds((long) finalTime) -
                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes((long)
                                        finalTime)))
                );*//*

                //current time then final time
                time.setText(String.format("%d:%d",
                        TimeUnit.MILLISECONDS.toMinutes((long) startTime),
                        TimeUnit.MILLISECONDS.toSeconds((long) startTime) -
                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes((long)
                                        startTime)))
                        +"/"+
                        String.format("%d:%d",
                                TimeUnit.MILLISECONDS.toMinutes((long) finalTime),
                                TimeUnit.MILLISECONDS.toSeconds((long) finalTime) -
                                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes((long)
                                                finalTime)))
                );

                //set seekbar according to sound
                seekBar.setProgress((int) startTime);
                myHandler.postDelayed(UpdateSongTime, 100);
            }
        }, 200);

        super.onPostExecute(aVoid);
    }

   public static Runnable UpdateSongTime = new Runnable() {
        public void run() {

            // Log.v("ak","run");
            try {
                if (Constants.mediaPlayer != null) {
                    startTime = Constants.mediaPlayer.getCurrentPosition();
                } else {
                }
            }catch (Exception e){}

            time.setText(String.format("%d:%d",
                    TimeUnit.MILLISECONDS.toMinutes((long) startTime),
                    TimeUnit.MILLISECONDS.toSeconds((long) startTime) -
                            TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes((long)
                                    startTime)))
                    +"/"+
                    String.format("%d:%d",
                            TimeUnit.MILLISECONDS.toMinutes((long) finalTime),
                            TimeUnit.MILLISECONDS.toSeconds((long) finalTime) -
                                    TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes((long)
                                            finalTime)))
            );
            seekBar.setProgress((int) startTime);
            myHandler.postDelayed(this, 100);
        }
    };
*/
}