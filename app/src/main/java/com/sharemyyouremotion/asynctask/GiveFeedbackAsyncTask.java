package com.sharemyyouremotion.asynctask;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;

import com.sharemyyouremotion.activity.MyQuestionFeedbackActivity;
import com.sharemyyouremotion.connection.MyHttpEntity;
import com.sharemyyouremotion.helper.Constants;
import com.sharemyyouremotion.helper.DialogClass;
import com.sharemyyouremotion.helper.StaticSharedpreference;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

public class GiveFeedbackAsyncTask extends AsyncTask<Void, Integer, String> {

    HttpClient httpClient = new DefaultHttpClient();
    private Activity context;
    private Exception exception;
    String mediaPath;
    String mediaType;
    String text;
    String isShareIdentity;
    DialogClass dialogClass;
    android.support.v7.app.AlertDialog alertDialog;
    String questionId;
    MyQuestionFeedbackActivity myQuestionFeedbackActivity;

    public GiveFeedbackAsyncTask(Activity context, String mediaPath, String mediaType, String text, String isShareIdentity, String questionId, MyQuestionFeedbackActivity myQuestionFeedbackActivity) {
        this.context = context;
        this.mediaPath = mediaPath;
        this.mediaType = mediaType;

        this.text = text;
        this.isShareIdentity = isShareIdentity;
        this.questionId = questionId;
        this.myQuestionFeedbackActivity = myQuestionFeedbackActivity;
    }

    @Override
    protected String doInBackground(Void... params) {

        HttpResponse httpResponse = null;
        HttpEntity httpEntity = null;
        String responseString = null;

        try {
            HttpPost httpPost = new HttpPost(Constants.BASE_URL + "GiveFeedback");
            MultipartEntityBuilder multipartEntityBuilder = MultipartEntityBuilder.create();

            if (!mediaType.equalsIgnoreCase("text")) {
                File file = new File(mediaPath);
                multipartEntityBuilder.addPart("files", new FileBody(file));
            }
            // Add the file to be uploaded
            //multipartEntityBuilder.addPart("files", new FileBody(file));
            multipartEntityBuilder.addTextBody("FeedbackType", mediaType);
            multipartEntityBuilder.addTextBody("GivenBy", StaticSharedpreference.getInt(Constants.USER_ID, context) + "");
            multipartEntityBuilder.addTextBody("FeedbackText", text);
            multipartEntityBuilder.addTextBody("IsIdentityShare", isShareIdentity);
            multipartEntityBuilder.addTextBody("QuestionId", questionId);

            // Progress listener - updates task's progress
            MyHttpEntity.ProgressListener progressListener =
                    new MyHttpEntity.ProgressListener() {
                        @Override
                        public void transferred(float progress) {
                            publishProgress((int) progress);
                        }
                    };

            // POST
            httpPost.setEntity(new MyHttpEntity(multipartEntityBuilder.build(),
                    progressListener));


            httpResponse = httpClient.execute(httpPost);
            httpEntity = httpResponse.getEntity();

            int statusCode = httpResponse.getStatusLine().getStatusCode();
            if (statusCode == 200) {
                StaticSharedpreference.saveInfo(Constants.MY_FEEDBACK_LIST, "", context);
                // Server response
                responseString = EntityUtils.toString(httpEntity);
            } else {
                responseString = "Error occurred! Http Status Code: "
                        + statusCode;
            }
            Log.v("akram", "response = " + responseString);
        } catch (UnsupportedEncodingException | ClientProtocolException e) {
            e.printStackTrace();
            Log.e("UPLOAD", e.getMessage());
            this.exception = e;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return responseString;
    }

    @Override
    protected void onPreExecute() {

        // Init and show dialog
        dialogClass = new DialogClass();
        alertDialog = dialogClass.compressDialog(context, "Uploading...");

    }

    @Override
    protected void onPostExecute(String result) {

        // Close dialog
        //  this.progressDialog.dismiss();
        if (result == null || result.equalsIgnoreCase(""))
            return;

        try {
            JSONObject jsonObject = new JSONObject(result);

            int status = jsonObject.getInt("StatusCode");

            if (status == 200) {
                myQuestionFeedbackActivity.getQuestionDetailAllFeedbacksByQuestionId();
                //dialogClass.messageSend(context);
            } else {
                dialogClass.messageDialog(context, jsonObject.getString("Message") + "");
            }

            MyQuestionFeedbackActivity.tvVideoAudio.setText("");
            MyQuestionFeedbackActivity.edtFeedback.setText("");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        alertDialog.dismiss();

    }

    @Override
    protected void onProgressUpdate(Integer... progress) {

        dialogClass.progress.setProgress((int) progress[0]);
        dialogClass.tvCount.setText((int) progress[0] + "%");
    }
}


