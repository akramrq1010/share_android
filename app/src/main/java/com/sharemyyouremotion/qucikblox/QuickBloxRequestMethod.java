package com.sharemyyouremotion.qucikblox;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.quickblox.auth.QBAuth;
import com.quickblox.auth.session.BaseService;
import com.quickblox.auth.session.QBSession;
import com.quickblox.chat.QBChatService;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.BaseServiceException;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.core.helper.StringifyArrayList;
import com.quickblox.core.helper.Utils;
import com.quickblox.messages.QBPushNotifications;
import com.quickblox.messages.model.QBDevice;
import com.quickblox.messages.model.QBEnvironment;
import com.quickblox.messages.model.QBNotificationChannel;
import com.quickblox.messages.model.QBSubscription;
import com.quickblox.sample.core.utils.SharedPrefsHelper;
import com.quickblox.users.QBUsers;
import com.quickblox.users.model.QBUser;
import com.sharemyyouremotion.R;
import com.sharemyyouremotion.activity.HomeActivity;
import com.sharemyyouremotion.activity.OtpActivity;
import com.sharemyyouremotion.activity.SignUpActivity;
import com.sharemyyouremotion.activity.SignUpSecondPageActivity;
import com.sharemyyouremotion.activity.SpecializationActivity;
import com.sharemyyouremotion.helper.Constants;
import com.sharemyyouremotion.helper.DialogClass;
import com.sharemyyouremotion.helper.StaticSharedpreference;
import com.sharemyyouremotion.model.SpecializationModel;
import com.sharemyyouremotion.qucikblox.ChatData.Snippet;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Random;

import retrofit2.Response;

import static java.security.AccessController.getContext;

public class QuickBloxRequestMethod {
    DialogClass dialogClass = new DialogClass();

    String username;
    public static QBUser qbUserLocal;
    Activity activity;
    QBChatService chatService;
    String quickBloxId = "";
    String checkCallFrom = "";

    String name;
    String email;
    String mobile;
    String dob;
    String password;
    String gender;
    String about;
    String isSocialUser;
    String socialType;
    String socialId;
    String url;

    public void quickBloxSignup(final Activity context, String name, String email, String mobile, String dob, String password, String gender,
                                String about, String url, String isSocialUser, String socialType, String socialId, final String fromWhere, final Response<JsonObject> jsonObject) {

        this.name = name;
        this.email = email;
        this.mobile = mobile;
        this.dob = dob;
        this.password = password;
        this.gender = gender;
        this.about = about;
        this.url = url;
        this.isSocialUser = isSocialUser;
        this.socialId = socialId;
        this.socialType = socialType;
        activity = context;
        dialogClass.progresesDialog(context);

        final int min = 1000;
        final int max = 9999;
        final int random = new Random().nextInt((max - min) + 1) + min;

        String storeName;
        storeName = name;
        name = name.replace(" ", "20");

        username = name + "" + random;

        QuickBloxCredential quickBloxCredential = new QuickBloxCredential();
        quickBloxCredential.initCredentials(context);

        QBUser qbUser = new QBUser();
        qbUser.setPassword(context.getResources().getString(R.string.quickBloxPass));
        qbUser.setFullName(storeName);
        qbUser.setLogin(username);
        StringifyArrayList<String> tagsArray = new StringifyArrayList<>();
        tagsArray.add("myChatRoom");
        qbUser.setTags(tagsArray);

        QBUsers.signUpSignInTask(qbUser).performAsync(new QBEntityCallback<QBUser>() {
            @Override
            public void onSuccess(QBUser qbUser, Bundle bundle) {
                // qbUser.setPassword(getString(R.string.quickBloxPass));
                //login(qbUser);

                DialogClass.custom_pd.dismiss();
                quickbloxlogin("" + qbUser.getLogin(), activity, "" + qbUser.getId(), fromWhere, jsonObject);

            }

            @Override
            public void onError(QBResponseException error) {
                DialogClass.custom_pd.dismiss();
            }
        });
    }

    String fromWhere;
    Response<JsonObject> response;

    public void quickbloxlogin(final String username, final Activity activity, String quickBloxId, final String fromWhere, final Response<JsonObject> response) {
        QuickBloxCredential helper = new QuickBloxCredential();

        dialogClass.progresesDialog(activity);
        this.fromWhere = fromWhere;
        this.username = username;
        this.checkCallFrom = checkCallFrom;
        this.response = response;

        this.activity = activity;
        this.quickBloxId = quickBloxId;
        helper.initCredentials(activity);
        QBUser qbUser = new QBUser(username, activity.getResources().getString(R.string.quickBloxPass));

        QBUsers.signIn(qbUser).performAsync(new QBEntityCallback<QBUser>() {
            @Override
            public void onSuccess(QBUser qbUser, Bundle bundle) {
                qbUserLocal = qbUser;


                DialogClass.custom_pd.dismiss();
                if (fromWhere.equalsIgnoreCase("signup")) {
                    ActivityOptions options1 =
                            ActivityOptions.makeCustomAnimation(activity, R.anim.slide_to_right, R.anim.slide_from_left);

                    Intent intent = new Intent(activity, OtpActivity.class);
                    intent.putExtra("name", name);
                    intent.putExtra("email", email);
                    intent.putExtra("mobile", mobile);
                    intent.putExtra("dob", dob);
                    intent.putExtra("password", password);
                    intent.putExtra("gender", gender);
                    intent.putExtra("about", about);
                    intent.putExtra("url", url);
                    intent.putExtra("isSocialUser", isSocialUser);
                    intent.putExtra("socialType", socialType);
                    intent.putExtra("socialId", socialId);
                    intent.putExtra("quickId", qbUser.getId() + "");
                    intent.putExtra("quickName", qbUser.getLogin() + "");
                    activity.startActivity(intent, options1.toBundle());
                } else if (fromWhere.equalsIgnoreCase("login")) {

                    StaticSharedpreference.saveInt(Constants.USER_ID,
                            response.body().get("UserProfile").getAsJsonObject().get("UserId").getAsInt(), activity);
                    StaticSharedpreference.saveInfo(Constants.NAME,
                            response.body().get("UserProfile").getAsJsonObject().get("Name").getAsString(), activity);
                    StaticSharedpreference.saveInfo(Constants.EMAIL,
                            response.body().get("UserProfile").getAsJsonObject().get("EmailId").getAsString(), activity);
                    StaticSharedpreference.saveInfo(Constants.MOBILE_NO,
                            response.body().get("UserProfile").getAsJsonObject().get("Mobile").getAsString(), activity);
                    StaticSharedpreference.saveInfo(Constants.DOB,
                            response.body().get("UserProfile").getAsJsonObject().get("Dob").getAsString(), activity);
                    StaticSharedpreference.saveInfo(Constants.GENDER,
                            response.body().get("UserProfile").getAsJsonObject().get("Gender").getAsString(), activity);
                    StaticSharedpreference.saveInfo(Constants.PROFILE_PIC,
                            response.body().get("UserProfile").getAsJsonObject().get("ProfileThumbnail").getAsString(), activity);

                    StaticSharedpreference.saveInfo(Constants.ABOUT,
                            response.body().get("UserProfile").getAsJsonObject().get("About").getAsString(), activity);


                    if (!response.body().get("UserProfile").getAsJsonObject().get("IsSpecializationFill").getAsBoolean()) {

                        Gson gson = new Gson();
                        Type listType = new TypeToken<ArrayList<SpecializationModel>>() {
                        }.getType();

                        ArrayList<SpecializationModel> categoriesModels =
                                gson.fromJson(response.body().getAsJsonArray("Categories"), listType);

                        String specialization = gson.toJson(categoriesModels);
                        StaticSharedpreference.saveInfo(Constants.SPECIALIZATION_LIST, specialization, activity);

                        ActivityOptions options1 =
                                ActivityOptions.makeCustomAnimation(activity, R.anim.slide_to_right, R.anim.slide_from_left);
                        StaticSharedpreference.saveInfo(Constants.GOTO_PAGE, "specialization", activity);
                        Intent intent = new Intent(activity, SpecializationActivity.class);
                        activity.startActivity(intent, options1.toBundle());

                    } else {

                        ActivityOptions options1 =
                                ActivityOptions.makeCustomAnimation(activity, R.anim.slide_to_right, R.anim.slide_from_left);
                        StaticSharedpreference.saveInfo(Constants.GOTO_PAGE, "home", activity);
                        activity.startActivity(new Intent(activity, HomeActivity.class), options1.toBundle());
                        activity.finishAffinity();
                    }


                } else if (fromWhere.equalsIgnoreCase("fb")) {
                    DialogClass.custom_pd.dismiss();
                    StaticSharedpreference.saveInt(Constants.USER_ID,
                            response.body().get("UserProfile").getAsJsonObject().get("UserId").getAsInt(), activity);

                    StaticSharedpreference.saveInfo(Constants.NAME,
                            response.body().get("UserProfile").getAsJsonObject().get("Name").getAsString(), activity);
                    StaticSharedpreference.saveInfo(Constants.EMAIL,
                            response.body().get("UserProfile").getAsJsonObject().get("EmailId").getAsString(), activity);
                    StaticSharedpreference.saveInfo(Constants.PROFILE_PIC,
                            response.body().get("UserProfile").getAsJsonObject().get("ProfileThumbnail").getAsString(), activity);


                    ActivityOptions options1 =
                            ActivityOptions.makeCustomAnimation(activity, R.anim.slide_to_right, R.anim.slide_from_left);
                    Intent intent = null;

                    if (response.body().get("UserProfile").getAsJsonObject().get("IsProfileCompleted").getAsBoolean()) {

                        StaticSharedpreference.saveInfo(Constants.ABOUT,
                                response.body().get("UserProfile").getAsJsonObject().get("About").getAsString(), activity);
                        StaticSharedpreference.saveInfo(Constants.MOBILE_NO,
                                response.body().get("UserProfile").getAsJsonObject().get("Mobile").getAsString(), activity);

                        StaticSharedpreference.saveInfo(Constants.DOB,
                                response.body().get("UserProfile").getAsJsonObject().get("Dob").getAsString(), activity);


                        if (response.body().get("UserProfile").getAsJsonObject().get("IsSpecializationFill").getAsBoolean()) {
                            StaticSharedpreference.saveInfo(Constants.GOTO_PAGE, "home", activity);
                            StaticSharedpreference.saveInfo(Constants.GENDER,
                                    response.body().get("UserProfile").getAsJsonObject().get("Gender").getAsString(), activity);


                            intent = new Intent(activity, HomeActivity.class);

                        } else {

                            Gson gson = new Gson();
                            Type listType = new TypeToken<ArrayList<SpecializationModel>>() {
                            }.getType();

                            ArrayList<SpecializationModel> categoriesModels =
                                    gson.fromJson(response.body().getAsJsonArray("Categories"), listType);

                            String specialization = gson.toJson(categoriesModels);
                            StaticSharedpreference.saveInfo(Constants.SPECIALIZATION_LIST, specialization, activity);
                            StaticSharedpreference.saveInfo(Constants.GOTO_PAGE, "specialization", activity);
                            intent = new Intent(activity, SpecializationActivity.class);

                        }
                    } else {
                        StaticSharedpreference.saveInfo(Constants.GOTO_PAGE, "signupSecond", activity);
                        StaticSharedpreference.saveInfo(Constants.EMAIL, email, activity);

                        intent = new Intent(activity, SignUpSecondPageActivity.class);
                        intent.putExtra("quickBloxId", qbUser.getId());
                        intent.putExtra("quickBloxName", qbUser.getLogin());

                    }
                    activity.startActivity(intent, options1.toBundle());
                    activity.finishAffinity();
                }
                // createSessionWithUser();
            }

            @Override
            public void onError(QBResponseException errors) {
                DialogClass.custom_pd.dismiss();
            }
        });
    }


    public void createSessionWithUser(final Activity activity) {

        this.activity = activity;
        Snippet createSessionWithUser = new Snippet("create session", "with user") {
            @Override
            public void execute() {

                QBAuth.createSession(new QBUser(qbUserLocal.getLogin(), activity.getString(R.string.quickBloxPass))).performAsync(new QBEntityCallback<QBSession>() {
                    @Override
                    public void onSuccess(QBSession session, Bundle args) {

                        qbUserLocal.setPassword(activity.getString(R.string.quickBloxPass));
                        //   login(qbUser,true,username,response.body(),context,activity,null);

                        SharedPreferences sharedPreferences = activity.getSharedPreferences("ShareAllYourEmotionFCM", activity.MODE_PRIVATE);
                        String regId = sharedPreferences.getString("fcm", "");
                        QbSubscription(qbUserLocal, regId, activity);

                    }

                    @Override
                    public void onError(QBResponseException errors) {

                    }
                });

                try {
                    BaseService.createFromExistentToken("31ed199120fb998dc472aea785a1825809ad5c04", null);
                } catch (BaseServiceException e) {
                    e.printStackTrace();
                }
            }
        };
        createSessionWithUser.execute();
    }


    private void QbSubscription(final QBUser qbUser, final String regId, final Activity activity) {

        Snippet createSubscription = new Snippet("create subscription") {
            @Override
            public void execute() {
                qbUserLocal.setPassword(activity.getString(R.string.quickBloxPass));
                QBSubscription subscription = new QBSubscription(QBNotificationChannel.GCM);
                subscription.setEnvironment(QBEnvironment.PRODUCTION);
                //
                String deviceId = Utils.generateDeviceId(activity);
                if (deviceId == null) {
                    deviceId = "UniversalDeviceId";
                }


                subscription.setDeviceUdid(deviceId);
                subscription.setRegistrationID(regId);
                subscription.setNotificationChannel(QBNotificationChannel.GCM);
                subscription.setDevice(new QBDevice(activity));

                QBPushNotifications.createSubscription(subscription).performAsync(new QBEntityCallback<ArrayList<QBSubscription>>() {

                    @Override
                    public void onSuccess(ArrayList<QBSubscription> subscriptions, Bundle args) {

                        if (chatService == null) {
                            QBChatService.setDebugEnabled(true);
                            chatService = QBChatService.getInstance();
                        }
                        SharedPrefsHelper.getInstance().saveQbUser(qbUser);

                        StaticSharedpreference.saveInfo(Constants.QUICK_BLOX_ID, "" + qbUser.getId(), activity);


                    }

                    @Override
                    public void onError(QBResponseException errors) {
                        // handleErrors(errors);
                    }
                });
            }
        };
        createSubscription.execute();
    }

    public void gotoNextScreen(QBUser qbUser) {

        if (fromWhere.equalsIgnoreCase("signup")) {
            DialogClass.custom_pd.dismiss();
            ActivityOptions options1 =
                    ActivityOptions.makeCustomAnimation(activity, R.anim.slide_to_right, R.anim.slide_from_left);

            Intent intent = new Intent(activity, OtpActivity.class);
            intent.putExtra("name", name);
            intent.putExtra("email", email);
            intent.putExtra("mobile", mobile);
            intent.putExtra("dob", dob);
            intent.putExtra("password", password);
            intent.putExtra("gender", gender);
            intent.putExtra("about", about);
            intent.putExtra("url", url);
            intent.putExtra("isSocialUser", isSocialUser);
            intent.putExtra("socialType", socialType);
            intent.putExtra("socialId", socialId);
            intent.putExtra("quickId", qbUser.getId() + "");
            intent.putExtra("quickName", qbUser.getLogin() + "");
            activity.startActivity(intent, options1.toBundle());
        } else if (fromWhere.equalsIgnoreCase("login")) {
            //  DialogClass.custom_pd.dismiss();
            /*save user id in share preference*/
            /*StaticSharedpreference.saveInt(Constants.USER_ID,
                    response.body().get("UserProfile").getAsJsonObject().get("UserId").getAsInt(), activity);
            StaticSharedpreference.saveInfo(Constants.NAME,
                    response.body().get("UserProfile").getAsJsonObject().get("Name").getAsString(), activity);
            StaticSharedpreference.saveInfo(Constants.EMAIL,
                    response.body().get("UserProfile").getAsJsonObject().get("EmailId").getAsString(), activity);
            StaticSharedpreference.saveInfo(Constants.MOBILE_NO,
                    response.body().get("UserProfile").getAsJsonObject().get("Mobile").getAsString(), activity);
            StaticSharedpreference.saveInfo(Constants.DOB,
                    response.body().get("UserProfile").getAsJsonObject().get("Dob").getAsString(), activity);
            StaticSharedpreference.saveInfo(Constants.GENDER,
                    response.body().get("UserProfile").getAsJsonObject().get("Gender").getAsString(), activity);
            StaticSharedpreference.saveInfo(Constants.PROFILE_PIC,
                    response.body().get("UserProfile").getAsJsonObject().get("ProfileThumbnail").getAsString(), activity);

            StaticSharedpreference.saveInfo(Constants.ABOUT,
                    response.body().get("UserProfile").getAsJsonObject().get("About").getAsString(), activity);


            if (!response.body().get("UserProfile").getAsJsonObject().get("IsSpecializationFill").getAsBoolean()) {

                Gson gson = new Gson();
                Type listType = new TypeToken<ArrayList<SpecializationModel>>() {
                }.getType();

                ArrayList<SpecializationModel> categoriesModels =
                        gson.fromJson(response.body().getAsJsonArray("Categories"), listType);

                String specialization = gson.toJson(categoriesModels);
                StaticSharedpreference.saveInfo(Constants.SPECIALIZATION_LIST, specialization, activity);

                ActivityOptions options1 =
                        ActivityOptions.makeCustomAnimation(activity, R.anim.slide_to_right, R.anim.slide_from_left);
                StaticSharedpreference.saveInfo(Constants.GOTO_PAGE, "specialization", activity);
                Intent intent = new Intent(activity, SpecializationActivity.class);
                activity.startActivity(intent, options1.toBundle());

            } else {

                ActivityOptions options1 =
                        ActivityOptions.makeCustomAnimation(activity, R.anim.slide_to_right, R.anim.slide_from_left);
                StaticSharedpreference.saveInfo(Constants.GOTO_PAGE, "home", activity);
                activity.startActivity(new Intent(activity, HomeActivity.class), options1.toBundle());
                activity.finishAffinity();
            }*/
        } else if (fromWhere.equalsIgnoreCase("fb")) {
            DialogClass.custom_pd.dismiss();
            StaticSharedpreference.saveInt(Constants.USER_ID,
                    response.body().get("UserProfile").getAsJsonObject().get("UserId").getAsInt(), activity);

            StaticSharedpreference.saveInfo(Constants.NAME,
                    response.body().get("UserProfile").getAsJsonObject().get("Name").getAsString(), activity);
            StaticSharedpreference.saveInfo(Constants.EMAIL,
                    response.body().get("UserProfile").getAsJsonObject().get("EmailId").getAsString(), activity);
            StaticSharedpreference.saveInfo(Constants.PROFILE_PIC,
                    response.body().get("UserProfile").getAsJsonObject().get("ProfileThumbnail").getAsString(), activity);


            ActivityOptions options1 =
                    ActivityOptions.makeCustomAnimation(activity, R.anim.slide_to_right, R.anim.slide_from_left);
            Intent intent = null;

            if (response.body().get("UserProfile").getAsJsonObject().get("IsProfileCompleted").getAsBoolean()) {

                StaticSharedpreference.saveInfo(Constants.ABOUT,
                        response.body().get("UserProfile").getAsJsonObject().get("About").getAsString(), activity);
                StaticSharedpreference.saveInfo(Constants.MOBILE_NO,
                        response.body().get("UserProfile").getAsJsonObject().get("Mobile").getAsString(), activity);

                StaticSharedpreference.saveInfo(Constants.DOB,
                        response.body().get("UserProfile").getAsJsonObject().get("Dob").getAsString(), activity);


                if (response.body().get("UserProfile").getAsJsonObject().get("IsSpecializationFill").getAsBoolean()) {
                    StaticSharedpreference.saveInfo(Constants.GOTO_PAGE, "home", activity);
                    StaticSharedpreference.saveInfo(Constants.GENDER,
                            response.body().get("UserProfile").getAsJsonObject().get("Gender").getAsString(), activity);


                    intent = new Intent(activity, HomeActivity.class);

                } else {

                    Gson gson = new Gson();
                    Type listType = new TypeToken<ArrayList<SpecializationModel>>() {
                    }.getType();

                    ArrayList<SpecializationModel> categoriesModels =
                            gson.fromJson(response.body().getAsJsonArray("Categories"), listType);

                    String specialization = gson.toJson(categoriesModels);
                    StaticSharedpreference.saveInfo(Constants.SPECIALIZATION_LIST, specialization, activity);
                    StaticSharedpreference.saveInfo(Constants.GOTO_PAGE, "specialization", activity);
                    intent = new Intent(activity, SpecializationActivity.class);

                }
            } else {
                StaticSharedpreference.saveInfo(Constants.GOTO_PAGE, "signupSecond", activity);
                StaticSharedpreference.saveInfo(Constants.EMAIL, email, activity);

                intent = new Intent(activity, SignUpSecondPageActivity.class);
                intent.putExtra("quickBloxId", qbUser.getId());
                intent.putExtra("quickBloxName", qbUser.getLogin());

            }
            activity.startActivity(intent, options1.toBundle());
            activity.finishAffinity();
        }
    }

}
