package com.sharemyyouremotion.qucikblox;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;

import com.quickblox.auth.session.QBSettings;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.ServiceZone;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.core.helper.StringifyArrayList;
import com.quickblox.users.QBUsers;
import com.quickblox.users.model.QBUser;
import com.sharemyyouremotion.R;

/**
 * Created by Avinash on 11/17/2017.
 */

public class QuickBloxCredential {

    //infoenum.testing

   /* String AppId = "60030";
    String AuthKey = "Edj3EthcZgq6tVh";
    String AuthSecret = "W8cSYg7Xjd37cWg";
    String AccountKey = "J5KF-wi6xL-vJBYHWHeF";
    String ApiDomain = "https://api.quickblox.com";
    String ChatDomain = "chat.quickblox.com";*/

   /* String AppId = "65025";
    String AuthKey = "2RkdEMnGYRdVFkR";
    String AuthSecret = "CjedMXGdCZ4BJew";
    String AccountKey = "G3RTST5xM6PJfsuHiL5r";
    String ApiDomain = "https://api.quickblox.com";
    String ChatDomain = "chat.quickblox.com";*/

    String AppId = "73354";
    String AuthKey = "36qRQxg6q8Lten4";
    String AuthSecret = "mBOxZMc6vBUy3E9";
    String AccountKey = "Ty2f7CNpxHGLNU78LbRG";
    String ApiDomain = "https://api.quickblox.com";
    String ChatDomain = "chat.quickblox.com";


    public void initCredentials(Context context) {

        QBSettings.getInstance().init(context, AppId, AuthKey, AuthSecret);
        QBSettings.getInstance().setAccountKey(AccountKey);
        if (!TextUtils.isEmpty(ApiDomain) && !TextUtils.isEmpty(ChatDomain)) {
            QBSettings.getInstance().setEndpoints(ApiDomain, ChatDomain, ServiceZone.DEVELOPMENT);
            QBSettings.getInstance().setZone(ServiceZone.DEVELOPMENT);
        }

    }


    public void quickBloxSignup(Context context, String email, String name, String username) {

        initCredentials(context);

        QBUser qbUser = new QBUser();
        qbUser.setEmail(email);
        qbUser.setPassword(context.getResources().getString(R.string.quickBloxPass));
        qbUser.setFullName(name);
        qbUser.setLogin(username);
        StringifyArrayList<String> tagsArray = new StringifyArrayList<>();
        tagsArray.add("myChatRoom");
        qbUser.setTags(tagsArray);

        QBUsers.signUpSignInTask(qbUser).performAsync(new QBEntityCallback<QBUser>() {
            @Override
            public void onSuccess(QBUser qbUser, Bundle bundle) {

            }

            @Override
            public void onError(QBResponseException error) {

            }
        });
    }


}
