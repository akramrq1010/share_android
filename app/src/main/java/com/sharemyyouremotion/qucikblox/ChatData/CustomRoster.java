package com.sharemyyouremotion.qucikblox.ChatData;

import android.util.Log;

import com.quickblox.chat.QBChatService;
import com.quickblox.chat.QBRoster;
import com.quickblox.chat.listeners.QBRosterListener;
import com.quickblox.chat.model.QBPresence;
import com.quickblox.chat.model.QBRosterEntry;

import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPException;

import java.util.Collection;

/**
 * Created by Avinash on 11/23/2017.
 */

public class CustomRoster {

    private static final String TAG = CustomRoster.class.getSimpleName();

    // Chat service
    //
    private QBChatService chatService;

    private QBRoster сhatRoster;
    private QBRosterListener rosterListener;

   public CustomRoster(QBRoster сhatRoster, QBRosterListener rosterListener){

       this.сhatRoster=сhatRoster;
       this.rosterListener=rosterListener;


       initRosterListener();
       getRosterUsers.execute();
       addUserToRoster.execute();




   }



    private void initRosterListener() {
        rosterListener = new QBRosterListener() {
            @Override
            public void entriesDeleted(Collection<Integer> userIds) {
                 Log.e("Avinash","entriesDeleted: " + userIds);
            }

            @Override
            public void entriesAdded(Collection<Integer> userIds) {
                 Log.e("Avinash","entriesAdded: " + userIds);
            }

            @Override
            public void entriesUpdated(Collection<Integer> userIds) {
                 Log.e("Avinash","entriesUpdated: " + userIds);
            }

            @Override
            public void presenceChanged(QBPresence presence) {
                 Log.e("Avinash","presenceChanged: " + presence);
            }
        };
    }


    Snippet sendPresence = new Snippet("send presence") {
        @Override
        public void execute() {
            if (сhatRoster == null) {
                 Log.e("Avinash","Please login first");
                return;
            }

//            QBPresence presence = new QBPresence(QBPresence.Type.online);
            QBPresence presence = new QBPresence(QBPresence.Type.online, "I'm at home", 1, QBPresence.Mode.available);
            try {
                сhatRoster.sendPresence(presence);
            } catch (SmackException.NotConnectedException e) {
                 Log.e("Avinash","error: " + e.getClass().getSimpleName());
            }
        }
    };

    Snippet getRosterUsers = new Snippet("get roster users") {
        @Override
        public void execute() {
            if (сhatRoster == null) {
                 Log.e("Avinash","Please login first");
                return;
            }

            Collection<QBRosterEntry> entries = сhatRoster.getEntries();
             Log.e("Avinash","Roster users:  " + entries);
        }
    };

    Snippet getUserPresence = new Snippet("get user's presence") {
        @Override
        public void execute() {
            if (сhatRoster == null) {
                 Log.e("Avinash","Please login first");
                return;
            }

            int userID = ApplicationConfig.getInstance().getTestUserId2();

            QBPresence presence = сhatRoster.getPresence(userID);
            if (presence == null) {
                 Log.e("Avinash","No user in your roster");
                return;
            }
            if (presence.getType() == QBPresence.Type.online) {
                 Log.e("Avinash","User " + userID + " is online");
            } else {
                 Log.e("Avinash","User " + userID + " is offline");
            }
        }
    };

    Snippet addUserToRoster = new Snippet("add user to roster") {
        @Override
        public void execute() {
            int userID = 567;

            if (сhatRoster.contains(userID)) {
                try {
                    сhatRoster.subscribe(userID);
                } catch (SmackException.NotConnectedException e) {
                     Log.e("Avinash","error: " + e.getClass().getSimpleName());
                }
            } else {
                try {
                    сhatRoster.createEntry(userID, null);
                    confirmAddRequest.execute();
                } catch (XMPPException e) {
                     Log.e("Avinash","error: " + e.getLocalizedMessage());
                } catch (SmackException.NotLoggedInException | SmackException.NotConnectedException | SmackException.NoResponseException e) {
                     Log.e("Avinash","error: " + e.getClass().getSimpleName());
                }
            }
        }
    };

    Snippet removeUserFromRoster = new Snippet("remove user from roster") {
        @Override
        public void execute() {
            int userID = ApplicationConfig.getInstance().getTestUserId2();

            try {
                сhatRoster.unsubscribe(userID);
            } catch (SmackException.NotConnectedException e) {
                e.printStackTrace();
            }
        }
    };


    Snippet confirmAddRequest = new Snippet("confirm add request") {
        @Override
        public void execute() {
            int userID = 567;

            try {
                сhatRoster.confirmSubscription(userID);
                getRosterUsers.execute();
            } catch (SmackException.NotConnectedException | SmackException.NotLoggedInException | SmackException.NoResponseException e) {
                 Log.e("Avinash","error: " + e.getClass().getSimpleName());
            } catch (XMPPException e) {
                Log.e("Avinash","error: " + e.getLocalizedMessage());
            }
        }
    };
}
