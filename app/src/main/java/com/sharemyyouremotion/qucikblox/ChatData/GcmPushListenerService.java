package com.sharemyyouremotion.qucikblox.ChatData;

import android.content.Intent;

import com.quickblox.sample.core.gcm.CoreGcmPushListenerService;
import com.quickblox.sample.core.utils.NotificationUtils;
import com.quickblox.sample.core.utils.ResourceUtils;
import com.sharemyyouremotion.R;


public class GcmPushListenerService extends CoreGcmPushListenerService {
    private static final int NOTIFICATION_ID = 1;

    @Override
    protected void showNotification(String message) {
        NotificationUtils.showNotification(this, SplashActivity.class,
                ResourceUtils.getString(R.string.app_name), message,
                R.mipmap.ic_notification, NOTIFICATION_ID);
    }

    @Override
    public void zzd(Intent intent) {

    }
}