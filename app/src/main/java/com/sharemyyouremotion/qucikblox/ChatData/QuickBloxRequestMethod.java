package com.sharemyyouremotion.qucikblox.ChatData;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.core.helper.StringifyArrayList;
import com.quickblox.users.QBUsers;
import com.quickblox.users.model.QBUser;

/**
 * Created by Akram on 11/18/2017.
 */

public class QuickBloxRequestMethod {

    public void quickbloxlogin(String username, final Context context, final Activity activity) {
        QuickBloxCredential helper = new QuickBloxCredential();

        helper.initCredentials(context);
        QBUser qbUser = new QBUser(username, "x6Bt0VDy5");

        QBUsers.signIn(qbUser).performAsync(new QBEntityCallback<QBUser>() {
            @Override
            public void onSuccess(QBUser qbUser, Bundle bundle) {

               activity.startActivity(new Intent(activity,HomeActivity.class));

            }

            @Override
            public void onError(QBResponseException errors) {

            }
        });
    }


    public void quickBloxSignup(final String username,final Activity activity) {

        QuickBloxCredential helper = new QuickBloxCredential();
        helper.initCredentials(activity);
        QBUser qbUser = new QBUser();
        qbUser.setPassword("x6Bt0VDy5");
        qbUser.setFullName(username);
        qbUser.setLogin(username);
        StringifyArrayList<String> tagsArray = new StringifyArrayList<>();
        tagsArray.add("myChatRoom");
        qbUser.setTags(tagsArray);

        QBUsers.signUpSignInTask(qbUser).performAsync(new QBEntityCallback<QBUser>() {
            @Override
            public void onSuccess(QBUser qbUser, Bundle bundle) {

                activity.startActivity(new Intent(activity,HomeActivity.class));}

            @Override
            public void onError(QBResponseException error) {

            }
        });
    }


}
