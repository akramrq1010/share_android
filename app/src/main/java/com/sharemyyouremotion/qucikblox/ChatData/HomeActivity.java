package com.sharemyyouremotion.qucikblox.ChatData;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.quickblox.auth.QBAuth;
import com.quickblox.auth.session.BaseService;
import com.quickblox.auth.session.QBSession;
import com.quickblox.chat.QBChatService;
import com.quickblox.chat.model.QBChatDialog;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.BaseServiceException;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.users.model.QBUser;
import com.sharemyyouremotion.R;


import java.util.List;

public class HomeActivity extends AppCompatActivity {
Button btn_login,btn_sign;
    private static final String EXTRA_DIALOG = "dialog";
EditText edt;

    private ListView usersListView;
    private QBChatDialog qbDialog;
    QuickBloxRequestMethod q=new QuickBloxRequestMethod();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       // setContentView(R.layout.demo_activity_home);
    //    edt=(EditText)findViewById(R.id.edt);
      //  btn_login=(Button)findViewById(R.id.btn_login);
//        btn_sign=(Button)findViewById(R.id.btn_sign);
//
//        btn_login.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if ((""+edt.getText().toString()).length()>0){
//
//                  q.quickbloxlogin(""+edt.getText().toString(),HomeActivity.this,HomeActivity.this);
//                }
//            }
//        });
//        btn_sign.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if ((""+edt.getText().toString()).length()>0){
//
//                    q.quickBloxSignup(""+edt.getText().toString(),HomeActivity.this);
//
//                }
//            }
//        });

        QBChatService.setDebugEnabled(true);
        QBChatService.ConfigurationBuilder chatServiceConfigurationBuilder = new QBChatService.ConfigurationBuilder();
        chatServiceConfigurationBuilder.setSocketTimeout(60); //Sets chat socket's read timeout in seconds
        chatServiceConfigurationBuilder.setKeepAlive(true); //Sets connection socket's keepAlive option.
        chatServiceConfigurationBuilder.setUseTls(true); //Sets the TLS security mode used when making the connection. By default TLS is disabled.
        QBChatService.setConfigurationBuilder(chatServiceConfigurationBuilder);


        createSessionWithUser.execute();

        qbDialog = (QBChatDialog) getIntent().getSerializableExtra(EXTRA_DIALOG);

        List<Integer> userIds = qbDialog.getOccupants();
        List<QBUser> users = QbUsersHolder.getInstance().getUsersByIds(userIds);

    }


    Snippet createSessionWithUser = new Snippet("create session", "with user") {
        @Override
        public void execute() {

            QBAuth.createSession(new QBUser("shubham",
                   "shubham123")).performAsync(new QBEntityCallback<QBSession>() {
                @Override
                public void onSuccess(QBSession session, Bundle args) {
                    Toast.makeText(getApplicationContext(),"created\n"+session.getToken(),Toast.LENGTH_LONG).show();
                }

                @Override
                public void onError(QBResponseException errors) {
                    Toast.makeText(getApplicationContext(),"failed",Toast.LENGTH_LONG).show();

                }
            });

            try {
                BaseService.createFromExistentToken("31ed199120fb998dc472aea785a1825809ad5c04", null);
            } catch (BaseServiceException e) {
                e.printStackTrace();
            }
        }
    };
}
