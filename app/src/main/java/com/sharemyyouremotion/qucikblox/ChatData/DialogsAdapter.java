package com.sharemyyouremotion.qucikblox.ChatData;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;


import com.quickblox.chat.listeners.QBChatDialogParticipantListener;
import com.quickblox.chat.model.QBChatDialog;
import com.quickblox.sample.core.ui.adapter.BaseSelectableListAdapter;
import com.quickblox.sample.core.utils.ResourceUtils;
import com.sharemyyouremotion.R;
import com.sharemyyouremotion.activity.ChatListActivity;


import java.util.Collection;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class DialogsAdapter extends BaseSelectableListAdapter<QBChatDialog> {

    private static final String EMPTY_STRING = "";
    private QBChatDialogParticipantListener participantListener;
    Activity activity;

    public DialogsAdapter(Context context, List<QBChatDialog> dialogs, Activity activity) {
        super(context, dialogs);
        this.activity = activity;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.adapter_chat, parent, false);

            holder = new ViewHolder();
            holder.rootLayout = (ViewGroup) convertView.findViewById(R.id.root);
            holder.nameTextView = (TextView) convertView.findViewById(R.id.text_dialog_name);
            holder.lastMessageTextView = (TextView) convertView.findViewById(R.id.text_dialog_last_message);
            holder.imgProfilePic = (CircleImageView) convertView.findViewById(R.id.imgProfilePic);
            holder.donutProgress = convertView.findViewById(R.id.donutProgress);
            holder.unreadCounterTextView = (TextView) convertView.findViewById(R.id.text_dialog_unread_count);


            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        QBChatDialog dialog = getItem(position);

        Collection<Integer> onlineUsers = null;

//        try {
//            onlineUsers = dialog.requestOnlineUsers();
//
//            for (int i = 0; i < onlineUsers.size(); i++) {
//                Log.e("Avinash"," "+onlineUsers.size());
//            }
//
//        } catch (XMPPException.XMPPErrorException | SmackException.NotConnectedException | SmackException.NoResponseException e) {
//            Log.e("Avinash","Error");
//        }


//        for (int i = 0; i < dialog.getOccupants().size(); i++) {
//            Log.e("AVinash","aa  "+ dialog.getOccupants().get(i));
//        }
//        try {
//            if (QBChatService.getInstance().getLastUserActivity(dialog.getOccupants().get(1))>1000){
//            Log.e("Avinash","offline");
//
//        }
//        else
//            {
//                Log.e("Avinash","online");
//            }
//            Log.e("Avinash",""+QBChatService.getInstance().getLastUserActivity(dialog.getOccupants().get(1)));
//          //  Log.e("Avinash",""+QBChatService.getInstance().getLastUserActivity(dialog.getId()));
//        } catch (XMPPException.XMPPErrorException e) {
//            e.printStackTrace();
//        } catch (SmackException.NotConnectedException e) {
//            e.printStackTrace();
//        } catch (SmackException.NoResponseException e) {
//            e.printStackTrace();
//        }
        Log.e("Avinash", "" + dialog.getCustomData());
        if (dialog.getCustomData() != null) {
/*
                Log.v("akramr", "" + dialog.getUserId());
                Log.v("akramr", "" + StaticSharedpreference.getInfo(StaticSharedpreference.quickBlocId, context));
                String g=""+dialog.getUserId();
                String h= StaticSharedpreference.getInfo(StaticSharedpreference.quickBlocId, context);

                if ((""+dialog.getUserId()).equals(StaticSharedpreference.getInfo(StaticSharedpreference.quickBlocId, context))) {
                    String s=""+dialog.getCustomData().get("reciverProfilePic");
                    ImageLoader.image("" + dialog.getCustomData().get("reciverProfilePic"),
                            holder.imgProfilePic, holder.donutProgress, activity);
                } else {
                    ImageLoader.image("" + dialog.getCustomData().get("senderProfilePic"),
                            holder.imgProfilePic, holder.donutProgress, activity);
                }*/
        } else {
            // holder.imgProfilePic.setBackgroundDrawable(UiUtils.getColorCircleDrawable(position));
            holder.imgProfilePic.setImageResource(R.drawable.ic_user);
        }


        holder.nameTextView.setText(QbDialogUtils.getDialogName(dialog));
        holder.lastMessageTextView.setText(prepareTextLastMessage(dialog));

        int unreadMessagesCount = getUnreadMsgCount(dialog);
        if (unreadMessagesCount == 0) {
            holder.unreadCounterTextView.setVisibility(View.GONE);
         } else {
            holder.unreadCounterTextView.setVisibility(View.VISIBLE);
            holder.unreadCounterTextView.setText(String.valueOf(unreadMessagesCount > 99 ? 99 : unreadMessagesCount));
        }

        holder.rootLayout.setBackgroundColor(isItemSelected(position) ? ResourceUtils.getColor(R.color.white) :
                ResourceUtils.getColor(android.R.color.transparent));

        return convertView;
    }

    private int getUnreadMsgCount(QBChatDialog chatDialog) {
        Integer unreadMessageCount = chatDialog.getUnreadMessageCount();
        if (unreadMessageCount == null) {
            return 0;
        } else {
            return unreadMessageCount;
        }

    }

    private boolean isLastMessageAttachment(QBChatDialog dialog) {
        String lastMessage = dialog.getLastMessage();
        Integer lastMessageSenderId = dialog.getLastMessageUserId();
        return TextUtils.isEmpty(lastMessage) && lastMessageSenderId != null;
    }

    private String prepareTextLastMessage(QBChatDialog chatDialog) {
        if (isLastMessageAttachment(chatDialog)) {
            return "attach";
        } else {
            return chatDialog.getLastMessage();
        }
    }

    private static class ViewHolder {
        ViewGroup rootLayout;
        CircleImageView imgProfilePic;
        ProgressBar donutProgress;
        TextView nameTextView;
        TextView lastMessageTextView;
        TextView unreadCounterTextView;
    }


}
