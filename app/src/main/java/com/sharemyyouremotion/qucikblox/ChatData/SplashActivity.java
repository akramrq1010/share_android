package com.sharemyyouremotion.qucikblox.ChatData;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.quickblox.auth.session.QBSessionManager;
import com.quickblox.chat.QBChatService;
import com.quickblox.chat.QBRoster;
import com.quickblox.chat.listeners.QBRosterListener;
import com.quickblox.chat.listeners.QBSubscriptionListener;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.sample.core.ui.dialog.ProgressDialogFragment;
import com.quickblox.sample.core.utils.SharedPrefsHelper;
import com.quickblox.users.QBUsers;
import com.quickblox.users.model.QBUser;
import com.sharemyyouremotion.R;
import com.sharemyyouremotion.qucikblox.App;


import org.jivesoftware.smack.ConnectionListener;
import org.jivesoftware.smack.XMPPConnection;

public class SplashActivity extends CoreSplashActivity {
    public static QBChatService chatService;

    public static QBRoster сhatRoster;
    public static QBRosterListener rosterListener;
    public static QBSubscriptionListener subscriptionListener;
    private static final String TAG = SplashActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (checkConfigsWithSnackebarError()) {
            proceedToTheNextActivityWithDelay();
        }
        proceedToTheNextActivityWithDelay();
    }

    @Override
    protected String getAppName() {
        return "";
    }

    @Override
    protected void proceedToTheNextActivity() {
        if (checkSignIn()) {
            restoreChatSession();
        } else {
            //  LoginActivity11.start(this);

//            Intent intent = new Intent(SplashActivity.this, LoginActivity11.class);
//            startActivity(intent);

            final QBUser user = new QBUser();
            // We use hardcoded password for all users for test purposes
            // Of course you shouldn't do that in your app
            user.setLogin("shubham");
            user.setPassword("akram123");

            //  login(user);
            quickbloxlogin();
        }
    }

    @Override
    protected boolean sampleConfigIsCorrect() {
        boolean result = super.sampleConfigIsCorrect();
      //  result = result && App.getSampleConfigs() != null;
        return result;
    }

    private void restoreChatSession() {
        if (ChatHelper.getInstance().isLogged()) {
           // ChatListActivity.start(this);
            finish();
        } else {
            QBUser currentUser = getUserFromSession();
            loginToChat(currentUser);
        }
    }

    private QBUser getUserFromSession() {
        QBUser user = SharedPrefsHelper.getInstance().getQbUser();
        user.setId(QBSessionManager.getInstance().getSessionParameters().getUserId());
        return user;
    }

    @Override
    protected boolean checkSignIn() {
        return SharedPrefsHelper.getInstance().hasQbUser();
    }

    private void loginToChat(final QBUser user) {
      //  ProgressDialogFragment.show(getSupportFragmentManager(), R.string.dlg_restoring_chat_session);

        ChatHelper.getInstance().loginToChat(user, new QBEntityCallback<Void>() {
            @Override
            public void onSuccess(Void result, Bundle bundle) {
                Log.v(TAG, "Chat login onSuccess()");
                //   initChatService();

                ProgressDialogFragment.hide(getSupportFragmentManager());
          //      ChatListActivity.start(SplashActivity.this);
                finish();
            }

            @Override
            public void onError(QBResponseException e) {
                ProgressDialogFragment.hide(getSupportFragmentManager());
                Log.w(TAG, "Chat login onError(): " + e);
//                showSnackbarError( findViewById(R.id.layout_root), R.string.error_recreate_session, e,
//                        new View.OnClickListener() {
//                            @Override
//                            public void onClick(View v) {
//                                loginToChat(user);
//                            }
//                        });
            }
        });
    }


    private void login(final QBUser user) {
        //ProgressDialogFragment.show(getSupportFragmentManager(), R.string.dlg_login);
        ChatHelper.getInstance().login(user, new QBEntityCallback<Void>() {
            @Override
            public void onSuccess(Void result, Bundle bundle) {
                SharedPrefsHelper.getInstance().saveQbUser(user);


            //    ChatListActivity.start(SplashActivity.this);
                finish();
                ProgressDialogFragment.hide(getSupportFragmentManager());
            }

            @Override
            public void onError(QBResponseException e) {
                ProgressDialogFragment.hide(getSupportFragmentManager());

            }
        });
    }

    public void quickbloxlogin() {
        QuickBloxCredential helper = new QuickBloxCredential();

        helper.initCredentials(SplashActivity.this);
        // QBUser qbUser = new QBUser("tiwari", "akram123");
        QBUser qbUser = new QBUser("shubham", "akram123");

        QBUsers.signIn(qbUser).performAsync(new QBEntityCallback<QBUser>() {
            @Override
            public void onSuccess(QBUser qbUser, Bundle bundle) {
                qbUser.setPassword("akram123");
                login(qbUser);

            }

            @Override
            public void onError(QBResponseException errors) {

            }
        });
    }

    private void initChatService() {
        QBChatService.ConfigurationBuilder configurationBuilder = new QBChatService.ConfigurationBuilder();
        configurationBuilder.setKeepAlive(true)
                .setSocketTimeout(0);
        QBChatService.setConfigurationBuilder(configurationBuilder);
        QBChatService.setDebugEnabled(true);
        QBChatService.setDefaultPacketReplyTimeout(10000);

        chatService = ChatHelper.getInstance().qbChatService;

        chatService.addConnectionListener(chatConnectionListener);
        // stream management
        chatService.setUseStreamManagement(true);
//        chatService.setUseStreamManagementResumption(true);

        initSubscriptionListener();

    }

    private void initSubscriptionListener() {
        subscriptionListener = new QBSubscriptionListener() {
            @Override
            public void subscriptionRequested(int userId) {
                Log.e("Avinash", "subscriptionRequested: " + userId);


                Toast.makeText(getApplicationContext(), "aa   " + userId, Toast.LENGTH_LONG).show();

                initRoster();
            }
        };
    }

    private static void initRoster() {
        сhatRoster = chatService.getRoster(QBRoster.SubscriptionMode.mutual, subscriptionListener);
        сhatRoster.addRosterListener(rosterListener);

        CustomRoster c = new CustomRoster(сhatRoster, rosterListener);

    }


    ConnectionListener chatConnectionListener = new ConnectionListener() {
        @Override
        public void connected(XMPPConnection connection) {
            Log.e("Avinash", "connected");
            initSubscriptionListener();
        }

        @Override
        public void authenticated(XMPPConnection connection, boolean authenticated) {
            Log.e("Avinash", "authenticated");
        }

        @Override
        public void connectionClosed() {
            Log.e("Avinash", "connectionClosed");
        }

        @Override
        public void connectionClosedOnError(final Exception e) {
            Log.e("Avinash", "connectionClosedOnError: " + e.getLocalizedMessage());
            Log.e("Avinash", "isLoggedIn: " + chatService.isLoggedIn());

        }

        @Override
        public void reconnectingIn(final int seconds) {
            if (seconds % 5 == 0) {

            }
        }

        @Override
        public void reconnectionFailed(Exception e) {

        }

        @Override
        public void reconnectionSuccessful() {
        }


    };


}