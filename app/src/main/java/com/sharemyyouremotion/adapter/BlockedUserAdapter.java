package com.sharemyyouremotion.adapter;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.sharemyyouremotion.R;
import com.sharemyyouremotion.activity.MyQuestionFeedbackActivity;
import com.sharemyyouremotion.activity.WebViewForVideoActivity;
import com.sharemyyouremotion.fragment.LoginFragment;
import com.sharemyyouremotion.fragment.MyQuestionFragment;
import com.sharemyyouremotion.helper.DialogClass;
import com.sharemyyouremotion.helper.TimeConveter;
import com.sharemyyouremotion.model.BlockUserModel;
import com.sharemyyouremotion.model.MyQuestionModel;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;


public class BlockedUserAdapter extends RecyclerView.Adapter<BlockedUserAdapter.MyViewHolder> {

    Activity activity;
    ArrayList<BlockUserModel> blockUserModels;

    public BlockedUserAdapter(Activity activity, ArrayList<BlockUserModel> blockUserModels) {
        this.activity = activity;
        this.blockUserModels = blockUserModels;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        MyViewHolder myViewHolder;
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_block_user, parent, false);

        myViewHolder = new MyViewHolder(view);


        return myViewHolder;
    }


    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {

    holder.tvName.setText(blockUserModels.get(position).getName());
        Glide.with(activity)
                .load(blockUserModels.get(position).getProfileThumbnail())
                .into(holder.imgProfile);
    }

    @Override
    public int getItemCount() {
        return blockUserModels.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView  tvName;
        CircleImageView imgProfile;

        public MyViewHolder(View itemView) {
            super(itemView);

            tvName = itemView.findViewById(R.id.tvName);
            imgProfile = itemView.findViewById(R.id.imgProfile);

        }
    }


}
