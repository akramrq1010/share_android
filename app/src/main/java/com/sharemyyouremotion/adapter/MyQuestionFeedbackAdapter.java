package com.sharemyyouremotion.adapter;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.quickblox.chat.QBChatService;
import com.quickblox.chat.QBRestChatService;
import com.quickblox.chat.model.QBChatDialog;
import com.quickblox.chat.model.QBDialogType;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.sample.core.utils.SharedPrefsHelper;
import com.quickblox.users.model.QBUser;
import com.sharemyyouremotion.R;
import com.sharemyyouremotion.activity.ChatActivity;
import com.sharemyyouremotion.activity.HomeActivity;
import com.sharemyyouremotion.activity.MyQuestionFeedbackActivity;
import com.sharemyyouremotion.activity.WebViewForVideoActivity;
import com.sharemyyouremotion.fragment.MyQuestionFragment;
import com.sharemyyouremotion.helper.Constants;
import com.sharemyyouremotion.helper.DialogClass;
import com.sharemyyouremotion.helper.StaticSharedpreference;
import com.sharemyyouremotion.helper.TimeConveter;
import com.sharemyyouremotion.model.MyQuestionFeedbackModel;
import com.sharemyyouremotion.qucikblox.ChatData.ChatHelper;
import com.sharemyyouremotion.qucikblox.ChatData.Snippet;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class MyQuestionFeedbackAdapter extends RecyclerView.Adapter<MyQuestionFeedbackAdapter.MyViewHolder> {

    Activity activity;
    ArrayList<MyQuestionFeedbackModel> myQuestionFeedbackModels;
    String questionType;
    MyQuestionFeedbackActivity myQuestionFeedbackActivity;

    public MyQuestionFeedbackAdapter(Activity activity, ArrayList<MyQuestionFeedbackModel> myQuestionFeedbackModels, String questionType, MyQuestionFeedbackActivity myQuestionFeedbackActivity) {
        this.activity = activity;
        this.myQuestionFeedbackModels = myQuestionFeedbackModels;
        this.questionType = questionType;
        this.myQuestionFeedbackActivity = myQuestionFeedbackActivity;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;


        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_my_question_feedback, parent, false);

        MyViewHolder myViewHolder = new MyViewHolder(view);

        myViewHolder.linearRating = view.findViewById(R.id.linearRating);


        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        final DialogClass dialogClass = new DialogClass();

        holder.linearRating.setTag(position);

        holder.linearRating.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!myQuestionFeedbackModels.get(position).getIsRatingGiven()) {
                    int pos = (Integer) view.getTag();
                    if (StaticSharedpreference.getInt(Constants.USER_ID, activity) != myQuestionFeedbackModels.get(pos).getGivenBy())
                        dialogClass.ratingDailog(activity, myQuestionFeedbackModels.get(pos).getGivenBy(), myQuestionFeedbackActivity,
                                myQuestionFeedbackModels.get(pos).getFeedbackId());
                    else
                        Toast.makeText(activity, "This is your feedback", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(activity, "You already give feedback for this user.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        holder.linearRating.setVisibility(View.VISIBLE);

        if (myQuestionFeedbackModels.get(position).getFeedbackType().equalsIgnoreCase("text")) {
            holder.tvText.setVisibility(View.VISIBLE);
            holder.relativeVideo.setVisibility(View.GONE);
            holder.relativeAudio.setVisibility(View.GONE);

            holder.tvText.setText(myQuestionFeedbackModels.get(position).getFeedbackText());


        } else if (myQuestionFeedbackModels.get(position).getFeedbackType().equalsIgnoreCase("audio")) {
            holder.tvText.setVisibility(View.GONE);
            holder.relativeVideo.setVisibility(View.GONE);
            holder.relativeAudio.setVisibility(View.VISIBLE);
            holder.imgPlayAudio.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    dialogClass.audioPlayDialog(activity, myQuestionFeedbackModels.get(position).getFeedbackAudio(), false);

                }
            });

        } else {
            holder.tvText.setVisibility(View.GONE);
            holder.relativeVideo.setVisibility(View.VISIBLE);
            holder.relativeAudio.setVisibility(View.GONE);

            holder.imgPlayVideo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ActivityOptions options1 =
                            ActivityOptions.makeCustomAnimation(activity, R.anim.slide_to_right, R.anim.slide_from_left);
                    Intent intent = new Intent(activity, WebViewForVideoActivity.class);
                    intent.putExtra("video", myQuestionFeedbackModels.get(position).getFeedbackVideo());
                    //  intent.putExtra("QuestionId",myQuestionFragments.get(position).getQuestionId());
                    activity.startActivity(intent, options1.toBundle());
                }
            });
        }

        holder.tvEmotionType.setVisibility(View.GONE);

        String date[] = myQuestionFeedbackModels.get(position).getCreatedOn().split("T");
        // holder.tvCreatedDate.setText(date[0]);

        String time = date[1].substring(0, 8);

        String finalData = TimeConveter.getdatetime(date[0], time);
        String dateFinal[] = finalData.split("T");
        String timeFinal = dateFinal[1].substring(0, 5);
        holder.tvCreatedDate.setText(timeFinal + " " + dateFinal[0]);

        Glide.with(activity)
                .load(myQuestionFeedbackModels.get(position).getUserThumbnail())
                .into(holder.imgProfile);

        if (MyQuestionFragment.screenValue == 1)
            holder.relativeChat.setVisibility(View.VISIBLE);

        holder.relativeChat.setTag(position);
        holder.relativeChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int pos = (Integer) view.getTag();

                DialogClass dialogClass1 = new DialogClass();
                dialogClass1.progresesDialog(activity);

                QBUser currentUser = SharedPrefsHelper.getInstance().getQbUser();

                QBChatService chatService = QBChatService.getInstance();
                if (!chatService.isLoggedIn()) {
                    loginToChat(currentUser, myQuestionFeedbackModels.get(pos).getQuickbloxId());
                } else {
                    chatSetting(myQuestionFeedbackModels.get(pos).getQuickbloxId());
                }

            }
        });

        holder.tvName.setVisibility(View.VISIBLE);
        if (myQuestionFeedbackModels.get(position).getIsIdentityShare()) {
            holder.tvName.setText(myQuestionFeedbackModels.get(position).getName() + "");
            Glide.with(activity)
                    .load(myQuestionFeedbackModels.get(position).getUserThumbnail())
                    .into(holder.imgProfile);
        } else {
            holder.tvName.setText(myQuestionFeedbackModels.get(position).getName() + "");
            //  holder.tvName.setText("User " + myQuestionFeedbackModels.get(position).getGivenBy() + "");
            if (myQuestionFeedbackModels.get(position).getGender().equalsIgnoreCase("female")) {
                Glide.with(activity)
                        .load(getImage("girl_user"))
                        .into(holder.imgProfile);
            } else {
                Glide.with(activity)
                        .load("")
                        .into(holder.imgProfile);
            }
        }

  /*      if (myQuestionFeedbackModels.get(position).getIsRatingGiven()) {

            holder.tvRating.setTextColor(activity.getResources().getColor(R.color.blue));
            holder.imgStar.setBackground(activity.getResources().getDrawable(R.drawable.ic_star_blue));
        } else {
            holder.tvRating.setTextColor(activity.getResources().getColor(R.color.black));
            holder.imgStar.setBackground(activity.getResources().getDrawable(R.drawable.ic_star));
        }
*/

        if (MyQuestionFragment.screenValue == 1) {
            if (myQuestionFeedbackModels.get(position).getIsRatingGiven()) {

                holder.linearRatingShow.setVisibility(View.VISIBLE);
                holder.ratingBarFeedback.setRating(myQuestionFeedbackModels.get(position).getIndividualRating());
                holder.linearRating.setVisibility(View.GONE);

            } else {
                holder.linearRatingShow.setVisibility(View.GONE);
                holder.linearRating.setVisibility(View.VISIBLE);

            }
            holder.ratingBarOverall.setRating(myQuestionFeedbackModels.get(position).getOverAllRating());
        } else {
            holder.linearRatingShow.setVisibility(View.GONE);
            holder.ratingBarFeedback.setRating(myQuestionFeedbackModels.get(position).getIndividualRating());
            holder.ratingBarOverall.setRating(myQuestionFeedbackModels.get(position).getOverAllRating());
            holder.linearRating.setVisibility(View.GONE);
        }

    }

    public int getImage(String imageName) {

        int drawableResourceId = activity.getResources().getIdentifier(imageName, "drawable", activity.getPackageName());

        return drawableResourceId;
    }

    @Override
    public int getItemCount() {
        return myQuestionFeedbackModels.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout relativeAudio, relativeVideo, relativeChat;
        TextView tvText, tvEmotionType, tvCreatedDate, tvName, tvRating;
        ImageView imgPlayAudio, imgPlayVideo, imgStar;
        CardView cardMain;
        LinearLayout linearRating, linearRatingShow;
        CircleImageView imgProfile;
        RatingBar ratingBarOverall, ratingBarFeedback;


        public MyViewHolder(View itemView) {
            super(itemView);
            relativeAudio = itemView.findViewById(R.id.relativeAudio);
            tvText = itemView.findViewById(R.id.tvText);
            tvCreatedDate = itemView.findViewById(R.id.tvCreatedDate);
            tvEmotionType = itemView.findViewById(R.id.tvEmotionType);
            relativeVideo = itemView.findViewById(R.id.relativeVideo);
            imgStar = itemView.findViewById(R.id.imgStar);
            tvRating = itemView.findViewById(R.id.tvRating);

            imgPlayAudio = itemView.findViewById(R.id.imgPlayAudio);
            imgPlayVideo = itemView.findViewById(R.id.imgPlayVideo);
            cardMain = itemView.findViewById(R.id.cardMain);
            linearRating = itemView.findViewById(R.id.linearRating);
            imgProfile = itemView.findViewById(R.id.imgProfile);
            relativeChat = itemView.findViewById(R.id.relativeChat);
            tvName = itemView.findViewById(R.id.tvName);

            ratingBarOverall = itemView.findViewById(R.id.ratingBarOverall);
            ratingBarFeedback = itemView.findViewById(R.id.ratingBarFeedback);
            linearRatingShow = itemView.findViewById(R.id.linearRatingShow);

        }
    }

    private void loginToChat(final QBUser user, final String quickBloxIdOpp) {
        //  ProgressDialogFragment.show(getSupportFragmentManager(), R.string.dlg_restoring_chat_session);

        ChatHelper.getInstance().loginToChat(user, new QBEntityCallback<Void>() {
            @Override
            public void onSuccess(Void result, Bundle bundle) {
                Log.v("akram", "Chat login onSuccess()");

                chatSetting(quickBloxIdOpp + "");

            }

            @Override
            public void onError(QBResponseException e) {
                Log.w("akram", "Chat login onError(): " + e);
            }
        });
    }


    private void chatSetting(final String opponentId) {

        final int oppoId = Integer.parseInt(opponentId);

        Snippet createPrivateDialog = new Snippet("Create Private Dialog") {
            @Override
            public void execute() {

                ArrayList<Integer> occupantIdsList = new ArrayList<>();
                occupantIdsList.add(oppoId);

                QBChatDialog dialog = new QBChatDialog();
                dialog.setType(QBDialogType.PRIVATE);
                dialog.setOccupantsIds(occupantIdsList);


                QBRestChatService.createChatDialog(dialog).performAsync(new QBEntityCallback<QBChatDialog>() {
                    @Override
                    public void onSuccess(QBChatDialog dialog, Bundle args) {
                        Log.v("akram", "quick blox chat dialog success");
                        SimpleDateFormat format = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy");

                        Log.v("akram", "qbchatdialog " + dialog);
                        DialogClass.custom_pd.dismiss();
                        ChatActivity.startForResult(activity, 165, dialog);

                       /* DialogClass.custom_pd.dismiss();
                        Intent intent = new Intent(activity, HomeActivity.class);
                        intent.putExtra("tab", "tab");
                        activity.startActivity(intent);
                        activity.finishAffinity();*/
                    }

                    @Override
                    public void onError(QBResponseException errors) {
                        // handleErrors(errors);
                        DialogClass.custom_pd.dismiss();
                    }
                });
            }
        };
        createPrivateDialog.execute();

    }

}
