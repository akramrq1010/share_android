package com.sharemyyouremotion.adapter;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.sharemyyouremotion.R;
import com.sharemyyouremotion.activity.MyQuestionFeedbackActivity;
import com.sharemyyouremotion.activity.WebViewForVideoActivity;
import com.sharemyyouremotion.fragment.LoginFragment;
import com.sharemyyouremotion.fragment.MyQuestionFragment;
import com.sharemyyouremotion.helper.DialogClass;
import com.sharemyyouremotion.helper.TimeConveter;
import com.sharemyyouremotion.model.GetMyDiaryModel;
import com.sharemyyouremotion.model.MyQuestionModel;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;


public class MyDiaryAdapter extends RecyclerView.Adapter<MyDiaryAdapter.MyViewHolder> {

    Activity activity;
    ArrayList<GetMyDiaryModel> getMyDiaryModels;

    public MyDiaryAdapter(Activity activity, ArrayList<GetMyDiaryModel> getMyDiaryModels) {
        this.activity = activity;
        this.getMyDiaryModels = getMyDiaryModels;

    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        MyViewHolder myViewHolder;
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_my_diary, parent, false);

        myViewHolder = new MyViewHolder(view);


        return myViewHolder;
    }


    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {


        holder.tvEmotion.setText(getMyDiaryModels.get(position).getEmotionIdName());
        holder.tvCategory.setText(getMyDiaryModels.get(position).getCategoryName());
        holder.tvAction.setText(getMyDiaryModels.get(position).getAction());
        holder.tvDisc.setText(getMyDiaryModels.get(position).getDiscription());
        holder.ratingBar.setRating(Float.parseFloat(getMyDiaryModels.get(position).getLevelRate()));


        String date[] = getMyDiaryModels.get(position).getCreatedOn().split("T");
        String time = date[1].substring(0, 8);

        String finalData = TimeConveter.getdatetime(date[0], time);
        String dateFinal[] = finalData.split("T");
        String timeFinal = dateFinal[1].substring(0, 5);
        holder.tvCreatedDate.setText(timeFinal + " " + dateFinal[0]);


    }

    @Override
    public int getItemCount() {
        return getMyDiaryModels.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tvEmotion, tvCategory, tvDisc, tvCreatedDate, tvAction;
        RatingBar ratingBar;

        public MyViewHolder(View itemView) {
            super(itemView);

            tvEmotion = itemView.findViewById(R.id.tvEmotion);
            tvCategory = itemView.findViewById(R.id.tvCategory);
            tvCreatedDate = itemView.findViewById(R.id.tvCreatedDate);
            tvDisc = itemView.findViewById(R.id.tvDisc);
            tvAction = itemView.findViewById(R.id.tvAction);
            ratingBar = itemView.findViewById(R.id.ratingBar);


        }
    }


}
