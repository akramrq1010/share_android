package com.sharemyyouremotion.adapter;

import android.app.Activity;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.bumptech.glide.request.target.Target;
import com.sharemyyouremotion.R;
import com.sharemyyouremotion.model.EmotionAndCategoriesModel;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class SelectYourEmotionAdapter extends RecyclerView.Adapter<SelectYourEmotionAdapter.MyViewHolder> {

    Activity activity;
    ArrayList<EmotionAndCategoriesModel.EmotionList> emotionLists;

    public SelectYourEmotionAdapter(Activity activity, ArrayList<EmotionAndCategoriesModel.EmotionList> emotionLists) {
        this.activity = activity;
        this.emotionLists = emotionLists;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_select_your_emotion, parent, false);

        MyViewHolder viewHolder = new MyViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, int position) {

        //TODO SERVICE
        holder.tvTitle.setText(emotionLists.get(position).getEmotionName());
        Glide.with(activity)
                .load(emotionLists.get(position).getEmotionThumbnail())
                .into(holder.img);


       /* Glide
                .with(activity)                // <--this being the current activity I'm trying to show the gif in.
                .load(emotionLists.get(position).getEmotionThumbnail()).listener(new RequestListener<String, GlideDrawable>() {
            @Override
            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                holder.progress.setVisibility(View.GONE);

                return false;
            }

            @Override
            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                holder.progress.setVisibility(View.GONE);

                return false;
            }
        })
                .fitCenter()
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .into(holder.img);*/


    }

    @Override
    public int getItemCount() {
        //TODO service
        return emotionLists.size();

        //TODO UI
        // return 10;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        CardView card;
        TextView tvTitle;
        CircleImageView img;
        ProgressBar progress;

        public MyViewHolder(View itemView) {
            super(itemView);
            card = itemView.findViewById(R.id.card);
            tvTitle = itemView.findViewById(R.id.tvTitle);
            img = itemView.findViewById(R.id.img);
            progress = itemView.findViewById(R.id.progress);


        }
    }

}
