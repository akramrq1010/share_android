package com.sharemyyouremotion.adapter;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.sharemyyouremotion.R;
import com.sharemyyouremotion.activity.MyQuestionFeedbackActivity;
import com.sharemyyouremotion.activity.WebViewForVideoActivity;
import com.sharemyyouremotion.fragment.MyQuestionFragment;
import com.sharemyyouremotion.helper.DialogClass;
import com.sharemyyouremotion.helper.TimeConveter;
import com.sharemyyouremotion.model.MyExperienceModel;
import com.sharemyyouremotion.model.MyQuestionModel;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;


public class MyExperienceAdapter extends RecyclerView.Adapter<MyExperienceAdapter.MyViewHolder> {

    Activity activity;
    ArrayList<MyExperienceModel> experienceModels;
    int pos = -1;
    DialogClass dialogClass = new DialogClass();

    public MyExperienceAdapter(Activity activity, ArrayList<MyExperienceModel> experienceModels) {
        this.activity = activity;
        this.experienceModels = experienceModels;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_my_question, parent, false);

        MyViewHolder myViewHolder = new MyViewHolder(view);


        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {

        if (experienceModels.get(position).getExperienceType().equalsIgnoreCase("text")) {
            holder.tvText.setVisibility(View.VISIBLE);
            holder.relativeVideo.setVisibility(View.GONE);
            holder.relativeAudio.setVisibility(View.GONE);

            holder.tvText.setText(experienceModels.get(position).getExperienceText());


        } else if (experienceModels.get(position).getExperienceType().equalsIgnoreCase("audio")) {
            holder.tvText.setVisibility(View.GONE);
            holder.relativeVideo.setVisibility(View.GONE);
            holder.relativeAudio.setVisibility(View.VISIBLE);
            holder.imgPlayAudio.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    dialogClass.audioPlayDialog(activity, experienceModels.get(position).getExperienceAudio(), false);

                }
            });

        } else {
            holder.tvText.setVisibility(View.GONE);
            holder.relativeVideo.setVisibility(View.VISIBLE);
            holder.relativeAudio.setVisibility(View.GONE);

            holder.imgPlayVideo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ActivityOptions options1 =
                            ActivityOptions.makeCustomAnimation(activity, R.anim.slide_to_right, R.anim.slide_from_left);
                    Intent intent = new Intent(activity, WebViewForVideoActivity.class);
                    intent.putExtra("video", experienceModels.get(position).getExperienceVideo());
                    //  intent.putExtra("QuestionId",myQuestionFragments.get(position).getQuestionId());
                    activity.startActivity(intent, options1.toBundle());
                }
            });
        }

        //common item for all types text, audio and video
        holder.tvEmotionType.setText(experienceModels.get(position).getEmotionName() + "/" + experienceModels
                .get(position).getCategoryName() + "/" + experienceModels.get(position).getEmotionName());

        holder.tvFeedbacksCount.setText("" + experienceModels.get(position).getEmotionId());
        String date[] = experienceModels.get(position).getCreatedOn().split("T");
     //   holder.tvCreatedDate.setText(date[0]);

        String time = date[1].substring(0, 8);

        String finalData = TimeConveter.getdatetime(date[0], time);
        String dateFinal[] = finalData.split("T");
        String timeFinal = dateFinal[1].substring(0,5);
        holder.tvCreatedDate.setText(timeFinal + " " + dateFinal[0]);

        holder.relativeFeedback.setVisibility(View.GONE);

        Glide.with(activity)
                .load(experienceModels.get(position).getUserThumbnail())
                .into(holder.imgProfile);

    }

    @Override
    public int getItemCount() {
        return experienceModels.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout relativeAudio, relativeVideo, relativeFeedback;
        TextView tvText, tvFeedbacksCount, tvEmotionType, tvCreatedDate;
        ImageView imgPlayAudio, imgPlayVideo;
        CardView cardMain;
        CircleImageView imgProfile;

        public MyViewHolder(View itemView) {
            super(itemView);
            relativeAudio = itemView.findViewById(R.id.relativeAudio);
            tvText = itemView.findViewById(R.id.tvText);
            tvFeedbacksCount = itemView.findViewById(R.id.tvFeedbacksCount);
            tvCreatedDate = itemView.findViewById(R.id.tvCreatedDate);
            tvEmotionType = itemView.findViewById(R.id.tvEmotionType);
            relativeVideo = itemView.findViewById(R.id.relativeVideo);

            imgPlayAudio = itemView.findViewById(R.id.imgPlayAudio);
            imgPlayVideo = itemView.findViewById(R.id.imgPlayVideo);
            cardMain = itemView.findViewById(R.id.cardMain);
            relativeFeedback = itemView.findViewById(R.id.relativeFeedback);
            imgProfile = itemView.findViewById(R.id.imgProfile);

        }
    }


}
