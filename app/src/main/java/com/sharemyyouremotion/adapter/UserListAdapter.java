package com.sharemyyouremotion.adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.sharemyyouremotion.R;
import com.sharemyyouremotion.fragment.EmotionBaseFragment;
import com.sharemyyouremotion.model.UserListModel;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class UserListAdapter extends RecyclerView.Adapter<UserListAdapter.MyViewHolder> {

    EmotionBaseFragment.FragmentInteractionListener interactionListener;
    ArrayList<UserListModel> userCheckBoxModel;
    Activity activity;

    public UserListAdapter(EmotionBaseFragment.FragmentInteractionListener interactionListener, ArrayList<UserListModel> userCheckBoxModel, Activity activity) {
        this.interactionListener = interactionListener;
        this.activity = activity;
        if (this.userCheckBoxModel != null) {
            this.userCheckBoxModel.clear();
        }

        this.userCheckBoxModel = userCheckBoxModel;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_user_list, parent, false);

        MyViewHolder myViewHolder = new MyViewHolder(view);


        myViewHolder.cardMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        final int pos = position;

        Log.v("akram", "vlae " + userCheckBoxModel.get(position).getChecked());


        holder.checkBox.setChecked(userCheckBoxModel.get(position).getChecked());
        holder.checkBox.setTag(userCheckBoxModel.get(position));
        holder.checkBox.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                CheckBox cb = (CheckBox) v;
                UserListModel contact = (UserListModel) cb.getTag();

                contact.setChecked(cb.isChecked());
                userCheckBoxModel.get(pos).setChecked(cb.isChecked());
            }
        });

        holder.tvName.setText(userCheckBoxModel.get(position).getName());
        holder.tvSPLevel.setText(userCheckBoxModel.get(position).getsPLevel());
        holder.ratingBar.setRating(userCheckBoxModel.get(position).getRateCount());

        Glide.with(activity)
                .load(userCheckBoxModel.get(position).getProfileThumbnail())
                .into(holder.imgProfile);
        //TODO service
        /*holder.ratingBar.setRating(userCheckBoxModel.get(position).getRateCount());



        holder.tvSPLevel.setText(userCheckBoxModel.get(position).getsPLevel());*/
    }

    @Override
    public int getItemCount() {
        return userCheckBoxModel.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        CardView cardMain;
        CheckBox checkBox;
        TextView tvName, tvSPLevel;
        CircleImageView imgProfile;
        RatingBar ratingBar;


        public MyViewHolder(View itemView) {
            super(itemView);

            cardMain = itemView.findViewById(R.id.cardMain);
            checkBox = itemView.findViewById(R.id.checkBox);
            tvName = itemView.findViewById(R.id.tvName);
            tvSPLevel = itemView.findViewById(R.id.tvSPLevel);

            ratingBar = itemView.findViewById(R.id.ratingBar);
            imgProfile = itemView.findViewById(R.id.imgProfile);
        }
    }

    public List<UserListModel> getUserList() {
        return userCheckBoxModel;
    }

}
