package com.sharemyyouremotion.adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.sharemyyouremotion.R;
import com.sharemyyouremotion.model.EmotionAndCategoriesModel;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class EmotionCategoryAdapter extends RecyclerView.Adapter<EmotionCategoryAdapter.MyViewHolder> {

    Activity activity;
    ArrayList<EmotionAndCategoriesModel.CategoryList> categoryLists;

    public EmotionCategoryAdapter(Activity activity, ArrayList<EmotionAndCategoriesModel.CategoryList> categoryLists) {
        this.activity = activity;
        this.categoryLists = categoryLists;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_emotion_category, parent, false);

        MyViewHolder viewHolder = new MyViewHolder(view);


        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        //TODO SERVICE
        holder.tvCategory.setText(categoryLists.get(position).getCategoryName());
        Glide.with(activity)
                .load(categoryLists.get(position).getCategoryThumbnail())
                .into(holder.img);
    }

    @Override
    public int getItemCount() {
        //TODO service
        return categoryLists.size();
        //TODO UI
      //  return 10;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvCategory;
        CircleImageView img;

        public MyViewHolder(View itemView) {
            super(itemView);
            tvCategory = itemView.findViewById(R.id.tvCategory);
            img = itemView.findViewById(R.id.img);

        }
    }

}
