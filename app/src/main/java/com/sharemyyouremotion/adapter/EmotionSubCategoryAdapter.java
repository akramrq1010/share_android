package com.sharemyyouremotion.adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.sharemyyouremotion.R;
import com.sharemyyouremotion.model.EmotionAndCategoriesModel;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class EmotionSubCategoryAdapter extends RecyclerView.Adapter<EmotionSubCategoryAdapter.MyViewHolder> {

    Activity activity;
    ArrayList<EmotionAndCategoriesModel.CategoryList.SubCategories> subCategories;

    public EmotionSubCategoryAdapter(Activity activity, ArrayList<EmotionAndCategoriesModel.CategoryList.SubCategories> subCategories) {
        this.activity = activity;
        this.subCategories = subCategories;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_emotion_category, parent, false);

        MyViewHolder viewHolder = new MyViewHolder(view);


        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        //TODO service
        holder.tvCategory.setText(subCategories.get(position).getSubCategoryName());
        Glide.with(activity)
                .load(subCategories.get(position).getSubCategoryThumbnail())
                .into(holder.img);
    }

    @Override
    public int getItemCount() {
        //TODO service
        return subCategories.size();
        //TODO UI
        // return 10;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvCategory;
        CircleImageView img;

        public MyViewHolder(View itemView) {
            super(itemView);
            tvCategory = itemView.findViewById(R.id.tvCategory);
            img = itemView.findViewById(R.id.img);

        }
    }

}
