package com.sharemyyouremotion.adapter;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.sharemyyouremotion.R;
import com.sharemyyouremotion.activity.MyQuestionFeedbackActivity;
import com.sharemyyouremotion.activity.WebViewForVideoActivity;
import com.sharemyyouremotion.helper.DialogClass;
import com.sharemyyouremotion.helper.TimeConveter;
import com.sharemyyouremotion.model.MyFeedbackModel;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class MyFeedbackAdapter extends RecyclerView.Adapter<MyFeedbackAdapter.MyViewHolder> {

    ArrayList<MyFeedbackModel> myQuestionFragments;
    Activity activity;
    DialogClass dialogClass = new DialogClass();

    public MyFeedbackAdapter(Activity activity, ArrayList<MyFeedbackModel> myQuestionFragments) {
        this.myQuestionFragments = myQuestionFragments;
        this.activity = activity;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_my_question, parent, false);

        MyViewHolder myViewHolder = new MyViewHolder(view);


        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        if (myQuestionFragments.get(position).getFeedbackType().equalsIgnoreCase("text")) {
            holder.tvText.setVisibility(View.VISIBLE);
            holder.relativeVideo.setVisibility(View.GONE);
            holder.relativeAudio.setVisibility(View.GONE);

            holder.tvText.setText(myQuestionFragments.get(position).getFeedbackText());


        } else if (myQuestionFragments.get(position).getFeedbackType().equalsIgnoreCase("audio")) {
            holder.tvText.setVisibility(View.GONE);
            holder.relativeVideo.setVisibility(View.GONE);
            holder.relativeAudio.setVisibility(View.VISIBLE);
            holder.imgPlayAudio.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    dialogClass.audioPlayDialog(activity, myQuestionFragments.get(position).getFeedbackAudio(), false);

                }
            });

        } else {
            holder.tvText.setVisibility(View.GONE);
            holder.relativeVideo.setVisibility(View.VISIBLE);
            holder.relativeAudio.setVisibility(View.GONE);

            holder.imgPlayVideo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ActivityOptions options1 =
                            ActivityOptions.makeCustomAnimation(activity, R.anim.slide_to_right, R.anim.slide_from_left);
                    Intent intent = new Intent(activity, WebViewForVideoActivity.class);
                    intent.putExtra("video", myQuestionFragments.get(position).getFeedbackVideo());
                    //  intent.putExtra("QuestionId",myQuestionFragments.get(position).getQuestionId());
                    activity.startActivity(intent, options1.toBundle());
                }
            });
        }

        //common item for all types text, audio and video
        holder.tvEmotionType.setText("Happy/Collage");

        holder.tvFeedbacksCount.setText("12");
        String date[] = myQuestionFragments.get(position).getCreatedOn().split("T");
       // holder.tvCreatedDate.setText(date[0]);

        String time = date[1].substring(0, 8);

        String finalData = TimeConveter.getdatetime(date[0], time);
        String dateFinal[] = finalData.split("T");
        String timeFinal = dateFinal[1].substring(0,5);
        holder.tvCreatedDate.setText(timeFinal + " " + dateFinal[0]);

        holder.cardMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ActivityOptions options1 =
                        ActivityOptions.makeCustomAnimation(activity, R.anim.slide_to_right, R.anim.slide_from_left);
                Intent intent = new Intent(activity, MyQuestionFeedbackActivity.class);
                intent.putExtra("QuestionId", myQuestionFragments.get(position).getQuestionId());
                activity.startActivity(intent, options1.toBundle());
            }
        });

        Glide.with(activity)
                .load(myQuestionFragments.get(position).getUserThumbnail())
                .into(holder.imgProfile);

        holder.relativeFeedback.setVisibility(View.GONE);
    }

    @Override
    public int getItemCount() {
        return myQuestionFragments.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout relativeAudio, relativeVideo, relativeFeedback;
        TextView tvText, tvFeedbacksCount, tvEmotionType, tvCreatedDate;
        ImageView imgPlayAudio, imgPlayVideo;
        CardView cardMain;
        CircleImageView imgProfile;

        public MyViewHolder(View itemView) {
            super(itemView);
            relativeAudio = itemView.findViewById(R.id.relativeAudio);
            tvText = itemView.findViewById(R.id.tvText);
            tvFeedbacksCount = itemView.findViewById(R.id.tvFeedbacksCount);
            tvCreatedDate = itemView.findViewById(R.id.tvCreatedDate);
            tvEmotionType = itemView.findViewById(R.id.tvEmotionType);
            relativeVideo = itemView.findViewById(R.id.relativeVideo);

            imgPlayAudio = itemView.findViewById(R.id.imgPlayAudio);
            imgPlayVideo = itemView.findViewById(R.id.imgPlayVideo);
            cardMain = itemView.findViewById(R.id.cardMain);
            imgProfile = itemView.findViewById(R.id.imgProfile);
            relativeFeedback = itemView.findViewById(R.id.relativeFeedback);
        }
    }

}
