package com.sharemyyouremotion.adapter;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.sharemyyouremotion.R;
import com.sharemyyouremotion.activity.AudioTestActivity;
import com.sharemyyouremotion.activity.MyQuestionFeedbackActivity;
import com.sharemyyouremotion.activity.WebViewForVideoActivity;
import com.sharemyyouremotion.asynctask.LoadingSongAsynctask;
import com.sharemyyouremotion.fragment.LoginFragment;
import com.sharemyyouremotion.fragment.MyQuestionFragment;
import com.sharemyyouremotion.helper.DialogClass;
import com.sharemyyouremotion.helper.TimeConveter;
import com.sharemyyouremotion.model.MyQuestionModel;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;


public class MyQuestionAdapter extends RecyclerView.Adapter<MyQuestionAdapter.MyViewHolder> {

    Activity activity;
    ArrayList<MyQuestionModel> myQuestionFragments;
    DialogClass dialogClass = new DialogClass();
    String user;

    public MyQuestionAdapter(Activity activity, ArrayList<MyQuestionModel> myQuestionFragments, String user) {
        this.activity = activity;
        this.myQuestionFragments = myQuestionFragments;
        this.user = user;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        MyViewHolder myViewHolder;
        View view;
        if (user.equalsIgnoreCase("my")) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_my_question, parent, false);
        } else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_my_feedback_other, parent, false);
        }
        myViewHolder = new MyViewHolder(view);


        return myViewHolder;
    }


    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {

        if (myQuestionFragments.get(position).getQuestionType().equalsIgnoreCase("text")) {
            holder.tvText.setVisibility(View.VISIBLE);
            holder.relativeVideo.setVisibility(View.GONE);
            holder.relativeAudio.setVisibility(View.GONE);

            holder.tvText.setText(myQuestionFragments.get(position).getQuestionText());


        } else if (myQuestionFragments.get(position).getQuestionType().equalsIgnoreCase("audio")) {
            holder.tvText.setVisibility(View.GONE);
            holder.relativeVideo.setVisibility(View.GONE);
            holder.relativeAudio.setVisibility(View.VISIBLE);
            holder.imgPlayAudio.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    dialogClass.audioPlayDialog(activity, myQuestionFragments.get(position).getQuestionAudio(), false);

                }
            });

        } else {
            holder.tvText.setVisibility(View.GONE);
            holder.relativeVideo.setVisibility(View.VISIBLE);
            holder.relativeAudio.setVisibility(View.GONE);

            holder.imgPlayVideo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ActivityOptions options1 =
                            ActivityOptions.makeCustomAnimation(activity, R.anim.slide_to_right, R.anim.slide_from_left);
                    Intent intent = new Intent(activity, WebViewForVideoActivity.class);
                    intent.putExtra("video", myQuestionFragments.get(position).getQuestionVideo());
                    //  intent.putExtra("QuestionId",myQuestionFragments.get(position).getQuestionId());
                    activity.startActivity(intent, options1.toBundle());
                }
            });
        }

        //common item for all types text, audio and video

        holder.tvFeedbacksCount.setText("" + myQuestionFragments.get(position).getEmotionId());
        String date[] = myQuestionFragments.get(position).getCreatedOn().split("T");
        String time = date[1].substring(0, 8);

        String finalData = TimeConveter.getdatetime(date[0], time);
        String dateFinal[] = finalData.split("T");
        String timeFinal = dateFinal[1].substring(0,5);
        holder.tvCreatedDate.setText(timeFinal + " " + dateFinal[0]);

        holder.cardMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (LoginFragment.guest.equalsIgnoreCase("guest")) {
                    return;
                }

                if (user.equalsIgnoreCase("my"))
                    MyQuestionFragment.screenValue = 1;
                else
                    MyQuestionFragment.screenValue = 2;

                ActivityOptions options1 =
                        ActivityOptions.makeCustomAnimation(activity, R.anim.slide_to_right, R.anim.slide_from_left);
                Intent intent = new Intent(activity, MyQuestionFeedbackActivity.class);
                intent.putExtra("QuestionId", myQuestionFragments.get(position).getQuestionId());
                activity.startActivity(intent, options1.toBundle());
            }
        });

        if (user.equalsIgnoreCase("other")) {
            holder.tvName.setVisibility(View.VISIBLE);
            if (myQuestionFragments.get(position).getIdentityShare()) {

                holder.tvName.setText(myQuestionFragments.get(position).getName() + " is ");
                Glide.with(activity)
                        .load(myQuestionFragments.get(position).getUserThumbnail())
                        .into(holder.imgProfile);
            } else {
                holder.tvName.setText(myQuestionFragments.get(position).getName() + " is ");
              //  holder.tvName.setText("User" + myQuestionFragments.get(position).getAskBy() + " is ");
                Glide.with(activity)
                        .load("")
                        .into(holder.imgProfile);
            }

            holder.tvEmotionType.setText(myQuestionFragments.get(position).getEmotionName());
            int emotionBac = myQuestionFragments.get(position).getEmotionId() % 10;
            holder.tvEmotionType.setBackgroundColor(MyQuestionFeedbackActivity.color[emotionBac]);


        } else {
            holder.tvEmotionType.setText(myQuestionFragments.get(position).getEmotionName() + "/" + myQuestionFragments
                    .get(position).getCategoryName() + "/" + myQuestionFragments.get(position).getSubCategoryName());

            // holder.tvName.setText(myQuestionFragments.get(position).getName());
            Glide.with(activity)
                    .load(myQuestionFragments.get(position).getUserThumbnail())
                    .into(holder.imgProfile);
        }
        if (myQuestionFragments.get(position).getFeedbackCount() > 1)
            holder.tvFeedbacksCount.setText(myQuestionFragments.get(position).getFeedbackCount() + " Feedbacks");
        else
            holder.tvFeedbacksCount.setText(myQuestionFragments.get(position).getFeedbackCount() + " Feedback");
    }

    @Override
    public int getItemCount() {
        return myQuestionFragments.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout relativeAudio, relativeVideo;
        TextView tvText, tvFeedbacksCount, tvEmotionType, tvCreatedDate, tvName;
        ImageView imgPlayAudio, imgPlayVideo;
        CardView cardMain;
        CircleImageView imgProfile;

        public MyViewHolder(View itemView) {
            super(itemView);
            relativeAudio = itemView.findViewById(R.id.relativeAudio);
            tvText = itemView.findViewById(R.id.tvText);
            tvFeedbacksCount = itemView.findViewById(R.id.tvFeedbacksCount);
            tvCreatedDate = itemView.findViewById(R.id.tvCreatedDate);
            tvEmotionType = itemView.findViewById(R.id.tvEmotionType);
            relativeVideo = itemView.findViewById(R.id.relativeVideo);

            imgPlayAudio = itemView.findViewById(R.id.imgPlayAudio);
            imgPlayVideo = itemView.findViewById(R.id.imgPlayVideo);
            cardMain = itemView.findViewById(R.id.cardMain);
            imgProfile = itemView.findViewById(R.id.imgProfile);
            tvName = itemView.findViewById(R.id.tvName);

        }
    }


}
