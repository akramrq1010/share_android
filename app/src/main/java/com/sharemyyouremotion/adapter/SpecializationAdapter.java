package com.sharemyyouremotion.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.RadioButton;
import android.widget.TextView;


import com.sharemyyouremotion.R;
import com.sharemyyouremotion.model.SpecializationModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by RM on 8/9/17.
 */

public class SpecializationAdapter extends RecyclerView.Adapter<SpecializationAdapter.MyViewHolder> implements Filterable {

    Activity activity;
    ArrayList<SpecializationModel> specializationModelsFiltered;
    ArrayList<SpecializationModel> specializationModels;

    public SpecializationAdapter(Activity activity, ArrayList<SpecializationModel> specializationModels) {
        this.activity = activity;
        this.specializationModels = specializationModels;
        this.specializationModelsFiltered = specializationModels;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvSpecialization;
        RadioButton radioPro, radioHigh, radioMedium, radioNormal;

        public MyViewHolder(View view) {
            super(view);

            tvSpecialization = view.findViewById(R.id.tvSpecialization);
            radioPro = view.findViewById(R.id.radioPro);
            radioHigh = view.findViewById(R.id.radioHigh);
            radioMedium = view.findViewById(R.id.radioMedium);
            radioNormal = view.findViewById(R.id.radioNormal);

        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_specialization, parent, false);

        return new MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        holder.tvSpecialization.setText(specializationModelsFiltered.get(position).getCategoryName());

        holder.radioPro.setTag(position);
        holder.radioHigh.setTag(position);
        holder.radioMedium.setTag(position);
        holder.radioNormal.setTag(position);

        holder.radioPro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int pos = (Integer) view.getTag();

                Log.v("akram", "click " + pos);

                specializationModelsFiltered.get(pos).setRadioButtonPosition("pro");

                holder.radioPro.setChecked(true);
                holder.radioHigh.setChecked(false);
                holder.radioMedium.setChecked(false);
                holder.radioNormal.setChecked(false);

            }
        });

        holder.radioHigh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int pos = (Integer) view.getTag();
                specializationModelsFiltered.get(pos).setRadioButtonPosition("high");
                holder.radioPro.setChecked(false);
                holder.radioHigh.setChecked(true);
                holder.radioMedium.setChecked(false);
                holder.radioNormal.setChecked(false);

            }
        });

        holder.radioMedium.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int pos = (Integer) view.getTag();
                specializationModelsFiltered.get(pos).setRadioButtonPosition("medium");
                holder.radioPro.setChecked(false);
                holder.radioHigh.setChecked(false);
                holder.radioMedium.setChecked(true);
                holder.radioNormal.setChecked(false);

            }
        });

        holder.radioNormal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int pos = (Integer) view.getTag();
                specializationModelsFiltered.get(pos).setRadioButtonPosition("normal");
                holder.radioPro.setChecked(false);
                holder.radioHigh.setChecked(false);
                holder.radioMedium.setChecked(false);
                holder.radioNormal.setChecked(true);

            }
        });

        Log.v("akram", "click  outside " + specializationModelsFiltered.get(position).getRadioButtonPosition());
        if (specializationModelsFiltered.get(position).getRadioButtonPosition() != null) {
            if (specializationModelsFiltered.get(position).getRadioButtonPosition().equalsIgnoreCase("Pro")) {
                holder.radioPro.setChecked(true);
                holder.radioHigh.setChecked(false);
                holder.radioMedium.setChecked(false);
                holder.radioNormal.setChecked(false);
            } else if (specializationModelsFiltered.get(position).getRadioButtonPosition().equalsIgnoreCase("High")) {
                holder.radioPro.setChecked(false);
                holder.radioHigh.setChecked(true);
                holder.radioMedium.setChecked(false);
                holder.radioNormal.setChecked(false);
            } else if (specializationModelsFiltered.get(position).getRadioButtonPosition().equalsIgnoreCase("Medium")) {
                holder.radioPro.setChecked(false);
                holder.radioHigh.setChecked(false);
                holder.radioMedium.setChecked(true);
                holder.radioNormal.setChecked(false);
            } else if (specializationModelsFiltered.get(position).getRadioButtonPosition().equalsIgnoreCase("Normal")) {
                holder.radioPro.setChecked(false);
                holder.radioHigh.setChecked(false);
                holder.radioMedium.setChecked(false);
                holder.radioNormal.setChecked(true);
            } else {
                holder.radioPro.setChecked(false);
                holder.radioHigh.setChecked(false);
                holder.radioMedium.setChecked(false);
                holder.radioNormal.setChecked(false);
            }
        } else {
            holder.radioPro.setChecked(false);
            holder.radioHigh.setChecked(false);
            holder.radioMedium.setChecked(false);
            holder.radioNormal.setChecked(false);
        }

        /*if(specializationModelsFiltered.get(position).getRadioButtonPosition()!=-1){
            ((RadioButton)holder.radioGroup.getChildAt(specializationModelsFiltered.get(position).getRadioButtonPosition())).setChecked(true);
        }
*/
        //   ((RadioButton)holder.radioGroup.getChildAt(-1)).setChecked(true);
        // holder.radioGroup.set;
    }

    @Override
    public int getItemCount() {
        return specializationModelsFiltered.size();
    }


    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    specializationModelsFiltered = specializationModels;
                } else {
                    ArrayList<SpecializationModel> filteredList = new ArrayList<>();
                    for (SpecializationModel row : specializationModels) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getCategoryName().toLowerCase().contains(charString.toLowerCase())/* || row.getPhone().contains(charSequence)*/) {
                            filteredList.add(row);
                        }
                    }

                    specializationModelsFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = specializationModelsFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                specializationModelsFiltered = (ArrayList<SpecializationModel>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public List<SpecializationModel> getSpecialization() {
        return specializationModelsFiltered;
    }
}
