package com.sharemyyouremotion.adapter;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.sharemyyouremotion.R;
import com.sharemyyouremotion.activity.MyQuestionFeedbackActivity;
import com.sharemyyouremotion.activity.WebViewForVideoActivity;
import com.sharemyyouremotion.fragment.MyQuestionFragment;
import com.sharemyyouremotion.helper.DialogClass;
import com.sharemyyouremotion.model.MyExperienceModel;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class ExperienceOtherUserAdapter extends RecyclerView.Adapter<ExperienceOtherUserAdapter.MyViewHolder> {

    int mediaValue;
    ArrayList<MyExperienceModel> myExperienceModels;
    Activity activity;
    DialogClass dialogClass = new DialogClass();


    public ExperienceOtherUserAdapter(int mediaValue, ArrayList<MyExperienceModel> myExperienceModels, Activity activity) {
        this.mediaValue = mediaValue;
        this.myExperienceModels = myExperienceModels;
        this.activity = activity;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_my_feedback_other, parent, false);

        MyViewHolder myViewHolder = new MyViewHolder(view);


        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {

        if (myExperienceModels.get(position).getExperienceType().equalsIgnoreCase("text")) {
            holder.tvText.setVisibility(View.VISIBLE);
            holder.relativeVideo.setVisibility(View.GONE);
            holder.relativeAudio.setVisibility(View.GONE);

            holder.tvText.setText(myExperienceModels.get(position).getExperienceText());


        } else if (myExperienceModels.get(position).getExperienceType().equalsIgnoreCase("audio")) {
            holder.tvText.setVisibility(View.GONE);
            holder.relativeVideo.setVisibility(View.GONE);
            holder.relativeAudio.setVisibility(View.VISIBLE);
            holder.imgPlayAudio.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    dialogClass.audioPlayDialog(activity, myExperienceModels.get(position).getExperienceAudio(), false);

                }
            });

        } else {
            holder.tvText.setVisibility(View.GONE);
            holder.relativeVideo.setVisibility(View.VISIBLE);
            holder.relativeAudio.setVisibility(View.GONE);

            holder.imgPlayVideo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ActivityOptions options1 =
                            ActivityOptions.makeCustomAnimation(activity, R.anim.slide_to_right, R.anim.slide_from_left);
                    Intent intent = new Intent(activity, WebViewForVideoActivity.class);
                    intent.putExtra("video", myExperienceModels.get(position).getExperienceVideo());
                    //  intent.putExtra("QuestionId",myQuestionFragments.get(position).getQuestionId());
                    activity.startActivity(intent, options1.toBundle());
                }
            });
        }

        //common item for all types text, audio and video
        holder.tvEmotionType.setText(myExperienceModels.get(position).getEmotionName());
        int emotionBac = myExperienceModels.get(position).getEmotionId() % 10;
        holder.tvEmotionType.setBackgroundColor(MyQuestionFeedbackActivity.color[emotionBac]);

        holder.relativeFeedback.setVisibility(View.GONE);
        String date[] = myExperienceModels.get(position).getCreatedOn().split("T");
        holder.tvCreatedDate.setText(date[0]);


        if (myExperienceModels.get(position).getIdentityShare()) {
            holder.tvName.setVisibility(View.VISIBLE);
            holder.tvName.setText(myExperienceModels.get(position).getName() + " is ");
            Log.v("akram", "pivc " + myExperienceModels.get(position).getUserThumbnail());
            Glide.with(activity)
                    .load(myExperienceModels.get(position).getUserThumbnail())
                    .into(holder.imgProfile);
        } else {

           // holder.tvName.setText("User" + myExperienceModels.get(position).getShareBy() + " is ");
            holder.tvName.setText(myExperienceModels.get(position).getName() + " is ");
            if (myExperienceModels.get(position).getGender().equalsIgnoreCase("female")) {
                Glide.with(activity)
                        .load(getImage("girl_user"))
                        .into(holder.imgProfile);
            } else {
                Glide.with(activity)
                        .load("")
                        .into(holder.imgProfile);
            }

        }
    }


    public int getImage(String imageName) {

        int drawableResourceId = activity.getResources().getIdentifier(imageName, "drawable", activity.getPackageName());

        return drawableResourceId;
    }
    @Override
    public int getItemCount() {
        return myExperienceModels.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        RelativeLayout relativeAudio, relativeVideo, relativeFeedback;
        TextView tvText, tvFeedbacksCount, tvEmotionType, tvCreatedDate, tvName;
        ImageView imgPlayAudio, imgPlayVideo;
        CardView cardMain;
        CircleImageView imgProfile;

        public MyViewHolder(View itemView) {
            super(itemView);

            relativeAudio = itemView.findViewById(R.id.relativeAudio);
            tvText = itemView.findViewById(R.id.tvText);
            tvFeedbacksCount = itemView.findViewById(R.id.tvFeedbacksCount);
            tvCreatedDate = itemView.findViewById(R.id.tvCreatedDate);
            tvEmotionType = itemView.findViewById(R.id.tvEmotionType);
            relativeVideo = itemView.findViewById(R.id.relativeVideo);

            imgPlayAudio = itemView.findViewById(R.id.imgPlayAudio);
            imgPlayVideo = itemView.findViewById(R.id.imgPlayVideo);
            cardMain = itemView.findViewById(R.id.cardMain);
            relativeFeedback = itemView.findViewById(R.id.relativeFeedback);
            imgProfile = itemView.findViewById(R.id.imgProfile);
            tvName = itemView.findViewById(R.id.tvName);
        }
    }

}
