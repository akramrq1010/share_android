package com.sharemyyouremotion.fragment;

import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.sharemyyouremotion.R;
import com.sharemyyouremotion.activity.SelectYourEmotionBaseActivity;
import com.sharemyyouremotion.adapter.EmotionCategoryAdapter;
import com.sharemyyouremotion.helper.ClickListener;
import com.sharemyyouremotion.helper.Constants;
import com.sharemyyouremotion.helper.RecyclerTouchListener;
import com.sharemyyouremotion.model.EmotionAndCategoriesModel;

import java.util.ArrayList;

import static com.sharemyyouremotion.activity.SelectYourEmotionBaseActivity.imgHome;
import static com.sharemyyouremotion.activity.SelectYourEmotionBaseActivity.toolbar;


public class EmotionCategoryFragment extends EmotionBaseFragment implements View.OnClickListener {
    RecyclerView recyclerView;

    TextView tvEmotion;

    public EmotionCategoryFragment() {
        // Required empty public constructor
    }

    public static EmotionCategoryFragment newInstance() {
        return new EmotionCategoryFragment();
    }

    @Override
    public FragmentId getFragmentId() {
        return FragmentId.EMOTION_CATEGORY_FRAGMENT;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        root = (ViewGroup) inflater.inflate(R.layout.fragment_emotion_category, container, false);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getActivity().getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.notificationBar));
        }
        getData();
        toolbar.setText("Category");
        initView();


        return root;
    }

    private void initView() {
        recyclerView = root.findViewById(R.id.recyclerView);
        tvEmotion = root.findViewById(R.id.tvEmotion);

        tvEmotion.setText(Constants.emotionStr);

        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(activity, 2);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        EmotionCategoryAdapter emotionCategoryAdapter = new EmotionCategoryAdapter(activity, Constants.categoryLists);
        recyclerView.setAdapter(emotionCategoryAdapter);

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(activity, recyclerView, new ClickListener() {
            @Override
            public void onClick(View view, int position) {

                Constants.categoryId = Constants.categoryLists.get(position).getCategoryId();
                Constants.categoryStr = Constants.categoryLists.get(position).getCategoryName();

                if (activity.getIntent().getStringExtra("screen").equalsIgnoreCase("diary"))
                    interactionListener.launchMyDiaryAddFragment();
                else
                    interactionListener.launchEmotionSubCategoryFragment(Constants.categoryLists, position);
            }

            @Override
            public void onLongClick(View view, int position) {
                //  popup();
            }
        }));

        imgHome.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgHome:
                activity.finish();
                break;
        }
    }

    public void getData() {
        Bundle bundle = activity.getIntent().getExtras();
        //categoryLists = (ArrayList<EmotionAndCategoriesModel.CategoryList>) bundle.getSerializable("category");
    }


    @Override
    public void onResume() {
        super.onResume();

        SelectYourEmotionBaseActivity.imgHome.setVisibility(View.VISIBLE);
    }

    @Override
    public void onPause() {
        super.onPause();
        SelectYourEmotionBaseActivity.imgHome.setVisibility(View.GONE);
    }

}
