package com.sharemyyouremotion.fragment;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import com.quickblox.chat.QBRestChatService;
import com.quickblox.chat.model.QBChatDialog;
import com.quickblox.chat.model.QBDialogCustomData;
import com.quickblox.chat.model.QBDialogType;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.sharemyyouremotion.R;
import com.sharemyyouremotion.activity.HomeActivity;
import com.sharemyyouremotion.activity.SelectYourEmotionBaseActivity;
import com.sharemyyouremotion.activity.SpecializationActivity;
import com.sharemyyouremotion.connection.BaseApiService;
import com.sharemyyouremotion.connection.Connection;
import com.sharemyyouremotion.helper.Constants;
import com.sharemyyouremotion.helper.MyBounceInterpolator;
import com.sharemyyouremotion.helper.StaticSharedpreference;
import com.sharemyyouremotion.helper.Utilities;
import com.sharemyyouremotion.model.EmotionAndCategoriesModel;
import com.sharemyyouremotion.model.SpecializationModel;
import com.sharemyyouremotion.qucikblox.ChatData.Snippet;
import com.sharemyyouremotion.qucikblox.QuickBloxRequestMethod;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeFragment extends Fragment implements View.OnClickListener {

    View root;
    ImageView imgSend, imgReceive;
    HomeActivity activity;
    public static int isSend = 0;
    LinearLayout linearSend, linearReceive;
    LinearLayout btnExp, btnDiary;
    CardView cardProgress;
    Utilities utilities;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root = (ViewGroup) inflater.inflate(R.layout.fragment_home, container, false);
        activity = (HomeActivity) getActivity();
        initViews();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                final Animation myAnim = AnimationUtils.loadAnimation(activity, R.anim.bounce);
                //imgSend.startAnimation(myAnim);
                MyBounceInterpolator interpolator = new MyBounceInterpolator(0.2, 20);
                myAnim.setInterpolator(interpolator);
                //  linearSend.startAnimation(myAnim);
                //    linearReceive.startAnimation(myAnim);

            }
        }, 300);

        int value = StaticSharedpreference.getInt(Constants.CHECK_HOME, activity);
        if (!LoginFragment.guest.equalsIgnoreCase("guest")) {
            if (value == 0) {
                QuickBloxRequestMethod quickBloxRequestMethod = new QuickBloxRequestMethod();
                quickBloxRequestMethod.createSessionWithUser(activity);
                StaticSharedpreference.saveInt(Constants.CHECK_HOME, 1, activity);
            }
        }
        //chatSetting(59089783);
        return root;
    }

    private void initViews() {

        linearSend = root.findViewById(R.id.linearSend);
        linearReceive = root.findViewById(R.id.linearReceive);
        btnExp = root.findViewById(R.id.btnExp);
        cardProgress = root.findViewById(R.id.cardProgress);
        btnDiary = root.findViewById(R.id.btnDiary);

        linearSend.setOnClickListener(this);
        linearReceive.setOnClickListener(this);
        btnExp.setOnClickListener(this);
        btnDiary.setOnClickListener(this);

        Map requestBody = new HashMap<>();

        requestBody.put("UserId", StaticSharedpreference.getInt(Constants.USER_ID, activity));

        for (int i = 0; i < requestBody.size(); i++) {
            Log.v("aklram", "value " + requestBody.get(i));
        }

        utilities = new Utilities(activity);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.linearSend: {
                isSend = 0;

                if (Constants.emotionLists.size() == 0) {
                    if (utilities.isOnline())
                        gotoNextScreen("feedback");
                } else {
                    gotoNextScreen("feedback");
                }
            }

            break;

            case R.id.linearReceive:
                isSend = 1;
                //TODO service open
            {
                if (Constants.emotionLists.size() == 0) {
                    gotoNextScreen("");
                } else {
                    gotoNextScreen("");
                }
            }

            break;

            case R.id.btnExp:
                isSend = 2;
            {
                if (Constants.emotionLists.size() == 0) {
                    gotoNextScreen("");
                } else {
                    gotoNextScreen("");
                }
            }


            break;


            case R.id.btnDiary:
                gotoNextScreen("diary");
                break;
        }
    }


    public void getEmotionAndCategories() {

        cardProgress.setVisibility(View.VISIBLE);

        BaseApiService apiService = Connection.getBaseApiServiceInstance();
        Call<EmotionAndCategoriesModel> call = apiService.getEmotionAndCategories();
        call.enqueue(new Callback<EmotionAndCategoriesModel>() {
            @Override
            public void onResponse(Call<EmotionAndCategoriesModel> call, Response<EmotionAndCategoriesModel> response) {
//                Log.d("tag", "onResponse" + "" + response.body().toString());

                if (response.body() != null && response.body().getStatusCode() == 200) {

                    cardProgress.setVisibility(View.GONE);

                    /*check specialization fill or not*/

                    Gson gson = new Gson();
                    Type listType = new TypeToken<ArrayList<SpecializationModel>>() {
                    }.getType();

                    Constants.emotionLists = response.body().getEmotionLists();
                    Constants.categoryLists = response.body().getCategoryLists();

                    gotoNextScreen("");

                } else {
                    Toast.makeText(activity, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
                cardProgress.setVisibility(View.GONE);

                /* JsonObject response = new  JsonParser().parse(out.body()).getAsJsonObject();*/

            }

            @Override
            public void onFailure(Call<EmotionAndCategoriesModel> call, Throwable t) {
                Log.v("okhttp", " throw = " + t);

                cardProgress.setVisibility(View.GONE);
            }
        });
    }

    private void gotoNextScreen(String screen) {
        ActivityOptions options1 =
                ActivityOptions.makeCustomAnimation(activity, R.anim.slide_to_right, R.anim.slide_from_left);

        Intent intent = new Intent(activity, SelectYourEmotionBaseActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable("emotion", Constants.emotionLists);
        bundle.putSerializable("category", Constants.categoryLists);

        bundle.putString("screen", screen);

        intent.putExtras(bundle);

        startActivity(intent, options1.toBundle());
    }


}
