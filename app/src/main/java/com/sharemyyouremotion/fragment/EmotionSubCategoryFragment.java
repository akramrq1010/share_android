package com.sharemyyouremotion.fragment;

import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.sharemyyouremotion.R;
import com.sharemyyouremotion.activity.SelectYourEmotionBaseActivity;
import com.sharemyyouremotion.adapter.EmotionCategoryAdapter;
import com.sharemyyouremotion.adapter.EmotionSubCategoryAdapter;
import com.sharemyyouremotion.helper.ClickListener;
import com.sharemyyouremotion.helper.Constants;
import com.sharemyyouremotion.helper.DialogClass;
import com.sharemyyouremotion.helper.RecyclerTouchListener;
import com.sharemyyouremotion.helper.Utilities;
import com.sharemyyouremotion.model.EmotionAndCategoriesModel;

import java.util.ArrayList;

import static com.sharemyyouremotion.activity.SelectYourEmotionBaseActivity.imgHome;
import static com.sharemyyouremotion.activity.SelectYourEmotionBaseActivity.toolbar;
import static com.sharemyyouremotion.fragment.HomeFragment.isSend;


public class EmotionSubCategoryFragment extends EmotionBaseFragment implements View.OnClickListener {
    RecyclerView recyclerView;

    int pos;
    Utilities utilities;
    DialogClass dialogClass;
    TextView tvEmotion;
    TextView tvNoData;

    public EmotionSubCategoryFragment() {
        // Required empty public constructor
    }

    public static EmotionSubCategoryFragment newInstance() {
        return new EmotionSubCategoryFragment();
    }

    @Override
    public FragmentId getFragmentId() {
        return FragmentId.EMOTION_CATEGORY_FRAGMENT;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        root = (ViewGroup) inflater.inflate(R.layout.fragment_emotion_category, container, false);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getActivity().getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.notificationBar));
        }

        //TODO service
        getData();
        toolbar.setText("Sub Category");
        initView();
        if (Constants.categoryLists.get(pos).getSubCategories().size() == 0) {
            tvNoData.setVisibility(View.VISIBLE);
            tvNoData.setText("Do not have subcategory for " + Constants.categoryStr);
        }


        return root;
    }

    private void initView() {
        recyclerView = root.findViewById(R.id.recyclerView);
        tvEmotion = root.findViewById(R.id.tvEmotion);
        tvNoData = root.findViewById(R.id.tvNoData);

        tvEmotion.setText(Constants.emotionStr + "/" + Constants.categoryStr);

        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(activity, 2);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        utilities = new Utilities(activity);
        dialogClass = new DialogClass();

        EmotionSubCategoryAdapter emotionSubCategoryAdapter = new EmotionSubCategoryAdapter(activity, Constants.categoryLists.get(pos).getSubCategories());

        recyclerView.setAdapter(emotionSubCategoryAdapter);

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(activity, recyclerView, new ClickListener() {
            @Override
            public void onClick(View view, int position) {

                Constants.subCategoryId = Constants.categoryLists.get(pos).getSubCategories().get(position).getSubCategoryId();
                Constants.subCategoryStr = Constants.categoryLists.get(pos).getSubCategories().get(position).getSubCategoryName();

                if (isSend == 0) {

                    dialogClass.selectMediaDialog(activity, interactionListener, 0, "send");

                } else if (isSend == 1) {

                    dialogClass.experienceSuggestion(activity, interactionListener, Constants.categoryLists.get(pos).getCategoryId());

                } else {
                    dialogClass.selectMediaDialog(activity, interactionListener, 0, "");
                }
            }

            @Override
            public void onLongClick(View view, int position) {
                //  popup();
            }
        }));
        imgHome.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgHome:
                activity.finish();
                break;
        }
    }

    public void getData() {
        Bundle bundle = activity.getIntent().getExtras();
        Bundle bundle1 = getArguments();
        pos = bundle1.getInt("pos", 0);
       // categoryLists = (ArrayList<EmotionAndCategoriesModel.CategoryList>) bundle.getSerializable("category");
    }

    @Override
    public void onResume() {
        super.onResume();

        SelectYourEmotionBaseActivity.imgHome.setVisibility(View.VISIBLE);
    }

    @Override
    public void onPause() {
        super.onPause();
        SelectYourEmotionBaseActivity.imgHome.setVisibility(View.GONE);
    }
}
