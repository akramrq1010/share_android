package com.sharemyyouremotion.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.ViewGroup;

import com.sharemyyouremotion.helper.DialogClass;
import com.sharemyyouremotion.helper.Utilities;
import com.sharemyyouremotion.model.EmotionAndCategoriesModel;
import com.sharemyyouremotion.model.GetMyDiaryModel;

import java.util.ArrayList;

public abstract class EmotionBaseFragment extends Fragment {

    protected FragmentInteractionListener interactionListener;

    protected ViewGroup root;

    protected FragmentActivity activity;
    protected DialogClass dialogClass;
    protected Utilities utilities;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = getActivity();
        dialogClass = new DialogClass();
        utilities = new Utilities(activity);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            interactionListener = (FragmentInteractionListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement FragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        interactionListener = null;
    }

    public abstract FragmentId getFragmentId();

    public enum FragmentId {
        SELECT_YOUR_EMOTION_FRAGMENT, EMOTION_CATEGORY_FRAGMENT, EMOTION_SUB_CATEGORY_FRAGMENT, USER_LIST_FRAGMENT,
        SHARE_EMOTION_FRAGMENT, FEEDBACK_OTHER_USER_FRAGMENT, SHARE_MY_EXPERIENCE_FRAGMENT, MY_DIARY_FRAGMENT
    }

    public interface FragmentInteractionListener {

        void launchSelectYourEmotionFragment(String screen);

        void launchEmotionCategoryFragment(ArrayList<EmotionAndCategoriesModel.CategoryList> categoryLists,String screen);

        void launchEmotionSubCategoryFragment(ArrayList<EmotionAndCategoriesModel.CategoryList> categoryLists, int pos);

        void launchUserListFragment(int categoryId);

        void launchShareEmotionFragment(int mediaValue);

        void launchFeedbackOtherUserFragment(int mediaValue);

        void launchShareMyExperienceFragment(int mediaValue, int categoryId);
        void launchMyDiaryFragment();
        void launchMyDiaryAddFragment();
        void launchMyDiaryEmotionListFragment(ArrayList<GetMyDiaryModel> getMyDiaryModels);


    }
}

