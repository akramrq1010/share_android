package com.sharemyyouremotion.fragment;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import com.sharemyyouremotion.R;
import com.sharemyyouremotion.activity.HomeActivity;
import com.sharemyyouremotion.activity.SpecializationActivity;
import com.sharemyyouremotion.connection.BaseApiService;
import com.sharemyyouremotion.connection.Connection;
import com.sharemyyouremotion.helper.Constants;
import com.sharemyyouremotion.helper.DialogClass;
import com.sharemyyouremotion.helper.StaticSharedpreference;
import com.sharemyyouremotion.helper.Utilities;
import com.sharemyyouremotion.model.SpecializationModel;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ForgotPasswordFragment extends SignupSigninBaseFragment implements View.OnClickListener {

    TextInputEditText edtEmail;
    CardView cardReset;


    public ForgotPasswordFragment() {
        // Required empty public constructor
    }

    public static ForgotPasswordFragment newInstance() {
        return new ForgotPasswordFragment();
    }

    @Override
    public FragmentId getFragmentId() {
        return FragmentId.LOGIN_FRAGMENT;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        root = (ViewGroup) inflater.inflate(R.layout.fragment_forgot_password, container, false);

        initView();

        return root;
    }

    private void initView() {
        edtEmail = root.findViewById(R.id.edtEmail);
        cardReset = root.findViewById(R.id.cardReset);

        cardReset.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cardReset:


                //TODO service
                if (edtEmail.getText().toString().equalsIgnoreCase("")) {
                    Toast.makeText(activity, "Please enter your email", Toast.LENGTH_LONG).show();
                } else if (!utilities.isValidEmail(edtEmail.getText().toString())) {
                    Toast.makeText(activity, "please enter valid email", Toast.LENGTH_LONG).show();
                } else {
                    if (utilities.isOnline())
                        forgotPasswordApi();
                }
                break;
        }
    }


    public void forgotPasswordApi() {

        dialogClass.progresesDialog(activity);

        Map requestBody = new HashMap<>();

        requestBody.put("EmailId", edtEmail.getText().toString());

        BaseApiService apiService = Connection.getBaseApiServiceInstance();
        Call<JsonObject> call = apiService.forgotPasswordApi(requestBody);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.body() != null && response.body().get("StatusCode").getAsInt() == 200) {

                    dialogClass.messageDialog(activity, response.body().get("Message").getAsString() + "");

                } else {
                    dialogClass.messageDialog(activity, response.body().get("Message").getAsString() + "");
                }
                DialogClass.custom_pd.dismiss();
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                DialogClass.custom_pd.dismiss();
            }
        });
    }

}
