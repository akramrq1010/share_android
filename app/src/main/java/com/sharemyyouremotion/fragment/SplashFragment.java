package com.sharemyyouremotion.fragment;

import android.app.ActivityOptions;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dk.view.patheffect.PathTextView;
import com.quickblox.auth.QBAuth;
import com.quickblox.auth.session.BaseService;
import com.quickblox.auth.session.QBSession;
import com.quickblox.auth.session.QBSessionManager;
import com.quickblox.chat.QBChatService;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.BaseServiceException;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.core.helper.Utils;
import com.quickblox.messages.QBPushNotifications;
import com.quickblox.messages.model.QBDevice;
import com.quickblox.messages.model.QBEnvironment;
import com.quickblox.messages.model.QBNotificationChannel;
import com.quickblox.messages.model.QBSubscription;
import com.quickblox.sample.core.ui.dialog.ProgressDialogFragment;
import com.quickblox.sample.core.utils.SharedPrefsHelper;
import com.quickblox.users.model.QBUser;
import com.sharemyyouremotion.R;
import com.sharemyyouremotion.activity.ChatListActivity;
import com.sharemyyouremotion.activity.HomeActivity;
import com.sharemyyouremotion.activity.SignUpActivity;
import com.sharemyyouremotion.activity.SignUpSecondPageActivity;
import com.sharemyyouremotion.activity.SpecializationActivity;
import com.sharemyyouremotion.helper.Constants;
import com.sharemyyouremotion.helper.DialogClass;
import com.sharemyyouremotion.helper.StaticSharedpreference;
import com.sharemyyouremotion.qucikblox.ChatData.ChatHelper;
import com.sharemyyouremotion.qucikblox.ChatData.Snippet;
import com.sharemyyouremotion.qucikblox.Config;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;


public class SplashFragment extends SignupSigninBaseFragment {

    Button btn;
    ImageView img;
    Animation animFadein, animFadein1;
    private PathTextView mPathTextView;
    int varFirst = 0;
    int varSecond = 0;

    String dateLIst[] = {"2018-10-03", "2018-09-30", "2017-10-03", "2018-09-28","2018-09-27"};

    public SplashFragment() {
        // Required empty public constructor
    }

    public static SplashFragment newInstance() {
        return new SplashFragment();
    }

    @Override
    public FragmentId getFragmentId() {
        return FragmentId.SPLASH_FRAGMENT;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        root = (ViewGroup) inflater.inflate(R.layout.fragment_splash, container, false);
        Log.v("akram", "on splash fragment");
        initView();

   /*     SimpleDateFormat s = new SimpleDateFormat("dd-MM-yyyy");
        Date myDate=null;
        try {
             myDate = s.parse("01-01-2015");
        } catch (ParseException e) {
            e.printStackTrace();
        }

     //   Date newDate = new Date(myDate.getTime() - 604800000L);

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(myDate);
        calendar.add(Calendar.DAY_OF_YEAR, -6);
        Date newDate = calendar.getTime();

      //  String date = getCalculatedDate("01-01-2015", "dd-MM-yyyy", -10);

        String date = s.format(newDate);*/

        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate = df.format(c);
        for (int i = 0; i < dateLIst.length; i++) {


            for (int i1 = 0; i1 < 7; i1++) {

                Date myDate = null;
                try {
                    myDate = df.parse(formattedDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                Calendar calendar = Calendar.getInstance();
                calendar.setTime(myDate);
                calendar.add(Calendar.DAY_OF_YEAR, -i1);
                Date newDate = calendar.getTime();

                //  String date = getCalculatedDate("01-01-2015", "dd-MM-yyyy", -10);

                String newDateStr = df.format(newDate);

                if (dateLIst[i].equalsIgnoreCase(newDateStr)) {
                    Log.v("akramraza", "date is=" + newDateStr);
                    break;

                }
                Log.v("akramraza", "date is after for=" + newDateStr);
            }

            Log.v("akramraza", "====================" );

        }


      //  Log.v("akram", "date " + date);
        return root;
    }

    public static String getCalculatedDate(String date, String dateFormat, int days) {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat s = new SimpleDateFormat(dateFormat);
        cal.add(Calendar.DAY_OF_YEAR, days);
        try {
            return s.format(new Date(s.parse(date).getTime()));
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            Log.e("TAG", "Error in Parsing Date : " + e.getMessage());
        }
        return null;
    }

    private void initView() {

        btn = root.findViewById(R.id.btn);
        img = root.findViewById(R.id.img);
        mPathTextView = root.findViewById(R.id.tv);

        animFadein = AnimationUtils.loadAnimation(activity,
                R.anim.together);

        animFadein1 = AnimationUtils.loadAnimation(activity,
                R.anim.fadi_in);

        img.startAnimation(animFadein);

        int color = Color.parseColor("#ffffff");
        int shadowColor = Color.parseColor("#000000");

        mPathTextView.setPaintType(PathTextView.Type.SINGLE);
        mPathTextView.setTextColor(color);
        mPathTextView.setTextSize(activity.getResources().getDimension(R.dimen.sp_16));
        mPathTextView.setTextWeight(4);
        mPathTextView.setDuration(3500);
        mPathTextView.setShadow(2, 2, 2, shadowColor);
        mPathTextView.init("A PLACE FOR EMOTIONS");


        String gotoPage = StaticSharedpreference.getInfo(Constants.GOTO_PAGE, activity);
        if (gotoPage == null || gotoPage.equalsIgnoreCase("")) {
        } else {
            proceedToTheNextActivity();
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                varFirst = 1;
                String gotoPage = StaticSharedpreference.getInfo(Constants.GOTO_PAGE, activity);
                if (gotoPage == null || gotoPage.equalsIgnoreCase("")) {

                    interactionListener.launchLoginFragment();
                } else {

                    if (varSecond == 1) {
                        try {
                            ActivityOptions options1 =
                                    ActivityOptions.makeCustomAnimation(activity, R.anim.slide_to_right, R.anim.slide_from_left);

                            if (gotoPage.equalsIgnoreCase("home")) {
                                startActivity(new Intent(activity, HomeActivity.class));
                                activity.finish();

                            } else if (gotoPage.equalsIgnoreCase("specialization")) {
                                startActivity(new Intent(activity, SpecializationActivity.class), options1.toBundle());
                                activity.finish();
                            } else {
                                startActivity(new Intent(activity, SignUpSecondPageActivity.class), options1.toBundle());
                                activity.finish();
                            }
                        } catch (Exception e) {
                            Toast.makeText(activity, "", Toast.LENGTH_SHORT).show();
                        }
                    }


                }

            }
        }, 5000);
    }

    protected void proceedToTheNextActivity() {
        if (checkSignIn()) {
            restoreChatSession();
        } else {
            varSecond = 1;
            // startActivity(new Intent(activity, SignUpActivity.class));
            // activity.finish();
        }
    }

    private void restoreChatSession() {
        if (ChatHelper.getInstance().isLogged()) {
            //       ChatListActivity.start(this);
            startActivity(new Intent(activity, HomeActivity.class));
            activity.finish();
        } else {

            QBUser currentUser = getUserFromSession();
            // QbSubscription(currentUser);
            try {
                createSessionWithUser(currentUser);
            } catch (Exception e) {
                Toast.makeText(activity, "Exception first", Toast.LENGTH_SHORT).show();
            }


        }
    }

    private void createSessionWithUser(final QBUser qbUserLocal) {


        Snippet createSessionWithUser = new Snippet("create session", "with user") {
            @Override
            public void execute() {

                QBAuth.createSession(new QBUser(qbUserLocal.getLogin(), getString(R.string.quickBloxPass))).performAsync(new QBEntityCallback<QBSession>() {
                    @Override
                    public void onSuccess(QBSession session, Bundle args) {

                        try {
                            QbSubscription(qbUserLocal);
                        } catch (Exception e) {
                            Toast.makeText(activity, "Exception first", Toast.LENGTH_SHORT).show();
                        }

                    }

                    @Override
                    public void onError(QBResponseException errors) {

                    }
                });

                try {
                    BaseService.createFromExistentToken("31ed199120fb998dc472aea785a1825809ad5c04", null);
                } catch (BaseServiceException e) {
                    e.printStackTrace();
                }
            }
        };
        createSessionWithUser.execute();

    }

    QBChatService chatService;

    private void QbSubscription(final QBUser qbUser) {

        Snippet createSubscription = new Snippet("create subscription") {
            @Override
            public void execute() {
                qbUser.setPassword(getResources().getString(R.string.quickBloxPass));
                QBSubscription subscription = new QBSubscription(QBNotificationChannel.GCM);
                subscription.setEnvironment(QBEnvironment.PRODUCTION);
                //
                String deviceId = Utils.generateDeviceId(activity);
                if (deviceId == null) {
                    deviceId = "UniversalDeviceId";
                }
                SharedPreferences sharedPreferences = activity.getSharedPreferences("ShareAllYourEmotionFCM", activity.MODE_PRIVATE);

                final String regId = sharedPreferences.getString("fcm", "");


                SharedPreferences pref = activity.getSharedPreferences(Config.SHARED_PREF, 0);
                // final String regId = pref.getString("regId", null);
                subscription.setDeviceUdid(deviceId);
                String registrationID = "APA91bGr9AcS9Wgv4p4BkBQAg_1YrJZpfa5GMXg7LAQU0lya8gbf9Iw1360602PunkWk_NOsLS2xEK8tPeBCBfSH4fobt7zW4KVlWGjUfR3itFbVa_UreBf6c-rZ8uP_0_vxPCO65ceqgnjvQqD6j8DjLykok7VF7UBBjsMZrTIFjKwmVeJqb1o";
                subscription.setRegistrationID(regId);
                subscription.setNotificationChannel(QBNotificationChannel.GCM);
                subscription.setDevice(new QBDevice(activity));

                QBPushNotifications.createSubscription(subscription).performAsync(new QBEntityCallback<ArrayList<QBSubscription>>() {

                    @Override
                    public void onSuccess(ArrayList<QBSubscription> subscriptions, Bundle args) {

                        if (chatService == null) {
                            QBChatService.setDebugEnabled(true);
                            chatService = QBChatService.getInstance();
                        }

                        Log.v("akram", "instance sharepreference " + regId);
                        SharedPrefsHelper.getInstance().saveQbUser(qbUser);


                        String gotoPage = StaticSharedpreference.getInfo(Constants.GOTO_PAGE, activity);


                        if (varFirst == 1) {
                            ActivityOptions options1 =
                                    ActivityOptions.makeCustomAnimation(activity, R.anim.slide_to_right, R.anim.slide_from_left);

                            if (gotoPage.equalsIgnoreCase("home")) {
                                startActivity(new Intent(activity, HomeActivity.class));
                                activity.finish();

                            } else if (gotoPage.equalsIgnoreCase("specialization")) {
                                startActivity(new Intent(activity, SpecializationActivity.class), options1.toBundle());
                                activity.finish();
                            } else {
                                startActivity(new Intent(activity, SignUpSecondPageActivity.class), options1.toBundle());
                                activity.finish();
                            }
                        } else {
                            varSecond = 1;
                        }

                        // loginToChat(qbUser);

                    }

                    @Override
                    public void onError(QBResponseException errors) {
                        // handleErrors(errors);
                    }
                });
            }
        };
        createSubscription.execute();
    }


    private QBUser getUserFromSession() {
        QBUser user = SharedPrefsHelper.getInstance().getQbUser();

        user.setId(user.getId());
        return user;
    }


    protected boolean checkSignIn() {
        return SharedPrefsHelper.getInstance().hasQbUser();
    }


}
