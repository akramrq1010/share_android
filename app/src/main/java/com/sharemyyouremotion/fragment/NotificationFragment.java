package com.sharemyyouremotion.fragment;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;
import com.sharemyyouremotion.R;
import com.sharemyyouremotion.activity.HomeActivity;
import com.sharemyyouremotion.activity.MyQuestionFeedbackActivity;
import com.sharemyyouremotion.activity.SpecializationActivity;
import com.sharemyyouremotion.adapter.NotificationAdapter;
import com.sharemyyouremotion.connection.BaseApiService;
import com.sharemyyouremotion.connection.Connection;
import com.sharemyyouremotion.helper.ClickListener;
import com.sharemyyouremotion.helper.Constants;
import com.sharemyyouremotion.helper.DialogClass;
import com.sharemyyouremotion.helper.RecyclerTouchListener;
import com.sharemyyouremotion.helper.StaticSharedpreference;
import com.sharemyyouremotion.helper.Utilities;
import com.sharemyyouremotion.model.NotificationModel;
import com.sharemyyouremotion.model.SpecializationModel;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationFragment extends Fragment {
    View root;
    RecyclerView recyclerView;
    HomeActivity activity;
    DialogClass dialogClass = new DialogClass();

    CardView cardProgress;
    TextView tvNoData;
    Utilities utilities;
    SwipyRefreshLayout swipeRefresh;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root = (ViewGroup) inflater.inflate(R.layout.fragment_my_question, container, false);

        activity = (HomeActivity) getActivity();
        initView();
        utilities = new Utilities(activity);
        if (Constants.notificationModels.size() == 0)
            getNotification();
        else {
            NotificationAdapter notificationAdapter = new NotificationAdapter(activity, Constants.notificationModels);
            recyclerView.setAdapter(notificationAdapter);
        }
        return root;
    }

    private void initView() {
        recyclerView = root.findViewById(R.id.recyclerView);
        cardProgress = root.findViewById(R.id.cardProgress);
        tvNoData = root.findViewById(R.id.tvNoData);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(activity);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());


        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(activity, recyclerView, new ClickListener() {
            @Override
            public void onClick(View view, int position) {


                ActivityOptions options1 =
                        ActivityOptions.makeCustomAnimation(activity, R.anim.slide_to_right, R.anim.slide_from_left);
                Intent intent = new Intent(activity, MyQuestionFeedbackActivity.class);
                intent.putExtra("QuestionId", Constants.notificationModels.get(position).getQuestionId());
                if (Constants.notificationModels.get(position).getType().equalsIgnoreCase("givefeedback")) {
                    MyQuestionFragment.screenValue = 1;
                } else {
                    MyQuestionFragment.screenValue = 0;
                }
                startActivity(intent, options1.toBundle());
            }

            @Override
            public void onLongClick(View view, int position) {
                //  popup();
            }
        }));

        swipeRefresh = (SwipyRefreshLayout) root.findViewById(R.id.swipeRefresh);

        swipeRefresh.setOnRefreshListener(new SwipyRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh(SwipyRefreshLayoutDirection direction) {
                swipeRefresh.setEnabled(false);
                getNotification();
            }
        });

    }


    public void getNotification() {
        swipeRefresh.setRefreshing(false);
        if (!utilities.isOnline())
            return;

        cardProgress.setVisibility(View.VISIBLE);
        Map requestBody = new HashMap<>();

        requestBody.put("UserId", StaticSharedpreference.getInt(Constants.USER_ID, activity));

        BaseApiService apiService = Connection.getBaseApiServiceInstance();
        Call<JsonObject> call = apiService.getNotification(requestBody);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.body() != null && response.body().get("StatusCode").getAsInt() == 200) {

                    Gson gson = new Gson();
                    Type listType = new TypeToken<ArrayList<NotificationModel>>() {
                    }.getType();

                    Constants.notificationModels =
                            gson.fromJson(response.body().getAsJsonArray("Notifications"), listType);

                    NotificationAdapter notificationAdapter = new NotificationAdapter(activity, Constants.notificationModels);
                    recyclerView.setAdapter(notificationAdapter);

                } else if (response.body() != null && response.body().get("StatusCode").getAsInt() == 404) {

                    tvNoData.setVisibility(View.VISIBLE);
                    tvNoData.setText(response.body().get("Message").getAsString() + "");
                } else {
                    dialogClass.messageDialog(activity, response.body().get("Message").getAsString() + "");
                }
                cardProgress.setVisibility(View.GONE);
                swipeRefresh.setEnabled(true);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.v("okhttp", " throw = " + t);
                swipeRefresh.setEnabled(true);
                cardProgress.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
    }
}
