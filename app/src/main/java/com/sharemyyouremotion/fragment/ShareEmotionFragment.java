package com.sharemyyouremotion.fragment;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityOptions;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.database.Cursor;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import com.sharemyyouremotion.R;
import com.sharemyyouremotion.activity.SignUpActivity;
import com.sharemyyouremotion.activity.SpecializationActivity;
import com.sharemyyouremotion.activity.WebViewForVideoActivity;
import com.sharemyyouremotion.connection.BaseApiService;
import com.sharemyyouremotion.connection.Connection;
import com.sharemyyouremotion.connection.ProgressRequestBody;
import com.sharemyyouremotion.connection.RequestClass;
import com.sharemyyouremotion.helper.Constants;
import com.sharemyyouremotion.helper.DialogClass;
import com.sharemyyouremotion.helper.PathUtil;
import com.sharemyyouremotion.helper.StaticSharedpreference;
import com.sharemyyouremotion.helper.Util;
import com.sharemyyouremotion.helper.Utilities;
import com.sharemyyouremotion.model.SpecializationModel;
import com.vincent.videocompressor.VideoCompress;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static com.sharemyyouremotion.activity.SelectYourEmotionBaseActivity.toolbar;


public class ShareEmotionFragment extends EmotionBaseFragment implements View.OnClickListener {

    LinearLayout linearSelectText, linearSelectAudio, linearSelectVideo;
    CardView cardTextContent, cardSend, cardSelectMedia;
    LinearLayout linearAudioContent, linearVideoContent, linearVideo, linearAudio;
    int mediaValue;
    TextView tvFeedbackType, tvSelectedUser, tvSelectedEmotion, tvVideoName, tvTitleFeedback;
    TextView tvAudioName;
    public static String mediaPath;

    public static String mediaType = "";
    String isSharePublicly;
    EditText edtQuestion;
    RequestClass requestClass;
    ImageView imgPlay, imgPlayVideo;


    public ShareEmotionFragment() {
        // Required empty public constructor
    }

    public static ShareEmotionFragment newInstance() {
        return new ShareEmotionFragment();
    }

    @Override
    public FragmentId getFragmentId() {
        return FragmentId.USER_LIST_FRAGMENT;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getActivity().getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.notificationBar));
        }

        root = (ViewGroup) inflater.inflate(R.layout.fragment_share_emotion, container, false);

        mediaPath = "";
        Bundle bundle = getArguments();
        mediaValue = bundle.getInt("media", 0);
        initView();
        if (mediaValue == 0)
            toolbar.setText("Share Emotion");
        else {
            toolbar.setText("Share Experience");
            tvSelectedUser.setVisibility(View.GONE);
            tvTitleFeedback.setVisibility(View.GONE);
        }


        return root;
    }

    private void initView() {
        linearSelectText = root.findViewById(R.id.linearSelectText);
        linearSelectAudio = root.findViewById(R.id.linearSelectAudio);
        linearSelectVideo = root.findViewById(R.id.linearSelectVideo);
        linearVideo = root.findViewById(R.id.linearVideo);
        linearAudio = root.findViewById(R.id.linearAudio);
        cardSend = root.findViewById(R.id.cardSend);
        cardSelectMedia = root.findViewById(R.id.cardSelectMedia);
        tvFeedbackType = root.findViewById(R.id.tvFeedbackType);
        tvSelectedUser = root.findViewById(R.id.tvSelectedUser);
        edtQuestion = root.findViewById(R.id.edtQuestion);
        tvSelectedEmotion = root.findViewById(R.id.tvSelectedEmotion);
        imgPlay = root.findViewById(R.id.imgPlay);
        tvAudioName = root.findViewById(R.id.tvAudioName);
        tvVideoName = root.findViewById(R.id.tvVideoName);
        imgPlayVideo = root.findViewById(R.id.imgPlayVideo);
        tvTitleFeedback = root.findViewById(R.id.tvTitleFeedback);

        tvSelectedEmotion.setText(Constants.emotionStr + "/" + Constants.categoryStr + "/" + Constants.subCategoryStr);

        int selectedUser = Constants.askToIds.split(",").length;
        if (Constants.askToIds.equalsIgnoreCase("")) {
            tvSelectedUser.setText("You did not select any user your question share publicly.");
            isSharePublicly = "true";
        } else {
            tvSelectedUser.setText("You have selected " + selectedUser + " User for receive feedback.");
            isSharePublicly = "false";
        }

        cardTextContent = root.findViewById(R.id.cardTextContent);
        linearAudioContent = root.findViewById(R.id.linearAudioContent);
        linearVideoContent = root.findViewById(R.id.linearVideoContent);

        linearSelectText.setOnClickListener(this);
        linearSelectAudio.setOnClickListener(this);
        linearSelectVideo.setOnClickListener(this);
        linearVideo.setOnClickListener(this);
        cardSend.setOnClickListener(this);
        linearAudio.setOnClickListener(this);
        imgPlay.setOnClickListener(this);
        imgPlayVideo.setOnClickListener(this);

        if (mediaValue == 1) {
            tvFeedbackType.setVisibility(View.GONE);
            cardSelectMedia.setVisibility(View.GONE);
            cardTextContent.setVisibility(View.VISIBLE);
            linearAudioContent.setVisibility(View.GONE);
            linearVideoContent.setVisibility(View.GONE);

        } else if (mediaValue == 2) {
            tvFeedbackType.setVisibility(View.GONE);
            cardSelectMedia.setVisibility(View.GONE);
            cardTextContent.setVisibility(View.GONE);
            linearAudioContent.setVisibility(View.VISIBLE);
            linearVideoContent.setVisibility(View.GONE);
        } else if (mediaValue == 3) {
            tvFeedbackType.setVisibility(View.GONE);
            cardSelectMedia.setVisibility(View.GONE);
            cardTextContent.setVisibility(View.GONE);
            linearAudioContent.setVisibility(View.GONE);
            linearVideoContent.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.linearSelectText:


                mediaPath = "";
                tvVideoName.setText("");

                linearSelectText.setBackgroundColor(getResources().getColor(R.color.media_select));
                linearSelectAudio.setBackgroundColor(getResources().getColor(R.color.blue));
                linearSelectVideo.setBackgroundColor(getResources().getColor(R.color.blue));

                cardTextContent.setVisibility(View.VISIBLE);
                linearAudioContent.setVisibility(View.GONE);
                linearVideoContent.setVisibility(View.GONE);


                break;
            case R.id.linearSelectAudio:

                mediaPath = "";
                tvVideoName.setText("");

                linearSelectText.setBackgroundColor(getResources().getColor(R.color.blue));
                linearSelectAudio.setBackgroundColor(getResources().getColor(R.color.media_select));
                linearSelectVideo.setBackgroundColor(getResources().getColor(R.color.blue));

                cardTextContent.setVisibility(View.GONE);
                linearAudioContent.setVisibility(View.VISIBLE);
                linearVideoContent.setVisibility(View.GONE);


                //TODO service
               /* requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE},

                        1);*/
                break;
            case R.id.linearSelectVideo:

                mediaPath = "";
                tvVideoName.setText("");

                linearSelectText.setBackgroundColor(getResources().getColor(R.color.blue));
                linearSelectAudio.setBackgroundColor(getResources().getColor(R.color.blue));
                linearSelectVideo.setBackgroundColor(getResources().getColor(R.color.media_select));

                cardTextContent.setVisibility(View.GONE);
                linearAudioContent.setVisibility(View.GONE);
                linearVideoContent.setVisibility(View.VISIBLE);

               /* Intent intent = new Intent();
                intent.setType("video*//*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent,"Select Video"),2);*/
                break;

            case R.id.cardSend:

                if (LoginFragment.guest.equalsIgnoreCase("guest")) {
                    Toast.makeText(activity, "Please sign up first", Toast.LENGTH_SHORT).show();
                    return;
                }

                requestClass = new RequestClass();
                if (mediaValue == 0) {

                    //for share emotion
                    if (cardTextContent.getVisibility() == View.VISIBLE) {
                        if (edtQuestion.getText().toString().equalsIgnoreCase("")) {
                            Toast.makeText(activity, "Please type your question", Toast.LENGTH_SHORT).show();
                        } else {
                            if (utilities.isOnline())
                                dialogClass.shareIdentity(activity, mediaPath, "text", edtQuestion.getText().toString(), isSharePublicly, this);
                        }
                    } else {
                        if (mediaPath.equalsIgnoreCase("")) {
                            Toast.makeText(activity, "Please select file", Toast.LENGTH_SHORT).show();
                        } else {
                            if (utilities.isOnline())
                                dialogClass.shareIdentity(activity, mediaPath, mediaType, edtQuestion.getText().toString(), isSharePublicly, this);
                        }

                    }
                    //for share experience
                } else {
                    if (cardTextContent.getVisibility() == View.VISIBLE) {
                        if (edtQuestion.getText().toString().equalsIgnoreCase("")) {
                            Toast.makeText(activity, "Please type your question", Toast.LENGTH_SHORT).show();
                        } else {
                            if (utilities.isOnline())
                                dialogClass.shareIdentity(activity, mediaPath, "text", edtQuestion.getText().toString(), "", this);
                        }
                    } else {
                        if (mediaPath.equalsIgnoreCase("")) {
                            Toast.makeText(activity, "Please select file", Toast.LENGTH_SHORT).show();
                        } else {
                            if (utilities.isOnline())
                                dialogClass.shareIdentity(activity, mediaPath, mediaType, edtQuestion.getText().toString(), "", this);
                        }
                    }
                }
                break;

            case R.id.linearVideo:
                requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE,
                                Manifest.permission.RECORD_AUDIO},

                        2);

                break;
            case R.id.linearAudio:
                requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE,
                                Manifest.permission.RECORD_AUDIO},

                        1);

                break;
            case R.id.imgPlay:
                if (mediaPath == null || mediaPath.equalsIgnoreCase(""))
                    Toast.makeText(activity, "Please select audio", Toast.LENGTH_LONG).show();
                else
                    dialogClass.audioPlayDialog(activity, mediaPath, true);

                break;

            case R.id.imgPlayVideo:
                if (mediaPath == null || mediaPath.equalsIgnoreCase("")) {
                    Toast.makeText(activity, "Please select video", Toast.LENGTH_LONG).show();
                } else {
                    ActivityOptions options1 =
                            ActivityOptions.makeCustomAnimation(activity, R.anim.slide_to_right, R.anim.slide_from_left);
                    Intent intent = new Intent(activity, WebViewForVideoActivity.class);
                    intent.putExtra("video", mediaPath);
                    intent.putExtra("local", "local");
                    //  intent.putExtra("QuestionId",myQuestionFragments.get(position).getQuestionId());
                    activity.startActivity(intent, options1.toBundle());
                }

                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {

        Map<String, Integer> perms = new HashMap<String, Integer>();
        // Initial
        perms.put(Manifest.permission.READ_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
        perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
        perms.put(Manifest.permission.CAMERA, PackageManager.PERMISSION_GRANTED);
        perms.put(Manifest.permission.RECORD_AUDIO, PackageManager.PERMISSION_GRANTED);

        // Fill with results
        for (int i = 0; i < permissions.length; i++)
            perms.put(permissions[i], grantResults[i]);
        // Check for ACCESS_FINE_LOCATION
        if (perms.get(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                && perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                && perms.get(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                && perms.get(Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED
                ) {

            switch (requestCode) {
                case 1:

                    dialogClass.selectAudioVideo(ShareEmotionFragment.this, activity, false, tvAudioName);


                    break;
                case 2:
                    dialogClass.selectAudioVideo(ShareEmotionFragment.this, activity, true, tvAudioName);



                   /* Intent intent = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent,1);*/


                  /*  Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                            android.provider.MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(galleryIntent, 2);*/

                    /* */


                    break;
                // other 'case' lines to check for other
                // permissions this app might request
            }

        } else {
            // Permission Denied
            Toast.makeText(getContext(), "Some Permission is Denied", Toast.LENGTH_SHORT)
                    .show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        //this is for audio
        if (resultCode == RESULT_OK) {
            if (requestCode == 1) {


                //the selected audio.
                Uri uri = data.getData();

                try {
                    mediaPath = PathUtil.getPath(activity, uri);
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                }
                Log.v("akram", "uri " + mediaPath);
                mediaType = "audio";
                tvAudioName.setText(new File(mediaPath).getName());

                //this for video
            } else if (requestCode == 2) {


                Uri selectedVideoUri = data.getData();
                try {
                    mediaPath = PathUtil.getPath(activity, selectedVideoUri);
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                }
                Log.v("akram", "uri " + mediaPath);
                mediaType = "video";
                tvVideoName.setText(new File(mediaPath).getName());

            } else if (requestCode == 3) {

                mediaPath = DialogClass.videoFile.getPath();


                Log.v("akram", "uri " + mediaPath);
                mediaType = "video";
                tvVideoName.setText(new File(mediaPath).getName());
            }
        }
        super.onActivityResult(requestCode, resultCode, data);

    }

    AlertDialog alertDialog;



    public void shareExperience(final Activity activity, String mediaPath, String mediaType, String text, String isShareIdentity) {


        Log.v("akram", "experience");
        MultipartBody.Part body;
        if (!mediaType.equalsIgnoreCase("text")) {
            alertDialog = dialogClass.compressDialog(activity, "Uploading...");
            File file = new File(mediaPath);
            RequestBody reqbodyFile = RequestBody.create(MediaType.parse("*/*"), file);

            // ProgressRequestBody fileBody = new ProgressRequestBody(file, this);
            body = MultipartBody.Part.createFormData("files", file.getName(), reqbodyFile);
        } else {
            dialogClass.progresesDialog(activity);
            RequestBody reqbodyFile = RequestBody.create(MediaType.parse("*/*"), "");
            body = MultipartBody.Part.createFormData("files", "", reqbodyFile);
        }

        RequestBody expType = RequestBody.create(MediaType.parse("text"), mediaType);
        RequestBody shareBy = RequestBody.create(MediaType.parse("text"), "" + StaticSharedpreference.getInt(Constants.USER_ID, activity));
        RequestBody emotionId = RequestBody.create(MediaType.parse("text"), "" + Constants.emotionId);
        RequestBody categoryId = RequestBody.create(MediaType.parse("text"), "" + Constants.categoryId);
        RequestBody subCategoryId = RequestBody.create(MediaType.parse("text"), "" + Constants.subCategoryId);
        RequestBody ExperienceText = RequestBody.create(MediaType.parse("text"), text);
        RequestBody isShareIdentityReq = RequestBody.create(MediaType.parse("text"), isShareIdentity);

        BaseApiService apiService = Connection.getBaseApiServiceInstance();
        Call<JsonObject> call = apiService.shareExperience(expType, shareBy, emotionId, categoryId, subCategoryId, ExperienceText, isShareIdentityReq, body);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
//                Log.d("tag", "onResponse" + "" + response.body().toString());
                if (response.body() != null && response.body().get("StatusCode").getAsInt() == 200) {

                    dialogClass.messageSend(activity);
                } else {
                    dialogClass.messageDialog(activity, response.body().get("Message").getAsString() + "");
                }
                if (alertDialog != null)
                    alertDialog.dismiss();
                if (DialogClass.custom_pd != null)
                    DialogClass.custom_pd.dismiss();

                /* JsonObject response = new  JsonParser().parse(out.body()).getAsJsonObject();*/

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.v("okhttp", " throw = " + t);

                if (alertDialog != null)
                    alertDialog.dismiss();
                if (DialogClass.custom_pd != null)
                    DialogClass.custom_pd.dismiss();
                dialogClass.messageDialog(activity, "Please try again");

            }
        });
    }


}
