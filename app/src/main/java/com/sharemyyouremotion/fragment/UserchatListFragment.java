package com.sharemyyouremotion.fragment;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;


import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;
import com.quickblox.chat.QBChatService;
import com.quickblox.chat.QBIncomingMessagesManager;
import com.quickblox.chat.QBSystemMessagesManager;
import com.quickblox.chat.exception.QBChatException;
import com.quickblox.chat.listeners.QBChatDialogMessageListener;
import com.quickblox.chat.listeners.QBSystemMessageListener;
import com.quickblox.chat.model.QBChatDialog;
import com.quickblox.chat.model.QBChatMessage;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.core.request.QBRequestGetBuilder;
import com.quickblox.messages.services.SubscribeService;
import com.quickblox.sample.core.ui.dialog.ProgressDialogFragment;
import com.quickblox.sample.core.utils.SharedPrefsHelper;
import com.quickblox.sample.core.utils.constant.GcmConsts;
import com.quickblox.users.model.QBUser;
import com.sharemyyouremotion.R;
import com.sharemyyouremotion.activity.ChatActivity;
import com.sharemyyouremotion.activity.ChatListActivity;
import com.sharemyyouremotion.activity.HomeActivity;
import com.sharemyyouremotion.qucikblox.App;
import com.sharemyyouremotion.qucikblox.ChatData.ChatHelper;
import com.sharemyyouremotion.qucikblox.ChatData.DialogsAdapter;
import com.sharemyyouremotion.qucikblox.ChatData.DialogsManager;
import com.sharemyyouremotion.qucikblox.ChatData.QbChatDialogMessageListenerImp;
import com.sharemyyouremotion.qucikblox.ChatData.QbDialogHolder;
import com.sharemyyouremotion.qucikblox.ChatData.QbEntityCallbackImpl;

import java.util.ArrayList;
import java.util.Collection;

import static android.app.Activity.RESULT_OK;

public class UserchatListFragment extends Fragment implements DialogsManager.ManagingDialogsCallbacks {
    View root;
    HomeActivity activity;

    private static final String TAG = ChatListActivity.class.getSimpleName();
    private static final int REQUEST_SELECT_PEOPLE = 174;
    private static final int REQUEST_DIALOG_ID_FOR_UPDATE = 165;

    private ProgressBar progressBar;
    //  private FloatingActionButton fab;
    private ActionMode currentActionMode;
    private SwipyRefreshLayout setOnRefreshListener;
    private QBRequestGetBuilder requestBuilder;
    private Menu menu;
    private int skipRecords = 0;
    private boolean isProcessingResultInProgress;

    private BroadcastReceiver pushBroadcastReceiver;
    //  private GooglePlayServicesHelper googlePlayServicesHelper;
    private DialogsAdapter dialogsAdapter;
    private QBChatDialogMessageListener allDialogsMessagesListener;
    private SystemMessagesListener systemMessagesListener;
    private QBSystemMessagesManager systemMessagesManager;
    private QBIncomingMessagesManager incomingMessagesManager;
    private DialogsManager dialogsManager;
    public static QBUser currentUser;
    ArrayList<QBChatDialog> dialogs1 = new ArrayList<>();
    TextView tvNoUser;
    App mApp;
    QBChatService chatService;

    public static void start(Context context) {
        Intent intent = new Intent(context, ChatListActivity.class);
        context.startActivity(intent);
    }

    public static void start(Context context, QBUser currentUser) {
        Intent intent = new Intent(context, ChatListActivity.class);
        context.startActivity(intent);
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root = (ViewGroup) inflater.inflate(R.layout.demo_activity_dialogs, container, false);

        activity = (HomeActivity) getActivity();

        mApp = (App) activity.getApplication();


        chatService = QBChatService.getInstance();

        if (!chatService.isLoggedIn()) {
            Log.v("akram", "qbchatservice not login");
        } else {
            Log.v("akram", "qbchat service login");
        }
        currentUser = SharedPrefsHelper.getInstance().getQbUser();
        if (!chatService.isLoggedIn()) {
            loginToChat(currentUser);
        }

        pushBroadcastReceiver = new PushBroadcastReceiver();

        allDialogsMessagesListener = new AllDialogsMessageListener();
        systemMessagesListener = new SystemMessagesListener();

        dialogsManager = new DialogsManager();


        initUi(root);

        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        //googlePlayServicesHelper.checkPlayServicesAvailable(this);

        new onStart().execute();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                LocalBroadcastManager.getInstance(activity).registerReceiver(pushBroadcastReceiver,
                        new IntentFilter(GcmConsts.ACTION_NEW_GCM_EVENT));
            }
        }, 500);
        mApp.clearNotification();
    }


    public class onStart extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if (QbDialogHolder.getInstance().getDialogs().size() > 0) {
                loadDialogsFromQb(true, true);
            } else {
                loadDialogsFromQb(false, true);
            }

        }

        @Override
        protected Void doInBackground(Void... voids) {
            registerQbChatListeners();


            return null;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        unregisterQbChatListeners();
    }

    @SuppressWarnings("unchecked")
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            isProcessingResultInProgress = true;
            if (requestCode == REQUEST_DIALOG_ID_FOR_UPDATE) {
                if (data != null) {
                    String dialogId = data.getStringExtra(ChatActivity.EXTRA_DIALOG_ID);
                    loadUpdatedDialog(dialogId);
                } else {
                    isProcessingResultInProgress = false;
                    updateDialogsList();
                }
            }
        } else {
            updateDialogsAdapter();
        }
    }


    private boolean isPrivateDialogExist(ArrayList<QBUser> allSelectedUsers) {
        ArrayList<QBUser> selectedUsers = new ArrayList<>();
        selectedUsers.addAll(allSelectedUsers);
        selectedUsers.remove(ChatHelper.getCurrentUser());
        return selectedUsers.size() == 1 && QbDialogHolder.getInstance().hasPrivateDialogWithUser(selectedUsers.get(0));
    }

    private void loadUpdatedDialog(String dialogId) {
        ChatHelper.getInstance().getDialogById(dialogId, new QbEntityCallbackImpl<QBChatDialog>() {
            @Override
            public void onSuccess(QBChatDialog result, Bundle bundle) {
                isProcessingResultInProgress = false;

                QbDialogHolder.getInstance().addDialog(result);

                updateDialogsAdapter();
            }

            @Override
            public void onError(QBResponseException e) {
                isProcessingResultInProgress = false;
            }
        });
    }


    private void userLogout() {
        ChatHelper.getInstance().destroy();
        SubscribeService.unSubscribeFromPushes(activity);
        SharedPrefsHelper.getInstance().removeQbUser();
        //  LoginActivity11.start(ChatListActivity.this);
        QbDialogHolder.getInstance().clear();
        ProgressDialogFragment.hide(getFragmentManager());
        activity.finish();
    }

    private void updateDialogsList() {
        requestBuilder.setSkip(skipRecords = 0);
        loadDialogsFromQb(true, true);
    }


    private void initUi(View root) {
        LinearLayout emptyHintLayout = (LinearLayout) root.findViewById(R.id.layout_chat_empty);
        ListView dialogsListView = (ListView) root.findViewById(R.id.list_dialogs_chats);
        tvNoUser = (TextView) root.findViewById(R.id.tvNoUser);
        progressBar = (ProgressBar) root.findViewById(R.id.progress_dialogs);
        //fab = _findViewById(R.id.fab_dialogs_new_chat);
        setOnRefreshListener = (SwipyRefreshLayout) root.findViewById(R.id.swipy_refresh_layout);
        TextView toolbarTitle = (TextView) root.findViewById(R.id.toolbarTitle);

        toolbarTitle.setText("Chat List");

        dialogsAdapter = new DialogsAdapter(activity, new ArrayList<>(QbDialogHolder.getInstance().getDialogs().values()), activity);
        dialogsListView.setAdapter(dialogsAdapter);
        dialogsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                QBChatDialog selectedDialog = (QBChatDialog) parent.getItemAtPosition(position);
                if (currentActionMode == null) {
                    if (chatService.isLoggedIn()) {
                        Log.v("akram", "selected user " + selectedDialog);
                        ChatActivity.startForResult(activity, REQUEST_DIALOG_ID_FOR_UPDATE, selectedDialog);
                    } else {
                        Toast.makeText(activity, "Something went wrong please try again.", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    dialogsAdapter.toggleSelection(selectedDialog);
                }
            }
        });
        dialogsListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                QBChatDialog selectedDialog = (QBChatDialog) parent.getItemAtPosition(position);
                //startSupportActionMode(new DeleteActionModeCallback());
                //  dialogsAdapter.selectItem(selectedDialog);
                return true;
            }
        });
        requestBuilder = new QBRequestGetBuilder();

        setOnRefreshListener.setOnRefreshListener(new SwipyRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh(SwipyRefreshLayoutDirection direction) {
                requestBuilder.setSkip(skipRecords += ChatHelper.DIALOG_ITEMS_PER_PAGE);
                loadDialogsFromQb(true, false);
            }
        });
    }

    private void registerQbChatListeners() {
        incomingMessagesManager = QBChatService.getInstance().getIncomingMessagesManager();
        systemMessagesManager = QBChatService.getInstance().getSystemMessagesManager();

        if (incomingMessagesManager != null) {
            incomingMessagesManager.addDialogMessageListener(allDialogsMessagesListener != null
                    ? allDialogsMessagesListener : new AllDialogsMessageListener());
        }

        if (systemMessagesManager != null) {
            systemMessagesManager.addSystemMessageListener(systemMessagesListener != null
                    ? systemMessagesListener : new SystemMessagesListener());
        }

        dialogsManager.addManagingDialogsCallbackListener(this);
    }

    private void unregisterQbChatListeners() {
        if (incomingMessagesManager != null) {
            incomingMessagesManager.removeDialogMessageListrener(allDialogsMessagesListener);
        }

        if (systemMessagesManager != null) {
            systemMessagesManager.removeSystemMessageListener(systemMessagesListener);
        }

        dialogsManager.removeManagingDialogsCallbackListener(this);
    }

    private void createDialog(final ArrayList<QBUser> selectedUsers) {
        ChatHelper.getInstance().createDialogWithSelectedUsers(selectedUsers,
                new QBEntityCallback<QBChatDialog>() {
                    @Override
                    public void onSuccess(QBChatDialog dialog, Bundle args) {
                        isProcessingResultInProgress = false;
                        dialogsManager.sendSystemMessageAboutCreatingDialog(systemMessagesManager, dialog);
                        ChatActivity.startForResult(activity, REQUEST_DIALOG_ID_FOR_UPDATE, dialog);
                        ProgressDialogFragment.hide(getFragmentManager());
                    }

                    @Override
                    public void onError(QBResponseException e) {
                        isProcessingResultInProgress = false;
                        ProgressDialogFragment.hide(getFragmentManager());

                    }
                }
        );
    }

    private void loadDialogsFromQb(final boolean silentUpdate, final boolean clearDialogHolder) {
        isProcessingResultInProgress = true;
        if (!silentUpdate) {
            progressBar.setVisibility(View.VISIBLE);
        }

        ChatHelper.getInstance().getDialogs(requestBuilder, new QBEntityCallback<ArrayList<QBChatDialog>>() {
            @Override
            public void onSuccess(ArrayList<QBChatDialog> dialogs, Bundle bundle) {
                isProcessingResultInProgress = false;
                progressBar.setVisibility(View.GONE);
                setOnRefreshListener.setRefreshing(false);

                if (clearDialogHolder) {
                    QbDialogHolder.getInstance().clear();
                }


                QbDialogHolder.getInstance().addDialogs(dialogs);
                updateDialogsAdapter();

                //  callCheckApi(clearDialogHolder, dialogs, silentUpdate);


            }

            @Override
            public void onError(QBResponseException e) {
                isProcessingResultInProgress = false;
                progressBar.setVisibility(View.GONE);
                setOnRefreshListener.setRefreshing(false);
                Toast.makeText(activity, e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void updateDialogsAdapter() {
        dialogs1 = new ArrayList<>(QbDialogHolder.getInstance().getDialogs().values());

        for (int j = 0; j < dialogs1.size(); j++) {
          /*  if (blockedUSers != null) {
                if (dialogs1.get(j).getOccupants().size() > 1) {
                    for (int i = 0; i < blockedUSers.size(); i++) {
                        if (blockedUSers.get(i).getQuickBloxID().equals("" + dialogs1.get(j).getOccupants().get(0)) || blockedUSers.get(i).getQuickBloxID().equals("" + dialogs1.get(j).getOccupants().get(1))) {

                            dialogs1.remove(j);
                            break;
                        }
                    }
                }
            }*/
        }
        Log.v("akram", "size list = " + dialogs1.size());

        if (dialogs1.size() == 0) {
            tvNoUser.setVisibility(View.VISIBLE);
        } else {
            tvNoUser.setVisibility(View.GONE);
        }

        dialogsAdapter.updateList(dialogs1);

    }

    @Override
    public void onDialogCreated(QBChatDialog chatDialog) {
        updateDialogsAdapter();
    }

    @Override
    public void onDialogUpdated(String chatDialog) {
        updateDialogsAdapter();
    }

    @Override
    public void onNewDialogLoaded(QBChatDialog chatDialog) {
        updateDialogsAdapter();
    }

    private class DeleteActionModeCallback implements ActionMode.Callback {

        public DeleteActionModeCallback() {
            //    fab.hide();
        }

        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            //   mode.getMenuInflater().inflate(R.menu.action_mode_dialogs, menu);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
               /* case R.id.menu_dialogs_action_delete:
                    deleteSelectedDialogs();
                    if (currentActionMode != null) {
                        currentActionMode.finish();
                    }
                    return true;*/
            }
            return false;
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            currentActionMode = null;
            dialogsAdapter.clearSelection();
            //fab.show();
        }

        private void deleteSelectedDialogs() {
            final Collection<QBChatDialog> selectedDialogs = dialogsAdapter.getSelectedItems();
            ChatHelper.getInstance().deleteDialogs(selectedDialogs, new QBEntityCallback<ArrayList<String>>() {
                @Override
                public void onSuccess(ArrayList<String> dialogsIds, Bundle bundle) {
                    QbDialogHolder.getInstance().deleteDialogs(dialogsIds);
                    updateDialogsAdapter();
                }

                @Override
                public void onError(QBResponseException e) {

                }
            });
        }
    }

    private class PushBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent

            Log.e("Avinash", "broad id   broad");

            String message = intent.getStringExtra(GcmConsts.EXTRA_GCM_MESSAGE);

            Log.v(TAG, "Received broadcast " + intent.getAction() + " with data: " + message);
            requestBuilder.setSkip(skipRecords = 0);
            loadDialogsFromQb(true, true);
        }
    }

    private class SystemMessagesListener implements QBSystemMessageListener {
        @Override
        public void processMessage(final QBChatMessage qbChatMessage) {
            Log.e("Avinash", "qbChatMessage id   qbChatMessage");

            dialogsManager.onSystemMessageReceived(qbChatMessage);
        }

        @Override
        public void processError(QBChatException e, QBChatMessage qbChatMessage) {

        }
    }

    private class AllDialogsMessageListener extends QbChatDialogMessageListenerImp {
        @Override
        public void processMessage(final String dialogId, final QBChatMessage qbChatMessage, Integer senderId) {
            Log.e("Avinash", "sener id   " + senderId);
            if (!senderId.equals(ChatHelper.getCurrentUser().getId())) {
                dialogsManager.onGlobalMessageReceived(dialogId, qbChatMessage);
            }
        }
    }

    private void loginToChat(final QBUser user) {
        //  ProgressDialogFragment.show(getSupportFragmentManager(), R.string.dlg_restoring_chat_session);

        ChatHelper.getInstance().loginToChat(user, new QBEntityCallback<Void>() {
            @Override
            public void onSuccess(Void result, Bundle bundle) {
                Log.v("akram", "Chat login onSuccess()");

                QBChatService chatService = QBChatService.getInstance();
                if (!chatService.isLoggedIn()) {
                    Log.v("akram", "qbchatservice not login method");
                } else {
                    Log.v("akram", "qbchat service login method");
                }
            }

            @Override
            public void onError(QBResponseException e) {
                Log.w("akram", "Chat login onError(): " + e);
            }
        });
    }

}
