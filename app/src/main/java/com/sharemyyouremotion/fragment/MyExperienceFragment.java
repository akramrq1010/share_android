package com.sharemyyouremotion.fragment;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;
import com.sharemyyouremotion.R;
import com.sharemyyouremotion.activity.HomeActivity;
import com.sharemyyouremotion.activity.MyQuestionFeedbackActivity;
import com.sharemyyouremotion.adapter.MyExperienceAdapter;
import com.sharemyyouremotion.adapter.MyFeedbackAdapter;
import com.sharemyyouremotion.adapter.MyQuestionAdapter;
import com.sharemyyouremotion.connection.BaseApiService;
import com.sharemyyouremotion.connection.Connection;
import com.sharemyyouremotion.helper.ClickListener;
import com.sharemyyouremotion.helper.Constants;
import com.sharemyyouremotion.helper.DialogClass;
import com.sharemyyouremotion.helper.RecyclerTouchListener;
import com.sharemyyouremotion.helper.StaticSharedpreference;
import com.sharemyyouremotion.helper.Utilities;
import com.sharemyyouremotion.model.MyExperienceModel;
import com.sharemyyouremotion.model.MyFeedbackModel;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyExperienceFragment extends Fragment {
    View root;
    RecyclerView recyclerView;
    HomeActivity activity;
    TextView tvNoData;
    CardView cardProgress;
    DialogClass dialogClass = new DialogClass();
    Utilities utilities;
    SwipyRefreshLayout swipeRefresh;

    ArrayList<MyExperienceModel> myExperienceModels = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root = (ViewGroup) inflater.inflate(R.layout.fragment_my_question, container, false);
        activity = (HomeActivity) getActivity();
        initView();
        utilities = new Utilities(activity);


        String experience = StaticSharedpreference.getInfo(Constants.MY_EXPERIENCE_LIST, activity);

        if (!experience.equalsIgnoreCase("")) {

            if (experience.equalsIgnoreCase("no")) {
                tvNoData.setVisibility(View.VISIBLE);
                tvNoData.setText("No Experience Found");
            } else {

                Gson gson = new Gson();
                Type type = new TypeToken<List<MyExperienceModel>>() {
                }.getType();
                myExperienceModels = gson.fromJson(experience, type);

                if (myExperienceModels.size() != 0) {
                    MyExperienceAdapter myExperienceAdapter = new MyExperienceAdapter(activity, myExperienceModels);
                    recyclerView.setAdapter(myExperienceAdapter);
                } else {
                    tvNoData.setVisibility(View.VISIBLE);
                    tvNoData.setText("No Experience Found");
                }
            }
        } else {
            getMyExperience();
        }


        return root;
    }

    private void initView() {
        recyclerView = root.findViewById(R.id.recyclerView);
        tvNoData = root.findViewById(R.id.tvNoData);
        cardProgress = root.findViewById(R.id.cardProgress);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(activity);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        swipeRefresh = root.findViewById(R.id.swipeRefresh);

        swipeRefresh.setOnRefreshListener(new SwipyRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh(SwipyRefreshLayoutDirection direction) {
                swipeRefresh.setEnabled(false);
                getMyExperience();
            }
        });
    }


    public void getMyExperience() {
        swipeRefresh.setRefreshing(false);
        if (!utilities.isOnline())
            return;

        cardProgress.setVisibility(View.VISIBLE);
        Map requestBody = new HashMap<>();

        requestBody.put("UserId", StaticSharedpreference.getInt(Constants.USER_ID, activity));

        BaseApiService apiService = Connection.getBaseApiServiceInstance();
        Call<JsonObject> call = apiService.getMyExperiences(requestBody);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.body() != null && response.body().get("StatusCode").getAsInt() == 200) {

                    Gson gson = new Gson();
                    Type listType = new TypeToken<ArrayList<MyExperienceModel>>() {
                    }.getType();

                    myExperienceModels =
                            gson.fromJson(response.body().getAsJsonArray("Experiences"), listType);

                    String myExperienceStr = gson.toJson(myExperienceModels);
                    StaticSharedpreference.saveInfo(Constants.MY_EXPERIENCE_LIST, myExperienceStr, activity);

                    MyExperienceAdapter myExperienceAdapter = new MyExperienceAdapter(activity, myExperienceModels);
                    recyclerView.setAdapter(myExperienceAdapter);

                } else if (response.body() != null && response.body().get("StatusCode").getAsInt() == 404) {
                    StaticSharedpreference.saveInfo(Constants.MY_EXPERIENCE_LIST, "no", activity);
                    tvNoData.setVisibility(View.VISIBLE);
                    tvNoData.setText("No Experience Found");

                } else {
                    dialogClass.messageDialog(activity, response.body().get("Message").getAsString() + "");
                }
                cardProgress.setVisibility(View.GONE);
                swipeRefresh.setEnabled(true);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.v("okhttp", " throw = " + t);
                cardProgress.setVisibility(View.GONE);
                swipeRefresh.setEnabled(true);
            }
        });
    }
}
