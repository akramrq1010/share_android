package com.sharemyyouremotion.fragment;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import com.sharemyyouremotion.R;
import com.sharemyyouremotion.activity.SelectYourEmotionBaseActivity;
import com.sharemyyouremotion.adapter.UserListAdapter;
import com.sharemyyouremotion.connection.BaseApiService;
import com.sharemyyouremotion.connection.Connection;
import com.sharemyyouremotion.helper.ClickListener;
import com.sharemyyouremotion.helper.Constants;
import com.sharemyyouremotion.helper.DialogClass;
import com.sharemyyouremotion.helper.RecyclerTouchListener;
import com.sharemyyouremotion.helper.StaticSharedpreference;
import com.sharemyyouremotion.helper.Utilities;
import com.sharemyyouremotion.model.SpecializationModel;
import com.sharemyyouremotion.model.UserListModel;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.sharemyyouremotion.activity.SelectYourEmotionBaseActivity.toolbar;


public class UserListFragment extends EmotionBaseFragment implements View.OnClickListener {

    RecyclerView recyclerView;

    UserListAdapter userListAdapter;
    TextView tvSelectNone, tvSelectedEmotion, tvNoData;
    ArrayList<UserListModel> categoriesModels = new ArrayList<>();
    Utilities utilities;

    public UserListFragment() {
        // Required empty public constructor
    }

    public static UserListFragment newInstance() {
        return new UserListFragment();
    }

    @Override
    public FragmentId getFragmentId() {
        return FragmentId.USER_LIST_FRAGMENT;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        root = (ViewGroup) inflater.inflate(R.layout.fragment_user_list, container, false);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getActivity().getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.notificationBar));
        }

        SelectYourEmotionBaseActivity.imgRight.setVisibility(View.VISIBLE);
        toolbar.setText("Select User");
        initView();
        utilities = new Utilities(activity);
        //TODO UI
        //loadData();

        //TODO service
        getEmotionAndCategories();

        return root;
    }

    private void initView() {
        recyclerView = root.findViewById(R.id.recyclerView);
        tvSelectNone = root.findViewById(R.id.tvSelectNone);
        tvSelectedEmotion = root.findViewById(R.id.tvSelectedEmotion);
        tvNoData = root.findViewById(R.id.tvNoData);

        tvSelectedEmotion.setText(Constants.emotionStr + "/" + Constants.categoryStr + "/" + Constants.subCategoryStr);

        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(activity);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        tvSelectNone.setOnClickListener(this);
        SelectYourEmotionBaseActivity.imgRight.setOnClickListener(this);
        SelectYourEmotionBaseActivity.imgHome.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgRight:

                List<UserListModel> stList = ((UserListAdapter) userListAdapter)
                        .getUserList();
                int count = 0;
                Constants.askToIds = "";
                for (int i = 0; i < stList.size(); i++) {
                    if (stList.get(i).getChecked()) {
                        count++;
                        if (Constants.askToIds.equalsIgnoreCase(""))
                            Constants.askToIds = "" + stList.get(i).getUserId();
                        else
                            Constants.askToIds = Constants.askToIds + "," + stList.get(i).getUserId();
                    }

                }
                if (count == 0) {
                    DialogClass dialogClass = new DialogClass();
                    dialogClass.noUserSelect(activity, interactionListener);
                } else {
                    interactionListener.launchShareEmotionFragment(0);
                }
                break;

            case R.id.tvSelectNone:

                for (int i = 0; i < categoriesModels.size(); i++) {
                    categoriesModels.get(i).setChecked(false);
                }

                userListAdapter = new UserListAdapter(interactionListener, categoriesModels, activity);
                recyclerView.setAdapter(userListAdapter);
                userListAdapter.notifyDataSetChanged();

                break;

            case R.id.imgHome:
                activity.finish();
                break;
        }
    }


    public void getEmotionAndCategories() {

        if (!utilities.isOnline())
            return;

        dialogClass.progresesDialog(activity);
        Map requestBody = new HashMap<>();

     /*   requestBody.put("EmailId", edtEmail.getText().toString());
        requestBody.put("Password", edtPassword.getText().toString());*/

        Bundle bundle1 = getArguments();

        requestBody.put("UserId", StaticSharedpreference.getInt(Constants.USER_ID, activity));
        requestBody.put("CategoryId", Constants.categoryId);
        //  requestBody.put("CategoryId", bundle1.getInt("categoryId", 0));

        BaseApiService apiService = Connection.getBaseApiServiceInstance();
        Call<JsonObject> call = apiService.getUserByCategory(requestBody);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
//                Log.d("tag", "onResponse" + "" + response.body().toString());


                if (response.body() != null && response.body().get("StatusCode").getAsInt() == 200) {

                    Gson gson = new Gson();
                    Type listType = new TypeToken<ArrayList<UserListModel>>() {
                    }.getType();

                    categoriesModels =
                            gson.fromJson(response.body().getAsJsonArray("UserList"), listType);
                    userListAdapter = new UserListAdapter(interactionListener, categoriesModels, activity);
                    recyclerView.setAdapter(userListAdapter);


                } else if (response.body() != null && response.body().get("StatusCode").getAsInt() == 404) {
                    tvNoData.setVisibility(View.VISIBLE);
                    tvNoData.setText(response.body().get("Message").getAsString() + "");
                } else {
                    dialogClass.messageDialog(activity, response.body().get("Message").getAsString() + "");
                }
                DialogClass.custom_pd.dismiss();

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.v("okhttp", " throw = " + t);

                DialogClass.custom_pd.dismiss();
            }
        });
    }


    @Override
    public void onResume() {
        super.onResume();

        SelectYourEmotionBaseActivity.imgRight.setVisibility(View.VISIBLE);
        SelectYourEmotionBaseActivity.imgHome.setVisibility(View.VISIBLE);
    }

    @Override
    public void onPause() {
        super.onPause();
        SelectYourEmotionBaseActivity.imgRight.setVisibility(View.GONE);
        SelectYourEmotionBaseActivity.imgHome.setVisibility(View.GONE);
    }


    //TODo ui
    public void loadData() {

        for (int i = 0; i < 10; i++) {

            UserListModel userListModel = new UserListModel("Gream Smith");

            categoriesModels.add(userListModel);

        }
        userListAdapter = new UserListAdapter(interactionListener, categoriesModels, activity);
        recyclerView.setAdapter(userListAdapter);

    }
}
