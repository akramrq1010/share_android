package com.sharemyyouremotion.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sharemyyouremotion.R;
import com.sharemyyouremotion.activity.ChatListActivity;
import com.sharemyyouremotion.activity.HomeActivity;
import com.sharemyyouremotion.adapter.ChatAdapter;
import com.sharemyyouremotion.helper.ClickListener;
import com.sharemyyouremotion.helper.RecyclerTouchListener;

public class ChatFragment extends Fragment {
    View root;
    RecyclerView recyclerView;
    HomeActivity activity;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root = (ViewGroup) inflater.inflate(R.layout.fragment_chat, container, false);

        activity = (HomeActivity) getActivity();
        initView();

        return root;
    }


    private void initView() {
        recyclerView = root.findViewById(R.id.recyclerView);


        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(activity);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        ChatAdapter chatAdapter = new ChatAdapter();
        recyclerView.setAdapter(chatAdapter);

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(activity, recyclerView, new ClickListener() {
            @Override
            public void onClick(View view, int position) {

                // interactionListener.launchShareEmotionFragment();

                startActivity(new Intent(activity, ChatListActivity.class));
            }

            @Override
            public void onLongClick(View view, int position) {
                //  popup();
            }
        }));

    }
}
