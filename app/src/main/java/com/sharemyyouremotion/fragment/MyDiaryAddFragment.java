package com.sharemyyouremotion.fragment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RatingBar;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.sharemyyouremotion.R;
import com.sharemyyouremotion.activity.SelectYourEmotionBaseActivity;
import com.sharemyyouremotion.connection.BaseApiService;
import com.sharemyyouremotion.connection.Connection;
import com.sharemyyouremotion.helper.Constants;
import com.sharemyyouremotion.helper.DialogClass;
import com.sharemyyouremotion.helper.StaticSharedpreference;
import com.sharemyyouremotion.qucikblox.QuickBloxRequestMethod;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.sharemyyouremotion.activity.SelectYourEmotionBaseActivity.toolbar;
import static com.sharemyyouremotion.fragment.LoginFragment.guest;


/**
 * Created by abc on 9/28/2018.
 */

public class MyDiaryAddFragment extends EmotionBaseFragment implements View.OnClickListener {

    CardView btnSubmit;
    TextInputEditText edtEmotion, edtCategory, edtAction, edtDesc;
    RatingBar rating;

    public MyDiaryAddFragment() {
        // Required empty public constructor
    }

    public static MyDiaryAddFragment newInstance() {
        return new MyDiaryAddFragment();
    }

    @Override
    public FragmentId getFragmentId() {
        return FragmentId.MY_DIARY_FRAGMENT;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        root = (ViewGroup) inflater.inflate(R.layout.fragment_my_diary_add, container, false);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getActivity().getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.notificationBar));
        }

        toolbar.setText("Add Diary");
        initView();

        return root;
    }

    private void initView() {
        edtEmotion = root.findViewById(R.id.edtEmotion);
        edtCategory = root.findViewById(R.id.edtCategory);
        btnSubmit = root.findViewById(R.id.btnSubmit);
        rating = root.findViewById(R.id.rating);
        edtAction = root.findViewById(R.id.edtAction);
        edtDesc = root.findViewById(R.id.edtDesc);


        edtEmotion.setText(Constants.emotionStr);
        edtCategory.setText(Constants.categoryStr);

        btnSubmit.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnSubmit:
                if (!guest.equalsIgnoreCase("guest")) {
                    if (!isValidate())
                        return;

                    addDiary();
                } else {
                    Toast.makeText(activity, "Please sign up first", Toast.LENGTH_SHORT).show();
                }
                break;


        }
    }

    public void addDiary() {

        if (!utilities.isOnline())
            return;

        dialogClass.progresesDialog(activity);
        Map requestBody = new HashMap<>();

        SharedPreferences sharedPreferences = activity.getSharedPreferences("ShareAllYourEmotionFCM", activity.MODE_PRIVATE);
        String token = sharedPreferences.getString("fcm", "");

        requestBody.put("UserId", StaticSharedpreference.getInt(Constants.USER_ID, activity));
        requestBody.put("EmotionId", Constants.emotionId);

        requestBody.put("CategoryId", Constants.categoryId);
        requestBody.put("Discription", edtDesc.getText().toString());
        requestBody.put("Action", edtAction.getText().toString());
        requestBody.put("LevelRate", rating.getRating());


        BaseApiService apiService = Connection.getBaseApiServiceInstance();
        Call<JsonObject> call = apiService.addDiary(requestBody);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.body() != null && response.body().get("StatusCode").getAsInt() == 200) {

                    StaticSharedpreference.saveInfo(Constants.DIARY_LIST, "", activity);
                    Intent intent = new Intent(activity, SelectYourEmotionBaseActivity.class);

                    Bundle bundle = new Bundle();
                    bundle.putString("screen", "diary");
                    intent.putExtras(bundle);

                    startActivity(intent);
                    activity.finish();

                } else {
                    dialogClass.messageDialog(activity, response.body().get("Message").getAsString() + "");
                }
                DialogClass.custom_pd.dismiss();
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.v("okhttp", " throw = " + t);

                DialogClass.custom_pd.dismiss();
            }
        });
    }


    private boolean isValidate() {

        if (edtDesc.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(activity, "Please enter Description", Toast.LENGTH_LONG).show();
        } else if (edtAction.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(activity, "Please enter Action", Toast.LENGTH_LONG).show();
        } else if (rating.getRating() == 0) {
            Toast.makeText(activity, "Please set Rating", Toast.LENGTH_LONG).show();
        } else {
            return true;
        }
        return false;
    }
}

