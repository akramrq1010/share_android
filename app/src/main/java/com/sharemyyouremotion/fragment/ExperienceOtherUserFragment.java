package com.sharemyyouremotion.fragment;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.sharemyyouremotion.R;
import com.sharemyyouremotion.activity.MyQuestionFeedbackActivity;
import com.sharemyyouremotion.activity.SelectYourEmotionBaseActivity;
import com.sharemyyouremotion.adapter.ExperienceOtherUserAdapter;
import com.sharemyyouremotion.adapter.MyQuestionAdapter;
import com.sharemyyouremotion.connection.BaseApiService;
import com.sharemyyouremotion.connection.Connection;
import com.sharemyyouremotion.helper.ClickListener;
import com.sharemyyouremotion.helper.Constants;
import com.sharemyyouremotion.helper.DialogClass;
import com.sharemyyouremotion.helper.RecyclerTouchListener;
import com.sharemyyouremotion.helper.StaticSharedpreference;
import com.sharemyyouremotion.helper.Utilities;
import com.sharemyyouremotion.model.EmotionAndCategoriesModel;
import com.sharemyyouremotion.model.MyExperienceModel;
import com.sharemyyouremotion.model.MyQuestionModel;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.sharemyyouremotion.activity.SelectYourEmotionBaseActivity.toolbar;
import static com.sharemyyouremotion.activity.SelectYourEmotionBaseActivity.tvSkipORAdd;

/**
 * Created by abc on 8/15/2018.
 */

public class ExperienceOtherUserFragment extends EmotionBaseFragment implements View.OnClickListener {
    RecyclerView recyclerView;
    ArrayList<EmotionAndCategoriesModel.CategoryList> categoryLists;
    int pos;
    Utilities utilities;
    DialogClass dialogClass = new DialogClass();
    int mediaValue, categoryId;
    TextView tvNoData;
    String expType = "";

    public ExperienceOtherUserFragment() {
        // Required empty public constructor
    }

    public static ExperienceOtherUserFragment newInstance() {
        return new ExperienceOtherUserFragment();
    }

    @Override
    public FragmentId getFragmentId() {
        return FragmentId.SHARE_MY_EXPERIENCE_FRAGMENT;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        root = (ViewGroup) inflater.inflate(R.layout.fragment_my_question, container, false);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getActivity().getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.notificationBar));
        }

        Bundle bundle = getArguments();

        mediaValue = bundle.getInt("media", 0);
        categoryId = bundle.getInt("categoryId", 0);

        if (mediaValue == 1)
            expType = "text";
        else if (mediaValue == 2)
            expType = "audio";
        else
            expType = "video";

        toolbar.setText("Experience");
        initView();

        utilities = new Utilities(activity);
        getExperienceByType();

        return root;
    }

    private void initView() {
        recyclerView = root.findViewById(R.id.recyclerView);
        tvNoData = root.findViewById(R.id.tvNoData);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(activity);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        tvSkipORAdd.setVisibility(View.VISIBLE);
        if (HomeFragment.isSend == 1)

            tvSkipORAdd.setText("Skip");
        else
            tvSkipORAdd.setText("Add New");

        tvSkipORAdd.setOnClickListener(this);
        SelectYourEmotionBaseActivity.imgHome.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvSkipORAdd:

                if (tvSkipORAdd.getText().toString().equalsIgnoreCase("Skip")) {
                    interactionListener.launchUserListFragment(categoryId);
                } else {
                    interactionListener.launchShareEmotionFragment(mediaValue);
                }

                break;

            case R.id.imgHome:

                activity.finish();
                break;
        }
    }


    public void getExperienceByType() {

        if (!utilities.isOnline())
            return;

        dialogClass.progresesDialog(activity);

        Map requestBody = new HashMap<>();
        requestBody.put("UserId", StaticSharedpreference.getInt(Constants.USER_ID, activity));
        requestBody.put("EmotionId", Constants.emotionId);
        requestBody.put("CategoryId", Constants.categoryId);
        requestBody.put("SubCategoryId", Constants.subCategoryId);
        requestBody.put("ExperienceType", expType);

        BaseApiService apiService = Connection.getBaseApiServiceInstance();
        Call<JsonObject> call = apiService.getExperiencesByType(requestBody);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.body() != null && response.body().get("StatusCode").getAsInt() == 200) {

                    Gson gson = new Gson();
                    Type listType = new TypeToken<ArrayList<MyExperienceModel>>() {
                    }.getType();

                    ArrayList<MyExperienceModel> myExperienceModels =
                            gson.fromJson(response.body().getAsJsonArray("Experiences"), listType);

                    ExperienceOtherUserAdapter experienceOtherUserAdapter = new ExperienceOtherUserAdapter(mediaValue, myExperienceModels, activity);
                    recyclerView.setAdapter(experienceOtherUserAdapter);

                } else if (response.body() != null && response.body().get("StatusCode").getAsInt() == 404) {
                    tvNoData.setVisibility(View.VISIBLE);
                    tvNoData.setText(response.body().get("Message").getAsString() + "");
                } else {
                    dialogClass.messageDialog(activity, response.body().get("Message").getAsString() + "");
                }
                DialogClass.custom_pd.dismiss();
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.v("okhttp", " throw = " + t);

                DialogClass.custom_pd.dismiss();
            }
        });
    }


    @Override
    public void onPause() {
        super.onPause();
        tvSkipORAdd.setVisibility(View.GONE);
        SelectYourEmotionBaseActivity.imgHome.setVisibility(View.GONE);
    }

    @Override
    public void onResume() {
        super.onResume();
        tvSkipORAdd.setVisibility(View.VISIBLE);
        SelectYourEmotionBaseActivity.imgHome.setVisibility(View.VISIBLE);

    }
}
