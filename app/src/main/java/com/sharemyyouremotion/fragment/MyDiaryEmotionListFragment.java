package com.sharemyyouremotion.fragment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RatingBar;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.sharemyyouremotion.R;
import com.sharemyyouremotion.activity.SelectYourEmotionBaseActivity;
import com.sharemyyouremotion.adapter.MyDiaryAdapter;
import com.sharemyyouremotion.connection.BaseApiService;
import com.sharemyyouremotion.connection.Connection;
import com.sharemyyouremotion.helper.Constants;
import com.sharemyyouremotion.helper.DialogClass;
import com.sharemyyouremotion.helper.StaticSharedpreference;
import com.sharemyyouremotion.model.GetMyDiaryModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.sharemyyouremotion.activity.SelectYourEmotionBaseActivity.toolbar;

/**
 * Created by abc on 9/28/2018.
 */

public class MyDiaryEmotionListFragment extends EmotionBaseFragment {

    RecyclerView recyclerView;
    ArrayList<GetMyDiaryModel> getMyDiaryModels = new ArrayList<>();
    SwipyRefreshLayout swipeRefresh;

    public MyDiaryEmotionListFragment() {
        // Required empty public constructor
    }

    public static MyDiaryEmotionListFragment newInstance() {
        return new MyDiaryEmotionListFragment();
    }

    @Override
    public FragmentId getFragmentId() {
        return FragmentId.MY_DIARY_FRAGMENT;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        root = (ViewGroup) inflater.inflate(R.layout.fragment_my_question, container, false);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getActivity().getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.notificationBar));
        }

        toolbar.setText("My Diary");
        getData();
        initView();

        return root;
    }


    private void initView() {
        recyclerView = root.findViewById(R.id.recyclerView);
        swipeRefresh = root.findViewById(R.id.swipeRefresh);

        swipeRefresh.setEnabled(false);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(activity);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        MyDiaryAdapter myDiaryAdapter = new MyDiaryAdapter(activity, MyDiaryFragment.getMyDiaryModels1);
        recyclerView.setAdapter(myDiaryAdapter);

    }

    private void getData() {
        Bundle bundle = activity.getIntent().getExtras();
        getMyDiaryModels = (ArrayList<GetMyDiaryModel>) bundle.getSerializable("diary");
    }


}

