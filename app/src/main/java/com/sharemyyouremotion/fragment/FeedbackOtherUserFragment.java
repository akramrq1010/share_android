package com.sharemyyouremotion.fragment;

import android.app.ActivityOptions;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.sharemyyouremotion.R;
import com.sharemyyouremotion.activity.MyProfileActivity;
import com.sharemyyouremotion.activity.MyQuestionFeedbackActivity;
import com.sharemyyouremotion.activity.SelectYourEmotionBaseActivity;
import com.sharemyyouremotion.adapter.FilterDataAdapter;
import com.sharemyyouremotion.adapter.MyQuestionAdapter;
import com.sharemyyouremotion.adapter.SelectYourEmotionAdapter;
import com.sharemyyouremotion.connection.BaseApiService;
import com.sharemyyouremotion.connection.Connection;
import com.sharemyyouremotion.helper.ClickListener;
import com.sharemyyouremotion.helper.Constants;
import com.sharemyyouremotion.helper.DialogClass;
import com.sharemyyouremotion.helper.RecyclerTouchListener;
import com.sharemyyouremotion.helper.StaticSharedpreference;
import com.sharemyyouremotion.model.EmotionAndCategoriesModel;
import com.sharemyyouremotion.model.FilterDataModel;
import com.sharemyyouremotion.model.MyQuestionModel;
import com.sharemyyouremotion.model.SpecializationModel;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.sharemyyouremotion.activity.SelectYourEmotionBaseActivity.toolbar;
import static com.sharemyyouremotion.activity.SelectYourEmotionBaseActivity.tvSkipORAdd;


public class FeedbackOtherUserFragment extends EmotionBaseFragment implements View.OnClickListener {

    RecyclerView recyclerView, recycler_view_filter;
    CardView cardProgress;
    TextView tvNoData, txt_clear_all;
    String expType = "";
    SwipyRefreshLayout swipeRefresh;
    ImageView imgFilter;
    ArrayList<FilterDataModel> filters = new ArrayList<>();
    ArrayAdapter adapter1_filer;
    FilterDataAdapter filterDataAdapter;
    ArrayList<MyQuestionModel> myQuestionModels;
    Typeface tfavv;

    public FeedbackOtherUserFragment() {
        // Required empty public constructor
    }

    public static FeedbackOtherUserFragment newInstance() {
        return new FeedbackOtherUserFragment();
    }

    @Override
    public FragmentId getFragmentId() {
        return FragmentId.FEEDBACK_OTHER_USER_FRAGMENT;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        root = (ViewGroup) inflater.inflate(R.layout.fragment_feedback_other_user, container, false);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getActivity().getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.notificationBar));
        }
        getData();
        toolbar.setText("Feedback");
        initView();

        if (Constants.emotionLists.size() == 0 || Constants.emotionListName.size() == 0) {

            getEmotionAndCategories();

        } else {

            getQuestionsByType("0", "0", "0", "");

        }
        return root;
    }


    private void initView() {
        recyclerView = root.findViewById(R.id.recyclerView);
        cardProgress = root.findViewById(R.id.cardProgress);
        tvNoData = root.findViewById(R.id.tvNoData);
        imgFilter = root.findViewById(R.id.imgFilter);
        recycler_view_filter = root.findViewById(R.id.recycler_view_filter);
        txt_clear_all = root.findViewById(R.id.txt_clear_all);


        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(activity);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), OrientationHelper.HORIZONTAL, false);
        recycler_view_filter.setLayoutManager(linearLayoutManager);
        recycler_view_filter.setItemAnimator(new DefaultItemAnimator());


        SelectYourEmotionBaseActivity.imgHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.finish();
            }
        });

        imgFilter.setOnClickListener(this);
        txt_clear_all.setOnClickListener(this);
        //  MyQuestionAdapter myQuestionAdapter = new MyQuestionAdapter();
        // recyclerView.setAdapter(myQuestionAdapter);

        recycler_view_filter.addOnItemTouchListener(new RecyclerTouchListener(activity, recycler_view_filter, new ClickListener() {
            @Override
            public void onClick(View view, int position) {

                delete_item(position);
            }

            @Override
            public void onLongClick(View view, int position) {
                //  popup();
            }
        }));

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imgFilter:

                filterDialog(activity);
                break;
            case R.id.txt_clear_all:
                txt_clear_all.setVisibility(View.GONE);
                filters.clear();
                filterDataAdapter.notifyDataSetChanged();
                getQuestionsByType("0", "0", "0", "");
                break;
        }
    }

    public void getQuestionsByType(String emotionId, String categoryId, String subCategoryId, String questionType) {

        if (!utilities.isOnline())
            return;

        cardProgress.setVisibility(View.VISIBLE);

        Map requestBody = new HashMap<>();
        requestBody.put("UserId", StaticSharedpreference.getInt(Constants.USER_ID, activity));
        requestBody.put("EmotionId", emotionId);
        requestBody.put("CategoryId", categoryId);
        requestBody.put("SubCategoryId", subCategoryId);
        requestBody.put("QuestionType", questionType);

        BaseApiService apiService = Connection.getBaseApiServiceInstance();
        Call<JsonObject> call = apiService.getQuestionsByType(requestBody);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.body() != null && response.body().get("StatusCode").getAsInt() == 200) {

                    /*check specialization is fill or not*/
                    tvNoData.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);
                    Gson gson = new Gson();
                    Type listType = new TypeToken<ArrayList<MyQuestionModel>>() {
                    }.getType();

                    myQuestionModels =
                            gson.fromJson(response.body().getAsJsonArray("Questions"), listType);

                    MyQuestionAdapter myQuestionAdapter = new MyQuestionAdapter(activity, myQuestionModels, "other");
                    recyclerView.setAdapter(myQuestionAdapter);


                } else if (response.body() != null && response.body().get("StatusCode").getAsInt() == 404) {
                    tvNoData.setVisibility(View.VISIBLE);
                    tvNoData.setText(response.body().get("Message").getAsString() + "");
                    myQuestionModels.clear();
                    recyclerView.setVisibility(View.GONE);

                } else {
                    dialogClass.messageDialog(activity, response.body().get("Message").getAsString() + "");
                }
                cardProgress.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.v("okhttp", " throw = " + t);

                cardProgress.setVisibility(View.GONE);
            }
        });
    }


    public void getEmotionAndCategories() {
        dialogClass.progresesDialog(activity);

        BaseApiService apiService = Connection.getBaseApiServiceInstance();
        Call<EmotionAndCategoriesModel> call = apiService.getEmotionAndCategories();
        call.enqueue(new Callback<EmotionAndCategoriesModel>() {
            @Override
            public void onResponse(Call<EmotionAndCategoriesModel> call, Response<EmotionAndCategoriesModel> response) {
//                Log.d("tag", "onResponse" + "" + response.body().toString());

                if (response.body() != null && response.body().getStatusCode() == 200) {

                    DialogClass.custom_pd.dismiss();

                    /*check specialization fill or not*/

                    Gson gson = new Gson();
                    Type listType = new TypeToken<ArrayList<SpecializationModel>>() {
                    }.getType();

                    Constants.emotionLists = response.body().getEmotionLists();
                    Constants.categoryLists = response.body().getCategoryLists();


                    for (int i = 0; i < Constants.emotionLists.size(); i++) {
                        Constants.emotionListName.add(Constants.emotionLists.get(i).getEmotionName());
                    }

                    for (int i = 0; i < Constants.categoryLists.size(); i++) {
                        Constants.categoryListName.add(Constants.categoryLists.get(i).getCategoryName());
                    }

                 /*   SelectYourEmotionAdapter selectYourEmotionAdapter = new SelectYourEmotionAdapter(activity, Constants.emotionLists);
                    recyclerView.setAdapter(selectYourEmotionAdapter);
*/
                    getQuestionsByType("0", "0", "0", "");

                } else {
                    Toast.makeText(activity, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
                DialogClass.custom_pd.dismiss();

                /* JsonObject response = new  JsonParser().parse(out.body()).getAsJsonObject();*/

            }

            @Override
            public void onFailure(Call<EmotionAndCategoriesModel> call, Throwable t) {
                Log.v("okhttp", " throw = " + t);

                DialogClass.custom_pd.dismiss();
            }
        });
    }

    public void delete_item(int position) {

        String data = filters.get(position).getData();
        int type = Integer.parseInt(filters.get(position).getType());
        switch (type) {
            case 1:
                emotionId = "0";
                break;
            case 2:
                categoryId = "0";
                break;
            case 3:
                subcategoryid = "0";
                break;

            case 4:
                questionType = "";
                break;


        }
        filters.remove(position);

        filterDataAdapter.notifyDataSetChanged();
        if (filters.size() == 0) {
            txt_clear_all.setVisibility(View.GONE);
        } else {
            txt_clear_all.setVisibility(View.VISIBLE);
        }

        getQuestionsByType(emotionId, categoryId, subcategoryid, questionType);

    }


    String emotionId = "0", categoryId = "0", subcategoryid = "0";
    int categorypos;
    String questionType = "text";

    public Dialog filterDialog(final Context context) {

        LayoutInflater factory = LayoutInflater.from(context);
        final View view = factory.inflate(R.layout.dialog_filter, null);
        final AlertDialog alertDialoge = new AlertDialog.Builder(context).create();

        LinearLayout linearText = view.findViewById(R.id.linearText);
        LinearLayout linearAudio = view.findViewById(R.id.linearAudio);
        LinearLayout linearVideo = view.findViewById(R.id.linearVideo);

        final AutoCompleteTextView edtSubcategory = view.findViewById(R.id.edtSubcategory);
        final AutoCompleteTextView edtCategory = view.findViewById(R.id.edtCategory);
        final AutoCompleteTextView edtEmotion = view.findViewById(R.id.edtEmotion);
        CardView cardApply = view.findViewById(R.id.cardApply);
        CardView cardCancel = view.findViewById(R.id.cardCancel);
        final RelativeLayout relative_video = view.findViewById(R.id.relative_video);
        final RelativeLayout relative_audio = view.findViewById(R.id.relative_audio);
        final RelativeLayout relative_text = view.findViewById(R.id.relative_text);


    /*    ArrayAdapter adapter = new ArrayAdapter<String>(activity,
                android.R.layout.simple_list_item_1, Constants.emotionListName);
        edtEmotion.setAdapter(adapter);*/

        spinnerEmotion(edtEmotion);
        spinnerCategory(edtCategory);

      /*  ArrayAdapter adapter1 = new ArrayAdapter<String>(activity,
                android.R.layout.simple_list_item_1, Constants.categoryListName);
        edtCategory.setAdapter(adapter1);*/

        edtEmotion.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                emotionId = Constants.emotionLists.get(i).getEmotionId() + "";
                edtCategory.setText("");
                edtSubcategory.setText("");

                categoryId = "0";
                subcategoryid = "0";

            }
        });


        edtCategory.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                categorypos = i;
                categoryId = Constants.categoryLists.get(i).getCategoryId() + "";
                Constants.subCategoryListName.clear();
                edtSubcategory.setText("");
                for (int i1 = 0; i1 < Constants.categoryLists.get(i).getSubCategories().size(); i1++) {

                    Constants.subCategoryListName.add(Constants.categoryLists.get(i).getSubCategories().get(i1).getSubCategoryName());

                }
                spinnerSubCategory(edtSubcategory);
               /*
                adapter1_filer = new ArrayAdapter<String>(activity,
                        android.R.layout.simple_list_item_1, Constants.subCategoryListName);
                edtSubcategory.setAdapter(adapter1_filer);*/

                subcategoryid = "0";

            }
        });

        edtSubcategory.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                subcategoryid = Constants.categoryLists.get(categorypos).getSubCategories().get(i).getSubCategoryId() + "";
            }
        });


        edtEmotion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edtEmotion.showDropDown();
            }
        });
        edtCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edtCategory.showDropDown();
            }
        });
        edtSubcategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edtSubcategory.showDropDown();
            }
        });


        cardApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                filters.clear();
                if (!edtEmotion.getText().toString().equalsIgnoreCase(""))
                    filters.add(new FilterDataModel(edtEmotion.getText().toString(), "1"));

                if (!edtCategory.getText().toString().equalsIgnoreCase(""))
                    filters.add(new FilterDataModel(edtCategory.getText().toString(), "2"));

                if (!edtSubcategory.getText().toString().equalsIgnoreCase(""))
                    filters.add(new FilterDataModel(edtSubcategory.getText().toString(), "3"));

                filters.add(new FilterDataModel(questionType, "4"));

                if (filters.size() == 0) {
                    recycler_view_filter.setVisibility(View.GONE);
                    txt_clear_all.setVisibility(View.GONE);
                } else {
                    recycler_view_filter.setVisibility(View.VISIBLE);
                    txt_clear_all.setVisibility(View.VISIBLE);
                    filterDataAdapter = new FilterDataAdapter(activity, filters);
                    recycler_view_filter.setAdapter(filterDataAdapter);
                }


                getQuestionsByType(emotionId, categoryId, subcategoryid, questionType);
                //  setFilterData();

                alertDialoge.dismiss();
            }
        });

        cardCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialoge.dismiss();
            }
        });


        linearText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                questionType = "text";
                relative_text.setBackground(activity.getResources().getDrawable(R.drawable.ripple_oval));
                relative_audio.setBackground(activity.getResources().getDrawable(R.drawable.ripple_oval_light));
                relative_video.setBackground(activity.getResources().getDrawable(R.drawable.ripple_oval_light));

            }
        });
        linearAudio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                questionType = "audio";
                relative_audio.setBackground(activity.getResources().getDrawable(R.drawable.ripple_oval));
                relative_text.setBackground(activity.getResources().getDrawable(R.drawable.ripple_oval_light));
                relative_video.setBackground(activity.getResources().getDrawable(R.drawable.ripple_oval_light));


            }
        });
        linearVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                questionType = "video";
                relative_video.setBackground(activity.getResources().getDrawable(R.drawable.ripple_oval));
                relative_text.setBackground(activity.getResources().getDrawable(R.drawable.ripple_oval_light));
                relative_audio.setBackground(activity.getResources().getDrawable(R.drawable.ripple_oval_light));
            }
        });

        alertDialoge.setCancelable(false);
        alertDialoge.setCanceledOnTouchOutside(true);
        alertDialoge.setView(view);

        alertDialoge.show();
        return alertDialoge;
    }

    public <ViewGroup> void spinnerEmotion(AutoCompleteTextView autoCompleteTextView) {

        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(activity, android.R.layout.simple_list_item_1, Constants.emotionListName) {
            public View getView(int position, View convertView, android.view.ViewGroup parent) {
                tfavv = Typeface.createFromAsset(activity.getAssets(), "COMIC.TTF");
                TextView v = (TextView) super.getView(position, convertView, parent);
                v.setTypeface(tfavv);
                return v;
            }

            public View getDropDownView(int position, View convertView, android.view.ViewGroup parent) {
                TextView v = (TextView) super.getView(position, convertView, parent);
                v.setTypeface(tfavv);
                return v;
            }
        };

        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        autoCompleteTextView.setAdapter(adapter1);
    }

    public <ViewGroup> void spinnerCategory(AutoCompleteTextView autoCompleteTextView) {

        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(activity, android.R.layout.simple_list_item_1, Constants.categoryListName) {
            public View getView(int position, View convertView, android.view.ViewGroup parent) {
                tfavv = Typeface.createFromAsset(activity.getAssets(), "COMIC.TTF");
                TextView v = (TextView) super.getView(position, convertView, parent);
                v.setTypeface(tfavv);
                return v;
            }

            public View getDropDownView(int position, View convertView, android.view.ViewGroup parent) {
                TextView v = (TextView) super.getView(position, convertView, parent);
                v.setTypeface(tfavv);
                return v;
            }
        };

        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        autoCompleteTextView.setAdapter(adapter1);
    }

    public <ViewGroup> void spinnerSubCategory(AutoCompleteTextView autoCompleteTextView) {

        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(activity, android.R.layout.simple_list_item_1, Constants.subCategoryListName) {
            public View getView(int position, View convertView, android.view.ViewGroup parent) {
                tfavv = Typeface.createFromAsset(activity.getAssets(), "COMIC.TTF");
                TextView v = (TextView) super.getView(position, convertView, parent);
                v.setTypeface(tfavv);
                return v;
            }

            public View getDropDownView(int position, View convertView, android.view.ViewGroup parent) {
                TextView v = (TextView) super.getView(position, convertView, parent);
                v.setTypeface(tfavv);
                return v;
            }
        };

        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        autoCompleteTextView.setAdapter(adapter1);
    }

    private void setFilterData() {

        String emotion = "", category = "", subcategory = "", text = "", audio = "", video = "";

        for (int i = 0; i < filters.size(); i++) {
            if (filters.get(i).getType().equalsIgnoreCase("1"))
                emotion = filters.get(i).getData();
            else if (filters.get(i).getType().equalsIgnoreCase("2"))
                category = filters.get(i).getData();
            else if (filters.get(i).getType().equalsIgnoreCase("3"))
                subcategory = filters.get(i).getData();
            else if (filters.get(i).getType().equalsIgnoreCase("4"))
                text = filters.get(i).getData();
            else if (filters.get(i).getType().equalsIgnoreCase("5"))
                audio = filters.get(i).getData();
            else if (filters.get(i).getType().equalsIgnoreCase("6"))
                video = filters.get(i).getData();

        }

        for (int i = 0; i < myQuestionModels.size(); i++) {


            //   if (emotion.equalsIgnoreCase(myQuestionModels.get(i).getEmotionName()))

            // if (category.equalsIgnoreCase(myQuestionModels.get(i).getCategoryName()))


        }
    }


    private void getData() {
        Bundle bundle = getArguments();

        int mediaValue = bundle.getInt("media", 0);

        if (mediaValue == 1)
            expType = "text";
        else if (mediaValue == 2)
            expType = "audio";
        else
            expType = "video";
    }

    @Override
    public void onPause() {
        super.onPause();

        SelectYourEmotionBaseActivity.imgHome.setVisibility(View.GONE);
    }

    @Override
    public void onResume() {
        super.onResume();

        SelectYourEmotionBaseActivity.imgHome.setVisibility(View.GONE);

    }


}
