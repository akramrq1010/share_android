package com.sharemyyouremotion.fragment;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;
import com.sharemyyouremotion.R;
import com.sharemyyouremotion.activity.AudioTestActivity;
import com.sharemyyouremotion.activity.HomeActivity;
import com.sharemyyouremotion.activity.MyQuestionFeedbackActivity;
import com.sharemyyouremotion.activity.WebViewForVideoActivity;
import com.sharemyyouremotion.adapter.MyQuestionAdapter;
import com.sharemyyouremotion.adapter.NotificationAdapter;
import com.sharemyyouremotion.connection.BaseApiService;
import com.sharemyyouremotion.connection.Connection;
import com.sharemyyouremotion.helper.ClickListener;
import com.sharemyyouremotion.helper.Constants;
import com.sharemyyouremotion.helper.DialogClass;
import com.sharemyyouremotion.helper.RecyclerTouchListener;
import com.sharemyyouremotion.helper.StaticSharedpreference;
import com.sharemyyouremotion.helper.Utilities;
import com.sharemyyouremotion.model.GetMyDiaryModel;
import com.sharemyyouremotion.model.MyQuestionModel;
import com.sharemyyouremotion.model.NotificationModel;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MyQuestionFragment extends Fragment {
    View root;
    RecyclerView recyclerView;
    HomeActivity activity;
    public static int screenValue = 0;

    DialogClass dialogClass = new DialogClass();
    TextView tvNoData;
    CardView cardProgress;
    Utilities utilities;
    SwipyRefreshLayout swipeRefresh;
    ArrayList<MyQuestionModel> questionModels = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root = (ViewGroup) inflater.inflate(R.layout.fragment_my_question, container, false);
        activity = (HomeActivity) getActivity();
        initView();
        utilities = new Utilities(activity);


        String question = StaticSharedpreference.getInfo(Constants.MY_QUESTION_LIST, activity);

        if (!question.equalsIgnoreCase("")) {

            if (question.equalsIgnoreCase("no")) {
                tvNoData.setVisibility(View.VISIBLE);
                tvNoData.setText("No Question Found");
            } else{
                Gson gson = new Gson();
            Type type = new TypeToken<List<MyQuestionModel>>() {
            }.getType();
            questionModels = gson.fromJson(question, type);

            if (questionModels.size() != 0) {
                MyQuestionAdapter myQuestionAdapter = new MyQuestionAdapter(activity, questionModels, "my");
                recyclerView.setAdapter(myQuestionAdapter);
            } else {
                tvNoData.setVisibility(View.VISIBLE);
                tvNoData.setText("No Question Found");
            }
        }
        } else {
            getMyQuestion();
        }

        return root;
    }

    private void initView() {
        recyclerView = root.findViewById(R.id.recyclerView);
        cardProgress = root.findViewById(R.id.cardProgress);
        tvNoData = root.findViewById(R.id.tvNoData);
        swipeRefresh = (SwipyRefreshLayout) root.findViewById(R.id.swipeRefresh);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(activity);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        swipeRefresh.setOnRefreshListener(new SwipyRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh(SwipyRefreshLayoutDirection direction) {
                swipeRefresh.setEnabled(false);
                getMyQuestion();
            }
        });

    }

    public void getMyQuestion() {

        swipeRefresh.setRefreshing(false);
        if (!utilities.isOnline())
            return;

        cardProgress.setVisibility(View.VISIBLE);

        Map requestBody = new HashMap<>();

        requestBody.put("UserId", StaticSharedpreference.getInt(Constants.USER_ID, activity));

        BaseApiService apiService = Connection.getBaseApiServiceInstance();
        Call<JsonObject> call = apiService.getMyQuestions(requestBody);
        call.enqueue(new Callback<JsonObject>() {

            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.body() != null && response.body().get("StatusCode").getAsInt() == 200) {


                    Gson gson = new Gson();
                    Type listType = new TypeToken<ArrayList<MyQuestionModel>>() {
                    }.getType();

                    questionModels =
                            gson.fromJson(response.body().getAsJsonArray("Questions"), listType);

                    String myQuestionListStr = gson.toJson(questionModels);
                    StaticSharedpreference.saveInfo(Constants.MY_QUESTION_LIST, myQuestionListStr, activity);

                    MyQuestionAdapter myQuestionAdapter = new MyQuestionAdapter(activity, questionModels, "my");
                    recyclerView.setAdapter(myQuestionAdapter);
                    //swipeRefresh.setRefreshing(true);

                } else if (response.body() != null && response.body().get("StatusCode").getAsInt() == 404) {
                    StaticSharedpreference.saveInfo(Constants.MY_QUESTION_LIST, "no", activity);
                    tvNoData.setVisibility(View.VISIBLE);
                    tvNoData.setText("No Question Found");
                } else {
                    dialogClass.messageDialog(activity, response.body().get("Message").getAsString() + "");
                }
                swipeRefresh.setEnabled(true);
                cardProgress.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.v("okhttp", " throw = " + t);
                swipeRefresh.setEnabled(true);
                cardProgress.setVisibility(View.GONE);
            }
        });
    }
}
