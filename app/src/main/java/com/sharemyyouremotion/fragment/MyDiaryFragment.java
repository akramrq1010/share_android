package com.sharemyyouremotion.fragment;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.animation.EasingFunction;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.MPPointF;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.sharemyyouremotion.R;
import com.sharemyyouremotion.activity.OtpActivity;
import com.sharemyyouremotion.connection.BaseApiService;
import com.sharemyyouremotion.connection.Connection;
import com.sharemyyouremotion.helper.Constants;
import com.sharemyyouremotion.helper.DialogClass;
import com.sharemyyouremotion.helper.StaticSharedpreference;
import com.sharemyyouremotion.helper.TimeConveter;
import com.sharemyyouremotion.model.EmotionAndCategoriesModel;
import com.sharemyyouremotion.model.GetMyDiaryModel;
import com.sharemyyouremotion.model.SpecializationModel;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.sharemyyouremotion.activity.SelectYourEmotionBaseActivity.toolbar;

/**
 * Created by abc on 9/28/2018.
 */

public class MyDiaryFragment extends EmotionBaseFragment implements View.OnClickListener, OnChartValueSelectedListener {

    CardView btnAddDiary;
    private PieChart mChart;
    Spinner edtEmotion;
    public static ArrayList<GetMyDiaryModel> getMyDiaryModels = new ArrayList<>();
    ArrayList<String> arrayListEmotionName = new ArrayList();
    ArrayList<String> arrayListCategoryName = new ArrayList();
    Map emotionlist = new HashMap();
    public static ArrayList<GetMyDiaryModel> getMyDiaryModels1 = new ArrayList<>();
    TextView tvNoData;

    RadioGroup radioGroup;
    RadioButton radioButtonAll, radioButtonMonthly, radioButtonWeekly, radioButtonToday;
    String duration = "All";
    Typeface tfavv;

    public MyDiaryFragment() {

        // Required empty public constructor
    }

    public static MyDiaryFragment newInstance() {
        return new MyDiaryFragment();
    }


    @Override
    public FragmentId getFragmentId() {
        return FragmentId.MY_DIARY_FRAGMENT;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        root = (ViewGroup) inflater.inflate(R.layout.fragment_my_diary, container, false);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getActivity().getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.notificationBar));
        }

        toolbar.setText("My Diary");
        initView();

        String diary = StaticSharedpreference.getInfo(Constants.DIARY_LIST, activity);

        if (!diary.equalsIgnoreCase("")) {

            Gson gson = new Gson();
            Type type = new TypeToken<List<GetMyDiaryModel>>() {
            }.getType();
            getMyDiaryModels = gson.fromJson(diary, type);

            setDataApi();
        } else {
            GetMyDailyDairy();
        }


        return root;
    }

    private void initView() {
        btnAddDiary = root.findViewById(R.id.btnAddDiary);
        edtEmotion = root.findViewById(R.id.edtEmotion);
        tvNoData = root.findViewById(R.id.tvNoData);
        radioGroup = root.findViewById(R.id.radioGroup);
        radioButtonAll = root.findViewById(R.id.radioButtonAll);
        radioButtonMonthly = root.findViewById(R.id.radioButtonMonthly);
        radioButtonToday = root.findViewById(R.id.radioButtonToday);
        radioButtonWeekly = root.findViewById(R.id.radioButtonWeekly);

        setClickListener();

        //initialize chart
        setChartData();
    }

    public <ViewGroup> void spinner2meth() {

        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(activity, android.R.layout.simple_list_item_1, arrayListCategoryName) {
            public View getView(int position, View convertView, android.view.ViewGroup parent) {
                tfavv = Typeface.createFromAsset(activity.getAssets(), "COMIC.TTF");
                TextView v = (TextView) super.getView(position, convertView, parent);
                v.setTypeface(tfavv);
                return v;
            }

            public View getDropDownView(int position, View convertView, android.view.ViewGroup parent) {
                TextView v = (TextView) super.getView(position, convertView, parent);
                v.setTypeface(tfavv);
                return v;
            }
        };

        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        edtEmotion.setAdapter(adapter1);
    }

    private void setClickListener() {
        radioButtonAll.setChecked(true);

        btnAddDiary.setOnClickListener(this);
        radioButtonAll.setOnClickListener(this);
        radioButtonToday.setOnClickListener(this);
        radioButtonWeekly.setOnClickListener(this);
        radioButtonMonthly.setOnClickListener(this);

        edtEmotion.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i == 0) {
                    filterEmotionByCategory("All", duration);
                } else {
                    String categoryName = arrayListCategoryName.get(i);
                    filterEmotionByCategory(categoryName, duration);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnAddDiary:
                interactionListener.launchSelectYourEmotionFragment("diary");
                break;
            case R.id.radioButtonAll:
                radioButtonToday.setChecked(false);
                radioButtonWeekly.setChecked(false);
                radioButtonAll.setChecked(true);
                radioButtonMonthly.setChecked(false);
                duration = "All";
                filterEmotionByCategory(edtEmotion.getSelectedItem().toString(), "All");
                break;

            case R.id.radioButtonToday:
                radioButtonToday.setChecked(true);
                radioButtonWeekly.setChecked(false);
                radioButtonAll.setChecked(false);
                radioButtonMonthly.setChecked(false);
                duration = "today";
                filterEmotionByCategory(edtEmotion.getSelectedItem().toString(), "today");
                break;

            case R.id.radioButtonWeekly:
                radioButtonToday.setChecked(false);
                radioButtonWeekly.setChecked(true);
                radioButtonAll.setChecked(false);
                radioButtonMonthly.setChecked(false);
                duration = "weekly";
                filterEmotionByCategory(edtEmotion.getSelectedItem().toString(), "weekly");
                break;
            case R.id.radioButtonMonthly:
                radioButtonToday.setChecked(false);
                radioButtonWeekly.setChecked(false);
                radioButtonAll.setChecked(false);
                radioButtonMonthly.setChecked(true);
                duration = "monthly";
                filterEmotionByCategory(edtEmotion.getSelectedItem().toString(), "monthly");
                break;
        }
    }


    public void GetMyDailyDairy() {

        dialogClass.progresesDialog(activity);

        Map requestBody = new HashMap<>();

        requestBody.put("UserId", StaticSharedpreference.getInt(Constants.USER_ID, activity));


        BaseApiService apiService = Connection.getBaseApiServiceInstance();
        Call<JsonObject> call = apiService.GetMyDailyDairy(requestBody);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
//                Log.d("tag", "onResponse" + "" + response.body().toString());

                if (response.body() != null && response.body().get("StatusCode").getAsInt() == 200) {

                    DialogClass.custom_pd.dismiss();

                    Gson gson = new Gson();
                    Type listType = new TypeToken<ArrayList<GetMyDiaryModel>>() {
                    }.getType();
                    getMyDiaryModels.clear();


                    if (response.body().get("DailyDairy").isJsonNull()) {

                        tvNoData.setVisibility(View.VISIBLE);
                        mChart.setVisibility(View.GONE);
                        return;
                    }

                    getMyDiaryModels = (ArrayList<GetMyDiaryModel>) gson.fromJson(response.body().getAsJsonArray("DailyDairy"), listType);

                    String diaryDatastr = gson.toJson(getMyDiaryModels);
                    StaticSharedpreference.saveInfo(Constants.DIARY_LIST, diaryDatastr, activity);

                    setDataApi();

                } else {
                    Toast.makeText(activity, response.body().get("Message").getAsString() + "", Toast.LENGTH_SHORT).show();
                }
                DialogClass.custom_pd.dismiss();

                /* JsonObject response = new  JsonParser().parse(out.body()).getAsJsonObject();*/

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.v("okhttp", " throw = " + t);

                DialogClass.custom_pd.dismiss();
            }
        });
    }

    private void setChartData() {

        mChart = root.findViewById(R.id.chart1);
        mChart.setUsePercentValues(true);
        mChart.getDescription().setEnabled(false);
        mChart.setExtraOffsets(5, 10, 5, 5);

        mChart.setDragDecelerationFrictionCoef(0.95f);

        //  mChart.setCenterTextTypeface(mTfLight);
        //  mChart.setCenterText(generateCenterSpannableText());

        mChart.setDrawHoleEnabled(true);
        mChart.setHoleColor(Color.WHITE);

        mChart.setTransparentCircleColor(Color.WHITE);
        mChart.setTransparentCircleAlpha(110);

        mChart.setHoleRadius(30f);
        mChart.setTransparentCircleRadius(30f);

        tfavv = Typeface.createFromAsset(activity.getAssets(), "COMIC.TTF");
        mChart.setCenterTextTypeface(tfavv);
        mChart.setEntryLabelTypeface(tfavv);
        mChart.setNoDataTextTypeface(tfavv);

        mChart.setDrawCenterText(true);

        mChart.setRotationAngle(0);
        // enable rotation of the chart by touch
        mChart.setRotationEnabled(false);
        mChart.setHighlightPerTapEnabled(true);

        // mChart.setUnit(" €");
        // mChart.setDrawUnitsInChart(true);

        // add a selection listener
        mChart.setOnChartValueSelectedListener(this);

        //setData(4, 100);

        //  mChart.animateY(1400, EaseInOutQuad);


       /* Legend l = mChart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.RIGHT);
        l.setOrientation(Legend.LegendOrientation.VERTICAL);
        l.setDrawInside(false);
        l.setXEntrySpace(7f);
        l.setYEntrySpace(0f);
        l.setYOffset(0f);*/
        mChart.getLegend().setEnabled(false);

        // entry label styling
        mChart.setEntryLabelColor(Color.BLACK);
        //  mChart.setEntryLabelTypeface(mTfRegular);
        mChart.setEntryLabelTextSize(12f);
    }

    //set data on chart
    private void setData() {


        ArrayList<PieEntry> entries = new ArrayList<PieEntry>();

        // NOTE: The order of the entries when being added to the entries array determines their position around the center of
        // the chart.
        if (arrayListEmotionName.size() == 0) {
            tvNoData.setVisibility(View.VISIBLE);
            mChart.setVisibility(View.GONE);
        } else {
            tvNoData.setVisibility(View.GONE);
            mChart.setVisibility(View.VISIBLE);
        }

        for (int i = 0; i < arrayListEmotionName.size(); i++) {
            Log.v("akram", "emotion list value " + emotionlist.get(arrayListEmotionName.get(i)));
            Log.v("akram", "emotion list name " + arrayListEmotionName.get(i).toString());
            entries.add(new PieEntry(Float.parseFloat(emotionlist.get(arrayListEmotionName.get(i)) + ""),
                    arrayListEmotionName.get(i).toString(),
                    getResources().getDrawable(R.drawable.ic_drop_dwon)));

        }

        PieDataSet dataSet = new PieDataSet(entries, "");

        dataSet.setDrawIcons(false);

        dataSet.setSliceSpace(3f);
        dataSet.setIconsOffset(new MPPointF(0, 40));
        dataSet.setSelectionShift(5f);

        // add a lot of colors

        ArrayList<Integer> colors = new ArrayList<Integer>();

        for (int c : ColorTemplate.VORDIPLOM_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.JOYFUL_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.COLORFUL_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.LIBERTY_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.PASTEL_COLORS)
            colors.add(c);

        colors.add(ColorTemplate.getHoloBlue());

        dataSet.setColors(colors);
        //dataSet.setSelectionShift(0f);

        PieData data = new PieData(dataSet);
        data.setValueFormatter(new PercentFormatter());
        data.setValueTextSize(11f);
        data.setValueTextColor(Color.BLACK);
        //  data.setValueTypeface(mTfLight);
        mChart.setData(data);

        // undo all highlights
        mChart.highlightValues(null);

        mChart.invalidate();
    }


    private void filterEmotionByCategory(String categoryName, String duration) {

        emotionlist.clear();
        arrayListEmotionName.clear();

        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate = df.format(c);

        String currentTime[] = formattedDate.split("-");

        String currentDay = currentTime[2];
        String currentMonth = Integer.parseInt(currentTime[1]) + "";
        String currentYear = currentTime[0];

        if (categoryName.equalsIgnoreCase("All") && duration.equalsIgnoreCase("All")) {

            for (int i = 0; i < getMyDiaryModels.size(); i++) {

                addFilterData(i);

            }

        } else if (duration.equalsIgnoreCase("All")) {
            for (int i = 0; i < getMyDiaryModels.size(); i++) {

                if (categoryName.equalsIgnoreCase(getMyDiaryModels.get(i).getCategoryName())) {
                    addFilterData(i);
                }
            }
        } else if (categoryName.equalsIgnoreCase("All")) {


            for (int i = 0; i < getMyDiaryModels.size(); i++) {

                String date[] = getMyDiaryModels.get(i).getCreatedOn().split("T");
                String time = date[1].substring(0, 8);
                String finalData = TimeConveter.getdatetime(date[0], time);
                String dateFinal[] = finalData.split("T");
                String dateArr[] = dateFinal[0].split("-");

                if (duration.equalsIgnoreCase("today")) {
                    if (dateArr[0].equalsIgnoreCase(currentYear) && dateArr[1].equalsIgnoreCase(currentMonth)
                            && dateArr[2].equalsIgnoreCase(currentDay))

                        addFilterData(i);

                } else if (duration.equalsIgnoreCase("weekly")) {

                    int dayInt = Integer.parseInt(dateArr[2]);

                    for (int i1 = 0; i1 < 7; i1++) {

                        Date myDate = null;
                        try {
                            myDate = df.parse(formattedDate);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                        Calendar calendar = Calendar.getInstance();
                        calendar.setTime(myDate);
                        calendar.add(Calendar.DAY_OF_YEAR, -i1);
                        Date newDate = calendar.getTime();

                        //  String date = getCalculatedDate("01-01-2015", "dd-MM-yyyy", -10);

                        String newDateStr = df.format(newDate);

                        if (dateFinal[0].equalsIgnoreCase(newDateStr)) {
                            addFilterData(i);
                            break;
                        }
                    }

                } else if (duration.equalsIgnoreCase("monthly")) {

                    if (dateArr[0].equalsIgnoreCase(currentYear) && dateArr[1].equalsIgnoreCase(currentMonth)
                            )

                        addFilterData(i);

                }
            }


        } else {


            for (int i = 0; i < getMyDiaryModels.size(); i++) {

                String date[] = getMyDiaryModels.get(i).getCreatedOn().split("T");
                String time = date[1].substring(0, 8);
                String finalData = TimeConveter.getdatetime(date[0], time);
                String dateFinal[] = finalData.split("T");
                String dateArr[] = dateFinal[0].split("-");

                if (categoryName.equalsIgnoreCase(getMyDiaryModels.get(i).getCategoryName())) {

                    if (duration.equalsIgnoreCase("today")) {
                        if (dateArr[0].equalsIgnoreCase(currentYear) && dateArr[1].equalsIgnoreCase(currentMonth)
                                && dateArr[2].equalsIgnoreCase(currentDay))

                            addFilterData(i);

                    } else if (duration.equalsIgnoreCase("weekly")) {

                        int dayInt = Integer.parseInt(dateArr[2]);

                        for (int i1 = 0; i1 < 7; i1++) {

                            Date myDate = null;
                            try {
                                myDate = df.parse(formattedDate);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                            Calendar calendar = Calendar.getInstance();
                            calendar.setTime(myDate);
                            calendar.add(Calendar.DAY_OF_YEAR, -i1);
                            Date newDate = calendar.getTime();

                            //  String date = getCalculatedDate("01-01-2015", "dd-MM-yyyy", -10);

                            String newDateStr = df.format(newDate);

                            if (dateFinal[0].equalsIgnoreCase(newDateStr)) {
                                addFilterData(i);
                                break;
                            }
                        }

                    } else if (duration.equalsIgnoreCase("monthly")) {

                        if (dateArr[0].equalsIgnoreCase(currentYear) && dateArr[1].equalsIgnoreCase(currentMonth)
                                )

                            addFilterData(i);

                    }


                }
            }
        }
        setData();
    }

    private void addFilterData(int i) {


        if (arrayListEmotionName.contains(getMyDiaryModels.get(i).getEmotionIdName())) {

            String emotionCount = emotionlist.get(getMyDiaryModels.get(i).getEmotionIdName()).toString();
            int emotionCountInt = Integer.parseInt(emotionCount) + 1;
            emotionlist.remove(getMyDiaryModels.get(i).getEmotionIdName());
            emotionlist.put(getMyDiaryModels.get(i).getEmotionIdName(), emotionCountInt);

        } else {
            arrayListEmotionName.add(getMyDiaryModels.get(i).getEmotionIdName());
            emotionlist.put(getMyDiaryModels.get(i).getEmotionIdName(), 1);
        }


    }


    @Override
    public void onValueSelected(Entry e, Highlight h) {

        if (e == null)
            return;
        Log.v("akram",
                "Value: " + e.getData() + ", index: " + h.getX()
                        + ", DataSet index: " + h.getDataSetIndex());

        int selectedEmotion = (int) h.getX();
        Log.v("akram", "selected emotion " + arrayListEmotionName.get(selectedEmotion));

        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate = df.format(c);

        String currentTime[] = formattedDate.split("-");

        String currentDay = currentTime[2];
        String currentMonth = Integer.parseInt(currentTime[1]) + "";
        String currentYear = currentTime[0];


        getMyDiaryModels1.clear();

        if (edtEmotion.getSelectedItem().toString().equalsIgnoreCase("All") && duration.equalsIgnoreCase("All")) {

            for (int i = 0; i < getMyDiaryModels.size(); i++) {

                if (getMyDiaryModels.get(i).getEmotionIdName().equalsIgnoreCase(arrayListEmotionName.get(selectedEmotion))) {

                    setDataOnChartClick(i);

                }
            }

        } else if (duration.equalsIgnoreCase("All")) {

            for (int i = 0; i < getMyDiaryModels.size(); i++) {


                if (edtEmotion.getSelectedItem().toString().equalsIgnoreCase(getMyDiaryModels.get(i).getCategoryName())) {

                    if (getMyDiaryModels.get(i).getEmotionIdName().equalsIgnoreCase(arrayListEmotionName.get(selectedEmotion))) {

                        setDataOnChartClick(i);

                    }
                }

            }
        } else if (edtEmotion.getSelectedItem().toString().equalsIgnoreCase("All")) {


            for (int i = 0; i < getMyDiaryModels.size(); i++) {

                if (getMyDiaryModels.get(i).getEmotionIdName().equalsIgnoreCase(arrayListEmotionName.get(selectedEmotion))) {

                    String date[] = getMyDiaryModels.get(i).getCreatedOn().split("T");
                    String time = date[1].substring(0, 8);
                    String finalData = TimeConveter.getdatetime(date[0], time);
                    String dateFinal[] = finalData.split("T");
                    String dateArr[] = dateFinal[0].split("-");

                    if (duration.equalsIgnoreCase("today")) {
                        if (dateArr[0].equalsIgnoreCase(currentYear) && dateArr[1].equalsIgnoreCase(currentMonth)
                                && dateArr[2].equalsIgnoreCase(currentDay))

                            setDataOnChartClick(i);

                    } else if (duration.equalsIgnoreCase("weekly")) {

                        for (int i1 = 0; i1 < 7; i1++) {

                            Date myDate = null;
                            try {
                                myDate = df.parse(formattedDate);
                            } catch (ParseException e1) {
                                e1.printStackTrace();
                            }

                            Calendar calendar = Calendar.getInstance();
                            calendar.setTime(myDate);
                            calendar.add(Calendar.DAY_OF_YEAR, -i1);
                            Date newDate = calendar.getTime();

                            //  String date = getCalculatedDate("01-01-2015", "dd-MM-yyyy", -10);

                            String newDateStr = df.format(newDate);

                            if (dateFinal[0].equalsIgnoreCase(newDateStr)) {
                                setDataOnChartClick(i);
                                break;
                            }
                        }

                    } else if (duration.equalsIgnoreCase("monthly")) {

                        if (dateArr[0].equalsIgnoreCase(currentYear) && dateArr[1].equalsIgnoreCase(currentMonth)
                                )

                            setDataOnChartClick(i);

                    }

                }
            }


        } else {
            for (int i = 0; i < getMyDiaryModels.size(); i++) {


                if (getMyDiaryModels.get(i).getEmotionIdName().equalsIgnoreCase(arrayListEmotionName.get(selectedEmotion))) {

                    String date[] = getMyDiaryModels.get(i).getCreatedOn().split("T");
                    String time = date[1].substring(0, 8);
                    String finalData = TimeConveter.getdatetime(date[0], time);
                    String dateFinal[] = finalData.split("T");
                    String dateArr[] = dateFinal[0].split("-");

                    if (edtEmotion.getSelectedItem().toString().equalsIgnoreCase(getMyDiaryModels.get(i).getCategoryName())) {

                        if (duration.equalsIgnoreCase("today")) {
                            if (dateArr[0].equalsIgnoreCase(currentYear) && dateArr[1].equalsIgnoreCase(currentMonth)
                                    && dateArr[2].equalsIgnoreCase(currentDay))

                                setDataOnChartClick(i);

                        } else if (duration.equalsIgnoreCase("weekly")) {

                            int dayInt = Integer.parseInt(dateArr[2]);

                            for (int i1 = 0; i1 < 7; i1++) {

                                Date myDate = null;
                                try {
                                    myDate = df.parse(formattedDate);
                                } catch (ParseException e1) {
                                    e1.printStackTrace();
                                }

                                Calendar calendar = Calendar.getInstance();
                                calendar.setTime(myDate);
                                calendar.add(Calendar.DAY_OF_YEAR, -i1);
                                Date newDate = calendar.getTime();

                                //  String date = getCalculatedDate("01-01-2015", "dd-MM-yyyy", -10);

                                String newDateStr = df.format(newDate);

                                if (dateFinal[0].equalsIgnoreCase(newDateStr)) {
                                    setDataOnChartClick(i);
                                    break;
                                }
                            }

                        } else if (duration.equalsIgnoreCase("monthly")) {

                            if (dateArr[0].equalsIgnoreCase(currentYear) && dateArr[1].equalsIgnoreCase(currentMonth)
                                    )

                                setDataOnChartClick(i);

                        }

                    }
                }
            }

        }

        interactionListener.launchMyDiaryEmotionListFragment(getMyDiaryModels1);
        for (int i = 0; i < getMyDiaryModels1.size(); i++) {
            Log.v("akram", "emotion " + getMyDiaryModels1.get(i).getEmotionIdName());
        }

    }

    private void setDataOnChartClick(int i) {

        GetMyDiaryModel getMyDiaryModel = new GetMyDiaryModel(getMyDiaryModels.get(i).getDairyId(),
                getMyDiaryModels.get(i).getUserId(), getMyDiaryModels.get(i).getEmotionId(), getMyDiaryModels.get(i).getCategoryId(),
                getMyDiaryModels.get(i).getEmotionIdName(), getMyDiaryModels.get(i).getCategoryName(), getMyDiaryModels.get(i).getDiscription(),
                getMyDiaryModels.get(i).getAction(), getMyDiaryModels.get(i).getLevelRate(), getMyDiaryModels.get(i).getCreatedOn());
        getMyDiaryModels1.add(getMyDiaryModel);
    }


    @Override
    public void onNothingSelected() {

    }


    public void setDataApi() {

        arrayListCategoryName.clear();
        emotionlist.clear();
        arrayListEmotionName.clear();

        ArrayList arrayListCheck = new ArrayList();

        arrayListCategoryName.add("All");
        for (int i = 0; i < getMyDiaryModels.size(); i++) {

            if (!arrayListCheck.contains(getMyDiaryModels.get(i).getCategoryId())) {
                arrayListCheck.add(getMyDiaryModels.get(i).getCategoryId());

                arrayListCategoryName.add(getMyDiaryModels.get(i).getCategoryName());
            }

            if (arrayListEmotionName.contains(getMyDiaryModels.get(i).getEmotionIdName())) {

                String emotionCount = emotionlist.get(getMyDiaryModels.get(i).getEmotionIdName()).toString();
                int emotionCountInt = Integer.parseInt(emotionCount) + 1;
                emotionlist.remove(getMyDiaryModels.get(i).getEmotionIdName());
                emotionlist.put(getMyDiaryModels.get(i).getEmotionIdName(), emotionCountInt);

            } else {
                arrayListEmotionName.add(getMyDiaryModels.get(i).getEmotionIdName());
                emotionlist.put(getMyDiaryModels.get(i).getEmotionIdName(), 1);
            }

        }


        for (int i = 0; i < arrayListEmotionName.size(); i++) {
            Log.v("akramraza", "value " + emotionlist.get(arrayListEmotionName.get(i)));
        }
        edtEmotion.clearFocus();

        spinner2meth();
       /* ArrayAdapter adapter = new ArrayAdapter<String>(activity,
                android.R.layout.simple_list_item_1, arrayListCategoryName);
        edtEmotion.setAdapter(adapter);*/
        setData();
    }


}

