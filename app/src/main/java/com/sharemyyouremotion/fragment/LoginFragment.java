package com.sharemyyouremotion.fragment;

import android.app.ActivityOptions;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import com.sharemyyouremotion.R;
import com.sharemyyouremotion.activity.SpecializationActivity;
import com.sharemyyouremotion.activity.HomeActivity;
import com.sharemyyouremotion.activity.LoginBaseActivity;
import com.sharemyyouremotion.activity.SignUpActivity;
import com.sharemyyouremotion.connection.BaseApiService;
import com.sharemyyouremotion.connection.Connection;
import com.sharemyyouremotion.helper.Constants;
import com.sharemyyouremotion.helper.DialogClass;
import com.sharemyyouremotion.helper.StaticSharedpreference;
import com.sharemyyouremotion.helper.Utilities;
import com.sharemyyouremotion.model.SpecializationModel;
import com.sharemyyouremotion.qucikblox.QuickBloxRequestMethod;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class LoginFragment extends SignupSigninBaseFragment implements View.OnClickListener {

    TextView tvNewSignUp, tvForgotPassword, tvGuest;
    LoginBaseActivity activity;
    CardView btnSignIn;
    TextInputEditText edtEmail, edtPassword;
    Utilities utilities;
    public static String guest = "";

    public LoginFragment() {
        // Required empty public constructor
    }

    public static LoginFragment newInstance() {
        return new LoginFragment();
    }

    @Override
    public FragmentId getFragmentId() {
        return FragmentId.LOGIN_FRAGMENT;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        root = (ViewGroup) inflater.inflate(R.layout.fragment_login, container, false);
        activity = (LoginBaseActivity) getActivity();

        /*find ids and initialize all classes*/
        initView();

        SharedPreferences pref = activity.getSharedPreferences("ShareAllYourEmotionFCM", 0);
        String regId = pref.getString("fcm", null);


        Log.e("akramraza", "Firebaseinner reg id: " + regId);


        return root;
    }

    private void initView() {

        tvNewSignUp = root.findViewById(R.id.tvNewSignUp);
        tvForgotPassword = root.findViewById(R.id.tvForgotPassword);
        btnSignIn = root.findViewById(R.id.btnSignIn);
        edtEmail = root.findViewById(R.id.edtEmail);
        edtPassword = root.findViewById(R.id.edtPassword);
        tvGuest = root.findViewById(R.id.tvGuest);

        tvNewSignUp.setOnClickListener(this);
        tvForgotPassword.setOnClickListener(this);
        btnSignIn.setOnClickListener(this);
        tvGuest.setOnClickListener(this);

        /*initialize class*/
        utilities = new Utilities(activity);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvNewSignUp:
                guest = "";
                ActivityOptions options =
                        ActivityOptions.makeCustomAnimation(activity, R.anim.slide_to_right, R.anim.slide_from_left);

                startActivity(new Intent(activity, SignUpActivity.class), options.toBundle());

                break;

            case R.id.tvForgotPassword:
                guest = "";
                interactionListener.launchForgotPasswordFragment();

                break;

            case R.id.btnSignIn:
                guest = "";
                //TODO service
                if (isValidate())
                    if (utilities.isOnline())
                        loginApi();
                //TODO UI
               /* ActivityOptions options1 =
                        ActivityOptions.makeCustomAnimation(activity, R.anim.slide_to_right, R.anim.slide_from_left);

                startActivity(new Intent(activity, HomeActivity.class), options1.toBundle());*/
                break;

            case R.id.tvGuest:

                guest = "guest";
                ActivityOptions options2 =
                        ActivityOptions.makeCustomAnimation(activity, R.anim.slide_to_right, R.anim.slide_from_left);

                Intent intent = new Intent(activity, HomeActivity.class);

                startActivity(intent, options2.toBundle());

                break;

        }
    }


    public void loginApi() {

        dialogClass.progresesDialog(activity);
        Map requestBody = new HashMap<>();

        SharedPreferences sharedPreferences = activity.getSharedPreferences("ShareAllYourEmotionFCM", activity.MODE_PRIVATE);
        String token = sharedPreferences.getString("fcm", "");

        requestBody.put("EmailId", edtEmail.getText().toString());
        requestBody.put("Password", edtPassword.getText().toString());
     /*   requestBody.put("EmailId", "akramr210@gmail.com");
        requestBody.put("Password", "123");*/
        requestBody.put("DeviceToken", token);
        requestBody.put("DeviceType", "android");


        BaseApiService apiService = Connection.getBaseApiServiceInstance();
        Call<JsonObject> call = apiService.loginApi(requestBody);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.body() != null && response.body().get("StatusCode").getAsInt() == 200) {


                   /* StaticSharedpreference.saveInfo(Constants.ABOUT,
                            response.body().get("UserProfile").getAsJsonObject().get("About").getAsString(), activity);

                 */
                    /*check specialization is fill or not*/

                    QuickBloxRequestMethod quickBloxRequestMethod = new QuickBloxRequestMethod();
                    quickBloxRequestMethod.quickbloxlogin(response.body().get("UserProfile").getAsJsonObject().get("QuickbloxName").getAsString() + "",
                            activity, response.body().get("UserProfile").getAsJsonObject().get("QuickbloxId").getAsString() + "", "login", response);

                } else {
                    dialogClass.messageDialog(activity, response.body().get("Message").getAsString() + "");
                }
                DialogClass.custom_pd.dismiss();
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.v("okhttp", " throw = " + t);

                DialogClass.custom_pd.dismiss();
            }
        });
    }


    private boolean isValidate() {

        if (edtEmail.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(activity, "Please enter Email Id", Toast.LENGTH_LONG).show();
        } else if (!utilities.isValidEmail(edtEmail.getText().toString())) {
            Toast.makeText(activity, "please enter valid email Id", Toast.LENGTH_LONG).show();
        } else if (edtPassword.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(activity, "Please enter Password", Toast.LENGTH_LONG).show();
        } else {
            return true;
        }
        return false;
    }
}
