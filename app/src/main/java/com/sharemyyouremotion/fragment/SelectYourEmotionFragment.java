package com.sharemyyouremotion.fragment;

import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sharemyyouremotion.R;
import com.sharemyyouremotion.adapter.SelectYourEmotionAdapter;
import com.sharemyyouremotion.connection.BaseApiService;
import com.sharemyyouremotion.connection.Connection;
import com.sharemyyouremotion.helper.ClickListener;
import com.sharemyyouremotion.helper.Constants;
import com.sharemyyouremotion.helper.DialogClass;
import com.sharemyyouremotion.helper.RecyclerTouchListener;
import com.sharemyyouremotion.model.EmotionAndCategoriesModel;
import com.sharemyyouremotion.model.SpecializationModel;

import java.lang.reflect.Type;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.sharemyyouremotion.activity.SelectYourEmotionBaseActivity.toolbar;


public class SelectYourEmotionFragment extends EmotionBaseFragment implements View.OnClickListener {

    RecyclerView recyclerView;


    public SelectYourEmotionFragment() {
        // Required empty public constructor
    }

    public static SelectYourEmotionFragment newInstance() {
        return new SelectYourEmotionFragment();
    }

    @Override
    public FragmentId getFragmentId() {
        return FragmentId.SELECT_YOUR_EMOTION_FRAGMENT;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        root = (ViewGroup) inflater.inflate(R.layout.fragment_select_your_emotion, container, false);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getActivity().getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.notificationBar));
        }

        //TODO service
        //getData();
        toolbar.setText("Emotion");
        initView();

        if(Constants.emotionLists.size()==0) {
            getEmotionAndCategories();
        }else{
            SelectYourEmotionAdapter selectYourEmotionAdapter = new SelectYourEmotionAdapter(activity, Constants.emotionLists);
            recyclerView.setAdapter(selectYourEmotionAdapter);
        }

        return root;
    }

    private void initView() {
        recyclerView = root.findViewById(R.id.recyclerView);

        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(activity, 2);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());


        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(activity, recyclerView, new ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Constants.emotionId = Constants.emotionLists.get(position).getEmotionId();
                Constants.emotionStr = Constants.emotionLists.get(position).getEmotionName();
                interactionListener.launchEmotionCategoryFragment(Constants.categoryLists,activity.getIntent().getStringExtra("screen"));
            }

            @Override
            public void onLongClick(View view, int position) {
                //  popup();
            }
        }));

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

        }
    }

    public void getEmotionAndCategories() {
        dialogClass.progresesDialog(activity);

        BaseApiService apiService = Connection.getBaseApiServiceInstance();
        Call<EmotionAndCategoriesModel> call = apiService.getEmotionAndCategories();
        call.enqueue(new Callback<EmotionAndCategoriesModel>() {
            @Override
            public void onResponse(Call<EmotionAndCategoriesModel> call, Response<EmotionAndCategoriesModel> response) {
//                Log.d("tag", "onResponse" + "" + response.body().toString());

                if (response.body() != null && response.body().getStatusCode() == 200) {

                    DialogClass.custom_pd.dismiss();

                    /*check specialization fill or not*/

                    Gson gson = new Gson();
                    Type listType = new TypeToken<ArrayList<SpecializationModel>>() {
                    }.getType();

                   Constants.emotionLists = response.body().getEmotionLists();
                    Constants.categoryLists = response.body().getCategoryLists();

                    SelectYourEmotionAdapter selectYourEmotionAdapter = new SelectYourEmotionAdapter(activity, Constants.emotionLists);
                    recyclerView.setAdapter(selectYourEmotionAdapter);

                } else {
                    Toast.makeText(activity, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
                DialogClass.custom_pd.dismiss();

                /* JsonObject response = new  JsonParser().parse(out.body()).getAsJsonObject();*/

            }

            @Override
            public void onFailure(Call<EmotionAndCategoriesModel> call, Throwable t) {
                Log.v("okhttp", " throw = " + t);

                DialogClass.custom_pd.dismiss();
            }
        });
    }

    public void getData() {
        Bundle bundle = activity.getIntent().getExtras();
        //emotionLists = (ArrayList<EmotionAndCategoriesModel.EmotionList>) bundle.getSerializable("emotion");
      //  categoryLists = (ArrayList<EmotionAndCategoriesModel.CategoryList>) bundle.getSerializable("category");

    }
}
