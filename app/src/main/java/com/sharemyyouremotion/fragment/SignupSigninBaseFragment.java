package com.sharemyyouremotion.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.ViewGroup;

import com.sharemyyouremotion.helper.DialogClass;
import com.sharemyyouremotion.helper.Utilities;

public abstract class SignupSigninBaseFragment extends Fragment {

    protected FragmentInteractionListener interactionListener;

    protected ViewGroup root;

    protected FragmentActivity activity;
    protected DialogClass dialogClass;
    protected Utilities utilities;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = getActivity();
        dialogClass = new DialogClass();
        utilities = new Utilities(activity);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            interactionListener = (FragmentInteractionListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement FragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        interactionListener = null;
    }

    public abstract FragmentId getFragmentId();

    public enum FragmentId {
        LOGIN_FRAGMENT, FORGOT_PASSWORD_FRAGMENT, SPLASH_FRAGMENT
    }

    public interface FragmentInteractionListener {

        void launchLoginFragment();

        void launchForgotPasswordFragment();

        void launchSplashFragment();


    }
}
