package com.sharemyyouremotion.activity;

import android.Manifest;
import android.app.Activity;
import android.app.ActivityOptions;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import com.sharemyyouremotion.R;
import com.sharemyyouremotion.adapter.MyQuestionFeedbackAdapter;
import com.sharemyyouremotion.adapter.NotificationAdapter;
import com.sharemyyouremotion.connection.BaseApiService;
import com.sharemyyouremotion.connection.Connection;
import com.sharemyyouremotion.connection.ProgressRequestBody;
import com.sharemyyouremotion.connection.RequestClass;
import com.sharemyyouremotion.fragment.MyQuestionFragment;
import com.sharemyyouremotion.fragment.ShareEmotionFragment;
import com.sharemyyouremotion.helper.Constants;
import com.sharemyyouremotion.helper.DialogClass;
import com.sharemyyouremotion.helper.PathUtil;
import com.sharemyyouremotion.helper.StaticSharedpreference;
import com.sharemyyouremotion.helper.Utilities;
import com.sharemyyouremotion.model.MyQuestionFeedbackModel;
import com.sharemyyouremotion.model.NotificationModel;
import com.sharemyyouremotion.model.SpecializationModel;

import java.io.File;
import java.lang.reflect.Type;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.sharemyyouremotion.fragment.ShareEmotionFragment.mediaPath;



public class MyQuestionFeedbackActivity extends AppCompatActivity implements View.OnClickListener, ProgressRequestBody.UploadCallbacks {

    RecyclerView recyclerView;
    LinearLayout linearEditField, linearVideo;
    CardView cardText;
    RelativeLayout linearRecycler;

    TextView tvFeedbackCount, tvTitle, tvEmotion,tvCategory;
    DialogClass dialogClass = new DialogClass();
    String questionId, questionType;
    RelativeLayout relativeSend;
    ImageView imgAudio, imgVideo, imgPlayVideo, imgPlayAudio;

    public static EditText edtFeedback;
    TextView tvNoData,  tvName;
    public static TextView tvVideoAudio;
    String audioLink, videoLink;
    Utilities utilities;
    CircleImageView imgProfile;
    RatingBar ratingBar;
    public static int color[] = {R.color.blue, R.color.color_accent, R.color.random_color_1, R.color.random_color_2, R.color.random_color_3
            , R.color.random_color_4, R.color.random_color_4, R.color.random_color_6, R.color.random_color_7, R.color.random_color_1};

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.notificationBar));
        }

        setContentView(R.layout.activity_my_question_feedback);
        mediaPath = "";
        initViews();
        utilities = new Utilities(this);
        getQuestionDetailAllFeedbacksByQuestionId();
    }


    private void initViews() {

        recyclerView = findViewById(R.id.recyclerView);
        linearEditField = findViewById(R.id.linearEditField);
        linearRecycler = findViewById(R.id.linearRecycler);
        linearVideo = findViewById(R.id.linearVideo);
        cardText = findViewById(R.id.cardText);
        relativeSend = findViewById(R.id.relativeSend);
        imgAudio = findViewById(R.id.imgAudio);
        imgVideo = findViewById(R.id.imgVideo);
        edtFeedback = findViewById(R.id.edtFeedback);
        tvNoData = findViewById(R.id.tvNoData);
        imgPlayVideo = findViewById(R.id.imgPlayVideo);
        imgPlayAudio = findViewById(R.id.imgPlayAudio);
        tvVideoAudio = findViewById(R.id.tvVideoAudio);
        tvName = findViewById(R.id.tvName);
        imgProfile = findViewById(R.id.imgProfile);
        ratingBar = findViewById(R.id.ratingBar);
        tvCategory = findViewById(R.id.tvCategory);

        tvFeedbackCount = findViewById(R.id.tvFeedbackCount);
        tvTitle = findViewById(R.id.tvTitle);
        tvEmotion = findViewById(R.id.tvEmotion);

        relativeSend.setOnClickListener(this);
        imgVideo.setOnClickListener(this);
        imgAudio.setOnClickListener(this);
        imgPlayVideo.setOnClickListener(this);
        imgPlayAudio.setOnClickListener(this);

        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.MATCH_PARENT);

        if (MyQuestionFragment.screenValue == 0) {

            cardText.setVisibility(View.VISIBLE);
            linearVideo.setVisibility(View.GONE);
            linearEditField.setVisibility(View.VISIBLE);

        } else if (MyQuestionFragment.screenValue == 1) {

            linearEditField.setVisibility(View.GONE);
            layoutParams.setMargins(0, 0, 0, 0);
            linearRecycler.setLayoutParams(layoutParams);

        } else {
            linearEditField.setVisibility(View.VISIBLE);
            cardText.setVisibility(View.GONE);
            linearVideo.setVisibility(View.VISIBLE);

            /*layoutParams.setMargins(0, 0, 0, 70);
            linearRecycler.setLayoutParams(layoutParams);*/

        }

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(MyQuestionFeedbackActivity.this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.relativeSend:

                if (questionType.equalsIgnoreCase("text"))
                    if (!edtFeedback.getText().toString().equalsIgnoreCase(""))
                        shareIdentity(this);
                    else
                        Toast.makeText(this, "Please enter feedback", Toast.LENGTH_SHORT).show();
                else
                    shareIdentity(this);
                break;

            case R.id.imgAudio:

                ActivityCompat.requestPermissions(MyQuestionFeedbackActivity.this, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.RECORD_AUDIO},

                        1);

                break;

            case R.id.imgVideo:

                ActivityCompat.requestPermissions(MyQuestionFeedbackActivity.this, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.RECORD_AUDIO},

                        2);

                break;

            case R.id.imgPlayAudio:

                dialogClass.audioPlayDialog(this, audioLink, false);
                break;

            case R.id.imgPlayVideo:

                ActivityOptions options1 =
                        ActivityOptions.makeCustomAnimation(this, R.anim.slide_to_right, R.anim.slide_from_left);
                Intent intent = new Intent(this, WebViewForVideoActivity.class);
                intent.putExtra("video", videoLink);
                //  intent.putExtra("QuestionId",myQuestionFragments.get(position).getQuestionId());
                startActivity(intent, options1.toBundle());

                break;


        }
    }

    RequestClass requestClass = new RequestClass();

    public Dialog shareIdentity(final Activity activity) {

        LayoutInflater factory = LayoutInflater.from(activity);
        final View view = factory.inflate(R.layout.dialog_no_user_seleted, null);
        final AlertDialog alertDialoge = new AlertDialog.Builder(activity).create();

        CardView cardNo = view.findViewById(R.id.cardNo);
        CardView cardYes = view.findViewById(R.id.cardYes);
        TextView tvTitle = view.findViewById(R.id.tvTitle);

        tvTitle.setText("Do you want to share your Identity?");

        cardYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // giveFeedback("true");

                requestClass.compressVideo(activity, mediaPath, questionType, questionId, edtFeedback.getText().toString(), "true", null, MyQuestionFeedbackActivity.this);

                alertDialoge.dismiss();
            }
        });

        cardNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                requestClass.compressVideo(activity, mediaPath, questionType, questionId, edtFeedback.getText().toString(), "false", null, MyQuestionFeedbackActivity.this);

                // giveFeedback("false");
                alertDialoge.dismiss();
            }
        });

        alertDialoge.setCancelable(false);
        alertDialoge.setCanceledOnTouchOutside(false);
        alertDialoge.setView(view);

        alertDialoge.show();
        return alertDialoge;

    }

    AlertDialog alertDialog;


    public void getQuestionDetailAllFeedbacksByQuestionId() {

        if (!utilities.isOnline())
            return;

        dialogClass.progresesDialog(this);
        Map requestBody = new HashMap<>();

        requestBody.put("QuestionId", getIntent().getIntExtra("QuestionId", 0));

        BaseApiService apiService = Connection.getBaseApiServiceInstance();
        Call<JsonObject> call = apiService.getQuestionDetailAllFeedbacksByQuestionId(requestBody);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.body() != null && response.body().get("StatusCode").getAsInt() == 404 ||
                        response.body().get("StatusCode").getAsInt() == 200) {

                    DialogClass.custom_pd.dismiss();

                    int emotionId = response.body().get("Question").getAsJsonObject().get("EmotionId").getAsInt();
                    int categoryId = response.body().get("Question").getAsJsonObject().get("CategoryId").getAsInt();
                    int subCategoryId = response.body().get("Question").getAsJsonObject().get("SubCategoryId").getAsInt();

                    questionId = "" + response.body().get("Question").getAsJsonObject().get("QuestionId").getAsInt();
                    questionType = "" + response.body().get("Question").getAsJsonObject().get("QuestionType").getAsString();
                    audioLink = "" + response.body().get("Question").getAsJsonObject().get("QuestionAudio").getAsString();
                    videoLink = "" + response.body().get("Question").getAsJsonObject().get("QuestionVideo").getAsString();

                    tvTitle.setText(response.body().get("Question").getAsJsonObject().get("QuestionText").getAsString());
                    String questionType = response.body().get("Question").getAsJsonObject().get("QuestionType").getAsString();

                  /*  tvEmotion.setText(response.body().get("Question").getAsJsonObject().get("EmotionName").getAsString()
                            + "/" + response.body().get("Question").getAsJsonObject().get("CategoryName").getAsString()
                            + "/" + response.body().get("Question").getAsJsonObject().get("SubCategoryName").getAsString());
*/
                    int emotionBac = emotionId % 10;
                    tvEmotion.setBackgroundColor(color[emotionBac]);
                    tvEmotion.setText(response.body().get("Question").getAsJsonObject().get("EmotionName").getAsString());
                    tvCategory.setText(response.body().get("Question").getAsJsonObject().get("CategoryName").getAsString()
                    +"/"+response.body().get("Question").getAsJsonObject().get("SubCategoryName").getAsString());
                    //    ratingBar.setRating(response.body().get("Question").getAsJsonObject().get("RateCount").getAsFloat());
                    ratingBar.setVisibility(View.GONE);

                    tvFeedbackCount.setText(response.body().get("Question").getAsJsonObject().get("FeedbackCount").getAsInt() + " Feedback");

                    if (response.body().get("Question").getAsJsonObject().get("IsIdentityShare").getAsBoolean()) {
                        tvName.setText(response.body().get("Question").getAsJsonObject().get("Name").getAsString() + " is ");
                        if (response.body().get("Question").getAsJsonObject().get("UserThumbnail").getAsString().contains(".jpg") ||
                                response.body().get("Question").getAsJsonObject().get("UserThumbnail").getAsString().contains(".png")) {
                            Glide.with(MyQuestionFeedbackActivity.this)
                                    .load(response.body().get("Question").getAsJsonObject().get("UserThumbnail").getAsString())
                                    .into(imgProfile);
                        } else {
                            if (response.body().get("Question").getAsJsonObject().get("Gender").getAsString().equalsIgnoreCase("female")) {
                                Glide.with(MyQuestionFeedbackActivity.this)
                                        .load(getImage("girl_user"))
                                        .into(imgProfile);
                            }
                        }
                    } else {
                        tvName.setText(response.body().get("Question").getAsJsonObject().get("Name").getAsString() + " is ");
                        // tvName.setText("User" + response.body().get("Question").getAsJsonObject().get("AskBy").getAsString() + " is ");
                        if (response.body().get("Question").getAsJsonObject().get("Gender").getAsString().equalsIgnoreCase("female")) {
                            Glide.with(MyQuestionFeedbackActivity.this)
                                    .load(getImage("girl_user"))
                                    .into(imgProfile);
                        }
                    }

                    if (questionType.equalsIgnoreCase("text")) {
                        cardText.setVisibility(View.VISIBLE);
                        linearVideo.setVisibility(View.GONE);
                        tvTitle.setVisibility(View.VISIBLE);

                    } else if (questionType.equalsIgnoreCase("audio")) {
                        cardText.setVisibility(View.GONE);
                        linearVideo.setVisibility(View.VISIBLE);
                        imgAudio.setVisibility(View.VISIBLE);
                        imgVideo.setVisibility(View.GONE);
                        imgPlayAudio.setVisibility(View.VISIBLE);
                    } else {
                        cardText.setVisibility(View.GONE);
                        linearVideo.setVisibility(View.VISIBLE);
                        imgAudio.setVisibility(View.GONE);
                        imgVideo.setVisibility(View.VISIBLE);
                        imgPlayVideo.setVisibility(View.VISIBLE);
                    }

                    if (MyQuestionFragment.screenValue == 1) {
                        cardText.setVisibility(View.GONE);
                        linearVideo.setVisibility(View.GONE);
                    }

                    ArrayList<MyQuestionFeedbackModel> notificationModels = new ArrayList<>();

                    if (response.body().get("StatusCode").getAsInt() == 200) {
                        Log.v("akram", "re " + response.body().get("Feedbacks").isJsonNull());
                        if (!response.body().get("Feedbacks").isJsonNull()) {
                            Gson gson = new Gson();
                            Type listType = new TypeToken<ArrayList<MyQuestionFeedbackModel>>() {
                            }.getType();

                            notificationModels =
                                    gson.fromJson(response.body().getAsJsonArray("Feedbacks"), listType);

                        }
                        tvNoData.setVisibility(View.GONE);
                    } else {
                        tvNoData.setVisibility(View.VISIBLE);
                        tvNoData.setText(response.body().get("Message").getAsString() + "");
                    }
                    MyQuestionFeedbackAdapter myQuestionFeedbackAdapter = new MyQuestionFeedbackAdapter(MyQuestionFeedbackActivity.this, notificationModels, questionType, MyQuestionFeedbackActivity.this);
                    recyclerView.setAdapter(myQuestionFeedbackAdapter);

                } else {
                    dialogClass.messageDialog(MyQuestionFeedbackActivity.this, response.body().get("Message").getAsString() + "");
                }
                DialogClass.custom_pd.dismiss();
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.v("okhttp", " throw = " + t);

                DialogClass.custom_pd.dismiss();
            }
        });
    }

    public int getImage(String imageName) {

        int drawableResourceId = getResources().getIdentifier(imageName, "drawable", getPackageName());

        return drawableResourceId;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {

        Map<String, Integer> perms = new HashMap<String, Integer>();
        // Initial
        perms.put(Manifest.permission.READ_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
        perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
        perms.put(Manifest.permission.CAMERA, PackageManager.PERMISSION_GRANTED);
        perms.put(Manifest.permission.RECORD_AUDIO, PackageManager.PERMISSION_GRANTED);

        // Fill with results
        for (int i = 0; i < permissions.length; i++)
            perms.put(permissions[i], grantResults[i]);
        // Check for ACCESS_FINE_LOCATION
        if (perms.get(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                && perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                && perms.get(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                && perms.get(Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED
                ) {

            switch (requestCode) {
                case 1:

                    dialogClass.selectAudioVideo(null, MyQuestionFeedbackActivity.this, false, tvVideoAudio);

                /*    Intent intent_upload = new Intent();
                    intent_upload.setType("audio*//*");
                    intent_upload.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(intent_upload, 1);*/

                    break;
                case 2:
                    dialogClass.selectAudioVideo(null, MyQuestionFeedbackActivity.this, true, tvVideoAudio);

                  /*  Intent intent = new Intent();
                    intent.setType("video*//*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intent, "Select Video"), 2);*/


                    break;
                // other 'case' lines to check for other
                // permissions this app might request
            }

        } else {
            // Permission Denied
            Toast.makeText(MyQuestionFeedbackActivity.this, "Some Permission is Denied", Toast.LENGTH_SHORT)
                    .show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        //this is for audio
        if (resultCode == RESULT_OK) {
            if (requestCode == 1) {


                //the selected audio.
                Uri uri = data.getData();

                try {
                    mediaPath = PathUtil.getPath(MyQuestionFeedbackActivity.this, uri);
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                }
                tvVideoAudio.setText("Please upload");
                Log.v("akram", "uri " + mediaPath);


                //this for video

            } else if (requestCode == 2) {


                Uri selectedVideoUri = data.getData();
                try {
                    mediaPath = PathUtil.getPath(MyQuestionFeedbackActivity.this, selectedVideoUri);
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                }
                Log.v("akram", "uri " + mediaPath);
                tvVideoAudio.setText("Please upload");

            } else if (requestCode == 3) {

                mediaPath = DialogClass.videoFile.getPath();


                Log.v("akram", "uri " + mediaPath);

                tvVideoAudio.setText(new File(mediaPath).getName());
            }
        }
        super.onActivityResult(requestCode, resultCode, data);

    }

    @Override
    public void onProgressUpdate(int percentage) {
        dialogClass.progress.setProgress(percentage);
        dialogClass.tvCount.setText(percentage + "%");
    }

    @Override
    public void onError() {

    }

    @Override
    public void onFinish() {

    }
}
