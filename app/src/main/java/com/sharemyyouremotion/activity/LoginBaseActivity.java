package com.sharemyyouremotion.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.sharemyyouremotion.R;
import com.sharemyyouremotion.fragment.ForgotPasswordFragment;
import com.sharemyyouremotion.fragment.LoginFragment;
import com.sharemyyouremotion.fragment.SignupSigninBaseFragment;
import com.sharemyyouremotion.fragment.SplashFragment;
import com.sharemyyouremotion.qucikblox.Config;


public class LoginBaseActivity extends AppCompatActivity implements SignupSigninBaseFragment.FragmentInteractionListener {
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

       String id =  FirebaseInstanceId.getInstance().getToken();
       Log.v("akramraza","on Splash"+id);



        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                // checking for type intent filter
                if (intent.getAction().equals(Config.REGISTRATION_COMPLETE)) {
                    // gcm successfully registered
                    // now subscribe to `global` topic to receive app wide notifications
                    FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL);

                    displayFirebaseRegId();

                } else if (intent.getAction().equals(Config.PUSH_NOTIFICATION)) {
                    // new push notification is received

                    String message = intent.getStringExtra("message");
//
                    Toast.makeText(getApplicationContext(), "Push notification: " + message, Toast.LENGTH_LONG).show();


                }
            }
        };

        String value = getIntent().getStringExtra("logout");
        if (value == null) {
            launchSplashFragment();
        } else {
            launchLoginFragment();
        }



    }

    private void displayFirebaseRegId() {


        SharedPreferences pref = getSharedPreferences("ShareAllYourEmotionFCM", 0);
        String regId = pref.getString("fcm", null);



        if (!TextUtils.isEmpty(regId)) {
            Log.e("TAG", "Firebaseinner reg id: " + regId);

        }


        //    txtRegId.setText("Firebase Reg Id: " + regId);
        // else
        //   txtRegId.setText("Firebase Reg Id is not received yet!");
    }


    @Override
    public void launchLoginFragment() {
        LoginFragment loginFragment = LoginFragment.newInstance();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_to_right, R.anim.slide_from_left, R.anim.slide_to_left, R.anim.slide_from_right);
        transaction.replace(R.id.container, loginFragment, SignupSigninBaseFragment.FragmentId.LOGIN_FRAGMENT.name())/*.addToBackStack(HomeBaseFragment.FragmentId.SAVED_FRAGMENT.name())*/;
        transaction.commit();
    }

    @Override
    public void launchForgotPasswordFragment() {
        ForgotPasswordFragment forgotPasswordFragment = ForgotPasswordFragment.newInstance();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_to_right, R.anim.slide_from_left, R.anim.slide_to_left, R.anim.slide_from_right);
        transaction.replace(R.id.container, forgotPasswordFragment, SignupSigninBaseFragment.FragmentId.FORGOT_PASSWORD_FRAGMENT.name()).addToBackStack(SignupSigninBaseFragment.FragmentId.FORGOT_PASSWORD_FRAGMENT.name());
        transaction.commit();
    }

    @Override
    public void launchSplashFragment() {
        SplashFragment splashFragment = SplashFragment.newInstance();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        // transaction.setCustomAnimations(R.anim.slide_to_right, R.anim.slide_from_left, R.anim.slide_to_left, R.anim.slide_from_right);
        transaction.replace(R.id.container, splashFragment, SignupSigninBaseFragment.FragmentId.SPLASH_FRAGMENT.name());
        transaction.commit();
    }


}
