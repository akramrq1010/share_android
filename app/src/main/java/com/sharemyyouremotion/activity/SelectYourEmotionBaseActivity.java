package com.sharemyyouremotion.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import com.sharemyyouremotion.R;
import com.sharemyyouremotion.fragment.EmotionBaseFragment;
import com.sharemyyouremotion.fragment.EmotionCategoryFragment;
import com.sharemyyouremotion.fragment.EmotionSubCategoryFragment;
import com.sharemyyouremotion.fragment.FeedbackOtherUserFragment;
import com.sharemyyouremotion.fragment.MyDiaryAddFragment;
import com.sharemyyouremotion.fragment.MyDiaryEmotionListFragment;
import com.sharemyyouremotion.fragment.MyDiaryFragment;
import com.sharemyyouremotion.fragment.SelectYourEmotionFragment;
import com.sharemyyouremotion.fragment.ShareEmotionFragment;
import com.sharemyyouremotion.fragment.ExperienceOtherUserFragment;
import com.sharemyyouremotion.fragment.UserListFragment;
import com.sharemyyouremotion.model.EmotionAndCategoriesModel;
import com.sharemyyouremotion.model.GetMyDiaryModel;

import java.util.ArrayList;

public class SelectYourEmotionBaseActivity extends AppCompatActivity implements EmotionBaseFragment.FragmentInteractionListener {
    public static TextView toolbar, tvSkipORAdd;
    public static ImageView imgRight, imgHome;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_your_emotion);

        toolbar = findViewById(R.id.tvToolbar);
        tvSkipORAdd = findViewById(R.id.tvSkipORAdd);
        imgRight = findViewById(R.id.imgRight);
        imgHome = findViewById(R.id.imgHome);

        Intent intent = getIntent();
        String screen = intent.getStringExtra("screen");

        if (screen.equalsIgnoreCase("diary"))
            launchMyDiaryFragment();
        else if(screen.equalsIgnoreCase("feedback"))
            launchFeedbackOtherUserFragment(1);
        else
            launchSelectYourEmotionFragment("");
    }


    @Override
    public void launchSelectYourEmotionFragment(String screen) {
        SelectYourEmotionFragment selectYourEmotionFragment = SelectYourEmotionFragment.newInstance();

        Bundle bundle = new Bundle();
        bundle.putString("screen", screen);
        selectYourEmotionFragment.setArguments(bundle);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_to_right, R.anim.slide_from_left, R.anim.slide_to_left, R.anim.slide_from_right);
        if (screen.equalsIgnoreCase("diary"))
            transaction.replace(R.id.container, selectYourEmotionFragment, EmotionBaseFragment.FragmentId.SELECT_YOUR_EMOTION_FRAGMENT.name()).addToBackStack(EmotionBaseFragment.FragmentId.SELECT_YOUR_EMOTION_FRAGMENT.name());
        else
            transaction.replace(R.id.container, selectYourEmotionFragment, EmotionBaseFragment.FragmentId.SELECT_YOUR_EMOTION_FRAGMENT.name())/*.addToBackStack(HomeBaseFragment.FragmentId.SAVED_FRAGMENT.name())*/;
        transaction.commit();
    }

    @Override
    public void launchEmotionCategoryFragment(ArrayList<EmotionAndCategoriesModel.CategoryList> categoryLists, String screen) {
        EmotionCategoryFragment emotionCategoryFragment = EmotionCategoryFragment.newInstance();

        Bundle bundle = new Bundle();
        bundle.putSerializable("category", categoryLists);
        bundle.putString("screen", screen);
        emotionCategoryFragment.setArguments(bundle);

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_to_right, R.anim.slide_from_left, R.anim.slide_to_left, R.anim.slide_from_right);
        transaction.replace(R.id.container, emotionCategoryFragment, EmotionBaseFragment.FragmentId.EMOTION_CATEGORY_FRAGMENT.name()).addToBackStack(EmotionBaseFragment.FragmentId.EMOTION_CATEGORY_FRAGMENT.name());
        transaction.commit();
    }

    @Override
    public void launchEmotionSubCategoryFragment(ArrayList<EmotionAndCategoriesModel.CategoryList> categoryLists, int pos) {
        EmotionSubCategoryFragment emotionSubCategoryFragment = EmotionSubCategoryFragment.newInstance();

        Bundle bundle = new Bundle();
        bundle.putSerializable("category", categoryLists);
        bundle.putInt("pos", pos);
        emotionSubCategoryFragment.setArguments(bundle);

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_to_right, R.anim.slide_from_left, R.anim.slide_to_left, R.anim.slide_from_right);
        transaction.replace(R.id.container, emotionSubCategoryFragment, EmotionBaseFragment.FragmentId.EMOTION_SUB_CATEGORY_FRAGMENT.name()).addToBackStack(EmotionBaseFragment.FragmentId.EMOTION_SUB_CATEGORY_FRAGMENT.name());
        transaction.commit();
    }

    @Override
    public void launchUserListFragment(int categoryId) {
        UserListFragment userListFragment = UserListFragment.newInstance();

        Bundle bundle = new Bundle();
        bundle.putInt("categoryId", categoryId);
        userListFragment.setArguments(bundle);

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_to_right, R.anim.slide_from_left, R.anim.slide_to_left, R.anim.slide_from_right);
        transaction.replace(R.id.container, userListFragment, EmotionBaseFragment.FragmentId.USER_LIST_FRAGMENT.name()).addToBackStack(EmotionBaseFragment.FragmentId.USER_LIST_FRAGMENT.name());
        transaction.commit();
    }

    @Override
    public void launchShareEmotionFragment(int mediaValue) {
        ShareEmotionFragment shareEmotionFragment = ShareEmotionFragment.newInstance();
        Bundle bundle = new Bundle();
        bundle.putInt("media", mediaValue);
        shareEmotionFragment.setArguments(bundle);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_to_right, R.anim.slide_from_left, R.anim.slide_to_left, R.anim.slide_from_right);
        transaction.replace(R.id.container, shareEmotionFragment, EmotionBaseFragment.FragmentId.SHARE_EMOTION_FRAGMENT.name()).addToBackStack(EmotionBaseFragment.FragmentId.SHARE_EMOTION_FRAGMENT.name());

        transaction.commit();
    }

    @Override
    public void launchFeedbackOtherUserFragment(int mediaValue) {
        FeedbackOtherUserFragment feedbackOtherUserFragment = FeedbackOtherUserFragment.newInstance();
        Bundle bundle = new Bundle();
        bundle.putInt("media", mediaValue);
        feedbackOtherUserFragment.setArguments(bundle);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_to_right, R.anim.slide_from_left, R.anim.slide_to_left, R.anim.slide_from_right);
        transaction.replace(R.id.container, feedbackOtherUserFragment, EmotionBaseFragment.FragmentId.FEEDBACK_OTHER_USER_FRAGMENT.name())/*.addToBackStack(EmotionBaseFragment.FragmentId.FEEDBACK_OTHER_USER_FRAGMENT.name())*/;

        transaction.commit();
    }

    @Override
    public void launchShareMyExperienceFragment(int mediaValue, int categoryId) {
        ExperienceOtherUserFragment shareMyExperienceFragment = ExperienceOtherUserFragment.newInstance();
        Bundle bundle = new Bundle();
        bundle.putInt("media", mediaValue);
        bundle.putInt("categoryId", categoryId);
        shareMyExperienceFragment.setArguments(bundle);

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_to_right, R.anim.slide_from_left, R.anim.slide_to_left, R.anim.slide_from_right);
        transaction.replace(R.id.container, shareMyExperienceFragment, EmotionBaseFragment.FragmentId.SHARE_MY_EXPERIENCE_FRAGMENT.name()).addToBackStack(EmotionBaseFragment.FragmentId.SHARE_MY_EXPERIENCE_FRAGMENT.name());

        transaction.commit();
    }

    @Override
    public void launchMyDiaryFragment() {
        MyDiaryFragment myDiaryFragment = MyDiaryFragment.newInstance();

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_to_right, R.anim.slide_from_left, R.anim.slide_to_left, R.anim.slide_from_right);
        transaction.replace(R.id.container, myDiaryFragment, EmotionBaseFragment.FragmentId.MY_DIARY_FRAGMENT.name())/*.addToBackStack(EmotionBaseFragment.FragmentId.MY_DIARY_FRAGMENT.name())*/;

        transaction.commit();
    }

    @Override
    public void launchMyDiaryAddFragment() {
        MyDiaryAddFragment myDiaryAddFragment = MyDiaryAddFragment.newInstance();

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_to_right, R.anim.slide_from_left, R.anim.slide_to_left, R.anim.slide_from_right);
        transaction.replace(R.id.container, myDiaryAddFragment, EmotionBaseFragment.FragmentId.MY_DIARY_FRAGMENT.name()).addToBackStack(EmotionBaseFragment.FragmentId.MY_DIARY_FRAGMENT.name());

        transaction.commit();
    }


    @Override
    public void launchMyDiaryEmotionListFragment(ArrayList<GetMyDiaryModel> getMyDiaryModels) {
        MyDiaryEmotionListFragment myDiaryAddFragment = MyDiaryEmotionListFragment.newInstance();

        Bundle bundle = new Bundle();
        bundle.putSerializable("diary", getMyDiaryModels);
        myDiaryAddFragment.setArguments(bundle);

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_to_right, R.anim.slide_from_left, R.anim.slide_to_left, R.anim.slide_from_right);
        transaction.replace(R.id.container, myDiaryAddFragment, EmotionBaseFragment.FragmentId.MY_DIARY_FRAGMENT.name()).addToBackStack(EmotionBaseFragment.FragmentId.MY_DIARY_FRAGMENT.name());

        transaction.commit();
    }

}
