package com.sharemyyouremotion.activity;

import android.app.ActivityOptions;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.sharemyyouremotion.R;
import com.sharemyyouremotion.helper.Constants;
import com.sharemyyouremotion.helper.StaticSharedpreference;
import com.sharemyyouremotion.helper.Utilities;

public class SignUpSecondPageActivity extends AppCompatActivity implements View.OnClickListener {
    CardView cardSignUp, cardEmail;
    TextInputEditText edtEmail, edtMobileNo, edtDOB, edtAbout;
    RadioButton radioSexButton;
    Utilities utilities;
    RadioGroup radioGroupGender;
    String email;
    AutoCompleteTextView edtGender;
    String array[] = new String[]{"Male", "Female", "Other", "Not to be disclosed"};
    Typeface tfavv;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup_second);

        initView();

    }

    private void initView() {
        cardSignUp = findViewById(R.id.cardSignUp);
        cardEmail = findViewById(R.id.cardEmail);

        edtEmail = findViewById(R.id.edtEmail);
        edtMobileNo = findViewById(R.id.edtMobileNo);
        edtDOB = findViewById(R.id.edtDOB);
        edtAbout = findViewById(R.id.edtAbout);
        edtGender = findViewById(R.id.edtGender);

        radioGroupGender = findViewById(R.id.radioGroupGender);

        cardSignUp.setOnClickListener(this);
        edtDOB.setOnClickListener(this);
        edtGender.setOnClickListener(this);

      /*  ArrayAdapter adapter = new ArrayAdapter<String>(SignUpSecondPageActivity.this,
                android.R.layout.simple_list_item_1, array);
        edtGender.setAdapter(adapter);*/

        email = StaticSharedpreference.getInfo(Constants.EMAIL, this);
        if (!email.equalsIgnoreCase("")) {
            cardEmail.setVisibility(View.GONE);
        }

        utilities = new Utilities(SignUpSecondPageActivity.this);

        spinner2meth();
    }

    public <ViewGroup> void spinner2meth() {

        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, array) {
            public View getView(int position, View convertView, android.view.ViewGroup parent) {
                tfavv = Typeface.createFromAsset(getAssets(), "COMIC.TTF");
                TextView v = (TextView) super.getView(position, convertView, parent);
                v.setTypeface(tfavv);
                return v;
            }

            public View getDropDownView(int position, View convertView, android.view.ViewGroup parent) {
                TextView v = (TextView) super.getView(position, convertView, parent);
                v.setTypeface(tfavv);
                return v;
            }
        };

        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        edtGender.setAdapter(adapter1);
    }



    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.edtDOB:
                utilities.detePicker(edtDOB);
                break;
            case R.id.cardSignUp:
                if (email.equalsIgnoreCase("")) {
                    if (utilities.isValidEmail(edtEmail.getText().toString()))
                        email = edtEmail.getText().toString();
                }
                int selectedId = radioGroupGender.getCheckedRadioButtonId();
                radioSexButton = findViewById(selectedId);

                if (isValidate()) {

                    ActivityOptions options1 =
                            ActivityOptions.makeCustomAnimation(SignUpSecondPageActivity.this, R.anim.slide_to_right, R.anim.slide_from_left);

                    Intent intent = new Intent(SignUpSecondPageActivity.this, OtpActivity.class);
                    intent.putExtra("email", email);
                    intent.putExtra("mobile", edtMobileNo.getText().toString());
                    intent.putExtra("dob", edtDOB.getText().toString());
                    intent.putExtra("gender", edtGender.getText().toString());
                    intent.putExtra("about", edtAbout.getText().toString());
                    intent.putExtra("quickBloxId", getIntent().getIntExtra("quickBloxId", 0));
                    intent.putExtra("quickBloxName", getIntent().getStringExtra("quickBloxName"));
                    startActivity(intent, options1.toBundle());

                }
                break;

            case R.id.edtGender:
                edtGender.showDropDown();
                break;
        }
    }

    private boolean isValidate() {
        if (email.equalsIgnoreCase("")) {
            Toast.makeText(this, "Please enter Email Id", Toast.LENGTH_LONG).show();
        } else if (!utilities.isValidEmail(email)) {
            Toast.makeText(this, "Please enter valid email id", Toast.LENGTH_SHORT).show();
        } else if (edtMobileNo.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(this, "Please enter Mobile Number", Toast.LENGTH_LONG).show();
        } else if (edtDOB.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(this, "Please Select date of Birth", Toast.LENGTH_LONG).show();
        } else if (edtGender.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(this, "Please select gender", Toast.LENGTH_LONG).show();
        } else {
            return true;
        }
        return false;
    }
}
