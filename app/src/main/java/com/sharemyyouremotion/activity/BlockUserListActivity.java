package com.sharemyyouremotion.activity;

import android.app.ActivityOptions;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.quickblox.chat.QBChatService;
import com.quickblox.chat.QBRestChatService;
import com.quickblox.chat.model.QBChatDialog;
import com.quickblox.chat.model.QBDialogType;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.sample.core.utils.SharedPrefsHelper;
import com.quickblox.users.model.QBUser;
import com.sharemyyouremotion.R;
import com.sharemyyouremotion.adapter.BlockedUserAdapter;
import com.sharemyyouremotion.connection.BaseApiService;
import com.sharemyyouremotion.connection.Connection;
import com.sharemyyouremotion.fragment.EmotionBaseFragment;
import com.sharemyyouremotion.helper.ClickListener;
import com.sharemyyouremotion.helper.Constants;
import com.sharemyyouremotion.helper.DialogClass;
import com.sharemyyouremotion.helper.RecyclerTouchListener;
import com.sharemyyouremotion.helper.StaticSharedpreference;
import com.sharemyyouremotion.model.BlockUserModel;
import com.sharemyyouremotion.model.SpecializationModel;
import com.sharemyyouremotion.qucikblox.ChatData.ChatHelper;
import com.sharemyyouremotion.qucikblox.ChatData.Snippet;

import org.w3c.dom.Text;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by abc on 10/1/2018.
 */

public class BlockUserListActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    ArrayList<BlockUserModel> blockUserModels = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_block_user);

        findViews();

    }

    private void findViews() {
        recyclerView = findViewById(R.id.recyclerView);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());


        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(this, recyclerView, new ClickListener() {
            @Override
            public void onClick(View view, int position) {
                noUserSelect(BlockUserListActivity.this, position);

            }

            @Override
            public void onLongClick(View view, int position) {
                //  popup();
            }
        }));
    }


    public void getBlockedUsers() {

        DialogClass dialogClass = new DialogClass();
        dialogClass.progresesDialog(this);

        Map requestBody = new HashMap<>();

        requestBody.put("BlockBy", StaticSharedpreference.getInt(Constants.USER_ID, this));

        BaseApiService apiService = Connection.getBaseApiServiceInstance();
        Call<JsonObject> call = apiService.getBlockedUsers(requestBody);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.body() != null && response.body().get("StatusCode").getAsInt() == 200) {

                    blockUserModels.clear();
                    Gson gson = new Gson();
                    Type listType = new TypeToken<ArrayList<BlockUserModel>>() {
                    }.getType();


                    if (response.body().get("Users").isJsonNull()) {
                        DialogClass.custom_pd.dismiss();
                        return;
                    }

                    blockUserModels = (ArrayList<BlockUserModel>) gson.fromJson(response.body().getAsJsonArray("Users"), listType);

                    BlockedUserAdapter blockedUserAdapter = new BlockedUserAdapter(BlockUserListActivity.this, blockUserModels);
                    recyclerView.setAdapter(blockedUserAdapter);

                }
                DialogClass.custom_pd.dismiss();
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.v("okhttp", " throw = " + t);

                DialogClass.custom_pd.dismiss();
            }
        });
    }

    public Dialog noUserSelect(final Context context, final int position) {

        LayoutInflater factory = LayoutInflater.from(context);
        final View view = factory.inflate(R.layout.dialog_no_user_seleted, null);
        final AlertDialog alertDialoge = new AlertDialog.Builder(context).create();

        CardView cardNo = view.findViewById(R.id.cardNo);
        CardView cardYes = view.findViewById(R.id.cardYes);
        TextView tvTitle = view.findViewById(R.id.tvTitle);

        tvTitle.setText("Do you want to unblock?");
        cardYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                unBlockUser(blockUserModels.get(position).getUserId() + "", blockUserModels.get(position).getQuickbloxId() + "");

                alertDialoge.dismiss();
            }
        });

        cardNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialoge.dismiss();
            }
        });

        alertDialoge.setCancelable(false);
        alertDialoge.setCanceledOnTouchOutside(true);
        alertDialoge.setView(view);

        alertDialoge.show();
        return alertDialoge;

    }


    public void unBlockUser(final String id, final String quickId) {

        DialogClass dialogClass = new DialogClass();
        dialogClass.progresesDialog(this);

        Map requestBody = new HashMap<>();

        QBUser user = SharedPrefsHelper.getInstance().getQbUser();

        requestBody.put("BlockBy", StaticSharedpreference.getInt(Constants.USER_ID, this) + "");
        requestBody.put("BlockTo", id);

        BaseApiService apiService = Connection.getBaseApiServiceInstance();
        Call<JsonObject> call = apiService.unblockUser(requestBody);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.body() != null && response.body().get("StatusCode").getAsInt() == 200) {


                    QBChatService chatService = QBChatService.getInstance();
                    QBUser user = SharedPrefsHelper.getInstance().getQbUser();

                    if (!chatService.isLoggedIn()) {
                        loginToChat(user, quickId);
                    } else {
                        chatSetting(quickId);
                    }

                }
                DialogClass.custom_pd.dismiss();
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.v("okhttp", " throw = " + t);

                DialogClass.custom_pd.dismiss();
            }
        });
    }

    private void loginToChat(final QBUser user, final String quickBloxIdOpp) {
        //  ProgressDialogFragment.show(getSupportFragmentManager(), R.string.dlg_restoring_chat_session);

        ChatHelper.getInstance().loginToChat(user, new QBEntityCallback<Void>() {
            @Override
            public void onSuccess(Void result, Bundle bundle) {
                Log.v("akram", "Chat login onSuccess()");

                chatSetting(quickBloxIdOpp + "");

            }

            @Override
            public void onError(QBResponseException e) {
                Log.w("akram", "Chat login onError(): " + e);
            }
        });
    }

    private void chatSetting(final String opponentId) {

        final int oppoId = Integer.parseInt(opponentId);

        Snippet createPrivateDialog = new Snippet("Create Private Dialog") {
            @Override
            public void execute() {

                ArrayList<Integer> occupantIdsList = new ArrayList<>();
                occupantIdsList.add(oppoId);

                QBChatDialog dialog = new QBChatDialog();
                dialog.setType(QBDialogType.PRIVATE);
                dialog.setOccupantsIds(occupantIdsList);


                QBRestChatService.createChatDialog(dialog).performAsync(new QBEntityCallback<QBChatDialog>() {
                    @Override
                    public void onSuccess(QBChatDialog dialog, Bundle args) {
                        Log.v("akram", "quick blox chat dialog success");
                        SimpleDateFormat format = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy");

                        Log.v("akram", "qbchatdialog " + dialog);
                        DialogClass.custom_pd.dismiss();
                        ChatActivity.startForResult(BlockUserListActivity.this, 165, dialog);

                       /* DialogClass.custom_pd.dismiss();
                        Intent intent = new Intent(activity, HomeActivity.class);
                        intent.putExtra("tab", "tab");
                        activity.startActivity(intent);
                        activity.finishAffinity();*/
                    }

                    @Override
                    public void onError(QBResponseException errors) {
                        // handleErrors(errors);
                        DialogClass.custom_pd.dismiss();
                    }
                });
            }
        };
        createPrivateDialog.execute();

    }

    @Override
    protected void onResume() {
        super.onResume();
        getBlockedUsers();
    }
}
