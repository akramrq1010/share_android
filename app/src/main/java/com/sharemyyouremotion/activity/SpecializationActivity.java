package com.sharemyyouremotion.activity;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import com.sharemyyouremotion.R;
import com.sharemyyouremotion.adapter.SpecializationAdapter;
import com.sharemyyouremotion.adapter.UserListAdapter;
import com.sharemyyouremotion.connection.BaseApiService;
import com.sharemyyouremotion.connection.Connection;
import com.sharemyyouremotion.helper.Constants;
import com.sharemyyouremotion.helper.DialogClass;
import com.sharemyyouremotion.helper.StaticSharedpreference;
import com.sharemyyouremotion.helper.Utilities;
import com.sharemyyouremotion.model.SpecializationModel;
import com.sharemyyouremotion.model.SpecializationUpdateModel;
import com.sharemyyouremotion.model.UserListModel;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SpecializationActivity extends AppCompatActivity implements View.OnClickListener {

    CardView cardSave;
    RecyclerView recyclerView;
    SpecializationAdapter specializationAdapter;
    ArrayList<SpecializationModel> specializationModels = new ArrayList<>();
    EditText edtSearch;

    DialogClass dialogClass;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_specialization);

        //TODO service
        getValues();

        findView();
        //  loadData();
    }


    private void findView() {

        recyclerView = findViewById(R.id.recyclerView);
        cardSave = findViewById(R.id.cardSave);
        edtSearch = findViewById(R.id.edtSearch);

        cardSave.setOnClickListener(this);

        /*initialize class*/
        dialogClass = new DialogClass();


        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(SpecializationActivity.this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        //TODO service
        specializationAdapter = new SpecializationAdapter(SpecializationActivity.this, specializationModels);
        recyclerView.setAdapter(specializationAdapter);

        edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                specializationAdapter.getFilter().filter(charSequence);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.cardSave:
                Utilities utilities = new Utilities(this);
                if (!utilities.isOnline())
                    return;

                //TODO UI
               /* ActivityOptions options1 =
                        ActivityOptions.makeCustomAnimation(SpecializationActivity.this, R.anim.slide_to_right, R.anim.slide_from_left);

                startActivity(new Intent(SpecializationActivity.this, HomeActivity.class), options1.toBundle());
*/

                //TODO service
                ArrayList<SpecializationUpdateModel.Specialization> specializationUpdateList = new ArrayList<>();
                SpecializationUpdateModel specializationUpdateModel = new SpecializationUpdateModel();

                List<SpecializationModel> specialization = ((SpecializationAdapter) specializationAdapter)
                        .getSpecialization();

                for (int i = 0; i < specialization.size(); i++) {
                    SpecializationUpdateModel.Specialization specialization1 = new SpecializationUpdateModel.Specialization();

                    if (specialization.get(i).getRadioButtonPosition() != null)
                        if (!specialization.get(i).getRadioButtonPosition().equalsIgnoreCase("")) {

                            int id = specialization.get(i).getCategoryId();
                            String level = specialization.get(i).getRadioButtonPosition();

                            specialization1.setCategoryId(id);
                            specialization1.setLevel(level);

                            specializationUpdateList.add(specialization1);
                        }
                }

                specializationUpdateModel.setSpecializations(specializationUpdateList);


                updateSpecialization(specializationUpdateModel);
                break;
        }
    }


    public void updateSpecialization(SpecializationUpdateModel specializationUpdateModel) {

        dialogClass.progresesDialog(this);

        specializationUpdateModel.setUserId(StaticSharedpreference.getInt(Constants.USER_ID, SpecializationActivity.this));

        BaseApiService apiService = Connection.getBaseApiServiceInstance();
        Call<JsonObject> call = apiService.updateSpecialization(specializationUpdateModel);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
//                Log.d("tag", "onResponse" + "" + response.body().toString());
                if (response.body() != null && response.body().get("StatusCode").getAsInt() == 200) {

                    StaticSharedpreference.removeKey(Constants.SPECIALIZATION_LIST, SpecializationActivity.this);
                    ActivityOptions options1 =
                            ActivityOptions.makeCustomAnimation(SpecializationActivity.this, R.anim.slide_to_right, R.anim.slide_from_left);
                    StaticSharedpreference.saveInfo(Constants.GOTO_PAGE, "home", SpecializationActivity.this);
                    startActivity(new Intent(SpecializationActivity.this, HomeActivity.class), options1.toBundle());
                    finishAffinity();
                } else {
                    dialogClass.messageDialog(SpecializationActivity.this, response.body().get("Message").getAsString() + "");
                    Toast.makeText(SpecializationActivity.this, response.body().get("Message").getAsString(), Toast.LENGTH_SHORT).show();
                }
                DialogClass.custom_pd.dismiss();

                /* JsonObject response = new  JsonParser().parse(out.body()).getAsJsonObject();*/

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.v("okhttp", " throw = " + t);

                DialogClass.custom_pd.dismiss();
            }
        });
    }


    private void getValues() {

        Gson gson = new Gson();
        String json = StaticSharedpreference.getInfo(Constants.SPECIALIZATION_LIST, this);
        Type type = new TypeToken<List<SpecializationModel>>() {
        }.getType();
        specializationModels = gson.fromJson(json, type);

    }
}
