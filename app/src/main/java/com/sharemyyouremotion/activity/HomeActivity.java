package com.sharemyyouremotion.activity;

import android.app.ActivityOptions;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.sharemyyouremotion.R;
import com.sharemyyouremotion.fragment.ChatFragment;
import com.sharemyyouremotion.fragment.HomeFragment;
import com.sharemyyouremotion.fragment.LoginFragment;
import com.sharemyyouremotion.fragment.MyExperienceFragment;
import com.sharemyyouremotion.fragment.MyFeedbackFragment;
import com.sharemyyouremotion.fragment.MyQuestionFragment;
import com.sharemyyouremotion.fragment.NotificationFragment;
import com.sharemyyouremotion.fragment.UserchatListFragment;
import com.sharemyyouremotion.helper.Constants;

import java.util.ArrayList;
import java.util.List;

import static com.sharemyyouremotion.fragment.LoginFragment.guest;

public class HomeActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private int[] tabIcons = {
            R.drawable.ic_save,
            R.drawable.ic_save,
            R.drawable.ic_save,
            R.drawable.ic_save,
            R.drawable.ic_save
    };
    ImageView imgProfile;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.notificationBar));
        }

        Log.v("akram", "message = " + getIntent().getStringExtra("hello"));

        initView();

        if (getIntent().getStringExtra("tab") != null)
            if (!getIntent().getStringExtra("tab").equalsIgnoreCase("")) {
                viewPager.setCurrentItem(4);
                tabLayout.setupWithViewPager(viewPager);
            }


    }

    private void initView() {
        toolbar = findViewById(R.id.toolbar);
        viewPager = findViewById(R.id.viewpager);
        tabLayout = findViewById(R.id.tabs);
        imgProfile = findViewById(R.id.imgProfile);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);

        imgProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ActivityOptions options1 =
                        ActivityOptions.makeCustomAnimation(HomeActivity.this, R.anim.slide_to_right, R.anim.slide_from_left);
                Intent intent = new Intent(HomeActivity.this, MyProfileActivity.class);
                startActivity(intent, options1.toBundle());
            }
        });

        // setSupportActionBar(toolbar);

        if (guest.equalsIgnoreCase("guest")) {
            imgProfile.setVisibility(View.GONE);
            setupViewPagerGuest(viewPager);
            tabLayout.setVisibility(View.GONE);
        } else {
            setupViewPager(viewPager);
        }

        tabLayout.setupWithViewPager(viewPager);

        changeTabsFont();
        //viewPager.setCurrentItem();
        //  setupTabIcons();
    }

    private void changeTabsFont() {
        ViewGroup vg = (ViewGroup) tabLayout.getChildAt(0);
        int tabsCount = vg.getChildCount();
        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
            int tabChildsCount = vgTab.getChildCount();
            for (int i = 0; i < tabChildsCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    AssetManager mgr = getAssets();
                    Typeface tf = Typeface.createFromAsset(mgr, "Lora-BoldItalic.ttf");//Font file in /assets
                    ((TextView) tabViewChild).setTypeface(tf);
                }
            }
        }
    }


    private void setupTabIcons() {

        TextView tabOne = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabOne.setText("  HOME");
        tabOne.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_home_new, 0, 0, 0);
        tabLayout.getTabAt(0).setCustomView(tabOne);

        TextView tabTwo = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabTwo.setText("  MY QUESTION");
        tabTwo.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_send, 0, 0, 0);
        tabLayout.getTabAt(1).setCustomView(tabTwo);

        TextView tabThree = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabThree.setText(" MY FEEDBACK");
        tabThree.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_receive, 0, 0, 0);
        tabLayout.getTabAt(2).setCustomView(tabThree);

        TextView tabFour = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabFour.setText(" CHAT");
        tabFour.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_cht, 0, 0, 0);
        tabLayout.getTabAt(3).setCustomView(tabFour);

        TextView tabFive = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabFive.setText(" NOTIFICATION");
        tabFive.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_notification, 0, 0, 0);
        tabLayout.getTabAt(4).setCustomView(tabFive);
    }

   /* private void setupTabIcons() {
        tabLayout.getTabAt(0).setIcon(tabIcons[0]);
        tabLayout.getTabAt(1).setIcon(tabIcons[1]);
        tabLayout.getTabAt(2).setIcon(tabIcons[2]);
        tabLayout.getTabAt(2).setIcon(tabIcons[2]);
        tabLayout.getTabAt(2).setIcon(tabIcons[2]);
    }*/

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new HomeFragment(), "Home");
        adapter.addFragment(new MyQuestionFragment(), "My Question");
        adapter.addFragment(new MyFeedbackFragment(), "My Feedback");
        adapter.addFragment(new MyExperienceFragment(), "My Experience");
        adapter.addFragment(new UserchatListFragment(), "Chat");
        //adapter.addFragment(new ChatFragment(), "Notification");
        adapter.addFragment(new NotificationFragment(), "Notification");

        viewPager.setAdapter(adapter);
    }

    private void setupViewPagerGuest(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new HomeFragment(), "Home");


        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (!guest.equalsIgnoreCase("guest")) {
            //noinspection SimplifiableIfStatement
            if (id == R.id.profile) {
                ActivityOptions options1 =
                        ActivityOptions.makeCustomAnimation(HomeActivity.this, R.anim.slide_to_right, R.anim.slide_from_left);
                Intent intent = new Intent(HomeActivity.this, MyProfileActivity.class);
                startActivity(intent, options1.toBundle());
                return true;
            } else {
                ActivityOptions options1 =
                        ActivityOptions.makeCustomAnimation(HomeActivity.this, R.anim.slide_to_right, R.anim.slide_from_left);
                Intent intent = new Intent(HomeActivity.this, BlockUserListActivity.class);
                startActivity(intent, options1.toBundle());
            }
        } else {
            Toast.makeText(this, "Please sign up first", Toast.LENGTH_SHORT).show();
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Constants.emotionLists.clear();
    }
}
