package com.sharemyyouremotion.activity;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.VideoView;

import com.sharemyyouremotion.R;
import com.sharemyyouremotion.helper.Constants;
import com.sharemyyouremotion.helper.DialogClass;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Created by abc on 8/24/2018.
 */

public class AudioTestActivity extends AppCompatActivity {

    public static SeekBar seekbarMusic;
    public static ImageView imgPlay, imgPause;
    public static MediaPlayer mediaPlayer;
    public static double finalTime;
    public int oneTimeOnly = 0;
    private Handler myHandler = new Handler();
    public static TextView tvRunningTime, tvFinalTime;
    Handler handler = new Handler();
    public static double startTime = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_audio_test);

        init();
        //playSong("http://devadmin.shareyouremotions.in/Uploads/Questions/audio/21/5a4bc580-1f9a-4b3b-aed4-fdaa3e5677ab.mp3");
    }

    private void init() {
        seekbarMusic = (SeekBar) findViewById(R.id.seekbarMusic);
        imgPlay = (ImageView) findViewById(R.id.imgPlay);
        imgPause = (ImageView) findViewById(R.id.imgPause);
        tvRunningTime = findViewById(R.id.tvRunningTime);
        tvFinalTime = findViewById(R.id.tvFinalTime);

        imgPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mediaPlayer.start();
            }
        });

        imgPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mediaPlayer.pause();
            }
        });


        seekbarMusic.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (mediaPlayer != null && fromUser) {
                    mediaPlayer.seekTo(progress);
                }
            }
        });
    }

    Runnable UpdateSongTime = new Runnable() {
        public void run() {

            // Log.v("ak","run");
            if (mediaPlayer != null) {
                startTime = mediaPlayer.getCurrentPosition();
            } else {
            }

            tvRunningTime.setText(String.format("%d min, %d sec",
                    TimeUnit.MILLISECONDS.toMinutes((long) startTime),
                    TimeUnit.MILLISECONDS.toSeconds((long) startTime) -
                            TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.
                                    toMinutes((long) startTime)))
            );
            seekbarMusic.setProgress((int) startTime);
            myHandler.postDelayed(this, 100);
        }
    };


    public void afterPrepare() {

        mediaPlayer.start();

        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {


              /*  if (StaticClass.fav_or_playlist.equalsIgnoreCase("fav")) {
                    if (StaticClass.getFavouriteSongsListArrModels.size() == (StaticClass.position)) {
                        StaticClass.position = 0;
                    }
                    uiThread(StaticClass.position);
                    String songLink = StaticClass.getFavouriteSongsListArrModels.get(StaticClass.position).getLink();
                    oneTimeOnly = 0;
                    playSong(songLink);
                } else {
                    if (StaticClass.getSongListArrModels.size() == (StaticClass.position)) {
                        StaticClass.position = 0;
                    }
                    uiThread(StaticClass.position);
                    String songLink = StaticClass.getSongListArrModels.get(StaticClass.position).getLink();
                    oneTimeOnly = 0;
                    playSong(songLink);
                }*/
            }
        });


        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                finalTime = mediaPlayer.getDuration();
                startTime = mediaPlayer.getCurrentPosition();

                if (oneTimeOnly == 0) {
                    seekbarMusic.setMax((int) finalTime);
                    oneTimeOnly = 1;
                }

                tvFinalTime.setText(String.format("%d min, %d sec",
                        TimeUnit.MILLISECONDS.toMinutes((long) finalTime),
                        TimeUnit.MILLISECONDS.toSeconds((long) finalTime) -
                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes((long)
                                        finalTime)))
                );

                tvRunningTime.setText(String.format("%d min, %d sec",
                        TimeUnit.MILLISECONDS.toMinutes((long) startTime),
                        TimeUnit.MILLISECONDS.toSeconds((long) startTime) -
                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes((long)
                                        startTime)))
                );

                //set seekbar according to sound
                seekbarMusic.setProgress((int) startTime);
                myHandler.postDelayed(UpdateSongTime, 100);
            }
        }, 200);
    }

    public void startCamera(View view) {
        //create Intent to record video and return it to the calling application
        Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);

        File photoFile = null;
        try {
            photoFile = createImageFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        videoFile = photoFile;

        Uri photoURI = FileProvider.getUriForFile(this,
                getPackageName() + ".provider", photoFile);

        intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
        intent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 5);
        //start the video capture Intent
        startActivityForResult(intent, 1);

    }

    File videoFile;

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                //display the video in VideoView
                VideoView videoView = (VideoView) findViewById(R.id.videoview);
                //get video uri
                Log.v("akram", "vide path " + videoFile.getPath());

                ActivityOptions options1 =
                        ActivityOptions.makeCustomAnimation(this, R.anim.slide_to_right, R.anim.slide_from_left);
                Intent intent = new Intent(this, WebViewForVideoActivity.class);
                intent.putExtra("video", videoFile.getPath());
                intent.putExtra("local", "local");
                //  intent.putExtra("QuestionId",myQuestionFragments.get(position).getQuestionId());
                startActivity(intent, options1.toBundle());


               /* Uri videoUri=data.getData();

                Log.v("akram","vide path "+videoFile.getPath());

                //set the video uri to VideoView
                videoView.setVideoURI(videoUri);
                //set media controller allowing you to play, pause, and move the video forward and backward
                videoView.setMediaController(new MediaController(this));
                //play the video
                videoView.start();
*/
            } else if (resultCode == RESULT_CANCELED) {
                // User cancelled the video capture
            } else {
                // video capture failed
            }
        }
    }

    public File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "MP4_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_MOVIES);

        File thumbimg = File.createTempFile(
                imageFileName,  /* prefix */
                ".mp4",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        return thumbimg;
    }

}
