package com.sharemyyouremotion.activity;

import android.graphics.Typeface;
import android.opengl.Visibility;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sharemyyouremotion.R;

/**
 * Created by abc on 10/13/2018.
 */

public class ArtificialFormActivity extends AppCompatActivity implements View.OnClickListener {
    AutoCompleteTextView edtArtificialRole, edtLanguage;
    String arrayRole[] = new String[]{"Father", "Mother", "Brother", "Sister", "Son", "Daughter"};
    String arrayLanguage[] = new String[]{"English", "Hindi", "Telugu", "Marathi", "Tamil", "Punjabi"};
    String arrayNo[] = new String[]{"0", "1", "2", "3"};
    Typeface tfavv;
    AutoCompleteTextView edtNoDaughter, edtNoSon, edtNoBrother, edtNoSister;
    LinearLayout linearSonDuaghter, linearDFirst, linearDSecond, linearDThird, linearSonFirst, linearSonSecond, linearSonThird,
            linearBrother, linearBrotherFirst, linearBrotherSecond, linearBrotherThird,
            linearSister, linearSisterFirst, linearSisterSecond, linearSisterThird;
    RelativeLayout relativeSonD, relativeBrother, relativeSister;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_artificial);
        initViews();

    }

    private void initViews() {
        edtArtificialRole = findViewById(R.id.edtArtificialRole);
        edtLanguage = findViewById(R.id.edtLanguage);
        edtNoDaughter = findViewById(R.id.edtNoDaughter);
        edtNoSon = findViewById(R.id.edtNoSon);
        linearSonDuaghter = findViewById(R.id.linearSonDuaghter);
        linearDFirst = findViewById(R.id.linearDFirst);
        linearDSecond = findViewById(R.id.linearDSecond);
        linearDThird = findViewById(R.id.linearDThird);
        linearSonFirst = findViewById(R.id.linearSonFirst);
        linearSonSecond = findViewById(R.id.linearSonSecond);
        linearSonThird = findViewById(R.id.linearSonThird);
        relativeSonD = findViewById(R.id.relativeSonD);

        relativeBrother = findViewById(R.id.relativeBrother);
        linearBrother = findViewById(R.id.linearBrother);
        edtNoBrother = findViewById(R.id.edtNoBrother);
        linearBrotherFirst = findViewById(R.id.linearBrotherFirst);
        linearBrotherSecond = findViewById(R.id.linearBrotherSecond);
        linearBrotherThird = findViewById(R.id.linearBrotherThird);

        relativeSister = findViewById(R.id.relativeSister);
        linearSister = findViewById(R.id.linearSister);
        edtNoSister = findViewById(R.id.edtNoSister);
        linearSisterFirst = findViewById(R.id.linearSisterFirst);
        linearSisterSecond = findViewById(R.id.linearSisterSecond);
        linearSisterThird = findViewById(R.id.linearSisterThird);

        clickListener();


        spinnerRole();
        spinnerLanguage();
        spinnerNoSon();
        spinnerNoDaughter();
        spinnerNoSister();
        spinnerNoBrother();
    }

    private void clickListener() {
        edtArtificialRole.setOnClickListener(this);
        edtLanguage.setOnClickListener(this);
        edtNoDaughter.setOnClickListener(this);
        edtNoSon.setOnClickListener(this);
        edtNoBrother.setOnClickListener(this);
        edtNoSister.setOnClickListener(this);

        dropDownListener();
    }

    private void dropDownListener() {
        edtArtificialRole.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (i == 1 || i == 0) {
                    linearSonDuaghter.setVisibility(View.VISIBLE);
                    relativeSonD.setVisibility(View.VISIBLE);
                    relativeBrother.setVisibility(View.GONE);
                    relativeSister.setVisibility(View.GONE);

                    brotherLayoutVisibility(8, 8, 8);
                    sisterLayoutVisibility(8, 8, 8);
                    sonLayoutVisibility(8, 8, 8);
                    daughterLayoutVisibility(8, 8, 8);
                    edtNoBrother.setText("");
                    edtNoSister.setText("");
                    edtNoSon.setText("");
                    edtNoDaughter.setText("");
                } else if (i == 2) {
                    linearBrother.setVisibility(View.VISIBLE);
                    relativeBrother.setVisibility(View.VISIBLE);
                    relativeSonD.setVisibility(View.GONE);
                    relativeSister.setVisibility(View.GONE);

                    sonLayoutVisibility(8, 8, 8);
                    daughterLayoutVisibility(8, 8, 8);
                    sisterLayoutVisibility(8, 8, 8);
                    brotherLayoutVisibility(8, 8, 8);
                    edtNoDaughter.setText("");
                    edtNoSon.setText("");
                    edtNoSister.setText("");
                } else if (i == 3) {
                    linearSister.setVisibility(View.VISIBLE);
                    relativeSister.setVisibility(View.VISIBLE);
                    relativeBrother.setVisibility(View.GONE);
                    relativeSonD.setVisibility(View.GONE);

                    sonLayoutVisibility(8, 8, 8);
                    daughterLayoutVisibility(8, 8, 8);
                    sisterLayoutVisibility(8, 8, 8);
                    edtNoDaughter.setText("");
                    edtNoSon.setText("");
                    brotherLayoutVisibility(8, 8, 8);
                    edtNoBrother.setText("");
                }

            }
        });


        edtNoSon.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (i == 0)
                    sonLayoutVisibility(8, 8, 8);
                else if (i == 1)
                    sonLayoutVisibility(0, 8, 8);
                else if (i == 2)
                    sonLayoutVisibility(0, 0, 8);
                else if (i == 3)
                    sonLayoutVisibility(0, 0, 0);
            }
        });

        edtNoDaughter.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (i == 0)
                    daughterLayoutVisibility(8, 8, 8);
                else if (i == 1)
                    daughterLayoutVisibility(0, 8, 8);
                else if (i == 2)
                    daughterLayoutVisibility(0, 0, 8);
                else if (i == 3)
                    daughterLayoutVisibility(0, 0, 0);
            }
        });

        edtNoBrother.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (i == 0)
                    brotherLayoutVisibility(8, 8, 8);
                else if (i == 1)
                    brotherLayoutVisibility(0, 8, 8);
                else if (i == 2)
                    brotherLayoutVisibility(0, 0, 8);
                else if (i == 3)
                    brotherLayoutVisibility(0, 0, 0);
            }
        });

        edtNoSister.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (i == 0)
                    sisterLayoutVisibility(8, 8, 8);
                else if (i == 1)
                    sisterLayoutVisibility(0, 8, 8);
                else if (i == 2)
                    sisterLayoutVisibility(0, 0, 8);
                else if (i == 3)
                    sisterLayoutVisibility(0, 0, 0);
            }
        });

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.edtArtificialRole:
                edtArtificialRole.showDropDown();
                break;
            case R.id.edtLanguage:
                edtLanguage.showDropDown();
                break;
            case R.id.edtNoSon:
                edtNoSon.showDropDown();
                break;
            case R.id.edtNoDaughter:
                edtNoDaughter.showDropDown();
                break;

            case R.id.edtNoBrother:
                edtNoBrother.showDropDown();
                break;

            case R.id.edtNoSister:
                edtNoSister.showDropDown();
                break;
        }
    }

    private void sonLayoutVisibility(int son1, int son2, int son3) {

        linearSonFirst.setVisibility(son1);
        linearSonSecond.setVisibility(son2);
        linearSonThird.setVisibility(son3);

    }

    private void daughterLayoutVisibility(int daughter1, int daughter2, int daughter3) {
        linearDFirst.setVisibility(daughter1);
        linearDSecond.setVisibility(daughter2);
        linearDThird.setVisibility(daughter3);
    }

    private void brotherLayoutVisibility(int brother1, int brother2, int brother3) {
        linearBrotherFirst.setVisibility(brother1);
        linearBrotherSecond.setVisibility(brother2);
        linearBrotherThird.setVisibility(brother3);
    }

    private void sisterLayoutVisibility(int sister1, int sister2, int sister3) {
        linearSisterFirst.setVisibility(sister1);
        linearSisterSecond.setVisibility(sister2);
        linearSisterThird.setVisibility(sister3);
    }


    public <ViewGroup> void spinnerNoSon() {

        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, arrayNo) {
            public View getView(int position, View convertView, android.view.ViewGroup parent) {
                tfavv = Typeface.createFromAsset(getAssets(), "COMIC.TTF");
                TextView v = (TextView) super.getView(position, convertView, parent);
                v.setTypeface(tfavv);
                return v;
            }

            public View getDropDownView(int position, View convertView, android.view.ViewGroup parent) {
                TextView v = (TextView) super.getView(position, convertView, parent);
                v.setTypeface(tfavv);
                return v;
            }
        };

        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        edtNoSon.setAdapter(adapter1);
    }

    public <ViewGroup> void spinnerNoDaughter() {

        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, arrayNo) {
            public View getView(int position, View convertView, android.view.ViewGroup parent) {
                tfavv = Typeface.createFromAsset(getAssets(), "COMIC.TTF");
                TextView v = (TextView) super.getView(position, convertView, parent);
                v.setTypeface(tfavv);
                return v;
            }

            public View getDropDownView(int position, View convertView, android.view.ViewGroup parent) {
                TextView v = (TextView) super.getView(position, convertView, parent);
                v.setTypeface(tfavv);
                return v;
            }
        };
        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        edtNoDaughter.setAdapter(adapter1);
    }

    public <ViewGroup> void spinnerNoBrother() {

        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, arrayNo) {
            public View getView(int position, View convertView, android.view.ViewGroup parent) {
                tfavv = Typeface.createFromAsset(getAssets(), "COMIC.TTF");
                TextView v = (TextView) super.getView(position, convertView, parent);
                v.setTypeface(tfavv);
                return v;
            }

            public View getDropDownView(int position, View convertView, android.view.ViewGroup parent) {
                TextView v = (TextView) super.getView(position, convertView, parent);
                v.setTypeface(tfavv);
                return v;
            }
        };
        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        edtNoBrother.setAdapter(adapter1);
    }

    public <ViewGroup> void spinnerNoSister() {

        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, arrayNo) {
            public View getView(int position, View convertView, android.view.ViewGroup parent) {
                tfavv = Typeface.createFromAsset(getAssets(), "COMIC.TTF");
                TextView v = (TextView) super.getView(position, convertView, parent);
                v.setTypeface(tfavv);
                return v;
            }

            public View getDropDownView(int position, View convertView, android.view.ViewGroup parent) {
                TextView v = (TextView) super.getView(position, convertView, parent);
                v.setTypeface(tfavv);
                return v;
            }
        };
        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        edtNoSister.setAdapter(adapter1);
    }

    public <ViewGroup> void spinnerRole() {

        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, arrayRole) {
            public View getView(int position, View convertView, android.view.ViewGroup parent) {
                tfavv = Typeface.createFromAsset(getAssets(), "COMIC.TTF");
                TextView v = (TextView) super.getView(position, convertView, parent);
                v.setTypeface(tfavv);
                return v;
            }

            public View getDropDownView(int position, View convertView, android.view.ViewGroup parent) {
                TextView v = (TextView) super.getView(position, convertView, parent);
                v.setTypeface(tfavv);
                return v;
            }
        };

        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        edtArtificialRole.setAdapter(adapter1);
    }


    public <ViewGroup> void spinnerLanguage() {

        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, arrayLanguage) {
            public View getView(int position, View convertView, android.view.ViewGroup parent) {
                tfavv = Typeface.createFromAsset(getAssets(), "COMIC.TTF");
                TextView v = (TextView) super.getView(position, convertView, parent);
                v.setTypeface(tfavv);
                return v;
            }

            public View getDropDownView(int position, View convertView, android.view.ViewGroup parent) {
                TextView v = (TextView) super.getView(position, convertView, parent);
                v.setTypeface(tfavv);
                return v;
            }
        };

        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        edtLanguage.setAdapter(adapter1);
    }
}
