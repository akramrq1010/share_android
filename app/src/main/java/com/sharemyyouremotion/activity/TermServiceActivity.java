package com.sharemyyouremotion.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebView;

import com.sharemyyouremotion.R;

/**
 * Created by abc on 10/10/2018.
 */

public class TermServiceActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_term_service);


        WebView webView = findViewById(R.id.webView);

        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setBuiltInZoomControls(true);

        // webView.setWebViewClient(new WebViewClient());
        webView.loadUrl("http://shareyouremotions.in/privacy_policy.html");
    }
}
