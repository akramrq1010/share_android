package com.sharemyyouremotion.activity;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.gson.JsonObject;
import com.sharemyyouremotion.R;
import com.sharemyyouremotion.connection.BaseApiService;
import com.sharemyyouremotion.connection.Connection;
import com.sharemyyouremotion.helper.Constants;
import com.sharemyyouremotion.helper.DialogClass;
import com.sharemyyouremotion.helper.StaticSharedpreference;
import com.sharemyyouremotion.helper.Utilities;
import com.sharemyyouremotion.qucikblox.QuickBloxRequestMethod;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import id.zelory.compressor.Compressor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by abc on 8/27/2018.
 */

public class MyProfileActivity extends AppCompatActivity implements View.OnClickListener {
    ImageView imgLogout;
    DialogClass dialogClass = new DialogClass();
    TextInputEditText edtName, edtEmail, edtMobileNo, edtDOB, edtAbout;
    CircleImageView imgProfile;
    RadioButton edtFemale, edtMale;
    String mediaPath = "";
    CardView cardUpdate;
    Utilities utilities;
    ImageView imgCamera;
    AutoCompleteTextView edtGender;
    String array[] = new String[]{"Male", "Female", "Other", "Not to be disclosed"};
    Typeface tfavv;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setBackgroundDrawableResource(R.drawable.bg_splash_bl);
        setContentView(R.layout.activity_my_profile);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.notificationBar));
        }
        utilities = new Utilities(this);
        initViews();

    }

    private void initViews() {
        imgLogout = findViewById(R.id.imgLogout);
        edtName = findViewById(R.id.edtName);
        edtEmail = findViewById(R.id.edtEmail);
        edtMobileNo = findViewById(R.id.edtMobileNo);
        edtDOB = findViewById(R.id.edtDOB);
        imgProfile = findViewById(R.id.imgProfile);
        edtFemale = findViewById(R.id.edtFemale);
        edtMale = findViewById(R.id.edtMale);
        edtAbout = findViewById(R.id.edtAbout);
        cardUpdate = findViewById(R.id.cardUpdate);
        imgCamera = findViewById(R.id.imgCamera);
        edtGender = findViewById(R.id.edtGender);

        imgLogout.setOnClickListener(this);
        cardUpdate.setOnClickListener(this);
        imgCamera.setOnClickListener(this);
        edtGender.setOnClickListener(this);

        setData();
        spinner2meth();
       /* ArrayAdapter adapter = new ArrayAdapter<String>(MyProfileActivity.this,
                android.R.layout.simple_list_item_1, array);
        edtGender.setAdapter(adapter);*/

    }

    public <ViewGroup> void spinner2meth() {

        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, array) {
            public View getView(int position, View convertView, android.view.ViewGroup parent) {
                tfavv = Typeface.createFromAsset(getAssets(), "COMIC.TTF");
                TextView v = (TextView) super.getView(position, convertView, parent);
                v.setTypeface(tfavv);
                return v;
            }

            public View getDropDownView(int position, View convertView, android.view.ViewGroup parent) {
                TextView v = (TextView) super.getView(position, convertView, parent);
                v.setTypeface(tfavv);
                return v;
            }
        };

        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        edtGender.setAdapter(adapter1);
    }

    private void setData() {
        edtName.setText(StaticSharedpreference.getInfo(Constants.NAME, this));
        edtEmail.setText(StaticSharedpreference.getInfo(Constants.EMAIL, this));
        edtMobileNo.setText(StaticSharedpreference.getInfo(Constants.MOBILE_NO, this));
        edtDOB.setText(StaticSharedpreference.getInfo(Constants.DOB, this));
        edtAbout.setText(StaticSharedpreference.getInfo(Constants.ABOUT, this));

        edtGender.setText(StaticSharedpreference.getInfo(Constants.GENDER, this));

        Glide.with(this)
                .load(StaticSharedpreference.getInfo(Constants.PROFILE_PIC, this))
                .into(imgProfile);

        if (StaticSharedpreference.getInfo(Constants.GENDER, this).equalsIgnoreCase("male")) {
            edtMale.setChecked(true);
        } else {
            edtFemale.setChecked(false);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imgLogout:
                dialogClass.logoutDialog(this);

                break;

            case R.id.cardUpdate:
                if (utilities.isOnline())
                    updateProfile();

                break;

            case R.id.imgCamera:
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE},

                        1);

                break;

            case R.id.edtGender:
             //   edtGender.showDropDown();

                break;
        }
    }


    public void updateProfile() {

        dialogClass.progresesDialog(this);


        MultipartBody.Part body;

        if (mediaPath.equalsIgnoreCase("")) {
            body = MultipartBody.Part.createFormData("files", "");
        } else {
            File file = new File(mediaPath);
            try {
                file = new Compressor(this).compressToFile(file);
            } catch (IOException e) {
                e.printStackTrace();
            }
            RequestBody reqbodyFile = RequestBody.create(MediaType.parse("image/*"), file);
            body = MultipartBody.Part.createFormData("files", file.getName(), reqbodyFile);

        }
        RequestBody name = RequestBody.create(MediaType.parse("text"), "" + edtName.getText().toString());
        final RequestBody about = RequestBody.create(MediaType.parse("text"), edtAbout.getText().toString());
        RequestBody userId = RequestBody.create(MediaType.parse("text"), StaticSharedpreference.getInt(Constants.USER_ID, MyProfileActivity.this) + "");

        BaseApiService apiService = Connection.getBaseApiServiceInstance();
        Call<JsonObject> call = apiService.updateProfile(userId, name, about, body);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
//                Log.d("tag", "onResponse" + "" + response.body().toString());
                if (response.body() != null && response.body().get("StatusCode").getAsInt() == 200) {

                    /*save userId*/
                    DialogClass.custom_pd.dismiss();
                    /*check user profile is completed or not*/
                    StaticSharedpreference.saveInfo(Constants.NAME,
                            response.body().get("UserProfile").getAsJsonObject().get("Name").getAsString(), MyProfileActivity.this);
                    StaticSharedpreference.saveInfo(Constants.PROFILE_PIC,
                            response.body().get("UserProfile").getAsJsonObject().get("ProfileThumbnail").getAsString(), MyProfileActivity.this);

                    onBackPressed();
                    Toast.makeText(MyProfileActivity.this, "Your Profile Updated Successfully.", Toast.LENGTH_SHORT).show();


                } else {
                    dialogClass.messageDialog(MyProfileActivity.this, response.body().get("Message").getAsString() + "");
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.v("okhttp", " throw = " + t);

                DialogClass.custom_pd.dismiss();
            }
        });

    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                Map<String, Integer> perms = new HashMap<String, Integer>();
                // Initial
                perms.put(Manifest.permission.READ_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.CAMERA, PackageManager.PERMISSION_GRANTED);

                // Fill with results
                for (int i = 0; i < permissions.length; i++)
                    perms.put(permissions[i], grantResults[i]);
                // Check for ACCESS_FINE_LOCATION
                if (perms.get(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                        ) {
                    // All Permissions Granted
                    // insertDummyContact();

                    utilities.selectImage(null, MyProfileActivity.this, false);

                } else {
                    // Permission Denied
                    Toast.makeText(this, "Some Permission is Denied", Toast.LENGTH_SHORT)
                            .show();
                }

            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == Utilities.INTENTCAMERA) {
                File path = Utilities.cameraFile;
                Glide.with(this)
                        .load(path.getPath())
                        .into(imgProfile);

                mediaPath = path.getPath();


            } else if (requestCode == Utilities.INTENTGALLERY && data.getData() != null) {
//                imagePaths.add(utilities.getPathFromUri(data.getData(), getActivity()));
                Glide.with(this)
                        .load(data.getData())
                        .into(imgProfile);

                Uri selectedImageUri = data.getData();
                String tempPath = utilities.getPathFromUri(selectedImageUri, this);
                //String tempPath = getPath(selectedImageUri);
                mediaPath = tempPath;

            }

        }
    }

}
