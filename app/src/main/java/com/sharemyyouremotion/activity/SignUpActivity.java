package com.sharemyyouremotion.activity;

import android.Manifest;
import android.app.Activity;
import android.app.ActivityOptions;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import com.sharemyyouremotion.R;
import com.sharemyyouremotion.connection.BaseApiService;
import com.sharemyyouremotion.connection.Connection;
import com.sharemyyouremotion.helper.Constants;
import com.sharemyyouremotion.helper.DialogClass;
import com.sharemyyouremotion.helper.FacebookLoginRespons;
import com.sharemyyouremotion.helper.StaticSharedpreference;
import com.sharemyyouremotion.helper.Utilities;
import com.sharemyyouremotion.model.SpecializationModel;
import com.sharemyyouremotion.qucikblox.QuickBloxRequestMethod;

import org.json.JSONObject;

import java.io.File;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignUpActivity extends AppCompatActivity implements View.OnClickListener, GoogleApiClient.OnConnectionFailedListener {
    CardView cardSignUp;
    CardView btnFBSignUp, btnGPSignUp;
    RadioGroup radioGroupGender;
    private CallbackManager callbackManager;
    TextInputEditText edtName, edtEmail, edtMobileNo, edtPassword, edtConfirmPassword, edtDOB, edtAbout;
    RadioButton radioSexButton;
    Utilities utilities;

    /*google +*/
    GoogleApiClient mGoogleApiClient;
    GoogleSignInOptions gso;
    DialogClass dialogClass;
    ImageView imgCamera;
    String profilePic = "";
    CircleImageView imgProfile;
    AutoCompleteTextView edtGender;
    String array[] = new String[]{"Male", "Female", "Other", "Not to be disclosed"};
    Typeface tfavv;
    TextView tvTermService;
    CheckBox cbTermService;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*set Layout bac*/
        getWindow().setBackgroundDrawableResource(R.drawable.bg_splash_bl);
        buildNewGoogleApiClient();
        setContentView(R.layout.activity_signup);

        /*initialize facebook sdk*/
        FacebookSdk.sdkInitialize(SignUpActivity.this);
        callbackManager = CallbackManager.Factory.create();
        loginfacebook(SignUpActivity.this, callbackManager);

        /*find ids and initialize objects*/
        initView();
    }

    private void initView() {

        cardSignUp = findViewById(R.id.cardSignUp);
        btnFBSignUp = findViewById(R.id.btnFBSignUp);
        btnGPSignUp = findViewById(R.id.btnGPSignUp);
        edtGender = findViewById(R.id.edtGender);

        edtName = findViewById(R.id.edtName);
        edtEmail = findViewById(R.id.edtEmail);
        edtMobileNo = findViewById(R.id.edtMobileNo);
        edtPassword = findViewById(R.id.edtPassword);
        edtConfirmPassword = findViewById(R.id.edtConfirmPassword);
        edtDOB = findViewById(R.id.edtDOB);
        imgProfile = findViewById(R.id.imgProfile);
        imgCamera = findViewById(R.id.imgCamera);
        edtAbout = findViewById(R.id.edtAbout);
        tvTermService = findViewById(R.id.tvTermService);
        cbTermService = findViewById(R.id.cbTermService);

        SpannableString content = new SpannableString("Terms of Service");
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        tvTermService.setText(content);

        radioGroupGender = findViewById(R.id.radioGroupGender);

        cardSignUp.setOnClickListener(this);
        btnFBSignUp.setOnClickListener(this);
        btnGPSignUp.setOnClickListener(this);
        edtDOB.setOnClickListener(this);
        imgCamera.setOnClickListener(this);
        edtGender.setOnClickListener(this);
        tvTermService.setOnClickListener(this);


      /*  ArrayAdapter adapter = new ArrayAdapter<String>(SignUpActivity.this,
                android.R.layout.simple_list_item_1, array);
        edtGender.setAdapter(adapter);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        edtGender.setAdapter(adapter);
*/

        /*initialize class*/
        utilities = new Utilities(SignUpActivity.this);
        dialogClass = new DialogClass();

        spinner2meth();
    }

    public <ViewGroup> void spinner2meth() {

        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, array) {
            public View getView(int position, View convertView, android.view.ViewGroup parent) {
                tfavv = Typeface.createFromAsset(getAssets(), "COMIC.TTF");
                TextView v = (TextView) super.getView(position, convertView, parent);
                v.setTypeface(tfavv);
                return v;
            }

            public View getDropDownView(int position, View convertView, android.view.ViewGroup parent) {
                TextView v = (TextView) super.getView(position, convertView, parent);
                v.setTypeface(tfavv);
                return v;
            }
        };

        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        edtGender.setAdapter(adapter1);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.cardSignUp:
                /*Get gender value through radio button*/
                int selectedId = radioGroupGender.getCheckedRadioButtonId();
                radioSexButton = findViewById(selectedId);

                if (!utilities.isOnline())
                    return;

                if (isValidate()) {
                    if (cbTermService.isChecked()) {
                        QuickBloxRequestMethod quickBloxRequestMethod = new QuickBloxRequestMethod();
                        quickBloxRequestMethod.quickBloxSignup(SignUpActivity.this, edtName.getText().toString(), edtEmail.getText().toString(),
                                edtMobileNo.getText().toString(), edtDOB.getText().toString(), edtPassword.getText().toString(), edtGender.getText().toString(),
                                edtAbout.getText().toString(), profilePic, "false", "", "", "signup", null);
                    } else {
                        Toast.makeText(this, "Please check terms of service", Toast.LENGTH_SHORT).show();
                    }

                }
                break;
            case R.id.btnFBSignUp:
                if (cbTermService.isChecked()) {
                    if (utilities.isOnline())
                        LoginManager.getInstance().logInWithReadPermissions(SignUpActivity.this, Arrays.asList("user_friends", "public_profile", "email"));
                } else {
                    Toast.makeText(this, "Please check terms of service", Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.btnGPSignUp:
                if (!utilities.isOnline())
                    return;

                if (cbTermService.isChecked()) {

                    Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
                    startActivityForResult(signInIntent, 3);

                } else {
                    Toast.makeText(this, "Please check terms of service", Toast.LENGTH_SHORT).show();
                }

                break;

            case R.id.edtDOB:

                utilities.detePicker(edtDOB);
                break;

            case R.id.imgCamera:

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE},

                        1);
                break;

            case R.id.edtGender:

                edtGender.showDropDown();
                break;

            case R.id.tvTermService:

                startActivity(new Intent(this, TermServiceActivity.class));
                break;
        }
    }


    public void createSignUp(final String nameStr, final String email, final String socialTypeStr, String S_id, String url) {

        dialogClass.progresesDialog(this);
        if (url.equalsIgnoreCase("null")) {
            url = "";
        }
        SharedPreferences sharedPreferences = getSharedPreferences("ShareAllYourEmotionFCM", MODE_PRIVATE);
        String token = sharedPreferences.getString("fcm", "");

        MultipartBody.Part body;
        RequestBody reqbodyFile = RequestBody.create(MediaType.parse("image/*"), "");
        body = MultipartBody.Part.createFormData("files", "", reqbodyFile);

        RequestBody clientDoc = RequestBody.create(MediaType.parse("text"), "" + "ClientDocs");
        RequestBody name = RequestBody.create(MediaType.parse("text"), "" + nameStr);
        RequestBody emailId = RequestBody.create(MediaType.parse("text"), "" + email);
        final RequestBody profilePic = RequestBody.create(MediaType.parse("text"), "" + url);
        RequestBody password = RequestBody.create(MediaType.parse("text"), "");
        RequestBody gender = RequestBody.create(MediaType.parse("text"), "");
        RequestBody mobile = RequestBody.create(MediaType.parse("text"), "");
        RequestBody isSocialUser = RequestBody.create(MediaType.parse("text"), "true");
        final RequestBody socialType = RequestBody.create(MediaType.parse("text"), "" + socialTypeStr);
        RequestBody socialId = RequestBody.create(MediaType.parse("text"), S_id);
        RequestBody deviceToken = RequestBody.create(MediaType.parse("text"), token);
        RequestBody deviceType = RequestBody.create(MediaType.parse("text"), "android");
        RequestBody about = RequestBody.create(MediaType.parse("text"), edtAbout.getText().toString());
        RequestBody quickId = RequestBody.create(MediaType.parse("text"), ""); // always blank for normal signup
        RequestBody quickName = RequestBody.create(MediaType.parse("text"), ""); // always blank for normal signup


        BaseApiService apiService = Connection.getBaseApiServiceInstance();
        Call<JsonObject> call = apiService.signUp(clientDoc, name, emailId, password, gender, mobile, isSocialUser,
                socialType, socialId, deviceToken, deviceType, gender, profilePic, about, quickId, quickName, body);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
//                Log.d("tag", "onResponse" + "" + response.body().toString());
                if (response.body() != null && response.body().get("StatusCode").getAsInt() == 200) {


                    /*save userId*/

                    QuickBloxRequestMethod quickBloxRequestMethod = new QuickBloxRequestMethod();
                    DialogClass.custom_pd.dismiss();
                    /*check user profile is completed or not*/
                    if (response.body().get("UserProfile").getAsJsonObject().get("IsProfileCompleted").getAsBoolean()) {

                        quickBloxRequestMethod.quickbloxlogin(response.body().get("UserProfile").getAsJsonObject().get("QuickbloxName").getAsString() + ""
                                , SignUpActivity.this, response.body().get("UserProfile").getAsJsonObject().get("QuickbloxId").getAsString() + "", "fb", response);


                    } else {
                        quickBloxRequestMethod.quickBloxSignup(SignUpActivity.this, nameStr, email, "", "", "", "",
                                "", "", "", socialTypeStr, "", "fb", response);


                    }


                } else {
                    dialogClass.messageDialog(SignUpActivity.this, response.body().get("Message").getAsString() + "");
                }


            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.v("okhttp", " throw = " + t);

                DialogClass.custom_pd.dismiss();
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                Map<String, Integer> perms = new HashMap<String, Integer>();
                // Initial
                perms.put(Manifest.permission.READ_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.CAMERA, PackageManager.PERMISSION_GRANTED);

                // Fill with results
                for (int i = 0; i < permissions.length; i++)
                    perms.put(permissions[i], grantResults[i]);
                // Check for ACCESS_FINE_LOCATION
                if (perms.get(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                        ) {
                    // All Permissions Granted
                    // insertDummyContact();

                    utilities.selectImage(null, SignUpActivity.this, false);

                } else {
                    // Permission Denied
                    Toast.makeText(this, "Some Permission is Denied", Toast.LENGTH_SHORT)
                            .show();
                }

            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 3) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            getSignInResult(result);
        } else if (requestCode == Utilities.INTENTCAMERA) {

            if (resultCode == RESULT_OK) {
                File path = Utilities.cameraFile;
                Glide.with(this)
                        .load(path.getPath())
                        .into(imgProfile);

                profilePic = path.getPath();

            }
        } else if (requestCode == Utilities.INTENTGALLERY && data.getData() != null) {

            if (resultCode == RESULT_OK) {
//                imagePaths.add(utilities.getPathFromUri(data.getData(), getActivity()));
                Glide.with(this)
                        .load(data.getData())
                        .into(imgProfile);

                Uri selectedImageUri = data.getData();
                String tempPath = utilities.getPathFromUri(selectedImageUri, this);
                //String tempPath = getPath(selectedImageUri);
                profilePic = tempPath;
            }
        } else {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }

    }

    private boolean isValidate() {

        if (edtName.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(this, "Please enter Name", Toast.LENGTH_LONG).show();
        } else if (edtEmail.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(this, "Please enter Email Id", Toast.LENGTH_LONG).show();
        } else if (!utilities.isValidEmail(edtEmail.getText().toString())) {
            Toast.makeText(this, "Please enter valid email id", Toast.LENGTH_SHORT).show();
        } else if (edtMobileNo.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(this, "Please enter Mobile Number", Toast.LENGTH_LONG).show();
        } else if (edtDOB.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(this, "Please Select Date of Birth", Toast.LENGTH_LONG).show();
        } else if (edtPassword.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(this, "Please enter Password", Toast.LENGTH_LONG).show();
        } else if (edtConfirmPassword.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(this, "Please enter Confirm Password", Toast.LENGTH_LONG).show();
        } else if (!edtConfirmPassword.getText().toString().equals(edtPassword.getText().toString())) {
            Toast.makeText(this, "Password and Confirm Password Doesn't match", Toast.LENGTH_LONG).show();
        } else if (edtGender.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(this, "Please select gender", Toast.LENGTH_LONG).show();
        } else {
            return true;
        }
        return false;
    }


    /*response from google login*/
    private void getSignInResult(GoogleSignInResult result) {

        if (result.isSuccess()) {

            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();

            Log.v("akramr", "user name " + acct.getDisplayName());
            Log.v("akramr", "email " + acct.getPhotoUrl());
           /* Static_variable.signup_type = "gplus";
            Static_variable.fname_str = acct.getDisplayName();
            Static_variable.email_str = acct.getEmail();
            Static_variable.social_id = acct.getId();


            if(acct.getPhotoUrl()!=null){
                Static_variable.social_pic_str = acct.getPhotoUrl().toString();

            }*/
            //createSignUp(acct.getDisplayName(), acct.getEmail(), "", "google", "true", acct.getId());


            createSignUp(acct.getDisplayName(), acct.getEmail(), "google", acct.getId(), acct.getPhotoUrl() + "");

        }
    }

    static JSONObject json = null;
    static String email_str, name, fbId;

    /*response from facebook login*/
    public JSONObject loginfacebook(final Activity activity, CallbackManager callbackManager) {

        //for facebook

        LoginButton b = new LoginButton(activity);
        b.setReadPermissions(Arrays.asList("user_friends", "public_profile", "email"));

        b.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            private ProfileTracker mProfileTracker;

            @Override
            public void onSuccess(LoginResult loginResult) {

                GraphRequest request = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {


                                json = response.getJSONObject();
                                Log.v("akram", "dob " + json);
                                try {

                                    if (json != null) {
                                        if (json.has("email")) {
                                            email_str = json.getString("email");
                                        } else {
                                            email_str = "";
                                        }
                                        name = json.getString("name");
                                        fbId = json.getString("id");
                                    }
                                } catch (Exception e) {
                                }
                                String url = "https://graph.facebook.com/" + fbId + "/picture?type=large";
                                createSignUp(name, email_str, "facebook", fbId, url);

                                /*ActivityOptions options1 =
                                        ActivityOptions.makeCustomAnimation(activity, R.anim.slide_to_right, R.anim.slide_from_left);

                                Intent intent = new Intent(activity, SignUpSecondPageActivity.class);
                                intent.putExtra("name", name);
                                intent.putExtra("email", "");
                                intent.putExtra("socialType", "facebook");
                                intent.putExtra("socialId", fbId);
                                activity.startActivity(intent, options1.toBundle());*/

                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,link,gender,email,picture");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                Log.v("akram", "cancel");
            }

            @Override
            public void onError(FacebookException error) {
                Log.v("akram", "error " + error);

            }
        });

        return json;
    }


    private void buildNewGoogleApiClient() {

        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_to_left, R.anim.slide_from_right);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
