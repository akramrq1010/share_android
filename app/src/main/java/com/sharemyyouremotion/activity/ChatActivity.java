package com.sharemyyouremotion.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.JsonObject;
import com.quickblox.chat.QBChatService;
import com.quickblox.chat.QBRestChatService;
import com.quickblox.chat.listeners.QBChatDialogTypingListener;
import com.quickblox.chat.model.QBAttachment;
import com.quickblox.chat.model.QBChatDialog;
import com.quickblox.chat.model.QBChatMessage;
import com.quickblox.chat.model.QBDialogType;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.core.helper.StringifyArrayList;
import com.quickblox.messages.QBPushNotifications;
import com.quickblox.messages.model.QBEnvironment;
import com.quickblox.messages.model.QBEvent;
import com.quickblox.messages.model.QBEventType;
import com.quickblox.messages.model.QBNotificationType;
import com.quickblox.messages.model.QBPushType;
import com.quickblox.sample.core.ui.dialog.ProgressDialogFragment;
import com.quickblox.sample.core.utils.SharedPrefsHelper;
import com.quickblox.sample.core.utils.Toaster;
import com.quickblox.sample.core.utils.imagepick.ImagePickHelper;
import com.quickblox.sample.core.utils.imagepick.OnImagePickedListener;
import com.quickblox.users.model.QBUser;
import com.sharemyyouremotion.R;
import com.sharemyyouremotion.connection.BaseApiService;
import com.sharemyyouremotion.connection.Connection;
import com.sharemyyouremotion.helper.Constants;
import com.sharemyyouremotion.helper.DialogClass;
import com.sharemyyouremotion.helper.StaticSharedpreference;
import com.sharemyyouremotion.qucikblox.App;
import com.sharemyyouremotion.qucikblox.ChatData.BaseActivity;
import com.sharemyyouremotion.qucikblox.ChatData.ChatAdapter;
import com.sharemyyouremotion.qucikblox.ChatData.ChatHelper;
import com.sharemyyouremotion.qucikblox.ChatData.PaginationHistoryListener;
import com.sharemyyouremotion.qucikblox.ChatData.QbChatDialogMessageListenerImp;
import com.sharemyyouremotion.qucikblox.ChatData.QbDialogHolder;
import com.sharemyyouremotion.qucikblox.ChatData.Snippet;
import com.sharemyyouremotion.qucikblox.ChatData.VerboseQbChatConnectionListener;
import com.sharemyyouremotion.qucikblox.QuickBloxRequestMethod;

import org.jivesoftware.smack.ConnectionListener;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPException;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import se.emilsjolander.stickylistheaders.StickyListHeadersListView;

//import com.quickblox.sample.chat.ui.adapter.AttachmentPreviewAdapter;
//import com.quickblox.sample.chat.ui.adapter.ChatAdapter;
//import com.quickblox.sample.chat.ui.widget.AttachmentPreviewAdapterView;
//import com.quickblox.sample.chat.utils.qb.PaginationHistoryListener;
//import com.quickblox.sample.chat.utils.qb.VerboseQbChatConnectionListener;

public class ChatActivity extends BaseActivity implements OnImagePickedListener {
    private static final String TAG = ChatActivity.class.getSimpleName();
    private static final int REQUEST_CODE_ATTACHMENT = 721;
    private static final int REQUEST_CODE_SELECT_PEOPLE = 752;
    private static final String PROPERTY_SAVE_TO_HISTORY = "save_to_history";
    public static final String EXTRA_DIALOG_ID = "dialogId";
    private QBChatDialogTypingListener privateChatDialogTypingListener;
    private ProgressBar progressBar;
    private StickyListHeadersListView messagesListView;
    private EditText messageEditText;
    CardView button_chat_send;
    private LinearLayout attachmentPreviewContainerLayout;
    private Snackbar snackbar;
    private ChatAdapter chatAdapter;
    TextView title;
    App mApp;
    ImageView imgEdit;
    String message = "";
    private ConnectionListener chatConnectionListener;
    private QBChatDialog qbChatDialog;
    private ArrayList<QBChatMessage> unShownMessages;
    private int skipPagination = 0;
    private ChatMessageListener chatMessageListener;

    public static void startForResult(Activity activity, int code, QBChatDialog dialogId) {
        Intent intent = new Intent(activity, ChatActivity.class);
        intent.putExtra(ChatActivity.EXTRA_DIALOG_ID, dialogId);
        activity.startActivityForResult(intent, code);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setBackgroundDrawableResource(R.drawable.bg_home);
        setContentView(R.layout.demo_activity_chat);
        mApp = (App) getApplication();


        title = findViewById(R.id.toolbarTitle);
        imgEdit = (ImageView) findViewById(R.id.imgEdit);
        button_chat_send = (CardView) findViewById(R.id.button_chat_send);

        imgEdit.setVisibility(View.GONE);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);


        Log.v(TAG, "onCreate ChatActivity on Thread ID = " + Thread.currentThread().getId());

        qbChatDialog = (QBChatDialog) getIntent().getSerializableExtra(EXTRA_DIALOG_ID);

        Log.v(TAG, "deserialized dialog = " + qbChatDialog);
        qbChatDialog.initForChat(QBChatService.getInstance());

        chatMessageListener = new ChatMessageListener();

        qbChatDialog.addMessageListener(chatMessageListener);

        initChatConnectionListener();

        initViews();
        initChat();

        initIsTypingListener();
        title.setText("" + qbChatDialog.getName());

        button_chat_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.v("akram", "click listener");
                onSendChatClick(view);
            }
        });
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        if (qbChatDialog != null) {
            outState.putString(EXTRA_DIALOG_ID, qbChatDialog.getDialogId());
        }
        super.onSaveInstanceState(outState, outPersistentState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (qbChatDialog == null) {
            qbChatDialog = QbDialogHolder.getInstance().getChatDialogById(savedInstanceState.getString(EXTRA_DIALOG_ID));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        ChatHelper.getInstance().addConnectionListener(chatConnectionListener);
        mApp.clearNotification();
    }

    @Override
    protected void onPause() {
        super.onPause();
        ChatHelper.getInstance().removeConnectionListener(chatConnectionListener);
    }

    @Override
    public void onBackPressed() {
        releaseChat();
        sendDialogId();

        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // getMenuInflater().inflate(R.menu.activity_chat, menu);

     /*   MenuItem menuItemLeave = menu.findItem(R.id.menu_chat_action_leave);
        MenuItem menuItemAdd = menu.findItem(R.id.menu_block_user);
        MenuItem menuItemDelete = menu.findItem(R.id.menu_chat_action_delete);

        menuItemLeave.setVisible(false);
        menuItemDelete.setVisible(false);
        menuItemAdd.setVisible(true);
      */


        getMenuInflater().inflate(R.menu.main_chat, menu);
        qbChatDialog.addIsTypingListener(privateChatDialogTypingListener);


        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {

            case R.id.block_user:

                callBlockApi();

                return true;

           /* case R.id.menu_report_user:
                callReportApi();

                return true;

            case R.id.menu_block_user:
                callBlockApi();

                return true;

            case R.id.menu_chat_action_leave:

                return true;

            case R.id.menu_chat_action_delete:
                //deleteChat();
                return true;

            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item)*/
        }
        return false;
    }

    public void callBlockApi() {

        DialogClass dialogClass = new DialogClass();
        dialogClass.progresesDialog(this);
        Map requestBody = new HashMap<>();

        QBUser currentUser = SharedPrefsHelper.getInstance().getQbUser();

        requestBody.put("BlockBy", currentUser.getId());

        if (qbChatDialog.getOccupants().get(0).equals(currentUser.getId())) {
            requestBody.put("BlockTo", qbChatDialog.getOccupants().get(1));
        } else {
            requestBody.put("BlockTo", qbChatDialog.getOccupants().get(0));
        }

        Log.v("akram", "blockby " + currentUser.getId());


        BaseApiService apiService = Connection.getBaseApiServiceInstance();
        Call<JsonObject> call = apiService.blockUser(requestBody);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.body() != null && response.body().get("StatusCode").getAsInt() == 200) {

                    deleteChat();
                }
                DialogClass.custom_pd.dismiss();
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.v("okhttp", " throw = " + t);

                DialogClass.custom_pd.dismiss();
            }
        });
    }

    private void sendDialogId() {
        Intent result = new Intent();
        result.putExtra(EXTRA_DIALOG_ID, qbChatDialog.getDialogId());
        setResult(RESULT_OK, result);
    }

    private void leaveGroupChat() {
        ProgressDialogFragment.show(getSupportFragmentManager());
        ChatHelper.getInstance().exitFromDialog(qbChatDialog, new QBEntityCallback<QBChatDialog>() {
            @Override
            public void onSuccess(QBChatDialog qbDialog, Bundle bundle) {
                ProgressDialogFragment.hide(getSupportFragmentManager());
                QbDialogHolder.getInstance().deleteDialog(qbDialog);
                finish();
            }

            @Override
            public void onError(QBResponseException e) {
                ProgressDialogFragment.hide(getSupportFragmentManager());
//                showErrorSnackbar(R.string.error_leave_chat, e, new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        leaveGroupChat();
//                    }
//                });
            }
        });
    }

    @SuppressWarnings("unchecked")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {

        }
    }

    @Override
    public void onImagePicked(int requestCode, File file) {
        switch (requestCode) {
            case REQUEST_CODE_ATTACHMENT:

                break;
        }
    }

    @Override
    public void onImagePickError(int requestCode, Exception e) {

    }

    @Override
    public void onImagePickClosed(int requestCode) {
        // ignore
    }

    @Override
    protected View getSnackbarAnchorView() {
        return findViewById(R.id.list_chat_messages);
    }

    public void onSendChatClick(View view) {

        String text = messageEditText.getText().toString().trim();
        if (!TextUtils.isEmpty(text)) {
            sendChatMessage(text, null);
        }
    }

    public void onAttachmentsClick(View view) {
        new ImagePickHelper().pickAnImage(this, REQUEST_CODE_ATTACHMENT);
    }

    public void showMessage(QBChatMessage message) {
        if (chatAdapter != null) {
            chatAdapter.add(message);
            scrollMessageListDown();
        } else {
            if (unShownMessages == null) {
                unShownMessages = new ArrayList<>();
            }
            unShownMessages.add(message);
        }
    }

    private void initViews() {
        //actionBar.setDisplayHomeAsUpEnabled(true);

        messagesListView = _findViewById(R.id.list_chat_messages);
        messageEditText = _findViewById(R.id.edit_chat_message);
        progressBar = _findViewById(R.id.progress_chat);
        attachmentPreviewContainerLayout = _findViewById(R.id.layout_attachment_preview_container);
        messageEditText.addTextChangedListener(new TextWatcher1(messageEditText));
    }

    private void sendChatMessage(String text, QBAttachment attachment) {
        message = messageEditText.getText().toString() + "";
        QBChatMessage chatMessage = new QBChatMessage();
        if (attachment != null) {
            chatMessage.addAttachment(attachment);
        } else {
            chatMessage.setBody(text);
        }
        chatMessage.setProperty(PROPERTY_SAVE_TO_HISTORY, "1");
        chatMessage.setDateSent(System.currentTimeMillis() / 1000);
        chatMessage.setMarkable(true);


        try {
            qbChatDialog.sendMessage(chatMessage);

            if (QBDialogType.PRIVATE.equals(qbChatDialog.getType())) {
                showMessage(chatMessage);
            }
            messageEditText.setText("");
            createEvent.execute();
        } catch (SmackException.NotConnectedException e) {
            Log.w(TAG, e);
            Toaster.shortToast("Can't send a message, You are not connected to chat");

        }
    }

    private void initChat() {
        switch (qbChatDialog.getType()) {
            case GROUP:
            case PUBLIC_GROUP:
                joinGroupChat();
                break;

            case PRIVATE:
                loadDialogUsers();
                break;

            default:
                Toaster.shortToast(String.format("%s %s", "unsupo", qbChatDialog.getType().name()));
                finish();
                break;
        }
    }

    private void joinGroupChat() {
        progressBar.setVisibility(View.VISIBLE);
        ChatHelper.getInstance().join(qbChatDialog, new QBEntityCallback<Void>() {
            @Override
            public void onSuccess(Void result, Bundle b) {
                if (snackbar != null) {
                    snackbar.dismiss();
                }
                loadDialogUsers();
            }

            @Override
            public void onError(QBResponseException e) {
                progressBar.setVisibility(View.GONE);

            }
        });
    }

    private void leaveGroupDialog() {
        try {
            ChatHelper.getInstance().leaveChatDialog(qbChatDialog);
        } catch (XMPPException | SmackException.NotConnectedException e) {
            Log.w(TAG, e);
        }
    }

    private void releaseChat() {
        qbChatDialog.removeMessageListrener(chatMessageListener);
        if (!QBDialogType.PRIVATE.equals(qbChatDialog.getType())) {
            leaveGroupDialog();
        }
    }

    private void updateDialog(final ArrayList<QBUser> selectedUsers) {
        ChatHelper.getInstance().updateDialogUsers(qbChatDialog, selectedUsers,
                new QBEntityCallback<QBChatDialog>() {
                    @Override
                    public void onSuccess(QBChatDialog dialog, Bundle args) {
                        qbChatDialog = dialog;
                        loadDialogUsers();
                    }

                    @Override
                    public void onError(QBResponseException e) {

                    }
                }
        );
    }

    private void loadDialogUsers() {
        ChatHelper.getInstance().getUsersFromDialog(qbChatDialog, new QBEntityCallback<ArrayList<QBUser>>() {
            @Override
            public void onSuccess(ArrayList<QBUser> users, Bundle bundle) {
                // setChatNameToActionBar();
                loadChatHistory();
            }

            @Override
            public void onError(QBResponseException e) {

            }
        });
    }

//    private void setChatNameToActionBar() {
//        String chatName = QbDialogUtils.getDialogName(qbChatDialog);
//        ActionBar ab = getSupportActionBar();
//        if (ab != null) {
//            ab.setTitle(chatName);
//            ab.setDisplayHomeAsUpEnabled(true);
//            ab.setHomeButtonEnabled(true);
//        }
//    }

    private void loadChatHistory() {
        ChatHelper.getInstance().loadChatHistory(qbChatDialog, skipPagination, new QBEntityCallback<ArrayList<QBChatMessage>>() {
            @Override
            public void onSuccess(ArrayList<QBChatMessage> messages, Bundle args) {
                // The newest messages should be in the end of list,
                // so we need to reverse list to show messages in the right order
                Collections.reverse(messages);
                if (chatAdapter == null) {
                    chatAdapter = new ChatAdapter(ChatActivity.this, qbChatDialog, messages, ChatActivity.this);
                    chatAdapter.setPaginationHistoryListener(new PaginationHistoryListener() {
                        @Override
                        public void downloadMore() {
                            loadChatHistory();
                        }
                    });
                    chatAdapter.setOnItemInfoExpandedListener(new ChatAdapter.OnItemInfoExpandedListener() {
                        @Override
                        public void onItemInfoExpanded(final int position) {
                            if (isLastItem(position)) {
                                // HACK need to allow info textview visibility change so posting it via handler
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        messagesListView.setSelection(position);
                                    }
                                });
                            } else {
                                messagesListView.smoothScrollToPosition(position);
                            }
                        }

                        private boolean isLastItem(int position) {
                            return position == chatAdapter.getCount() - 1;
                        }
                    });
                    if (unShownMessages != null && !unShownMessages.isEmpty()) {
                        List<QBChatMessage> chatList = chatAdapter.getList();
                        for (QBChatMessage message : unShownMessages) {
                            if (!chatList.contains(message)) {
                                chatAdapter.add(message);
                            }
                        }
                    }
                    messagesListView.setAdapter(chatAdapter);
                    messagesListView.setAreHeadersSticky(false);
                    messagesListView.setDivider(null);
                } else {
                    chatAdapter.addList(messages);
                    messagesListView.setSelection(messages.size());
                }
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onError(QBResponseException e) {
                progressBar.setVisibility(View.GONE);
                skipPagination -= ChatHelper.CHAT_HISTORY_ITEMS_PER_PAGE;

            }
        });
        skipPagination += ChatHelper.CHAT_HISTORY_ITEMS_PER_PAGE;
    }

    private void scrollMessageListDown() {
        messagesListView.setSelection(messagesListView.getCount() - 1);
    }

    private void deleteChat() {
        ChatHelper.getInstance().deleteDialog(qbChatDialog, new QBEntityCallback<Void>() {
            @Override
            public void onSuccess(Void aVoid, Bundle bundle) {
                setResult(RESULT_OK);
                finish();
            }

            @Override
            public void onError(QBResponseException e) {

            }
        });
    }


    Snippet deleteDialog = new Snippet("Blocking user") {
        @Override
        public void execute() {

            //String dialogID = "5444bba7535c121d3302245f";
            String dialogID = qbChatDialog.getDialogId();

            QBRestChatService.deleteDialog(dialogID, true).performAsync(new QBEntityCallback<Void>() {
                @Override
                public void onSuccess(Void result, Bundle bundle) {
                    Log.i(TAG, "dialog deleted");

                    setResult(RESULT_OK);
                    finish();

                }

                @Override
                public void onError(QBResponseException errors) {

                }
            });
        }
    };


    private void initChatConnectionListener() {
        chatConnectionListener = new VerboseQbChatConnectionListener(getSnackbarAnchorView()) {
            @Override
            public void reconnectionSuccessful() {
                super.reconnectionSuccessful();
                skipPagination = 0;
                switch (qbChatDialog.getType()) {
                    case GROUP:
                        chatAdapter = null;
                        // Join active room if we're in Group Chat
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                joinGroupChat();
                            }
                        });
                        break;
                }
            }
        };
    }

    public class ChatMessageListener extends QbChatDialogMessageListenerImp {
        @Override
        public void processMessage(String s, QBChatMessage qbChatMessage, Integer integer) {
            showMessage(qbChatMessage);
        }
    }


    Snippet sendStopTypingInPrivateChat = new Snippet("send stop typing (private chat)") {
        @Override
        public void execute() {
            if (qbChatDialog == null) {
                Log.e("Avinash", ("Stop Typing other user"));

                return;
            }

            qbChatDialog.addIsTypingListener(privateChatDialogTypingListener);

            Log.e("Avinash", ("Stop Typing me"));
            initIsTypingListener();

            try {
                qbChatDialog.sendStopTypingNotification();
            } catch (XMPPException | SmackException.NotConnectedException e) {

                Log.e("Avinash", ("send stop typing error: "));
            }
        }
    };
    Snippet sendIsTypingInPrivateChat = new Snippet("send is typing (private chat)") {
        @Override
        public void execute() {
            if (qbChatDialog == null) {
                Log.e("Avinash", ("Typing other user"));

                return;
            }

            qbChatDialog.addIsTypingListener(privateChatDialogTypingListener);

            Log.e("Avinash", ("Typing me"));

            initIsTypingListener();

            try {
                qbChatDialog.sendIsTypingNotification();
            } catch (XMPPException | SmackException.NotConnectedException e) {
                Log.e("Avinash", ("send stop typing error: "));

            }
        }
    };


    private void initIsTypingListener() {
//
//        // Create 'is typing' listener
//        //


        privateChatDialogTypingListener = new QBChatDialogTypingListener() {
            @Override
            public void processUserIsTyping(String dialogId, Integer senderId) {
                Log.e("Avinash", ("send stop typing error: "));
                //   txt.setText("Typing.....");
                //  setActionBarTitle("Typing.....");

                title.setText("Typing.....");
            }

            @Override
            public void processUserStopTyping(String dialogId, Integer senderId) {
                Log.e("Avinash", (" stop typing error: "));

                // setActionBarTitle(ChatHelper.getCurrentUser().getFullName());
                title.setText("" + qbChatDialog.getName());
            }
        };
    }


    public class TextWatcher1 implements TextWatcher {
        EditText e;
        Timer timer;

        public TextWatcher1(EditText e) {
            this.e = e;

        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {


        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (timer != null) {
                timer.cancel();
                sendIsTypingInPrivateChat.execute();
            }
        }

        @Override
        public void afterTextChanged(Editable s) {

            //timer  =new Timer();
            final long DELAY = 1000; //
//            timer.cancel();
            timer = new Timer();
            timer.schedule(
                    new TimerTask() {
                        @Override
                        public void run() {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (e.getId() == R.id.edit_chat_message) {


                                        sendStopTypingInPrivateChat.execute();

                                    }

                                }
                            });


                        }
                    },
                    DELAY
            );
        }
    }


    protected QBEvent buildEvent() {
        // recipient
        StringifyArrayList<Integer> userIds = new StringifyArrayList<>();
        StringifyArrayList<Integer> setId = new StringifyArrayList<>();

        //  userIds.add(34197134);


        QBEvent event = new QBEvent();

        event.setType(QBEventType.ONE_SHOT);
        event.setEnvironment(QBEnvironment.PRODUCTION);
        event.setNotificationType(QBNotificationType.PUSH);
        // event.setNotificationChannel(QBNotificationChannel.GCM);
        // event.setId(3232435);

        event.setMessage("hello");
        event.setPushType(QBPushType.GCM);
        HashMap<String, Object> data = new HashMap<>();
        data.put("data.message", "" + message);

        data.put("data.title", "" + StaticSharedpreference.getInfo(Constants.NAME, getApplicationContext()));
        if (qbChatDialog.getOccupants() != null) {
            if (StaticSharedpreference.getInfo(Constants.QUICK_BLOX_ID, getApplicationContext()).equals("" + qbChatDialog.getOccupants().get(0))) {
                data.put("data.otherID", "" + qbChatDialog.getOccupants().get(1));
                data.put("data.meID", "" + qbChatDialog.getOccupants().get(0));
                setId.add(qbChatDialog.getOccupants().get(1));
                event.setUserIds(setId);
            } else {
                data.put("data.otherID", "" + qbChatDialog.getOccupants().get(0));
                data.put("data.meID", "" + qbChatDialog.getOccupants().get(1));

                setId.add(qbChatDialog.getOccupants().get(0));
                event.setUserIds(setId);

                userIds.add(qbChatDialog.getOccupants().get(0));
                userIds.add(qbChatDialog.getOccupants().get(1));
            }


            data.put("data.timestamp", "" + System.currentTimeMillis());
            userIds.add(qbChatDialog.getOccupants().get(0));
            userIds.add(qbChatDialog.getOccupants().get(1));
            //  data.put("data.image", "" + StaticSharedpreference.getInfo(StaticSharedpreference.profilePic, getApplicationContext()));


        } else {
            data.put("data.otherID", "");
            data.put("data.meID", "");
            data.put("data.timestamp", "" + System.currentTimeMillis());
            //  data.put("data.image", "" + StaticSharedpreference.getInfo(StaticSharedpreference.profilePic, getApplicationContext()));

            userIds.add(qbChatDialog.getOccupants().get(0));
            userIds.add(qbChatDialog.getOccupants().get(1));
        }

        //   data.put("data.param3", "value1");
        //
        //event.setUserIds(userIds);
        event.setMessage(data);
        //   event.setMessage("123455676645645654");

//            // generic push - will be delivered to all platforms (Android, iOS, WP, Blackberry..)
//            //
//            event.setMessage("This is simple generic push notification!");


//            // generic push with custom parameters - http://quickblox.com/developers/Messages#Use_custom_parameters
//            //
//            JSONObject json = new JSONObject();
//            try {
//                json.put("message", "This is generic push notification with custom params!");
//                json.put("param1", "value1");
//                json.put("ios_badge", "4"); // iOS badge value
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//            //
//            event.setMessage(json.toString());


//            // Android based push
//            //
//            event.setPushType(QBPushType.GCM);
//            HashMap<String, Object> data = new HashMap<>();
//            data.put("data.message", "This is Android based push notification!");
//            data.put("data.param1", "value1");
//            //
//            event.setMessage(data);


        // iOS based push
        //
//        event.setPushType(QBPushType.GCM);
//        HashMap<String, Object> data = new HashMap<>();
//
//        //
//        event.setMessage(data);

        return event;
    }


    Snippet createEvent = new Snippet("create event (send push)") {
        @Override
        public void execute() {

            QBEvent event = buildEvent();

            QBPushNotifications.createEvent(event).performAsync(new QBEntityCallback<QBEvent>() {
                @Override
                public void onSuccess(QBEvent qbEvent, Bundle args) {
                    Log.i(TAG, ">>> new event: " + qbEvent.toString());
                }

                @Override
                public void onError(QBResponseException errors) {

                }
            });
        }
    };


}
