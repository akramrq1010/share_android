package com.sharemyyouremotion.activity;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sharemyyouremotion.R;


public class SplashActivity extends AppCompatActivity {

    Button btn;
    LinearLayout linear;
    ImageView img, img1;
    TextView tv, tv1;
    Animation animFadein, animFadein1, animFadein2, animFadein3, animFadein4, animFadein5;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setBackgroundDrawableResource(R.drawable.bg_splash_bl);
        setContentView(R.layout.fragment_splash);

        btn = findViewById(R.id.btn);
        linear = findViewById(R.id.linear);
        btn = findViewById(R.id.btn);
        linear = findViewById(R.id.linear);
        img = findViewById(R.id.img);
        tv = findViewById(R.id.tv);


        Intent intent = getIntent();
        String value = "1";

        animFadein = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.together);

        animFadein1 = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.fadi_in);

        animFadein2 = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.bounce);
        animFadein3 = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.move_left_to_right);

        animFadein4 = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.move_right_to_left);

        animFadein5 = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.move_bottom_to_top);

        if (value.equalsIgnoreCase("1")) {

            linear.setVisibility(View.VISIBLE);
            tv1.setVisibility(View.GONE);
            img1.setVisibility(View.GONE);
            img.startAnimation(animFadein);
            tv.startAnimation(animFadein1);

        } else if (value.equalsIgnoreCase("2")) {

            linear.setVisibility(View.VISIBLE);
            tv1.setVisibility(View.GONE);
            img1.setVisibility(View.GONE);
            linear.startAnimation(animFadein2);

        } else if (value.equalsIgnoreCase("3")) {
            tv1.startAnimation(animFadein3);
            img1.startAnimation(animFadein4);
        } else {
            tv1.startAnimation(animFadein5);
            img1.startAnimation(animFadein2);
        }


        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // img.startAnimation(animFadein);
                // tv.startAnimation(animFadein1);

                tv.startAnimation(animFadein5);
                img.startAnimation(animFadein2);


            }
        });


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                ActivityOptions options =
                        ActivityOptions.makeCustomAnimation(SplashActivity.this, R.anim.slide_to_right, R.anim.slide_from_left);

                Intent intent = new Intent(SplashActivity.this, LoginBaseActivity.class);
                startActivity(intent, options.toBundle());
                finish();

            }
        }, 3000);
    }
}
