package com.sharemyyouremotion.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.webkit.WebView;

import com.sharemyyouremotion.R;

/**
 * Created by abc on 8/24/2018.
 */

public class WebViewForVideoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview_video);

        WebView webView = findViewById(R.id.webView);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setBuiltInZoomControls(true);

        Intent intent = getIntent();

        if (intent.getStringExtra("local") == null || intent.getStringExtra("local").equalsIgnoreCase(""))
            webView.loadUrl(intent.getStringExtra("video"));
        else
            webView.loadUrl("file://" + intent.getStringExtra("video"));

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
    }
}
