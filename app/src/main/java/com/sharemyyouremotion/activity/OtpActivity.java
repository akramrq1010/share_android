package com.sharemyyouremotion.activity;

import android.app.ActivityOptions;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;

import com.sharemyyouremotion.R;
import com.sharemyyouremotion.connection.BaseApiService;
import com.sharemyyouremotion.connection.Connection;
import com.sharemyyouremotion.helper.Constants;
import com.sharemyyouremotion.helper.DialogClass;
import com.sharemyyouremotion.helper.StaticSharedpreference;
import com.sharemyyouremotion.model.SpecializationModel;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.prefs.Preferences;

import id.zelory.compressor.Compressor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by abc on 8/4/2018.
 */

public class OtpActivity extends AppCompatActivity implements View.OnClickListener {
    CardView cardSubmit;
    TextInputEditText edtOtp;
    TextView tvResend;

    // [START declare_auth]
    private FirebaseAuth mAuth;
    // [END declare_auth]
    String mVerificationId;
    private PhoneAuthProvider.ForceResendingToken mResendToken;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;
    String mobileNo;

    DialogClass dialogClass = new DialogClass();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_opt);
        mAuth = FirebaseAuth.getInstance();
        intiViews();

        mobileNo = "+91" + getIntent().getStringExtra("mobile");
        startPhoneNumberVerification(mobileNo);

    }

    private void intiViews() {
        cardSubmit = findViewById(R.id.cardSubmit);
        edtOtp = findViewById(R.id.edtOtp);
        tvResend = findViewById(R.id.tvResend);

        cardSubmit.setOnClickListener(this);
        tvResend.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.cardSubmit:
                if (edtOtp.getText().toString().length() > 0) {
                    dialogClass.progresesDialog(this);
                    verifyCode();
                } else
                    Toast.makeText(OtpActivity.this, "Please enter OTP", Toast.LENGTH_SHORT).show();

                break;

            case R.id.tvResend:

                resendCode();

                break;

        }
    }


    private void startPhoneNumberVerification(String phoneNumber) {
        // [START start_phone_auth]
        //setupVerificationCallback();
        setUpVerificationCallbacks();

        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNumber,        // Phone number to verify
                60,                 // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                this,               // Activity (for callback binding)
                mCallbacks);        // OnVerificationStateChangedCallbacks
    }

    private void setUpVerificationCallbacks() {

        mCallbacks =
                new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

                    @Override
                    public void onVerificationCompleted(PhoneAuthCredential credential) {
                        Log.v("akramq", "onVerification Completed");
                        //dialogClass.progresesDialog(OtpActivity.this);
                        signInWithPhoneAuthCredentialAuto(credential);
                    }

                    @Override
                    public void onVerificationFailed(FirebaseException e) {
                        DialogClass.custom_pd.dismiss();
                        if (e instanceof FirebaseAuthInvalidCredentialsException) {
                            // Invalid request
                            Log.d("akram", "Invalid credential: "
                                    + e.getLocalizedMessage());
                            Toast.makeText(OtpActivity.this, "" + e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                        } else if (e instanceof FirebaseTooManyRequestsException) {
                            // SMS quota exceeded
                            Log.d("akram", "SMS Quota exceeded.");
                        }
                    }

                    @Override
                    public void onCodeSent(String verificationId1,
                                           PhoneAuthProvider.ForceResendingToken token) {
                        mResendToken = token;
                        mVerificationId = verificationId1;
                        Log.v("akramq", "onCode send" + verificationId1);
                        //sendButton.setEnabled(false);
                        tvResend.setEnabled(true);

                    }
                };
    }

    private void verifyCode() {
        String code = "+91" + edtOtp.getText().toString().trim();
        PhoneAuthCredential phoneAuthCredential = PhoneAuthProvider.getCredential(mVerificationId, code);
        signInWithPhoneAuthCredentialsManual(phoneAuthCredential);
    }

    private void resendCode() {

        setUpVerificationCallbacks();
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                mobileNo,
                60,
                TimeUnit.SECONDS,
                this,
                mCallbacks,
                mResendToken);
    }

    private void signInWithPhoneAuthCredentialsManual(PhoneAuthCredential phoneAuthCredential) {
        mAuth.signInWithCredential(phoneAuthCredential).addOnCompleteListener(this,
                new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Toast.makeText(OtpActivity.this, "verification completed", Toast.LENGTH_LONG).show();
                            DialogClass.custom_pd.dismiss();
                            if (getIntent().getStringExtra("isSocialUser") != null) {
                                createSignUp();
                            } else {
                                updateSocialProfile();
                            }

                        } else {
                            DialogClass.custom_pd.dismiss();
                            Toast.makeText(OtpActivity.this, "Please enter correct otp", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    private void signInWithPhoneAuthCredentialAuto(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Log.v("akram", "on success auto");
                            Toast.makeText(OtpActivity.this, "verification completed", Toast.LENGTH_SHORT).show();
                            //  btnSubmit.setEnabled(false);
                            if (getIntent().getStringExtra("isSocialUser") != null) {
                                createSignUp();
                            } else {
                                updateSocialProfile();
                            }


                            //  FirebaseUser user = task.getResult().getUser();

                        } else {
                            if (task.getException() instanceof
                                    FirebaseAuthInvalidCredentialsException) {
                                // The verification code entered was invalid
                            }
                        }
                    }
                });
    }


    public void createSignUp() {
        dialogClass.progresesDialog(this);
        SharedPreferences sharedPreferences = getSharedPreferences("ShareAllYourEmotionFCM", MODE_PRIVATE);
        String token = sharedPreferences.getString("fcm", "");

        MultipartBody.Part body;
        if (getIntent().getStringExtra("url").equalsIgnoreCase("")) {
            RequestBody reqbodyFile = RequestBody.create(MediaType.parse("text"), "");
            body = MultipartBody.Part.createFormData("files", "");
        } else {
            File file = new File(getIntent().getStringExtra("url"));
            try {
                file = new Compressor(this).compressToFile(file);
            } catch (IOException e) {
                e.printStackTrace();
            }
            RequestBody reqbodyFile = RequestBody.create(MediaType.parse("image/*"), file);
            body = MultipartBody.Part.createFormData("files", file.getName(), reqbodyFile);
        }
        RequestBody clientDoc = RequestBody.create(MediaType.parse("text"), "" + "ClientDocs");
        RequestBody name = RequestBody.create(MediaType.parse("text"), "" + getIntent().getStringExtra("name"));
        RequestBody emailId = RequestBody.create(MediaType.parse("text"), "" + getIntent().getStringExtra("email"));
        RequestBody password = RequestBody.create(MediaType.parse("text"), "" + getIntent().getStringExtra("password"));
        RequestBody gender = RequestBody.create(MediaType.parse("text"), "" + getIntent().getStringExtra("gender"));
        RequestBody dob = RequestBody.create(MediaType.parse("text"), "" + getIntent().getStringExtra("dob"));
        RequestBody mobile = RequestBody.create(MediaType.parse("text"), "" + mobileNo);
        RequestBody isSocialUser = RequestBody.create(MediaType.parse("text"), "" + getIntent().getStringExtra("isSocialUser"));
        RequestBody socialType = RequestBody.create(MediaType.parse("text"), getIntent().getStringExtra("socialType"));
        RequestBody socialId = RequestBody.create(MediaType.parse("text"), getIntent().getStringExtra("socialId"));
        RequestBody deviceToken = RequestBody.create(MediaType.parse("text"), token);
        RequestBody deviceType = RequestBody.create(MediaType.parse("text"), "android");
        RequestBody profilePic = RequestBody.create(MediaType.parse("text"), ""); // always blank for normal signup
        RequestBody about = RequestBody.create(MediaType.parse("text"), getIntent().getStringExtra("about")); // always blank for normal signup
        RequestBody quickId = RequestBody.create(MediaType.parse("text"), getIntent().getStringExtra("quickId")); // always blank for normal signup
        RequestBody quickName = RequestBody.create(MediaType.parse("text"), getIntent().getStringExtra("quickName")); // always blank for normal signup

        BaseApiService apiService = Connection.getBaseApiServiceInstance();
        Call<JsonObject> call = apiService.signUp(clientDoc, name, emailId, password, gender, mobile, isSocialUser,
                socialType, socialId, deviceToken, deviceType, dob, profilePic, about, quickId, quickName, body);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
//                Log.d("tag", "onResponse" + "" + response.body().toString());
                if (response.body() != null && response.body().get("StatusCode").getAsInt() == 200) {

                    StaticSharedpreference.saveInt(Constants.USER_ID,
                            response.body().get("UserProfile").getAsJsonObject().get("UserId").getAsInt(), OtpActivity.this);

                    StaticSharedpreference.saveInfo(Constants.NAME,
                            response.body().get("UserProfile").getAsJsonObject().get("Name").getAsString(), OtpActivity.this);
                    StaticSharedpreference.saveInfo(Constants.EMAIL,
                            response.body().get("UserProfile").getAsJsonObject().get("EmailId").getAsString(), OtpActivity.this);
                    StaticSharedpreference.saveInfo(Constants.MOBILE_NO,
                            response.body().get("UserProfile").getAsJsonObject().get("Mobile").getAsString(), OtpActivity.this);
                    StaticSharedpreference.saveInfo(Constants.DOB,
                            response.body().get("UserProfile").getAsJsonObject().get("Dob").getAsString(), OtpActivity.this);
                    StaticSharedpreference.saveInfo(Constants.GENDER,
                            response.body().get("UserProfile").getAsJsonObject().get("Gender").getAsString(), OtpActivity.this);
                    StaticSharedpreference.saveInfo(Constants.PROFILE_PIC,
                            response.body().get("UserProfile").getAsJsonObject().get("ProfileThumbnail").getAsString(), OtpActivity.this);

                    StaticSharedpreference.saveInfo(Constants.ABOUT,
                            response.body().get("UserProfile").getAsJsonObject().get("About").getAsString(), OtpActivity.this);


                    Gson gson = new Gson();
                    Type listType = new TypeToken<ArrayList<SpecializationModel>>() {
                    }.getType();

                    ArrayList<SpecializationModel> categoriesModels = (ArrayList<SpecializationModel>) gson.fromJson(response.body().getAsJsonArray("Categories"), listType);

                    ActivityOptions options1 =
                            ActivityOptions.makeCustomAnimation(OtpActivity.this, R.anim.slide_to_right, R.anim.slide_from_left);

                    String specialization = gson.toJson(categoriesModels);
                    StaticSharedpreference.saveInfo(Constants.SPECIALIZATION_LIST, specialization, OtpActivity.this);
                    StaticSharedpreference.saveInfo(Constants.GOTO_PAGE, "specialization", OtpActivity.this);
                    Intent intent = new Intent(OtpActivity.this, SpecializationActivity.class);
                    startActivity(intent, options1.toBundle());
                    finishAffinity();

                } else {
                    dialogClass.messageDialog(OtpActivity.this, response.body().get("Message").getAsString() + "");
                }
                DialogClass.custom_pd.dismiss();

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.v("okhttp", " throw = " + t);

                DialogClass.custom_pd.dismiss();
            }
        });
    }

    public void updateSocialProfile() {

        dialogClass.progresesDialog(OtpActivity.this);

        Map requestBody = new HashMap<>();

        requestBody.put("UserId", StaticSharedpreference.getInt(Constants.USER_ID, OtpActivity.this));
        requestBody.put("EmailId", getIntent().getStringExtra("email"));
        requestBody.put("Gender", getIntent().getStringExtra("gender"));
        requestBody.put("Mobile", getIntent().getStringExtra("mobile"));
        requestBody.put("Dob", getIntent().getStringExtra("dob"));
        requestBody.put("About", getIntent().getStringExtra("about"));
        requestBody.put("QuickbloxId", getIntent().getIntExtra("quickBloxId", 0));
        requestBody.put("QuickbloxName", getIntent().getStringExtra("quickBloxName"));


        BaseApiService apiService = Connection.getBaseApiServiceInstance();
        Call<JsonObject> call = apiService.completeProfile(requestBody);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.body() != null && response.body().get("StatusCode").getAsInt() == 200) {

                    StaticSharedpreference.saveInfo(Constants.EMAIL,
                            getIntent().getStringExtra("email"), OtpActivity.this);
                    StaticSharedpreference.saveInfo(Constants.MOBILE_NO,
                            getIntent().getStringExtra("mobile"), OtpActivity.this);
                    StaticSharedpreference.saveInfo(Constants.DOB,
                            getIntent().getStringExtra("dob"), OtpActivity.this);
                    StaticSharedpreference.saveInfo(Constants.GENDER,
                            getIntent().getStringExtra("gender"), OtpActivity.this);

                    StaticSharedpreference.saveInfo(Constants.ABOUT,
                            getIntent().getStringExtra("about"), OtpActivity.this);


                    Gson gson = new Gson();
                    Type listType = new TypeToken<ArrayList<SpecializationModel>>() {
                    }.getType();

                    ArrayList<SpecializationModel> categoriesModels = (ArrayList<SpecializationModel>) gson.fromJson(response.body().getAsJsonArray("Categories"), listType);

                    ActivityOptions options1 =
                            ActivityOptions.makeCustomAnimation(OtpActivity.this, R.anim.slide_to_right, R.anim.slide_from_left);

                    String specialization = gson.toJson(categoriesModels);
                    StaticSharedpreference.saveInfo(Constants.SPECIALIZATION_LIST, specialization, OtpActivity.this);
                    StaticSharedpreference.saveInfo(Constants.GOTO_PAGE, "specialization", OtpActivity.this);
                    Intent intent = new Intent(OtpActivity.this, SpecializationActivity.class);
                    startActivity(intent, options1.toBundle());
                    finishAffinity();
                } else {
                    dialogClass.messageDialog(OtpActivity.this, response.body().get("Message").getAsString() + "");
                }
                DialogClass.custom_pd.dismiss();

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.v("okhttp", " throw = " + t);

                DialogClass.custom_pd.dismiss();
            }
        });
    }
}
