/**/
package com.sharemyyouremotion.connection;

import com.google.gson.JsonObject;
import com.sharemyyouremotion.model.EmotionAndCategoriesModel;
import com.sharemyyouremotion.model.SpecializationUpdateModel;


import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

/**
 * Created by cis on 30/11/16.
 */

public interface BaseApiService {

    /*User Profile APIs*/
    @Headers("Content-Type: application/json")
    @POST("Login")
    Call<JsonObject> loginApi(@Body Map map);

    @Headers("Content-Type: application/json")
    @POST("ForgotPassword")
    Call<JsonObject> forgotPasswordApi(@Body Map map);

    @Multipart
    @POST("SignUp")
    Call<JsonObject> signUp(@Part("ClientDocs") RequestBody clientDocs, @Part("Name") RequestBody name
            , @Part("EmailId") RequestBody email, @Part("Password") RequestBody password, @Part("Gender") RequestBody gender,
                            @Part("Mobile") RequestBody mobile, @Part("IsSocialUser") RequestBody isSocialUser
            , @Part("SocialType") RequestBody socialType, @Part("SocialId") RequestBody socialId,
                            @Part("DeviceToken") RequestBody deviceToken, @Part("DeviceType") RequestBody deviceType, @Part("Dob") RequestBody dob
            , @Part("ProfilePicture") RequestBody profilePicture, @Part("About") RequestBody about, @Part("QuickbloxId") RequestBody quickbloxId,
                            @Part("QuickbloxName") RequestBody quickblocName, @Part MultipartBody.Part file);

    @Headers("Content-Type: application/json")
    @POST("CompleteProfile")
    Call<JsonObject> completeProfile(@Body Map map);

    @Headers("Content-Type: application/json")
    @POST("UpdateSpecializationByUserId")
    Call<JsonObject> updateSpecialization(@Body SpecializationUpdateModel specializationUpdateModel);

    @Headers("Content-Type: application/json")
    @POST("GetEmotionsAndCategoriesWithSubCategories")
    Call<EmotionAndCategoriesModel> getEmotionAndCategories();

    @Headers("Content-Type: application/json")
    @POST("GetUsersByCategoryId")
    Call<JsonObject> getUserByCategory(@Body Map map);

    @Multipart
    @POST("AskQuestion")
    Call<JsonObject> shareEmotion(@Part("QuestionType") RequestBody questionType, @Part("AskBy") RequestBody askBy
            , @Part("AskTo") RequestBody askTo, @Part("EmotionId") RequestBody emotionId, @Part("CategoryId") RequestBody categoryId,
                                  @Part("SubCategoryId") RequestBody subCategoryId, @Part("SharePublicly") RequestBody sharePublicly
            , @Part("ExpectedFeedbackType") RequestBody expectedFeedbackType, @Part("QuestionText") RequestBody questionText,
                                  @Part("IsIdentityShare") RequestBody isIdentityShare, @Part MultipartBody.Part file);


    @Multipart
    @POST("AskQuestion")
    Call<JsonObject> shareEmotion1(@Part MultipartBody.Part file);


    @Multipart
    @POST("ShareExperience")
    Call<JsonObject> shareExperience(@Part("ExperienceType") RequestBody experienceType, @Part("ShareBy") RequestBody shareBy
            , @Part("EmotionId") RequestBody emotionId, @Part("CategoryId") RequestBody categoryId,
                                     @Part("SubCategoryId") RequestBody subCategoryId, @Part("ExperienceText") RequestBody experienceText
            , @Part("IsIdentityShare") RequestBody isIdentityShare, @Part MultipartBody.Part file);


    @Headers("Content-Type: application/json")
    @POST("GetNotificationsByUserId")
    Call<JsonObject> getNotification(@Body Map map);

    @Headers("Content-Type: application/json")
    @POST("GetQuestionDetailAllFeedbacksByQuestionId")
    Call<JsonObject> getQuestionDetailAllFeedbacksByQuestionId(@Body Map map);

    @Multipart
    @POST("GiveFeedback")
    Call<JsonObject> giveFeedback(@Part("QuestionId") RequestBody questionId, @Part("GivenBy") RequestBody givenBy
            , @Part("FeedbackType") RequestBody feedbackType, @Part("FeedbackText") RequestBody feedbackText
            , @Part("IsIdentityShare") RequestBody isIdentityShare, @Part MultipartBody.Part file);


    @Headers("Content-Type: application/json")
    @POST("GetMyQuestions")
    Call<JsonObject> getMyQuestions(@Body Map map);

    @Headers("Content-Type: application/json")
    @POST("GetMyFeedbacks")
    Call<JsonObject> getMyFeedback(@Body Map map);

    @Headers("Content-Type: application/json")
    @POST("GetMyExperiences")
    Call<JsonObject> getMyExperiences(@Body Map map);


    @Headers("Content-Type: application/json")
    @POST("RateUser")
    Call<JsonObject> rateUser(@Body Map map);

    @Headers("Content-Type: application/json")
    @POST("GetExperiencesByType")
    Call<JsonObject> getExperiencesByType(@Body Map map);

    @Headers("Content-Type: application/json")
    @POST("GetQuestionsByType")
    Call<JsonObject> getQuestionsByType(@Body Map map);


    @Multipart
    @POST("UpdateProfile")
    Call<JsonObject> updateProfile(@Part("UserId") RequestBody userId, @Part("Name") RequestBody name
            , @Part("About") RequestBody about, @Part MultipartBody.Part file);

    @Headers("Content-Type: application/json")
    @POST("logout")
    Call<JsonObject> logout(@Body Map map);


    @Headers("Content-Type: application/json")
    @POST("AddDailyDairy")
    Call<JsonObject> addDiary(@Body Map map);

    @Headers("Content-Type: application/json")
    @POST("GetMyDailyDairy")
    Call<JsonObject> GetMyDailyDairy(@Body Map map);

    @Headers("Content-Type: application/json")
    @POST("BlockUser")
    Call<JsonObject> blockUser(@Body Map map);

    @Headers("Content-Type: application/json")
    @POST("UnblockUser")
    Call<JsonObject> unblockUser(@Body Map map);

    @Headers("Content-Type: application/json")
    @POST("GetBlockedUsers")
    Call<JsonObject> getBlockedUsers(@Body Map map);
    /*

    @FormUrlEncoded
    @POST("./")
    Call<LatestNewsBean> latestNews(@Field("action") String action);

    @FormUrlEncoded
    @POST("./")
    Call<LatestNewsBean> jainNews(@Field("action") String action);

    @FormUrlEncoded
    @POST("./")
    Call<LatestNewsBean> minoritySchemes(@Field("action") String action);


    @FormUrlEncoded
    @POST("./")
    Call<JsonObject> resgister(@Field("action") String action,
                               @Field("mobile_no") String mobile_no,
                               @Field("email") String email,
                               @Field("password") String password,
                               @Field("name") String name,
                               @Field("country") String country);


    @FormUrlEncoded
    @POST("./")
    Call<JsonObject> getAdList(@Field("action") String action);


    @FormUrlEncoded
    @POST("./")
    Call<ParichayBean> getParichay(@Field("action") String action);


    @FormUrlEncoded
    @POST("./")
    Call<JsonObject> changePassword(@Field("action") String action,
                                    @Field("id") String id, @Field("oldPwd") String oldPwd,
                                    @Field("newPwd") String newPwd,
                                    @Field("cnfrmNewPwd") String cnfrmNewPwd);

    @FormUrlEncoded
    @POST("./")
    Call<LatestNewsBean> aboutUs(@Field("action") String action);


    @FormUrlEncoded
    @POST("./")
    Call<ShowProfileBean> showProfile(@Field("action") String action,
                                      @Field("id") String id);


    @FormUrlEncoded
    @POST("./")
    Call<ShowProfileBean> updateProfile(@Field("action") String action,
                                        @Field("name") String name, @Field("father_name") String father_name, @Field("status") String status
            , @Field("city") String city, @Field("colony") String colony, @Field("state") String state, @Field("country") String country
            , @Field("address1") String address1, @Field("address2") String address2, @Field("email") String email, @Field("dob") String dob
            , @Field("gotra") String gotra, @Field("maritalStatus") String maritalStatus, @Field("home_town") String home_town
            , @Field("profession") String profession, @Field("contact") String contact, @Field("password") String password, @Field("memberid") String memberid
    );


    @FormUrlEncoded
    @POST("./")
    Call<ShowProfileBean> updateProfileSubMember(@Field("addtional_json") String additionalData, @Field("action") String action,
                                                 @Field("name") String name, @Field("father_name") String father_name, @Field("status") String status
            , @Field("city") String city, @Field("colony") String colony, @Field("state") String state, @Field("country") String country
            , @Field("address1") String address1, @Field("address2") String address2, @Field("email") String email, @Field("dob") String dob
            , @Field("gotra") String gotra, @Field("maritalStatus") String maritalStatus, @Field("home_town") String home_town
            , @Field("profession") String profession, @Field("contact") String contact, @Field("password") String password, @Field("memberid") String memberid
            , @Field("option") String option, @Field("sex") String sex
    );


    @FormUrlEncoded
    @POST("./")
    Call<JsonObject> getLocation(@Field("action") String action);


    @FormUrlEncoded
    @POST("./")
    Call<JsonObject> getHistory(@Field("action") String action);


    @FormUrlEncoded
    @POST("./")
    Call<JsonObject> getMemberList(@Field("action") String action,
                                   @Field("colony_id") String colony_id);


    @FormUrlEncoded
    @POST("./")
    Call<JsonObject> addFamilyMember(@Field("action") String action,
                                     @Field("memberId") String memberId, @Field("memberid") String memberid,
                                     @Field("name") String name, @Field("father_name") String father_name
            , @Field("dob") String dob, @Field("contact") String contact
    );

    @Multipart
    @POST("./")
    Call<JsonObject> uploadPic(@Part("id") RequestBody action, @Part("type") RequestBody projectID, @Part MultipartBody.Part file);

    @Multipart
    @POST("./")
    Call<JsonObject> uploadFamilyPic(@Part("action") RequestBody action, @Part MultipartBody.Part file);


    @Multipart
    @POST("./")
    Call<JsonObject> addEmployement(@Part("action") RequestBody action, @Part("user_id") RequestBody userId
            , @Part("title") RequestBody title, @Part("description") RequestBody description, @Part("email") RequestBody email
            , @Part("mobile") RequestBody mobile
            , @Part("timestamp") RequestBody timestamp, @Part MultipartBody.Part file);

    @Multipart
    @POST("./")
    Call<JsonObject> editEmployement(@Part("action") RequestBody action, @Part("user_id") RequestBody userId
            , @Part("title") RequestBody title, @Part("description") RequestBody description, @Part("email") RequestBody email
            , @Part("mobile") RequestBody mobile
            , @Part("timestamp") RequestBody timestamp, @Part("rojgar_id") RequestBody rojgarId, @Part MultipartBody.Part file);

    @Multipart
    @POST("./")
    Call<JsonObject> addEmployementWithoutImage(@Part("action") RequestBody action, @Part("user_id") RequestBody userId
            , @Part("title") RequestBody title, @Part("description") RequestBody description, @Part("email") RequestBody email
            , @Part("mobile") RequestBody mobile
            , @Part("timestamp") RequestBody timestamp);

    @Multipart
    @POST("./")
    Call<JsonObject> editEmployementWithoutImage(@Part("action") RequestBody action, @Part("user_id") RequestBody userId
            , @Part("title") RequestBody title, @Part("description") RequestBody description, @Part("email") RequestBody email
            , @Part("mobile") RequestBody mobile
            , @Part("timestamp") RequestBody timestamp, @Part("rojgar_id") RequestBody rojgarId, @Part("rojgar_image") RequestBody image);


    @GET("user")
    Call<JsonObject> getEmployement(@QueryMap Map<String, String> params);


    @GET("./")
    Call<JsonObject> getGallerydata(@Query("action") String action);

    @FormUrlEncoded
    @POST("./")
    Call<JsonObject> paymentStatus(@Field("action") String action,
                                   @Field("user_id") String user_id,
                                   @Field("is_paid_user") String is_paid_user,
                                   @Field("time_paid") String time_paid,
                                   @Field("payment_id") String payment_id, @Field("form_data") String formData);

    @GET("./")
    Call<JsonObject> getVersion(@Query("action") String action);

    @GET("./")
    Call<JsonObject> getNewMember(@Query("action") String action);*/

}
