package com.sharemyyouremotion.connection;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sharemyyouremotion.helper.Constants;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;




/**
 * Created by cis on 30/11/16.
 */

public class Connection {
  //  public static String BASE_API_URL = "http://devservices.shareyouremotions.in/api/Services/";
    public static String BASE_API_URL = "http://shareservices.funraaga.com/api/Services/";

    private static OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
    private static HttpLoggingInterceptor logging = new HttpLoggingInterceptor();

    public static void configClient() {
        httpClient.connectTimeout(60, TimeUnit.MINUTES);
        httpClient.readTimeout(60, TimeUnit.MINUTES);

            BASE_API_URL = Constants.BASE_URL;

    }

    public static void addLoggingIfNeeded() {
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
            httpClient.interceptors().add(logging);
    }



    public static BaseApiService getBaseApiServiceInstance() {
        addLoggingIfNeeded();
        configClient();

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_API_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(httpClient.build())
                .build();

        return retrofit.create(BaseApiService.class);
    }



}
