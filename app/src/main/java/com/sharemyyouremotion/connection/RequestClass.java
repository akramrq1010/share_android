package com.sharemyyouremotion.connection;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.facebook.login.LoginManager;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.quickblox.chat.QBChatService;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.core.helper.Lo;
import com.sharemyyouremotion.R;
import com.sharemyyouremotion.activity.HomeActivity;
import com.sharemyyouremotion.activity.LoginBaseActivity;
import com.sharemyyouremotion.activity.MyQuestionFeedbackActivity;
import com.sharemyyouremotion.activity.SpecializationActivity;
import com.sharemyyouremotion.asynctask.GiveFeedbackAsyncTask;
import com.sharemyyouremotion.asynctask.ShareEmotionAsyncTask;
import com.sharemyyouremotion.asynctask.ShareExperienceAsyncTask;
import com.sharemyyouremotion.fragment.ShareEmotionFragment;
import com.sharemyyouremotion.helper.Constants;
import com.sharemyyouremotion.helper.DialogClass;
import com.sharemyyouremotion.helper.StaticSharedpreference;
import com.sharemyyouremotion.helper.Util;
import com.sharemyyouremotion.model.SpecializationModel;
import com.vincent.videocompressor.VideoCompress;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import id.zelory.compressor.Compressor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by abc on 8/25/2018.
 */

public class RequestClass {


    public void giveRating(final Activity activity, int giveRating, String rateCount, final MyQuestionFeedbackActivity myQuestionFeedbackActivity,
                           int feedbackId) {
        final DialogClass dialogClass = new DialogClass();
        dialogClass.progresesDialog(activity);

        Map requestBody = new HashMap<>();

        requestBody.put("UserId", StaticSharedpreference.getInt(Constants.USER_ID, activity));
        requestBody.put("RatingToUserId", giveRating);
        requestBody.put("RateCount", rateCount);
        requestBody.put("FeedbackId", feedbackId);


        BaseApiService apiService = Connection.getBaseApiServiceInstance();
        Call<JsonObject> call = apiService.rateUser(requestBody);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                DialogClass.custom_pd.dismiss();
                if (response.body() != null && response.body().get("StatusCode").getAsInt() == 200) {
                    myQuestionFeedbackActivity.getQuestionDetailAllFeedbacksByQuestionId();
                    Toast.makeText(activity, response.body().get("Message").getAsString() + "", Toast.LENGTH_SHORT).show();

                } else {
                    dialogClass.messageDialog(activity, response.body().get("Message").getAsString() + "");
                }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.v("okhttp", " throw = " + t);

                DialogClass.custom_pd.dismiss();
            }
        });
    }

    Activity activity;

    String media = "";
    DialogClass dialogClass;
    AlertDialog alertDialog;

    public void compressVideo(final Activity activity, final String mediaPath, final String mediaType, final String isSharePublicly, final String text, final String isShareIdentity, final ShareEmotionFragment shareEmotionFragment, final MyQuestionFeedbackActivity myQuestionFeedbackActivity) {
        dialogClass = new DialogClass();
        //  dialogClass.progresesDialog(activity);


        this.activity = activity;
        if (mediaType.equalsIgnoreCase("video")) {

            alertDialog = dialogClass.compressDialog(activity, "Compressing...");
            media = mediaPath;
            destPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath()
                    + File.separator + "VID_" + new SimpleDateFormat("yyyyMMdd_HHmmss", getLocale()).format(new Date()) + ".mp4";
            VideoCompress.compressVideoLow(mediaPath, destPath, new VideoCompress.CompressListener() {
                @Override
                public void onStart() {

                    startTime = System.currentTimeMillis();
                    Util.writeFile(activity, "Start at: " + new SimpleDateFormat("HH:mm:ss", getLocale()).format(new Date()) + "\n");
                }

                @Override
                public void onSuccess() {
                    media = destPath;
                    alertDialog.dismiss();

                    if (myQuestionFeedbackActivity == null) {

                        if (isSharePublicly.equalsIgnoreCase("")) {
                            ShareExperienceAsyncTask shareExperienceAsyncTask = new ShareExperienceAsyncTask(activity, media, mediaType, text, isShareIdentity);
                            shareExperienceAsyncTask.execute();
                            // shareEmotionFragment.shareExperience(activity, media, mediaType, text, isShareIdentity);
                        } else {
                            ShareEmotionAsyncTask shareEmotionAsyncTask = new ShareEmotionAsyncTask(activity, media, mediaType, isSharePublicly, text, isShareIdentity);
                            shareEmotionAsyncTask.execute();
                            // shareEmotionFragment.shareEmotion(activity, media, mediaType, isSharePublicly, text, isShareIdentity);
                        }
                    } else {
                        GiveFeedbackAsyncTask giveFeedbackAsyncTask = new GiveFeedbackAsyncTask(activity, media, mediaType, text, isShareIdentity, isSharePublicly,myQuestionFeedbackActivity);
                        giveFeedbackAsyncTask.execute();
                        //myQuestionFeedbackActivity.giveFeedback(isShareIdentity, media);
                    }
                    Log.v("akram", "success");
                    endTime = System.currentTimeMillis();
                    Util.writeFile(activity, "End at: " + new SimpleDateFormat("HH:mm:ss", getLocale()).format(new Date()) + "\n");
                    Util.writeFile(activity, "Total: " + ((endTime - startTime) / 1000) + "s" + "\n");
                    Util.writeFile(activity);
                }

                @Override
                public void onFail() {
                    alertDialog.dismiss();
                    // tv_indicator.setText("Compress Failed!");
                    // pb_compress.setVisibility(View.INVISIBLE);
                    if (myQuestionFeedbackActivity == null) {
                        if (isSharePublicly.equalsIgnoreCase("")) {
                            ShareExperienceAsyncTask shareExperienceAsyncTask = new ShareExperienceAsyncTask(activity, mediaPath, mediaType, text, isShareIdentity);
                            shareExperienceAsyncTask.execute();
                            // shareEmotionFragment.shareExperience(activity, mediaPath, mediaType, text, isShareIdentity);
                        } else {
                            ShareEmotionAsyncTask shareEmotionAsyncTask = new ShareEmotionAsyncTask(activity, mediaPath, mediaType, isSharePublicly, text, isShareIdentity);
                            shareEmotionAsyncTask.execute();
                            // shareEmotionFragment.shareEmotion(activity, mediaPath, mediaType, isSharePublicly, text, isShareIdentity);
                        }
                    } else {
                        GiveFeedbackAsyncTask giveFeedbackAsyncTask = new GiveFeedbackAsyncTask(activity, mediaPath, mediaType, text, isShareIdentity, isSharePublicly,myQuestionFeedbackActivity);
                        giveFeedbackAsyncTask.execute();
                        // myQuestionFeedbackActivity.giveFeedback(isShareIdentity, mediaPath);
                    }
                    endTime = System.currentTimeMillis();
                    Util.writeFile(activity, "Failed Compress!!!" + new SimpleDateFormat("HH:mm:ss", getLocale()).format(new Date()));
                }

                @Override
                public void onProgress(float percent) {
                    //  tv_progress.setText(String.valueOf(percent) + "%");
                    dialogClass.progress.setProgress(Math.round(percent));
                    dialogClass.tvCount.setText(Math.round(percent) + "%");
                }
            });
        } else {
            if (myQuestionFeedbackActivity == null) {
                if (isSharePublicly.equalsIgnoreCase("")) {
                    ShareExperienceAsyncTask shareExperienceAsyncTask = new ShareExperienceAsyncTask(activity, mediaPath, mediaType, text, isShareIdentity);
                    shareExperienceAsyncTask.execute();
                    // shareEmotionFragment.shareExperience(activity, mediaPath, mediaType, text, isShareIdentity);
                } else {
                    ShareEmotionAsyncTask shareEmotionAsyncTask = new ShareEmotionAsyncTask(activity, mediaPath, mediaType, isSharePublicly, text, isShareIdentity);
                    shareEmotionAsyncTask.execute();
                    //shareEmotionFragment.shareEmotion(activity, mediaPath, mediaType, isSharePublicly, text, isShareIdentity);
                }
            } else {
                GiveFeedbackAsyncTask giveFeedbackAsyncTask = new GiveFeedbackAsyncTask(activity, mediaPath, mediaType, text, isShareIdentity, isSharePublicly,myQuestionFeedbackActivity);
                giveFeedbackAsyncTask.execute();

                // myQuestionFeedbackActivity.giveFeedback(isShareIdentity, mediaPath);
            }
        }

    }


    public void logout(final AlertDialog alertDialoge, final Activity activity) {
        final DialogClass dialogClass = new DialogClass();
        dialogClass.progresesDialog(activity);

        Map requestBody = new HashMap<>();

        requestBody.put("UserId", StaticSharedpreference.getInt(Constants.USER_ID, activity));

        BaseApiService apiService = Connection.getBaseApiServiceInstance();
        Call<JsonObject> call = apiService.logout(requestBody);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                DialogClass.custom_pd.dismiss();
                if (response.body() != null && response.body().get("StatusCode").getAsInt() == 200) {

                    /*clear all data*/
                    StaticSharedpreference.deleteSharedPreference(activity);
                    Constants.emotionLists.clear();
                    Constants.categoryLists.clear();

                    Constants.notificationModels.clear();

                    final QBChatService chatService = QBChatService.getInstance();
                    if (chatService != null) {
                        chatService.logout(new QBEntityCallback<Void>() {
                            @Override
                            public void onSuccess(Void aVoid, Bundle bundle) {
                                chatService.destroy();
                            }

                            @Override
                            public void onError(QBResponseException e) {
                                Log.d("akram", "logout onError " + e.getMessage());
                                chatService.destroy();
                            }
                        });
                    }

                    alertDialoge.dismiss();
                    try {
                        LoginManager.getInstance().logOut();
                    } catch (Exception e) {
                    }
                    Intent intent = new Intent(activity, LoginBaseActivity.class);
                    intent.putExtra("logout", "1");
                    activity.startActivity(intent);
                    activity.finishAffinity();

                } else {
                    dialogClass.messageDialog(activity, response.body().get("Message").getAsString() + "");
                }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.v("okhttp", " throw = " + t);

                DialogClass.custom_pd.dismiss();
            }
        });
    }


    long startTime, endTime;
    String destPath = "";

    @SuppressWarnings("deprecation")
    public static Locale getSystemLocaleLegacy(Configuration config) {
        return config.locale;
    }

    @TargetApi(Build.VERSION_CODES.N)
    public static Locale getSystemLocale(Configuration config) {
        return config.getLocales().get(0);
    }

    private Locale getLocale() {
        Configuration config = activity.getResources().getConfiguration();
        Locale sysLocale = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            sysLocale = getSystemLocale(config);
        } else {
            sysLocale = getSystemLocaleLegacy(config);
        }

        return sysLocale;
    }

}
