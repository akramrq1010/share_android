package com.sharemyyouremotion.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by abc on 8/19/2018.
 */

public class NotificationModel {

    @SerializedName("NotificationId")
    @Expose
    private Integer notificationId;
    @SerializedName("Type")
    @Expose
    private String type;
    @SerializedName("Text")
    @Expose
    private String text;
    @SerializedName("Sender")
    @Expose
    private Integer sender;
    @SerializedName("Receiver")
    @Expose
    private Integer receiver;
    @SerializedName("IsRead")
    @Expose
    private Boolean isRead;
    @SerializedName("CreatedOn")
    @Expose
    private String createdOn;

    @SerializedName("QuestionId")
    @Expose
    private Integer QuestionId;

    public Integer getNotificationId() {
        return notificationId;
    }

    public String getType() {
        return type;
    }

    public String getText() {
        return text;
    }

    public Integer getSender() {
        return sender;
    }

    public Integer getReceiver() {
        return receiver;
    }

    public Boolean getRead() {
        return isRead;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public Integer getQuestionId() {
        return QuestionId;
    }
}
