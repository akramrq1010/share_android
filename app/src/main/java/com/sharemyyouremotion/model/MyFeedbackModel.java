package com.sharemyyouremotion.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MyFeedbackModel {

    @SerializedName("FeedbackId")
    @Expose
    private Integer feedbackId;
    @SerializedName("QuestionId")
    @Expose
    private Integer questionId;
    @SerializedName("GivenBy")
    @Expose
    private Integer givenBy;
    @SerializedName("FeedbackType")
    @Expose
    private String feedbackType;
    @SerializedName("FeedbackText")
    @Expose
    private String feedbackText;
    @SerializedName("FeedbackAudio")
    @Expose
    private String feedbackAudio;
    @SerializedName("FeedbackVideo")
    @Expose
    private String feedbackVideo;
    @SerializedName("UserImage")
    @Expose
    private String userImage;
    @SerializedName("UserThumbnail")
    @Expose
    private String userThumbnail;
    @SerializedName("CreatedOn")
    @Expose
    private String createdOn;

    public Integer getFeedbackId() {
        return feedbackId;
    }

    public Integer getQuestionId() {
        return questionId;
    }

    public Integer getGivenBy() {
        return givenBy;
    }

    public String getFeedbackType() {
        return feedbackType;
    }

    public String getFeedbackText() {
        return feedbackText;
    }

    public String getFeedbackAudio() {
        return feedbackAudio;
    }

    public String getFeedbackVideo() {
        return feedbackVideo;
    }

    public String getUserImage() {
        return userImage;
    }

    public String getUserThumbnail() {
        return userThumbnail;
    }

    public String getCreatedOn() {
        return createdOn;
    }
}
