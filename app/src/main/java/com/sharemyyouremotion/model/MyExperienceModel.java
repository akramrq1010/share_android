package com.sharemyyouremotion.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by abc on 8/25/2018.
 */

public class MyExperienceModel {


    @SerializedName("ExperienceId")
    @Expose
    private Integer experienceId;
    @SerializedName("ShareBy")
    @Expose
    private Integer shareBy;
    @SerializedName("EmotionId")
    @Expose
    private Integer emotionId;
    @SerializedName("CategoryId")
    @Expose
    private Integer categoryId;
    @SerializedName("SubCategoryId")
    @Expose
    private Integer subCategoryId;

    @SerializedName("EmotionName")
    @Expose
    private String emotionName;
    @SerializedName("CategoryName")
    @Expose
    private String categoryName;
    @SerializedName("SubCategoryName")
    @Expose
    private String subCategoryName;

    @SerializedName("ExperienceType")
    @Expose
    private String experienceType;
    @SerializedName("ExperienceText")
    @Expose
    private String experienceText;
    @SerializedName("ExperienceAudio")
    @Expose
    private String experienceAudio;
    @SerializedName("ExperienceVideo")
    @Expose
    private String experienceVideo;
    @SerializedName("UserImage")
    @Expose
    private String userImage;
    @SerializedName("UserThumbnail")
    @Expose
    private String userThumbnail;
    @SerializedName("CreatedOn")
    @Expose
    private String createdOn;


    @SerializedName("Name")
    @Expose
    private String Name;

    @SerializedName("Gender")
    @Expose
    private String Gender;

    public String getGender() {
        return Gender;
    }

    @SerializedName("IsIdentityShare")
    @Expose
    private Boolean IsIdentityShare;

    public String getName() {
        return Name;
    }

    public Boolean getIdentityShare() {
        return IsIdentityShare;
    }

    public String getEmotionName() {
        return emotionName;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public String getSubCategoryName() {
        return subCategoryName;
    }

    public Integer getExperienceId() {
        return experienceId;
    }

    public Integer getShareBy() {
        return shareBy;
    }

    public Integer getEmotionId() {
        return emotionId;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public Integer getSubCategoryId() {
        return subCategoryId;
    }

    public String getExperienceType() {
        return experienceType;
    }

    public String getExperienceText() {
        return experienceText;
    }

    public String getExperienceAudio() {
        return experienceAudio;
    }

    public String getExperienceVideo() {
        return experienceVideo;
    }

    public String getUserImage() {
        return userImage;
    }

    public String getUserThumbnail() {
        return userThumbnail;
    }

    public String getCreatedOn() {
        return createdOn;
    }
}
