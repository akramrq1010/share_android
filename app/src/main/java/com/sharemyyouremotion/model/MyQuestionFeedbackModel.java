package com.sharemyyouremotion.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MyQuestionFeedbackModel {
    @SerializedName("FeedbackId")
    @Expose
    private Integer feedbackId;
    @SerializedName("QuestionId")
    @Expose
    private Integer questionId;
    @SerializedName("GivenBy")
    @Expose
    private Integer givenBy;
    @SerializedName("FeedbackType")
    @Expose
    private String feedbackType;
    @SerializedName("FeedbackText")
    @Expose
    private String feedbackText;
    @SerializedName("FeedbackAudio")
    @Expose
    private String feedbackAudio;
    @SerializedName("FeedbackVideo")
    @Expose
    private String feedbackVideo;
    @SerializedName("UserImage")
    @Expose
    private String userImage;
    @SerializedName("UserThumbnail")
    @Expose
    private String userThumbnail;
    @SerializedName("CreatedOn")
    @Expose
    private String createdOn;

    @SerializedName("QuickbloxId")
    @Expose
    private String QuickbloxId;

    @SerializedName("IsIdentityShare")
    @Expose
    private Boolean isIdentityShare;

    @SerializedName("Name")
    @Expose
    private String Name;

    @SerializedName("Gender")
    @Expose
    private String Gender;

    @SerializedName("IsRatingGiven")
    @Expose
    private Boolean IsRatingGiven;

    @SerializedName("OverAllRating")
    @Expose
    private Float OverAllRating;

    @SerializedName("IndividualRating")
    @Expose
    private Float IndividualRating;


    public Float getOverAllRating() {
        return OverAllRating;
    }

    public Float getIndividualRating() {
        return IndividualRating;
    }

    public String getGender() {
        return Gender;
    }

    public Boolean getIsRatingGiven() {
        return IsRatingGiven;
    }

    public Boolean getIsIdentityShare() {
        return isIdentityShare;
    }

    public String getName() {
        return Name;
    }

    public String getQuickbloxId() {
        return QuickbloxId;
    }

    public Integer getFeedbackId() {
        return feedbackId;
    }

    public Integer getQuestionId() {
        return questionId;
    }

    public Integer getGivenBy() {
        return givenBy;
    }

    public String getFeedbackType() {
        return feedbackType;
    }

    public String getFeedbackText() {
        return feedbackText;
    }

    public String getFeedbackAudio() {
        return feedbackAudio;
    }

    public String getFeedbackVideo() {
        return feedbackVideo;
    }

    public String getUserImage() {
        return userImage;
    }

    public String getUserThumbnail() {
        return userThumbnail;
    }

    public String getCreatedOn() {
        return createdOn;
    }
}
