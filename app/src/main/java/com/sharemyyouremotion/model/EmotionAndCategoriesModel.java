package com.sharemyyouremotion.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by abc on 8/12/2018.
 */

public class EmotionAndCategoriesModel implements Serializable {

    int StatusCode;
    String Message;

    @SerializedName("EmotionList")
    @Expose
    ArrayList<EmotionList> emotionLists = new ArrayList<>();

    @SerializedName("CategoryList")
    @Expose
    ArrayList<CategoryList> categoryLists = new ArrayList<>();

    public int getStatusCode() {
        return StatusCode;
    }

    public String getMessage() {
        return Message;
    }

    public ArrayList<EmotionList> getEmotionLists() {
        return emotionLists;
    }

    public ArrayList<CategoryList> getCategoryLists() {
        return categoryLists;
    }

    public class EmotionList implements Serializable{

        int EmotionId;
        String EmotionName;
        String EmotionImage;
        String EmotionThumbnail;

        public int getEmotionId() {
            return EmotionId;
        }

        public String getEmotionName() {
            return EmotionName;
        }

        public String getEmotionImage() {
            return EmotionImage;
        }

        public String getEmotionThumbnail() {
            return EmotionThumbnail;
        }
    }

    public class CategoryList implements Serializable{


        int CategoryId;
        String CategoryName;
        String CategoryImage;
        String CategoryThumbnail;

        @SerializedName("SubCategories")
        @Expose
        ArrayList<SubCategories> subCategories = new ArrayList<>();

        public int getCategoryId() {
            return CategoryId;
        }

        public String getCategoryName() {
            return CategoryName;
        }

        public String getCategoryImage() {
            return CategoryImage;
        }

        public String getCategoryThumbnail() {
            return CategoryThumbnail;
        }

        public ArrayList<SubCategories> getSubCategories() {
            return subCategories;
        }

        public class SubCategories implements Serializable{

            int SubCategoryId;
            int CategoryId;
            String SubCategoryName;
            String SubCategoryImage;
            String SubCategoryThumbnail;

            public int getSubCategoryId() {
                return SubCategoryId;
            }

            public int getCategoryId() {
                return CategoryId;
            }

            public String getSubCategoryName() {
                return SubCategoryName;
            }

            public String getSubCategoryImage() {
                return SubCategoryImage;
            }

            public String getSubCategoryThumbnail() {
                return SubCategoryThumbnail;
            }
        }
    }
}
