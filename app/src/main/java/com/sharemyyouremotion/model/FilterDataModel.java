package com.sharemyyouremotion.model;

/**
 * Created by abc on 10/3/2018.
 */

public class FilterDataModel {
    String data,type;

    public FilterDataModel(String data, String type) {
        this.data = data;
        this.type = type;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
