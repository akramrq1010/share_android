package com.sharemyyouremotion.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by abc on 9/30/2018.
 */

public class GetMyDiaryModel implements Serializable{

    @SerializedName("DairyId")
    @Expose
    private Integer dairyId;
    @SerializedName("UserId")
    @Expose
    private Integer userId;
    @SerializedName("EmotionId")
    @Expose
    private Integer emotionId;
    @SerializedName("CategoryId")
    @Expose
    private Integer categoryId;

    @SerializedName("EmotionName")
    @Expose
    private String emotionIdName;
    @SerializedName("CategoryName")
    @Expose
    private String categoryName;

    @SerializedName("Discription")
    @Expose
    private String discription;
    @SerializedName("Action")
    @Expose
    private String action;
    @SerializedName("LevelRate")
    @Expose
    private String levelRate;
    @SerializedName("CreatedOn")
    @Expose
    private String createdOn;


    public String getEmotionIdName() {
        return emotionIdName;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public Integer getDairyId() {
        return dairyId;
    }

    public Integer getUserId() {
        return userId;
    }

    public Integer getEmotionId() {
        return emotionId;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public String getDiscription() {
        return discription;
    }

    public String getAction() {
        return action;
    }

    public String getLevelRate() {
        return levelRate;
    }

    public String getCreatedOn() {
        return createdOn;
    }


    public GetMyDiaryModel(Integer dairyId, Integer userId, Integer emotionId, Integer categoryId, String emotionIdName, String categoryName, String discription, String action, String levelRate, String createdOn) {
        this.dairyId = dairyId;
        this.userId = userId;
        this.emotionId = emotionId;
        this.categoryId = categoryId;
        this.emotionIdName = emotionIdName;
        this.categoryName = categoryName;
        this.discription = discription;
        this.action = action;
        this.levelRate = levelRate;
        this.createdOn = createdOn;
    }
}
