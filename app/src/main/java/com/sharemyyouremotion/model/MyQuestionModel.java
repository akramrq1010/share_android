package com.sharemyyouremotion.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MyQuestionModel {


    @SerializedName("QuestionId")
    @Expose
    private Integer questionId;
    @SerializedName("AskBy")
    @Expose
    private Integer askBy;
    @SerializedName("EmotionId")
    @Expose
    private Integer emotionId;
    @SerializedName("CategoryId")
    @Expose
    private Integer categoryId;
    @SerializedName("SubCategoryId")
    @Expose
    private Integer subCategoryId;

    @SerializedName("FeedbackCount")
    @Expose
    private Integer FeedbackCount;

    @SerializedName("EmotionName")
    @Expose
    private String emotionName;
    @SerializedName("CategoryName")
    @Expose
    private String categoryName;
    @SerializedName("SubCategoryName")
    @Expose
    private String subCategoryName;

    @SerializedName("QuestionType")
    @Expose
    private String questionType;
    @SerializedName("SharePublicly")
    @Expose
    private Boolean sharePublicly;
    @SerializedName("ExpectedFeedbackType")
    @Expose
    private String expectedFeedbackType;
    @SerializedName("QuestionText")
    @Expose
    private String questionText;
    @SerializedName("QuestionAudio")
    @Expose
    private String questionAudio;
    @SerializedName("QuestionVideo")
    @Expose
    private String questionVideo;
    @SerializedName("UserImage")
    @Expose
    private String userImage;
    @SerializedName("UserThumbnail")
    @Expose
    private String userThumbnail;
    @SerializedName("CreatedOn")
    @Expose
    private String createdOn;

     @SerializedName("Name")
    @Expose
    private String Name;


     @SerializedName("IsIdentityShare")
    @Expose
    private Boolean IsIdentityShare;

    public String getName() {
        return Name;
    }

    public Boolean getIdentityShare() {
        return IsIdentityShare;
    }

    public Integer getFeedbackCount() {
        return FeedbackCount;
    }

    public String getEmotionName() {
        return emotionName;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public String getSubCategoryName() {
        return subCategoryName;
    }

    public Integer getQuestionId() {
        return questionId;
    }

    public Integer getAskBy() {
        return askBy;
    }

    public Integer getEmotionId() {
        return emotionId;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public Integer getSubCategoryId() {
        return subCategoryId;
    }

    public String getQuestionType() {
        return questionType;
    }

    public Boolean getSharePublicly() {
        return sharePublicly;
    }

    public String getExpectedFeedbackType() {
        return expectedFeedbackType;
    }

    public String getQuestionText() {
        return questionText;
    }

    public String getQuestionAudio() {
        return questionAudio;
    }

    public String getQuestionVideo() {
        return questionVideo;
    }

    public String getUserImage() {
        return userImage;
    }

    public String getUserThumbnail() {
        return userThumbnail;
    }

    public String getCreatedOn() {
        return createdOn;
    }
}
