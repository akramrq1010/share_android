package com.sharemyyouremotion.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by abc on 10/1/2018.
 */

public class BlockUserModel {

    @SerializedName("UserId")
    @Expose
    private Integer userId;
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("ProfilePicture")
    @Expose
    private String profilePicture;
    @SerializedName("QuickbloxId")
    @Expose
    private String quickbloxId;
    @SerializedName("QuickbloxName")
    @Expose
    private String quickbloxName;
    @SerializedName("ProfileThumbnail")
    @Expose
    private String profileThumbnail;

    public Integer getUserId() {
        return userId;
    }

    public String getName() {
        return name;
    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public String getQuickbloxId() {
        return quickbloxId;
    }

    public String getQuickbloxName() {
        return quickbloxName;
    }

    public String getProfileThumbnail() {
        return profileThumbnail;
    }
}
