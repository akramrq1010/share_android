package com.sharemyyouremotion.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SpecializationModel implements Serializable {

    @SerializedName("CategoryId")
    @Expose
    private Integer categoryId;
    @SerializedName("CategoryName")
    @Expose
    private String categoryName;
    @SerializedName("Level")
    @Expose
    private String level;
    @SerializedName("CategoryImage")
    @Expose
    private String categoryImage;
    @SerializedName("CategoryThumbnail")
    @Expose
    private String categoryThumbnail;

    private String radioButtonPosition = "";

    public String getRadioButtonPosition() {
        return radioButtonPosition;
    }

    public void setRadioButtonPosition(String radioButtonPosition) {
        this.radioButtonPosition = radioButtonPosition;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getCategoryImage() {
        return categoryImage;
    }

    public void setCategoryImage(String categoryImage) {
        this.categoryImage = categoryImage;
    }

    public String getCategoryThumbnail() {
        return categoryThumbnail;
    }

    public void setCategoryThumbnail(String categoryThumbnail) {
        this.categoryThumbnail = categoryThumbnail;
    }

    public SpecializationModel(String categoryName) {
        this.categoryName = categoryName;
    }
}
