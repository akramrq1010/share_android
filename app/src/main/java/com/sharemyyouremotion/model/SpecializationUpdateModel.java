package com.sharemyyouremotion.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by abc on 8/11/2018.
 */

public class SpecializationUpdateModel {
    @SerializedName("UserId")
    @Expose
    private Integer userId;
    @SerializedName("Specializations")
    @Expose
    private List<Specialization> specializations = null;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public List<Specialization> getSpecializations() {
        return specializations;
    }

    public void setSpecializations(List<Specialization> specializations) {
        this.specializations = specializations;
    }



    public static class Specialization {

        @SerializedName("CategoryId")
        @Expose
        private Integer categoryId;
        @SerializedName("Level")
        @Expose
        private String level;

        public Integer getCategoryId() {
            return categoryId;
        }

        public void setCategoryId(Integer categoryId) {
            this.categoryId = categoryId;
        }

        public String getLevel() {
            return level;
        }

        public void setLevel(String level) {
            this.level = level;
        }
    }
}