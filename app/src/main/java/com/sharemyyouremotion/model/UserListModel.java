package com.sharemyyouremotion.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by abc on 8/9/2018.
 */

public class UserListModel {


    private boolean isChecked = false;

    @SerializedName("UserId")
    @Expose
    private Integer userId;
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("ProfilePicture")
    @Expose
    private String profilePicture;
    @SerializedName("ProfileThumbnail")
    @Expose
    private String profileThumbnail;
    @SerializedName("SPLevel")
    @Expose
    private String sPLevel;
    @SerializedName("RateCount")
    @Expose
    private Float rateCount;

      public boolean isChecked() {
        return isChecked;
    }

    public boolean getChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public Integer getUserId() {
        return userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public String getProfileThumbnail() {
        return profileThumbnail;
    }

    public String getsPLevel() {
        return sPLevel;
    }

    public Float getRateCount() {
        return rateCount;
    }


    public UserListModel(String name) {
        this.name = name;
    }
}
