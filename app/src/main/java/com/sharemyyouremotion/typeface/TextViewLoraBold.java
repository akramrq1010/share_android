package com.sharemyyouremotion.typeface;

/**
 * Created by DELL on 28-Jun-16.
 */
import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import java.util.Hashtable;


public class TextViewLoraBold extends TextView {

    Context context;
    String ttfName;

    String TAG = getClass().getName();

    public TextViewLoraBold(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;

        for (int i = 0; i < attrs.getAttributeCount(); i++) {
          //  Log.i(TAG, attrs.getAttributeName(i));

            init();
        }

    }

    private void init() {
        setTypeface(getFont(getContext()));
    }

    @Override
    public void setTypeface(Typeface tf) {

        // TODO Auto-generated method stub
        super.setTypeface(tf);
    }
    private static final Hashtable<String, Typeface> cache = new Hashtable<String, Typeface>();


    public static Typeface getFont(Context c) {
      Typeface  font = Typeface.createFromAsset(c.getAssets(),  "Lora-BoldItalic.ttf");

        return font;

    }
}