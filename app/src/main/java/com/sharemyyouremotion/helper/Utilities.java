package com.sharemyyouremotion.helper;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;


import com.sharemyyouremotion.R;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Created by CodeHunter on 25/3/17.
 */

public class Utilities {

    private ProgressDialog loadingDialog;
    public Activity activity;
    private AlertDialog alertDialog;
    public static int INTENTCAMERA = 4, INTENTGALLERY = 5;
    public static File cameraFile;


    public Utilities(Activity activity) {
        this.activity = activity;

    }

    public void showLoadingDialog(final String title, final String message) {
        if (activity != null) {
            activity.runOnUiThread(new Runnable() {
                public void run() {
                    try {
                        loadingDialog = ProgressDialog.show(activity, title, message, true, false);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    public void dismissLoadingDialog() {
        if (activity != null && loadingDialog != null) {
            try {
                loadingDialog.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        }
        Toast.makeText(activity, activity.getString(R.string.error_no_internet), Toast.LENGTH_SHORT).show();
        return false;
    }

    public void showToast(String message) {
        Toast.makeText(activity, message, Toast.LENGTH_LONG).show();
    }


    public boolean isValidEmail(String address) {

        if (address != null || !address.equals("")) {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(address).matches();
        } else {
            return false;
        }
    }

    public boolean isValidPincode(String pincode) {
        if (pincode == null) {
            return false;
        } else {
            String PINCODE_PATTERN = "^[0-9]{6}$";
            Pattern pattern = Pattern.compile(PINCODE_PATTERN);
            Matcher matcher = pattern.matcher(pincode);
            return matcher.matches();
        }
    }


    public boolean isValidMobile(String phone) {
        boolean check = false;
        if (!Pattern.matches("[a-zA-Z]+", phone)) {
            if (phone.length() < 6 || phone.length() > 13) {
                // if(phone.length() != 10) {
                check = false;
            } else {
                check = true;
            }
        } else {
            check = false;
        }
        return check;
    }

    public boolean isValidPassword(String password) {
        Pattern p = Pattern.compile("((?!\\s)\\A)(\\s|(?<!\\s)\\S){6,20}\\Z");
        if (password == null) {
            return false;
        } else {
            Matcher m = p.matcher(password);
            return m.matches();
        }
    }


    public boolean isValidName(String firstname) {
        Pattern p = Pattern.compile("^[ a-zA-Z]{3,20}$");   //Will accept any string with a-z or A-Z of 3-20 characters length
        if (firstname == null) {
            return false;
        } else {
            Matcher m = p.matcher(firstname);
            return m.matches();
        }
    }

    public boolean isValidAge(String age) {
        Pattern p = Pattern.compile("^[1-9]{1,3}$");    //will accept all numbers from 1-9 with 1-3 digits (1-999)
        if (age == null || age.equals("")) {
            return false;
        } else {
            Matcher m = p.matcher(age);
            return m.matches();
        }
    }

    public String getDate(long milliSeconds, String dateFormat) {
        /*Sample for data formatter "dd/MM/yyyy hh:mm:ss.SSS"*/
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }

    public boolean isEmpty(CharSequence chars) {
        return TextUtils.isEmpty(chars);
    }

    public boolean isEmptyEdittext(EditText editText) {
        return TextUtils.isEmpty(editText.getText().toString().trim());
    }


    /**
     * @param fragment must be notnull
     *                 Pick image from gallery or camera from @{@link Fragment}
     */
    @NonNull
    public void selectImage(final Fragment fragment, final Activity activity, final boolean isFragment) {

        AlertDialog.Builder build = new AlertDialog.Builder(activity);
        View dialogView = ((LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.dialog_gallery_camera, null);
        dialogView.findViewById(R.id.linearGallery).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                if (isFragment)
                    fragment.startActivityForResult(Intent.createChooser(intent, activity.getString(R.string.please_select_image)), INTENTGALLERY);
                else
                    activity.startActivityForResult(Intent.createChooser(intent, activity.getString(R.string.please_select_image)), INTENTGALLERY);


            }
        });
        dialogView.findViewById(R.id.linearCamera).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                // Ensure that there's a camera activity to handle the intent
                if (takePictureIntent.resolveActivity(activity.getPackageManager()) != null) {
                    // Create the File where the photo should go
                    File photoFile = null;
                    try {
                        photoFile = createImageFile();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    // Continue only if the File was successfully created
                    if (photoFile != null) {
                        cameraFile = photoFile;
                      /*  Uri photoURI = FileProvider.getUriForFile(activity,
                                "com.jain.parwar.provider",
                                photoFile);*/
                        Uri photoURI = FileProvider.getUriForFile(activity,
                                activity.getPackageName() + ".provider", photoFile);
                        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                        if (isFragment)
                            fragment.startActivityForResult(takePictureIntent, INTENTCAMERA);
                        else
                            activity.startActivityForResult(takePictureIntent, INTENTCAMERA);
                    }
                }
                alertDialog.dismiss();
            }
        });
        build.setView(dialogView);
        build.setCancelable(true);
        alertDialog = build.create();
        alertDialog.show();
    }


    public File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);

        File thumbimg = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        return thumbimg;
    }

    public String getPathFromUri(Uri uri, Activity activity) {
        Cursor cursor = activity.getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        String document_id = cursor.getString(0);
        document_id = document_id.substring(document_id.lastIndexOf(":") + 1);
        cursor.close();
        cursor = activity.getContentResolver().query(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
        cursor.moveToFirst();
        String path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
        cursor.close();

        return path;
    }

    public String getVideoPathFromUri(Uri uri, Activity activity) {
        String[] projection = {MediaStore.Video.Media.DATA};
        Cursor cursor = activity.getContentResolver().query(uri, projection, null, null, null);
        if (cursor != null) {
            // HERE YOU WILL GET A NULLPOINTER IF CURSOR IS NULL
            // THIS CAN BE, IF YOU USED OI FILE MANAGER FOR PICKING THE MEDIA
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } else
            return null;

    }

    public void detePicker(final TextInputEditText textInputEditText) {

        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR); // current year
        int mMonth = c.get(Calendar.MONTH); // current month
        int mDay = c.get(Calendar.DAY_OF_MONTH); // current day
        // date picker dialog
        DatePickerDialog datePickerDialog = new DatePickerDialog(activity,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        // set day of month , month and year value in the edit text
                        int month = monthOfYear + 1;

                        String day, monthStr;
                        if (dayOfMonth < 10) {
                            day = "0" + dayOfMonth;
                        } else {
                            day = dayOfMonth + "";
                        }

                        if (month < 10) {
                            monthStr = "0" + month;
                        } else {
                            monthStr = month + "";
                        }


                        textInputEditText.setText(day + "-" + monthStr + "-" + year);

                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis() - 1000);
        datePickerDialog.show();
    }

    public static File createVideoFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "MP4_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_MOVIES);

        File thumbimg = File.createTempFile(
                imageFileName,  /* prefix */
                ".mp4",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        return thumbimg;
    }

    static Random random = new Random();

    public static String CreateRandomAudioFileName(int string) {
        StringBuilder stringBuilder = new StringBuilder(string);
        int i = 0;
        while (i < string) {
            stringBuilder.append("ABCDEFGHIJKLMNOP".
                    charAt(random.nextInt("ABCDEFGHIJKLMNOP".length())));

            i++;
        }
        return stringBuilder.toString();
    }

}
