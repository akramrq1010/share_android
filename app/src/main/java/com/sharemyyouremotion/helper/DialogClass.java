package com.sharemyyouremotion.helper;

import android.app.Activity;
import android.app.Dialog;

import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.login.LoginManager;
import com.quickblox.chat.QBChatService;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.sharemyyouremotion.R;
import com.sharemyyouremotion.activity.HomeActivity;
import com.sharemyyouremotion.activity.LoginBaseActivity;
import com.sharemyyouremotion.activity.MyQuestionFeedbackActivity;
import com.sharemyyouremotion.activity.OtpActivity;
import com.sharemyyouremotion.asynctask.LoadingSongAsynctask;

import com.sharemyyouremotion.connection.RequestClass;
import com.sharemyyouremotion.fragment.EmotionBaseFragment;
import com.sharemyyouremotion.fragment.HomeFragment;
import com.sharemyyouremotion.fragment.ShareEmotionFragment;
import com.sharemyyouremotion.model.EmotionAndCategoriesModel;
import com.sharemyyouremotion.model.MyExperienceModel;
import com.sharemyyouremotion.model.MyFeedbackModel;
import com.sharemyyouremotion.model.MyQuestionModel;
import com.sharemyyouremotion.model.NotificationModel;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import static com.sharemyyouremotion.fragment.ShareEmotionFragment.mediaPath;

public class DialogClass {

    public static Dialog custom_pd;

    public Dialog progresesDialog(final Activity activity) {

        custom_pd = new Dialog(activity, android.R.style.Theme_Translucent);
        custom_pd.requestWindowFeature(Window.FEATURE_NO_TITLE);
        custom_pd.setContentView(R.layout.dialog_progress);
        custom_pd.setCancelable(false);
        custom_pd.setCanceledOnTouchOutside(false);
        custom_pd.show();

        return custom_pd;

    }

    public Dialog messageSend(final Activity context) {

        LayoutInflater factory = LayoutInflater.from(context);
        final View view = factory.inflate(R.layout.dialog_message_send, null);
        final AlertDialog alertDialoge = new AlertDialog.Builder(context).create();

        TextView textView = view.findViewById(R.id.tvOk);
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.startActivity(new Intent(context, HomeActivity.class));
                context.finishAffinity();
            }
        });

        alertDialoge.setCancelable(false);
        alertDialoge.setCanceledOnTouchOutside(false);
        alertDialoge.setView(view);

        alertDialoge.show();
        return alertDialoge;
    }


    public Dialog noUserSelect(final Context context, final EmotionBaseFragment.FragmentInteractionListener interactionListener) {

        LayoutInflater factory = LayoutInflater.from(context);
        final View view = factory.inflate(R.layout.dialog_no_user_seleted, null);
        final AlertDialog alertDialoge = new AlertDialog.Builder(context).create();

        CardView cardNo = view.findViewById(R.id.cardNo);
        CardView cardYes = view.findViewById(R.id.cardYes);

        cardYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                interactionListener.launchShareEmotionFragment(0);

                alertDialoge.dismiss();
            }
        });

        cardNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialoge.dismiss();
            }
        });

        alertDialoge.setCancelable(false);
        alertDialoge.setCanceledOnTouchOutside(true);
        alertDialoge.setView(view);

        alertDialoge.show();
        return alertDialoge;

    }


    RequestClass requestClass = new RequestClass();

    public Dialog shareIdentity(final Activity activity, final String mediaPath, final String mediaType, final String experienceTitle, final String isSharePublicly, final ShareEmotionFragment shareEmotionFragment) {

        LayoutInflater factory = LayoutInflater.from(activity);
        final View view = factory.inflate(R.layout.dialog_no_user_seleted, null);
        final AlertDialog alertDialoge = new AlertDialog.Builder(activity).create();

        CardView cardNo = view.findViewById(R.id.cardNo);
        CardView cardYes = view.findViewById(R.id.cardYes);
        TextView tvTitle = view.findViewById(R.id.tvTitle);

        tvTitle.setText("Do you want to share your Identity?");

        cardYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (isSharePublicly.equalsIgnoreCase("")) {
                    // shareEmotionFragment.shareExperience(activity, mediaPath, mediaType, experienceTitle, "true");
                    requestClass.compressVideo(activity, mediaPath, mediaType, isSharePublicly, experienceTitle, "true", shareEmotionFragment, null);
                } else {
                    requestClass.compressVideo(activity, mediaPath, mediaType, isSharePublicly, experienceTitle, "true", shareEmotionFragment, null);
                }

                alertDialoge.dismiss();
            }
        });

        cardNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isSharePublicly.equalsIgnoreCase("")) {
                    // shareEmotionFragment.shareExperience(activity, mediaPath, mediaType, experienceTitle, "false");
                    requestClass.compressVideo(activity, mediaPath, mediaType, isSharePublicly, experienceTitle, "false", shareEmotionFragment, null);
                } else {
                    requestClass.compressVideo(activity, mediaPath, mediaType, isSharePublicly, experienceTitle, "false", shareEmotionFragment, null);
                }

                alertDialoge.dismiss();
            }
        });

        alertDialoge.setCancelable(false);
        alertDialoge.setCanceledOnTouchOutside(false);
        alertDialoge.setView(view);

        alertDialoge.show();
        return alertDialoge;

    }


    public void logoutDialog(final Activity activity) {

        LayoutInflater factory = LayoutInflater.from(activity);
        final View view = factory.inflate(R.layout.dialog_no_user_seleted, null);
        final AlertDialog alertDialoge = new AlertDialog.Builder(activity).create();

        TextView tvTitle = view.findViewById(R.id.tvTitle);
        tvTitle.setText(activity.getResources().getString(R.string.logout_text));


        CardView cardNo = view.findViewById(R.id.cardNo);
        CardView cardYes = view.findViewById(R.id.cardYes);


        cardYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utilities utilities = new Utilities(activity);
                if (utilities.isOnline()) {
                    alertDialoge.dismiss();
                    requestClass.logout(alertDialoge, activity);
                }
            }
        });

        cardNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialoge.dismiss();
            }
        });

        alertDialoge.setCancelable(false);
        alertDialoge.setCanceledOnTouchOutside(true);
        alertDialoge.setView(view);

        alertDialoge.show();
    }

    public ProgressBar progress;
    public TextView tvCount;

    public AlertDialog compressDialog(final Activity activity, String text) {

        LayoutInflater factory = LayoutInflater.from(activity);
        final View view = factory.inflate(R.layout.dialog_compress, null);
        final AlertDialog alertDialoge = new AlertDialog.Builder(activity).create();

        progress = view.findViewById(R.id.progress);
        TextView tvTitle = view.findViewById(R.id.tvTitle);
        tvCount = view.findViewById(R.id.tvCount);

        tvTitle.setText(text);

        alertDialoge.setCancelable(false);
        alertDialoge.setCanceledOnTouchOutside(true);
        alertDialoge.setView(view);

        alertDialoge.show();

        return alertDialoge;
    }

    public Dialog ratingDailog(final Activity activity, final int giveRating, final MyQuestionFeedbackActivity myQuestionFeedbackActivity, final int feedbackId) {

        LayoutInflater factory = LayoutInflater.from(activity);
        final View view = factory.inflate(R.layout.dialog_rating, null);
        final AlertDialog alertDialoge = new AlertDialog.Builder(activity).create();

        CardView cardNo = view.findViewById(R.id.cardNo);
        CardView cardYes = view.findViewById(R.id.cardYes);
        final RatingBar rating = view.findViewById(R.id.rating);

        final RequestClass requestClass = new RequestClass();


        cardYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                float ratingValue = rating.getRating();
                if (ratingValue > 0) {
                    alertDialoge.dismiss();
                    requestClass.giveRating(activity, giveRating, ratingValue + "", myQuestionFeedbackActivity, feedbackId);
                } else {
                    Toast.makeText(activity, "Please select rating", Toast.LENGTH_SHORT).show();
                }
            }
        });

        cardNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialoge.dismiss();
            }
        });

        alertDialoge.setCancelable(false);
        alertDialoge.setCanceledOnTouchOutside(true);
        alertDialoge.setView(view);

        alertDialoge.show();
        return alertDialoge;

    }


    public Dialog selectMediaDialog(final Context context, final EmotionBaseFragment.FragmentInteractionListener interactionListener
            , final int categoryId, final String from) {

        LayoutInflater factory = LayoutInflater.from(context);
        final View view = factory.inflate(R.layout.dialog_select_media, null);
        final AlertDialog alertDialoge = new AlertDialog.Builder(context).create();

        LinearLayout linearText = view.findViewById(R.id.linearText);
        LinearLayout linearAudio = view.findViewById(R.id.linearAudio);
        LinearLayout linearVideo = view.findViewById(R.id.linearVideo);


        linearText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (from.equalsIgnoreCase(""))
                    interactionListener.launchShareMyExperienceFragment(1, categoryId);
                else
                    interactionListener.launchFeedbackOtherUserFragment(1);
                alertDialoge.dismiss();
            }
        });
        linearAudio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (from.equalsIgnoreCase(""))
                    interactionListener.launchShareMyExperienceFragment(2, categoryId);
                else
                    interactionListener.launchFeedbackOtherUserFragment(2);

                alertDialoge.dismiss();
            }
        });
        linearVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (from.equalsIgnoreCase(""))
                    interactionListener.launchShareMyExperienceFragment(3, categoryId);
                else
                    interactionListener.launchFeedbackOtherUserFragment(3);

                alertDialoge.dismiss();
            }
        });

        alertDialoge.setCancelable(false);
        alertDialoge.setCanceledOnTouchOutside(true);
        alertDialoge.setView(view);

        alertDialoge.show();
        return alertDialoge;

    }


    public Dialog experienceSuggestion(final Context context, final EmotionBaseFragment.FragmentInteractionListener interactionListener, final int categoryId) {

        LayoutInflater factory = LayoutInflater.from(context);
        final View view = factory.inflate(R.layout.dialog_no_user_seleted, null);
        final AlertDialog alertDialoge = new AlertDialog.Builder(context).create();

        CardView cardNo = view.findViewById(R.id.cardNo);
        CardView cardYes = view.findViewById(R.id.cardYes);
        TextView tvTitle = view.findViewById(R.id.tvTitle);

        tvTitle.setText("Do You want to see previous suggestion?");

        cardYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialoge.dismiss();
                selectMediaDialog(context, interactionListener, categoryId, "");

            }
        });

        cardNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                interactionListener.launchUserListFragment(categoryId);
                alertDialoge.dismiss();
            }
        });

        alertDialoge.setCancelable(false);
        alertDialoge.setCanceledOnTouchOutside(true);
        alertDialoge.setView(view);

        alertDialoge.show();
        return alertDialoge;

    }

    public Dialog messageDialog(final Activity context, String msg) {

        LayoutInflater factory = LayoutInflater.from(context);
        final View view = factory.inflate(R.layout.dialog_message, null);
        final AlertDialog alertDialoge = new AlertDialog.Builder(context).create();

        CardView cardOk = view.findViewById(R.id.cardOk);
        TextView tvTitle = view.findViewById(R.id.tvTitle);

        cardOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialoge.dismiss();
                if (context instanceof OtpActivity) {
                    context.onBackPressed();
                }
            }
        });

        tvTitle.setText(msg);
        alertDialoge.setCancelable(false);
        alertDialoge.setCanceledOnTouchOutside(true);
        alertDialoge.setView(view);

        alertDialoge.show();
        return alertDialoge;

    }

    TextView tvRunningTime, tvFinalTime;
    SeekBar seekbarMusic;
    ImageView imgPause;
    ImageView imgPlay;
    ProgressBar progressAudio;
    MediaPlayer mediaPlayer;
    CardView cardStop;
    boolean isLocal;

    public Dialog audioPlayDialog(final Activity activity, String link, boolean isLocal) {
        this.isLocal = isLocal;
        LayoutInflater factory = LayoutInflater.from(activity);
        final View view = factory.inflate(R.layout.dialog_audio_play, null);
        final AlertDialog alertDialoge = new AlertDialog.Builder(activity).create();

        tvRunningTime = view.findViewById(R.id.tvRunningTime);
        tvFinalTime = view.findViewById(R.id.tvFinalTime);
        seekbarMusic = view.findViewById(R.id.seekbarMusic);
        imgPause = view.findViewById(R.id.imgPause);
        imgPlay = view.findViewById(R.id.imgPlay);
        progressAudio = view.findViewById(R.id.progressAudio);
        cardStop = view.findViewById(R.id.cardStop);
        cardStop.setEnabled(false);
        playSong(link, activity);

        imgPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imgPlay.setVisibility(View.GONE);
                imgPause.setVisibility(View.VISIBLE);
                mediaPlayer.start();
            }
        });

        imgPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imgPlay.setVisibility(View.VISIBLE);
                imgPause.setVisibility(View.GONE);
                mediaPlayer.pause();
            }
        });

        cardStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mediaPlayer.stop();
                mediaPlayer.release();
                alertDialoge.dismiss();
            }
        });


        alertDialoge.setCancelable(false);
        alertDialoge.setCanceledOnTouchOutside(false);
        alertDialoge.setView(view);

        alertDialoge.show();
        return alertDialoge;

    }

    AlertDialog alertDialog;
    public static File videoFile;


    public void selectAudioVideo(final Fragment fragment, final Activity activity, final boolean isVideo, final TextView fileName) {

        AlertDialog.Builder build = new AlertDialog.Builder(activity);
        View dialogView = ((LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.dialog_gallery_camera, null);
        TextView tvCamera = dialogView.findViewById(R.id.tvCamera);
        ImageView imgCamera = dialogView.findViewById(R.id.imgCamera);

        if (!isVideo) {
            tvCamera.setText("Record");
            imgCamera.setBackground(activity.getResources().getDrawable(R.drawable.ic_record));
        }

        dialogView.findViewById(R.id.linearGallery).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isVideo) {
                    Intent intent = new Intent();
                    intent.setType("video/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    if (fragment != null)
                        fragment.startActivityForResult(Intent.createChooser(intent, "Select Video"), 2);
                    else
                        activity.startActivityForResult(Intent.createChooser(intent, "Select Video"), 2);

                } else {
                    Intent intent_upload = new Intent();
                    intent_upload.setType("audio/*");
                    intent_upload.setAction(Intent.ACTION_GET_CONTENT);
                    if (fragment != null)
                        fragment.startActivityForResult(intent_upload, 1);
                    else
                        activity.startActivityForResult(intent_upload, 1);
                }

                alertDialog.dismiss();

            }
        });
        dialogView.findViewById(R.id.linearCamera).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isVideo) {

                    Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                    File photoFile = null;
                    try {
                        photoFile = Utilities.createVideoFile();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    videoFile = photoFile;

                    Uri photoURI = FileProvider.getUriForFile(activity,
                            activity.getPackageName() + ".provider", photoFile);

                    intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                    intent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 60);
                    //start the video capture Intent
                    if (fragment != null)
                        fragment.startActivityForResult(intent, 3);
                    else
                        activity.startActivityForResult(intent, 3);

                } else {

                    recorderDialog(activity, fileName, fragment);

                }

                alertDialog.dismiss();
            }
        });
        build.setView(dialogView);
        build.setCancelable(true);
        alertDialog = build.create();
        alertDialog.show();

    }

    static String AudioSavePathInDevice;
    static MediaRecorder mediaRecorder;
    int pos = 0;

    public Dialog recorderDialog(final Activity activity, final TextView fileName, final Fragment fragment) {

        LayoutInflater factory = LayoutInflater.from(activity);
        final View view = factory.inflate(R.layout.dialog_recorder, null);
        final AlertDialog alertDialoge = new AlertDialog.Builder(activity).create();

        final CardView cardRecord = view.findViewById(R.id.cardRecord);
        CardView cardStop = view.findViewById(R.id.cardStop);
        final TextView tvRunningTime = view.findViewById(R.id.tvRunningTime);
        final TextView tvStop = view.findViewById(R.id.tvStop);
        final SeekBar seekBar = view.findViewById(R.id.seekbar);

        final Handler mHandler = new Handler();
        cardRecord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AudioSavePathInDevice =
                        Environment.getExternalStorageDirectory().getAbsolutePath() + "/" +
                                Utilities.CreateRandomAudioFileName(5) + "AudioRecording.mp3";

                MediaRecorderReady();

                mediaRecorder.setMaxDuration(1000 * 60 * 3);

                try {
                    mediaRecorder.prepare();
                    mediaRecorder.start();
                } catch (IllegalStateException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                seekBar.setMax(180);
//Make sure you update Seekbar on UI thread
                activity.runOnUiThread(new Runnable() {

                    @Override
                    public void run() {


                        int minute = pos / 60;
                        int modulas = 0;
                        if (minute > 0) {
                            modulas = pos % 60;
                        } else {
                            modulas = pos;
                        }
                        if (modulas > 9)
                            tvRunningTime.setText(minute + ":" + modulas);
                        else
                            tvRunningTime.setText(minute + ":0" + modulas);

                        seekBar.setProgress(pos);

                        pos = pos + 1;
                        Log.v("akram", "on run ui");
                        if (pos != 182) {
                            mHandler.removeCallbacksAndMessages(null);
                            mHandler.postDelayed(this, 1000);
                        } else {
                            pos = 0;
                            mediaPath = AudioSavePathInDevice;
                            ShareEmotionFragment.mediaType = "audio";
                            fileName.setText(new File(mediaPath).getName());
                            mediaRecorder.stop();
                            alertDialoge.dismiss();
                        }

                    }
                });


                cardRecord.setVisibility(View.GONE);
                tvStop.setText("Stop");
            }
        });

        cardStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (tvStop.getText().toString().equalsIgnoreCase("cancel")) {
                    alertDialoge.dismiss();
                } else {
                    mediaPath = AudioSavePathInDevice;
                    ShareEmotionFragment.mediaType = "audio";

                    if (fragment == null)
                        fileName.setText("Please upload");
                    else
                        fileName.setText(new File(mediaPath).getName());

                    mHandler.removeCallbacksAndMessages(null);
                    mediaRecorder.stop();
                    alertDialoge.dismiss();
                }
            }
        });

        alertDialoge.setCancelable(false);
        alertDialoge.setCanceledOnTouchOutside(false);
        alertDialoge.setView(view);

        alertDialoge.show();
        return alertDialoge;

    }

    public void MediaRecorderReady() {
        mediaRecorder = new MediaRecorder();
        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.DEFAULT);
        mediaRecorder.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB);
        mediaRecorder.setOutputFile(AudioSavePathInDevice);
    }


    public void playSong(String audioLink, Activity activity) {

        if (!isLocal)
            audioLink = audioLink.replace(" ", "%20");

        LoadingSongAsynctask songAsynctask = new LoadingSongAsynctask(audioLink, activity);
        songAsynctask.execute();

    }


    public class LoadingSongAsynctask extends AsyncTask<Void, Void, Void> {

        Handler handler = new Handler();
        double startTime = 0;
        double finalTime;
        int oneTimeOnly = 0;
        private Handler myHandler = new Handler();
        String audioLink;
        Activity activity;


        public LoadingSongAsynctask(String audioLink, Activity activity) {
            this.audioLink = audioLink;
            this.activity = activity;
        }

        @Override
        protected void onPreExecute() {
            progressAudio.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            mediaPlayer = new MediaPlayer();
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            try {
                Log.v("akram", "link " + audioLink);
                mediaPlayer.setDataSource(audioLink);
                mediaPlayer.prepare();
            } catch (IOException e) {
                Log.v("akram", "IOException " + e);
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            progressAudio.setVisibility(View.GONE);
            imgPause.setVisibility(View.VISIBLE);

            mediaPlayer.start();

            cardStop.setEnabled(true);

            mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {

                }
            });

            seekbarMusic.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                    if (mediaPlayer != null && b) {
                        mediaPlayer.seekTo(i);
                    }
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {

                }
            });


            mediaPlayer.setOnInfoListener(new MediaPlayer.OnInfoListener() {
                @Override
                public boolean onInfo(MediaPlayer mediaPlayer, int what, int i1) {
                    if (what == MediaPlayer.MEDIA_INFO_BUFFERING_START) {
                        // show progress dialog
                        progressAudio.setVisibility(View.VISIBLE);
                        imgPause.setVisibility(View.GONE);
                        Log.v("akram", "start media buffer");

                    } else if (what == MediaPlayer.MEDIA_INFO_BUFFERING_END) {
                        // dismiss progress dialog
                        progressAudio.setVisibility(View.GONE);
                        imgPause.setVisibility(View.VISIBLE);
                        Log.v("akram", "end media buffer");
                    }
                    return false;
                }
            });
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {

                    try {
                        finalTime = mediaPlayer.getDuration();
                        startTime = mediaPlayer.getCurrentPosition();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    if (oneTimeOnly == 0) {
                        seekbarMusic.setMax((int) finalTime);
                        oneTimeOnly = 1;
                    }

                    tvFinalTime.setText(String.format("%d" + ":" + "%d",
                            TimeUnit.MILLISECONDS.toMinutes((long) finalTime),
                            TimeUnit.MILLISECONDS.toSeconds((long) finalTime) -
                                    TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes((long)
                                            finalTime)))
                    );

                    tvRunningTime.setText(String.format("%d" + ":" + "%d",
                            TimeUnit.MILLISECONDS.toMinutes((long) startTime),
                            TimeUnit.MILLISECONDS.toSeconds((long) startTime) -
                                    TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes((long)
                                            startTime)))
                    );

                    //set seekbar according to sound
                    seekbarMusic.setProgress((int) startTime);
                    myHandler.postDelayed(UpdateSongTime, 100);
                }
            }, 200);

            super.onPostExecute(aVoid);
        }

        public Runnable UpdateSongTime = new Runnable() {
            public void run() {

                // Log.v("ak","run");
                try {
                    if (mediaPlayer != null) {
                        startTime = mediaPlayer.getCurrentPosition();
                    } else {
                    }
                } catch (Exception e) {
                }

                tvRunningTime.setText(String.format("%d" + ":" + "%d",
                        TimeUnit.MILLISECONDS.toMinutes((long) startTime),
                        TimeUnit.MILLISECONDS.toSeconds((long) startTime) -
                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.
                                        toMinutes((long) startTime)))
                );
                seekbarMusic.setProgress((int) startTime);
                myHandler.postDelayed(this, 100);
            }
        };

    }


}
