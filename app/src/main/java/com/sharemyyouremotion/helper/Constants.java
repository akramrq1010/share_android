package com.sharemyyouremotion.helper;

import android.media.MediaPlayer;

import com.sharemyyouremotion.model.EmotionAndCategoriesModel;
import com.sharemyyouremotion.model.MyExperienceModel;
import com.sharemyyouremotion.model.MyFeedbackModel;
import com.sharemyyouremotion.model.MyQuestionModel;
import com.sharemyyouremotion.model.NotificationModel;

import java.util.ArrayList;

public class Constants {
    public static final String BASE_URL = "http://devservices.shareyouremotions.in/api/Services/";
   // public static final String BASE_URL = "http://shareservices.funraaga.com/api/Services/";

    /*all share preferance variables*/
    public static String USER_ID = "USER_ID";
    public static String SPECIALIZATION_LIST = "SPECIALIZATION_LIST";
    public static String DIARY_LIST = "DIARY_LIST";
    public static String MY_QUESTION_LIST = "MY_QUESTION_LIST";
    public static String MY_FEEDBACK_LIST = "MY_FEEDBACK_LIST";
    public static String MY_EXPERIENCE_LIST = "MY_EXPERIENCE_LIST";
    public static String GOTO_PAGE = "GOTO_PAGE";
    public static String EMAIL = "EMAIL";
    public static String NAME = "NAME";
    public static String MOBILE_NO = "MOBILE_NO";
    public static String DOB = "DOB";
    public static String ABOUT = "ABOUT";
    public static String PROFILE_PIC = "PROFILE_PIC";
    public static String GENDER = "GENDER";
    public static String QUICK_BLOX_ID = "QUICK_BLOX_ID";
    public static String CHECK_HOME = "CHECK_HOME";

    public static ArrayList<EmotionAndCategoriesModel.EmotionList> emotionLists = new ArrayList<>();
    public static ArrayList<EmotionAndCategoriesModel.CategoryList> categoryLists = new ArrayList<>();

    public static ArrayList<NotificationModel> notificationModels = new ArrayList<>();

    public static ArrayList<String> emotionListName = new ArrayList<>();
    public static ArrayList<String> categoryListName = new ArrayList<>();
    public static ArrayList<String> subCategoryListName = new ArrayList<>();

    public static int emotionId, categoryId, subCategoryId;
    public static String emotionStr, categoryStr, subCategoryStr;

    public static String askToIds = "";



}
